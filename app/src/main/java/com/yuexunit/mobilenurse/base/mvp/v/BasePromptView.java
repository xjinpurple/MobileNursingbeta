package com.yuexunit.mobilenurse.base.mvp.v;

/**
 * Created by sslcjy on 16/2/17.
 */
public interface BasePromptView {

    /**显示与消失加载框*/
    void loadingDialogStatus(int status);

    /**各种系统提示内容*/
    void loadingToastStatus(String Content);

}
