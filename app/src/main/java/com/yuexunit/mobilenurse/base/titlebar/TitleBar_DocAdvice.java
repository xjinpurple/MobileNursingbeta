package com.yuexunit.mobilenurse.base.titlebar;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;

import org.kymjs.kjframe.KJActivity;

/**
 * Created by Ҧƽ on 2015/11/18.
 */
public abstract class TitleBar_DocAdvice extends KJActivity {

    public ImageView titlebar_docadvice_img_back;
    public TextView titlebar_docadvice_tv_title;
    public TextView docactvice_name;
    public TextView docactvice_bedno;
    public TextView docactvice_visitno;
    public RelativeLayout titlebar_advice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        try {
            titlebar_advice = (RelativeLayout) findViewById(R.id.titlebar_advice);
            titlebar_docadvice_img_back = (ImageView) findViewById(R.id.titlebar_docadvice_img_back);
            titlebar_docadvice_img_back.setOnClickListener(this);
            setTitle();
        } catch (NullPointerException e) {
            throw new NullPointerException(
                    "TitleBar Notfound from Activity layout");
        }
        super.onStart();
    }

    /* (non-Javadoc)
     * @see org.kymjs.kjframe.ui.FrameActivity#widgetClick(android.view.View)
     */
    @Override
    public void widgetClick(View v) {
        // TODO Auto-generated method stub
        super.widgetClick(v);

        switch (v.getId()) {
            case R.id.titlebar_docadvice_img_back:
                onBackClick();
                break;

            default:
                break;
        }
    }

    protected void onBackClick() {}

    protected void setTitle()
    {

    }
}
