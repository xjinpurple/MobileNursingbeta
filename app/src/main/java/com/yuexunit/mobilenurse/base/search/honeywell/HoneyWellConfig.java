package com.yuexunit.mobilenurse.base.search.honeywell;

import android.content.Context;

import com.honeywell.aidc.AidcManager;
import com.honeywell.aidc.BarcodeReader;

/**
 * Created by work-jx on 2016/2/26.
 */
public class HoneyWellConfig {
    private  static BarcodeReader barcodeReader;
    private  static AidcManager manager;

    // create the AidcManager providing a Context and a
    // CreatedCallback implementation.
    public static void  HoneyWell_Create(Context context) {
        AidcManager.create(context,new AidcManager.CreatedCallback()
        {

        @Override
        public void onCreated (AidcManager aidcManager){
        manager = aidcManager;
        barcodeReader = manager.createBarcodeReader();
        }
        });
    }

    public static BarcodeReader getBarcodeObject() {
        return barcodeReader;
    }

    public static void HoneyWell_Destory()
    {
        if (barcodeReader != null) {
            // close BarcodeReader to clean up resources.
            barcodeReader.close();
            barcodeReader = null;
        }

        if (manager != null) {
            // close AidcManager to disconnect from the scanner service.
            // once closed, the object can no longer be used.
            manager.close();
        }
    }
}
