package com.yuexunit.mobilenurse.base.bean;

/**
 * Created by 姚平 on 2015/12/7.
 */

//交易返回区(head)
public class Head {

    //交易返回值
    public String ret_code;
    //交易返回内容
    public String ret_info;

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_info() {
        return ret_info;
    }

    public void setRet_info(String ret_info) {
        this.ret_info = ret_info;
    }

}
