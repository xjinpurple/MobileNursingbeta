/*
 * Copyright (c) 2015, 张涛.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yuexunit.mobilenurse.base;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.yuexunit.mobilenurse.module.Login.bean.Login_Bq;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;
import com.yuexunit.mobilenurse.util.CrashHandler;

import org.apache.log4j.net.SocketAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class AppContext extends Application {

    public static int screenW;
    public static int screenH;
    private static Context appContext;
    public static boolean isRelease;

    public ArrayList<Login_Bq> Bqlist = new ArrayList<Login_Bq>();
    public ArrayList<SignsInput_Data> baseTypes = new ArrayList<SignsInput_Data>();
    public ArrayList<SignsInput_Data> allTypes = new ArrayList<SignsInput_Data>();
    public ArrayList<Sign_Single> singleTypes = new ArrayList<Sign_Single>();

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this.getApplicationContext();
        CrashHandler.create(this);
        //初始化Log4j配置
        SetLog4jParams();

        //启动Chrome调试App
        StartStetho();
    }


    private void SetLog4jParams() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                org.apache.log4j.Logger root = org.apache.log4j.Logger.getRootLogger();
                final SocketAppender appender = new SocketAppender("158.18.3.1", 5544);
//                final SocketAppender appender = new SocketAppender("158.18.99.99", 5544);
                appender.setLocationInfo(true);
                root.addAppender(appender);
                Logger logger = LoggerFactory.getLogger(this.getClass());
            }
        }).start();
    }

    private void StartStetho() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());


    }

    public ArrayList<Login_Bq> getBqlist() {
        return Bqlist;
    }

    public void setBqlist(ArrayList<Login_Bq> bqlist) {
        Bqlist = bqlist;
    }

    public ArrayList<SignsInput_Data> getAllTypes() {
        return allTypes;
    }

    public void setAllTypes(ArrayList<SignsInput_Data> allTypes) {
        this.allTypes = allTypes;
    }

    public ArrayList<SignsInput_Data> getBaseTypes() {
        return baseTypes;
    }

    public void setBaseTypes(ArrayList<SignsInput_Data> baseTypes) {
        this.baseTypes = baseTypes;
    }

    public ArrayList<Sign_Single> getSingleTypes() {
        return singleTypes;
    }

    public void setSingleTypes(ArrayList<Sign_Single> singleTypes) {
        this.singleTypes = singleTypes;
    }
}
