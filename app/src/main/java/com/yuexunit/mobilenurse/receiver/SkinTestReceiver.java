package com.yuexunit.mobilenurse.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.yuexunit.mobilenurse.R;

public class SkinTestReceiver extends BroadcastReceiver {
	
	private NotificationManager manager;
	
    /** 
     * called when the BroadcastReceiver is receiving an Intent broadcast. 
     */  
    @Override  
    public void onReceive(Context context, Intent intent) {  
		
        /* start another activity - MyAlarm to display the alarm 直接跳出activity*/  
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
//        intent.setClass(context, MyAlarm.class);  
//        context.startActivity(intent);  
    	Toast.makeText(context, "有病人皮试结束了", Toast.LENGTH_LONG).show();
    	
    	manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
    	//例如这个id就是你传过来的
    	Bundle bundle = intent.getExtras();
		String id  = bundle.getString("id");
    	//MainActivity是你点击通知时想要跳转的Activity
//    	Intent playIntent = new Intent(context, MyAlarm.class);
//    	playIntent.putExtra("id", id);
//    	PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    	
    	NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    	builder.setContentTitle(bundle.getString("title")).setContentText(bundle.getString("name")).setSmallIcon(R.mipmap.ic_launcher).setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true).setSubText(bundle.getString("subtxt"));
    	manager.notify(Integer.valueOf(id), builder.build());

    }  
  
}
