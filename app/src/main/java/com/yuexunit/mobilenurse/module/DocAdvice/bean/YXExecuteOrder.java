package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/14.
 */
public class YXExecuteOrder {

    public YXExecuteOrder body;
    public Head head;
    public ExecuteOrder_Response response;

    public ExecuteOrder_Response getResponse() {
        return response;
    }

    public void setResponse(ExecuteOrder_Response response) {
        this.response = response;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public YXExecuteOrder getBody() {
        return body;
    }

    public void setBody(YXExecuteOrder body) {
        this.body = body;
    }
}
