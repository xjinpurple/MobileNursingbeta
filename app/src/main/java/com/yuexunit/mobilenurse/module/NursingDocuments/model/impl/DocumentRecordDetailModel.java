package com.yuexunit.mobilenurse.module.NursingDocuments.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.NursingDocuments.model.IDocumentRecordDetailModel;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;

import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by work-jx on 2016/10/13.
 */
public class DocumentRecordDetailModel implements IDocumentRecordDetailModel {
    //记录Log
    private final Logger log = ProUtil.getLogger(DocumentRecordDetailModel.class);

    @Override
    public Observable<String> uploadRecord(final Map<String, String> praise) {
        return Observable.create(new Observable.OnSubscribe<Object>() {

            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;

                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT_EXEC;

                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.DocumentRecord_Json(praise.get("inpNo"), praise.get("timestamp"), praise.get("timepoint"), praise.get("templateId"), praise.get("signData"), praise.get("recorder")));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht =  new HttpTransportSE(url,AppConfig.TIMEOUT);
                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.debug(CreateJson.LOG_JSON(url+",kmhs_enr_insert", "2", praise.get("inpNo")+praise.get("timestamp")+praise.get("recorder")+praise.get("signData")+praise.get("timepoint")+praise.get("templateId"), response.toString()));
                    if (response != null) {
                        JSONObject all, body;
                        try {
                            all = new JSONObject(response.toString());
                            body = all.getJSONObject("body");
                            result = body.getString("response");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                            subscriber.onError(e);
                        }
                        if ("null".equals(result))
                        {
                            subscriber.onError(new Exception());
                        }
                    } else {
                        subscriber.onError(new Exception());
                    }

                } catch (Exception e) {
                    log.error("Exception", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                subscriber.onNext(response);
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }
}
