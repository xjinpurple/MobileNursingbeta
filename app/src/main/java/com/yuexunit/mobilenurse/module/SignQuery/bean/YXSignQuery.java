package com.yuexunit.mobilenurse.module.SignQuery.bean;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/2/25.
 */
public class YXSignQuery {


    /**
     * patient_id : 1
     * visit_id : 1
     * recording_date : 2016-02-23 00:00:00
     * time_point : 2016-01-30 02:00:00
     * vital_signs : 血压
     * units : mmHg
     * vital_signs_values_c : 666/555
     * recorder : 陈亚君
     * recorder_date : 2016-01-30 16:13:54
     */

    private ArrayList<SignQueryEntity> data;

    public void setData(ArrayList<SignQueryEntity> data) {
        this.data = data;
    }

    public ArrayList<SignQueryEntity> getData() {
        return data;
    }

    public static class SignQueryEntity {
        private String patient_id;
        private int visit_id;
        private String recording_date;
        private String time_point;
        private String vital_signs;
        private String units;
        private String vital_signs_values_c;
        private String recorder;
        private String recorder_date;

        public void setPatient_id(String patient_id) {
            this.patient_id = patient_id;
        }

        public void setVisit_id(int visit_id) {
            this.visit_id = visit_id;
        }

        public void setRecording_date(String recording_date) {
            this.recording_date = recording_date;
        }

        public void setTime_point(String time_point) {
            this.time_point = time_point;
        }

        public void setVital_signs(String vital_signs) {
            this.vital_signs = vital_signs;
        }

        public void setUnits(String units) {
            this.units = units;
        }

        public void setVital_signs_values_c(String vital_signs_values_c) {
            this.vital_signs_values_c = vital_signs_values_c;
        }

        public void setRecorder(String recorder) {
            this.recorder = recorder;
        }

        public void setRecorder_date(String recorder_date) {
            this.recorder_date = recorder_date;
        }

        public String getPatient_id() {
            return patient_id;
        }

        public int getVisit_id() {
            return visit_id;
        }

        public String getRecording_date() {
            return recording_date;
        }

        public String getTime_point() {
            return time_point;
        }

        public String getVital_signs() {
            return vital_signs;
        }

        public String getUnits() {
            return units;
        }

        public String getVital_signs_values_c() {
            return vital_signs_values_c;
        }

        public String getRecorder() {
            return recorder;
        }

        public String getRecorder_date() {
            return recorder_date;
        }

        @Override
        public String toString() {
            return "SignQueryEntity{" +
                    "patient_id='" + patient_id + '\'' +
                    ", visit_id=" + visit_id +
                    ", recording_date='" + recording_date + '\'' +
                    ", time_point='" + time_point + '\'' +
                    ", vital_signs='" + vital_signs + '\'' +
                    ", units='" + units + '\'' +
                    ", vital_signs_values_c='" + vital_signs_values_c + '\'' +
                    ", recorder='" + recorder + '\'' +
                    ", recorder_date='" + recorder_date + '\'' +
                    '}';
        }
    }

}
