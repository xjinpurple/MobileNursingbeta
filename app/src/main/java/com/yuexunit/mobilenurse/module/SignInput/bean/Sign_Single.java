package com.yuexunit.mobilenurse.module.SignInput.bean;

/**
 * Created by work-jx on 2016/5/11.
 */
public class Sign_Single {

    private String code;

    private String name;

    private String unit;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
