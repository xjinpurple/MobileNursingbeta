package com.yuexunit.mobilenurse.module.Lesions.presenter.impl;

import com.yuexunit.mobilenurse.module.Lesions.presenter.ILesionsPresenter;
import com.yuexunit.mobilenurse.module.Lesions.ui.view.ILesionView;
import com.yuexunit.mobilenurse.module.Login.bean.Login_Bq;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/21.
 */
public class LesionPresenter implements ILesionsPresenter{

    private ILesionView view;

    public LesionPresenter(ILesionView view){
        this.view = view;
    }

    @Override
    public void showLesionList(ArrayList<Login_Bq> bqList) {
        view.showLesionList(bqList);
    }
}
