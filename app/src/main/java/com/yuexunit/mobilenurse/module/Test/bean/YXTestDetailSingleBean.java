package com.yuexunit.mobilenurse.module.Test.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/22.
 */
public class YXTestDetailSingleBean {
    public YXTestDetailSingleBean body;
    public Head head;
    public TestDetail_Single_Response response;

    public YXTestDetailSingleBean getBody() {
        return body;
    }

    public void setBody(YXTestDetailSingleBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public TestDetail_Single_Response getResponse() {
        return response;
    }

    public void setResponse(TestDetail_Single_Response response) {
        this.response = response;
    }
}
