package com.yuexunit.mobilenurse.module.Patients.bean;

/**
 * Created by work-jx on 2016/10/9.
 */
public class PatientClassSingle_Response {
    public PatientClassList_Patinfo patclass;

    public PatientClassList_Patinfo getPatclass() {
        return patclass;
    }

    public void setPatclass(PatientClassList_Patinfo patclass) {
        this.patclass = patclass;
    }
}
