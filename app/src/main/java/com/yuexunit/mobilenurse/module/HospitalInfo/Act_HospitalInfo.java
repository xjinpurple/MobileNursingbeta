package com.yuexunit.mobilenurse.module.HospitalInfo;

import android.os.Bundle;

import com.yuexunit.mobilenurse.module.HospitalInfo.fragment.Frag_Cost;
import com.yuexunit.mobilenurse.module.HospitalInfo.fragment.Frag_DocAdviceQuery;
import com.yuexunit.mobilenurse.module.HospitalInfo.fragment.Frag_Inspect;
import com.yuexunit.mobilenurse.module.HospitalInfo.fragment.Frag_SignQueryFive_Two;
import com.yuexunit.mobilenurse.module.HospitalInfo.fragment.Frag_Test;
import com.yuexunit.mobilenurse.widget.IndicatorFragmentActivity;

import java.util.List;

/**
 * Created by work-jx on 2016/7/13.
 */
public class Act_HospitalInfo extends IndicatorFragmentActivity{

    public static final int FRAGMENT_ONE = 0;
    public static final int FRAGMENT_TWO = 1;
    public static final int FRAGMENT_THREE = 2;

    private Frag_Test contentFragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentFragment1 = new Frag_Test();
    }

    @Override
    protected int supplyTabs(List<TabInfo> tabs) {
        tabs.add(new TabInfo(FRAGMENT_ONE, "体征",
                Frag_SignQueryFive_Two.class));
        tabs.add(new TabInfo(FRAGMENT_TWO, "医嘱",
                Frag_DocAdviceQuery.class));
        tabs.add(new TabInfo(FRAGMENT_THREE, "化验",
                Frag_Test.class));
        tabs.add(new TabInfo(3, "检查",
                Frag_Inspect.class));
        tabs.add(new TabInfo(4, "费用",
                Frag_Cost.class));

        return FRAGMENT_ONE;
    }
}
