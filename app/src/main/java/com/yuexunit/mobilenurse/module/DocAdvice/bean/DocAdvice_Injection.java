package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-12-13.
 */
public class DocAdvice_Injection {

    //医嘱信息
    public DocAdvice_Orderlist docAdvice_orderlist;
    //状态
    public String status;
    //执行次数及总数
    public String count;
    //明细信息
    public ArrayList<DocAdvice_OrderDetail> despensing_detail;
    //医嘱执行
    public ArrayList<DocAdvice_OrderExec> despensing_exec;

    public DocAdvice_Orderlist getDocAdvice_orderlist() {
        return docAdvice_orderlist;
    }

    public void setDocAdvice_orderlist(DocAdvice_Orderlist docAdvice_orderlist) {
        this.docAdvice_orderlist = docAdvice_orderlist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<DocAdvice_OrderDetail> getDespensing_detail() {
        return despensing_detail;
    }

    public void setDespensing_detail(ArrayList<DocAdvice_OrderDetail> despensing_detail) {
        this.despensing_detail = despensing_detail;
    }

    public ArrayList<DocAdvice_OrderExec> getDespensing_exec() {
        return despensing_exec;
    }

    public void setDespensing_exec(ArrayList<DocAdvice_OrderExec> despensing_exec) {
        this.despensing_exec = despensing_exec;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
