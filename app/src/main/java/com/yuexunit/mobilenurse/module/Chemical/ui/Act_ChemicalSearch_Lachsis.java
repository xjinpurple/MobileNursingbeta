package com.yuexunit.mobilenurse.module.Chemical.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/9/5.
 */
public class Act_ChemicalSearch_Lachsis extends KJActivity{
    @Bind(R.id.chemicalsearch_edt)
    EditText chemicalsearchEdt;

    private ValueBroadcastReceiver valueBroadcastReceiver = null;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_chemiaclsearch);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        startBarcodeBroadcastReceiver();
    }

    @Override
    public void onStop(){
        super.onStop();
        stopBarcodeBroadcastReceiver();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        stopBarcodeBroadcastReceiver();
    }

    @OnClick({R.id.chemicalsearch_img_back, R.id.chemicalsearch_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chemicalsearch_img_back:
                finish();
                break;
            case R.id.chemicalsearch_btn:
                if(chemicalsearchEdt.getText().toString().length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE))) {
                    String type_tag = chemicalsearchEdt.getText().toString().substring(8, 9);
                    if (type_tag.equals(AppConfig.TAG_INJECTION)) {
                        Intent intent_new = new Intent();
                        intent_new.setClass(aty, Act_ChemicalDetail.class);
                        intent_new.putExtra("barcode", chemicalsearchEdt.getText().toString());
//                    intent_new.putExtra("barcode", "20160628100176");
                        startActivity(intent_new);
                        chemicalsearchEdt.setText("");
                    } else {
                        ViewInject.toast("非化药条码");
                    }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
                break;
        }
    }

    /**
     * 停止接收广播
     */
    private void stopBarcodeBroadcastReceiver() {
        try {
            if (valueBroadcastReceiver != null)
                unregisterReceiver(valueBroadcastReceiver);
        } catch (Exception e) {

        }
    }

    /**
     * 开始接收广播
     */
    private void startBarcodeBroadcastReceiver() {
        try {
            if (valueBroadcastReceiver == null)
                valueBroadcastReceiver = new ValueBroadcastReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction("lachesis_barcode_value_notice_broadcast");
            registerReceiver(valueBroadcastReceiver, filter);
        } catch (Exception e) {

        }
    }

    /**
     * 关机广播接收者
     *
     * @author
     *
     */
    private class ValueBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            Log.i("ValueBroadcastReceiver", "onReceive......");
            final String value = arg1
                    .getStringExtra("lachesis_barcode_value_notice_broadcast_data_string");
            if (value.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE)))
            {
                String type_tag = value.substring(8, 9);
                if(type_tag.equals(AppConfig.TAG_INJECTION)) {
                    Intent intent_new = new Intent();
                    intent_new.setClass(aty, Act_ChemicalDetail.class);
                    intent_new.putExtra("barcode", value);
                    startActivity(intent_new);
                    chemicalsearchEdt.setText("");
                }
                else {
                    ViewInject.toast("非化药条码");
                }
            }
            else{
                ViewInject.toast("非化药条码");
            }
        }
    }

}
