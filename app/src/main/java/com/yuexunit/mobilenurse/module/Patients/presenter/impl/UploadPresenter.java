package com.yuexunit.mobilenurse.module.Patients.presenter.impl;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientSign;
import com.yuexunit.mobilenurse.module.Patients.model.IUploadModel;
import com.yuexunit.mobilenurse.module.Patients.model.impl.UploadModel;
import com.yuexunit.mobilenurse.module.Patients.presenter.IUploadPresenter;
import com.yuexunit.mobilenurse.module.Patients.ui.view.IUpLoadView;
import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/3/25.
 */
public class UploadPresenter implements IUploadPresenter
{
    private IUploadModel model;
    private IUpLoadView view;

    //记录Log
    private final Logger log = LoggerFactory.getLogger(UploadModel.class);

    public CompositeSubscription compositeSubscription = new CompositeSubscription();

    ArrayList<PatientSign>  patientsignlist_detail = new ArrayList<PatientSign>();

    public UploadPresenter(IUploadModel model, IUpLoadView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void oneKeyUpload(ArrayList<All_Sqlite_SignInputBean> list) {
        if(!(list.size() >0)){
            ViewInject.toast("未有需要上传的体征数据信息...");
            return;
        }
        for (final All_Sqlite_SignInputBean sqlite_signInputBean : list) {
            HashMap<String, String> params = new HashMap();
            params.put("inpNo", sqlite_signInputBean.getInpNo());
            params.put("timestamp", sqlite_signInputBean.getTimestamp());
            params.put("recorder", sqlite_signInputBean.getRecorder());
            params.put("signData", sqlite_signInputBean.getSignData());
            params.put("timepoint", sqlite_signInputBean.getTimepoint());
            model.uploadTypesData(params).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ActionBean>() {
                        @Override
                        public void onCompleted() {
                            log.info("PatientListPresenter_oneKeyUpload_onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            ViewInject.toast("一键上传体征信息失败...");
                            log.info("PatientListPresenter_oneKeyUpload_onError:"+e);
                        }

                        @Override
                        public void onNext(ActionBean actionBean) {
                            log.info("PatientListPresenter_oneKeyUpload_onNext:"+actionBean.toString());
                            view.oneKeyUpload(actionBean, sqlite_signInputBean);
                        }
                    });
        }

    }

    @Override
    public void showPatientSign(ArrayList<All_Sqlite_SignInputBean> list,ArrayList<SignsInput_Data>  allTypes) {
        for (All_Sqlite_SignInputBean sqlite_signInputBean : list)
        {

            String [] sign = sqlite_signInputBean.getSignData().split(",");
            for (int i = 0;i<sign.length;i++)
            {
                PatientSign patientSign = new PatientSign();
                String [] single_sign = sign[i].split(":");
                for(int j = 0;j<allTypes.size();j++)
                {
                    int type = allTypes.get(j).getSign_id();
                    if(single_sign[0].equals(type+""))
                    {
                        patientSign.setType(allTypes.get(j).getName());
                        patientSign.setUnit(allTypes.get(j).getUnit());
                    }
                }
                patientSign.setCount(single_sign[1]);
                patientSign.setVisitno(sqlite_signInputBean.getInpNo());
                patientSign.setTime(sqlite_signInputBean.getTimepoint());
                patientSign.setName(sqlite_signInputBean.getName());
                patientsignlist_detail.add(patientSign);
            }
        }
        view.showPatientSign(patientsignlist_detail);
    }
}
