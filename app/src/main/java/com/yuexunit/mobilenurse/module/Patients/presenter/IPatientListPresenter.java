package com.yuexunit.mobilenurse.module.Patients.presenter;

import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/22.
 */
public interface IPatientListPresenter {

    /**
     * 一键上传功能
     */
    void oneKeyUpload(ArrayList<All_Sqlite_SignInputBean> list);
    /**
     * 加载病人列表
     */
    void showPatientList(String Wardno);

    /**
     * 获取病人列表信息_分类
     */
    void showPatientClassifyList(String wardId,String careLevel,String isKF,String isZS,String isLAB);

    //获取基本体征项
    public void getBaseTypes(String areaId);

    //获取全部体征项
    public void getAllTypes();

}
