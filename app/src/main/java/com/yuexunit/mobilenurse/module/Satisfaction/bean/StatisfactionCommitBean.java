package com.yuexunit.mobilenurse.module.Satisfaction.bean;

/**
 * Created by work-jx on 2016/6/7.
 */
public class StatisfactionCommitBean {
    private String PJBBH;
    private String PJXBH;
    private String PFXBH;
    private String MZBRBH;
    private String ZYBRBH;
    private String PFSJ;

    public String getPJBBH() {
        return PJBBH;
    }

    public void setPJBBH(String PJBBH) {
        this.PJBBH = PJBBH;
    }

    public String getPJXBH() {
        return PJXBH;
    }

    public void setPJXBH(String PJXBH) {
        this.PJXBH = PJXBH;
    }

    public String getPFXBH() {
        return PFXBH;
    }

    public void setPFXBH(String PFXBH) {
        this.PFXBH = PFXBH;
    }

    public String getMZBRBH() {
        return MZBRBH;
    }

    public void setMZBRBH(String MZBRBH) {
        this.MZBRBH = MZBRBH;
    }

    public String getZYBRBH() {
        return ZYBRBH;
    }

    public void setZYBRBH(String ZYBRBH) {
        this.ZYBRBH = ZYBRBH;
    }

    public String getPFSJ() {
        return PFSJ;
    }

    public void setPFSJ(String PFSJ) {
        this.PFSJ = PFSJ;
    }
}
