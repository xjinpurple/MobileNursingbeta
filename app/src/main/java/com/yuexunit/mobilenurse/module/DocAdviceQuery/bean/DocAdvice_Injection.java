package com.yuexunit.mobilenurse.module.DocAdviceQuery.bean;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-12-13.
 */
public class DocAdvice_Injection {

    //医嘱信息
    public DocAdvice_Orderlist docAdvice_orderlist;
    //明细信息
    public ArrayList<DocAdvice_OrderDetail> despensing_detail;

    public DocAdvice_Orderlist getDocAdvice_orderlist() {
        return docAdvice_orderlist;
    }

    public void setDocAdvice_orderlist(DocAdvice_Orderlist docAdvice_orderlist) {
        this.docAdvice_orderlist = docAdvice_orderlist;
    }

    public ArrayList<DocAdvice_OrderDetail> getDespensing_detail() {
        return despensing_detail;
    }

    public void setDespensing_detail(ArrayList<DocAdvice_OrderDetail> despensing_detail) {
        this.despensing_detail = despensing_detail;
    }
}
