package com.yuexunit.mobilenurse.module.Patients.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.AppContext;
import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Patients.adapter.PatinetSignListAdapter;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientSign;
import com.yuexunit.mobilenurse.module.Patients.model.impl.UploadModel;
import com.yuexunit.mobilenurse.module.Patients.presenter.IUploadPresenter;
import com.yuexunit.mobilenurse.module.Patients.presenter.impl.UploadPresenter;
import com.yuexunit.mobilenurse.module.Patients.ui.Act_Main_Normal;
import com.yuexunit.mobilenurse.module.Patients.ui.view.IUpLoadView;
import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import org.kymjs.kjframe.KJDB;
import org.kymjs.kjframe.ui.KJFragment;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/3/25.
 */
public class Frag_Upload extends KJFragment implements IUpLoadView{

    @Bind(R.id.titlebar_text_title)
    TextView titlebarTextTitle;
    @Bind(R.id.right_text)
    TextView rightText;
    @Bind(R.id.titlebar_img_menu)
    ImageView titlebarImgMenu;
    @Bind(R.id.upload_list)
    ListView uploadList;
    private Act_Main_Normal aty;
    //KJDB访问数据库
    KJDB kjdb;

    IUploadPresenter presenter;
    //护士相关信息
    private String NurseName;
    public PatinetSignListAdapter adapeter;

    ArrayList<All_Sqlite_SignInputBean>  patientsignlist = new ArrayList<All_Sqlite_SignInputBean>();
    ArrayList<SignsInput_Data>  allTypes = new ArrayList<SignsInput_Data>();

    @Override
    protected View inflaterView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        aty = (Act_Main_Normal) getActivity();
        View layout;
        layout = View.inflate(aty, R.layout.frag_upload, null);
        ButterKnife.bind(this, layout);
        return layout;
    }

    @Override
    protected void initData() {
        super.initData();
        presenter = new UploadPresenter(new UploadModel(),this);
        kjdb = KJDB.create();
        NurseName = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGNAME);
        allTypes = ((AppContext) getActivity().getApplication()).getAllTypes();
        patientsignlist = (ArrayList<All_Sqlite_SignInputBean>) kjdb.findAllByWhere(All_Sqlite_SignInputBean.class,"recorder='"+NurseName+"'");
        presenter.showPatientSign(patientsignlist,allTypes);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.right_text, R.id.titlebar_img_menu})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.right_text:
                presenter.oneKeyUpload(patientsignlist);
                break;
            case R.id.titlebar_img_menu:
                presenter.oneKeyUpload(patientsignlist);
                break;
        }
    }

    @Override
    public void oneKeyUpload(ActionBean bean, All_Sqlite_SignInputBean signInputBean) {
        if (bean.getCode() == 200) {
            kjdb.delete(signInputBean);
            ViewInject.toast("上传体征信息成功.");
        } else {
            ViewInject.toast(bean.getMessage());
        }
    }

    @Override
    public void showPatientSign(ArrayList<PatientSign> patientsignlist_detail) {
        adapeter = new PatinetSignListAdapter(uploadList,patientsignlist_detail,R.layout.patientsignlist);
        uploadList.setAdapter(adapeter);
    }

}
