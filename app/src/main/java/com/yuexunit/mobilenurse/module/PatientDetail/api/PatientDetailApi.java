package com.yuexunit.mobilenurse.module.PatientDetail.api;

import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsSingleInput;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by work-jx on 2016/5/11.
 */
public interface PatientDetailApi {
    //获得病人体征项
    @GET("ydhl/api/patient")
    Observable<YXSignsSingleInput> SingleTypes(@Query("patientId") String areaId);
}
