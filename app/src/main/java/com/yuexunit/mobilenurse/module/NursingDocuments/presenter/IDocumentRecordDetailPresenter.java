package com.yuexunit.mobilenurse.module.NursingDocuments.presenter;

import java.util.Map;

/**
 * Created by work-jx on 2016/10/13.
 */
public interface IDocumentRecordDetailPresenter {
    /**
     * 上传护理单记录
     */
    void uploadRecord(Map<String, String> praise);
}
