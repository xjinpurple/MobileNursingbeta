package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/14.
 */
public class YXCollectBean {

    public YXCollectBean body;
    public Head head;
    public Collect_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Collect_Response getResponse() {
        return response;
    }

    public void setResponse(Collect_Response response) {
        this.response = response;
    }

    public YXCollectBean getBody() {
        return body;
    }

    public void setBody(YXCollectBean body) {
        this.body = body;
    }
}
