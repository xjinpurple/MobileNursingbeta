package com.yuexunit.mobilenurse.module.Inspect.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exreqlist;
import com.yuexunit.mobilenurse.module.Inspect.ui.Act_InspectDetail;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Created by work-jx on 2015/12/9.
 */
public class InspectAdapter extends KJAdapter<InjectionList_Exreqlist> {

    public InspectAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, InjectionList_Exreqlist injectionList_exreqlist, boolean b) {
        TextView testlist_item_statu = (TextView)adapterHolder.getView(R.id.testlist_item_statu);
        ImageView ic_arrow = (ImageView)adapterHolder.getView(R.id.ic_arrow);
        adapterHolder.setText(R.id.testlist_item_name,injectionList_exreqlist.getClassname());
        adapterHolder.setText(R.id.testlist_item_num,"单号:"+injectionList_exreqlist.getRequestno());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",java.util.Locale.getDefault());
        Date curDate = injectionList_exreqlist.getEntrytime();
        String str= formatter.format(curDate);
        adapterHolder.setText(R.id.testlist_item_data,"开单时间:"+str);
        adapterHolder.setText(R.id.testlist_item_statu, injectionList_exreqlist.getStatusname());
        if(Integer.valueOf(injectionList_exreqlist.getStatuscode()) >=11 && Integer.valueOf(injectionList_exreqlist.getStatuscode())<=90)
        {
            testlist_item_statu.setTextColor(mCxt.getResources().getColor(R.color.test_04));
            ic_arrow.setVisibility(View.VISIBLE);
        }
        else
        {
            testlist_item_statu.setTextColor(mCxt.getResources().getColor(R.color.test_05));
            ic_arrow.setVisibility(View.INVISIBLE);
        }
        onPicClick(adapterHolder.getConvertView(), injectionList_exreqlist.getRequestno() + "", Integer.valueOf(injectionList_exreqlist.getStatuscode()));
    }

    private void onPicClick(View view,final String Requestno,final int Statuscode) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Statuscode == 11 || Statuscode==13) {
                    Bundle bundle = new Bundle();
                    bundle.putString("requestno", Requestno);
                    Intent intent = new Intent(mCxt, Act_InspectDetail.class);
                    intent.putExtras(bundle);
                    mCxt.startActivity(intent);
                } else {
                    ViewInject.toast("未出报告单!");
                }
            }
        });
    }
}
