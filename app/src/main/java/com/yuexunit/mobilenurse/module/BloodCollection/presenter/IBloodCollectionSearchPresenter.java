package com.yuexunit.mobilenurse.module.BloodCollection.presenter;

/**
 * Created by work-jx on 2017/5/4.
 */
public interface IBloodCollectionSearchPresenter {
    public void showBloodCollectionList(String wardId);
}
