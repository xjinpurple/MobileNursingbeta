package com.yuexunit.mobilenurse.module.NursingPlan.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.NursingPlan.adapter.NursingPlanAdapter;
import com.yuexunit.mobilenurse.module.NursingPlan.bean.NursingPlanBean;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/1/22.
 */
public class Act_NursingPlan extends KJActivity {

    @Bind(R.id.docactvice_name)
    TextView docactviceName;
    @Bind(R.id.docactvice_bedno)
    TextView docactviceBedno;
    @Bind(R.id.docactvice_visitno)
    TextView docactviceVisitno;
    @Bind(R.id.nuseingplan_list)
    SwipeMenuListView nuseingplanList;

    private BaseAdapter adapter;

    ArrayList<NursingPlanBean> nursingPlanBeans = new ArrayList<NursingPlanBean>();

    @Override
    public void setRootView() {
        setContentView(R.layout.act_nuseingplan);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        setContent();
        set_swipelist();

        NursingPlanBean nursingPlanBean = new NursingPlanBean();
        nursingPlanBean.setTitle("测试计划");
        nursingPlanBean.setDate("2017-01-01");
        nursingPlanBean.setName("叶思宇");
        nursingPlanBeans.add(nursingPlanBean);
        nursingPlanBeans.add(nursingPlanBean);
        nursingPlanBeans.add(nursingPlanBean);
        nursingPlanBeans.add(nursingPlanBean);
        nursingPlanBeans.add(nursingPlanBean);

        adapter = new NursingPlanAdapter(nuseingplanList,nursingPlanBeans,R.layout.item_nuseingplan);
        nuseingplanList.setAdapter(adapter);
    }

    public void setContent()
    {
        docactviceName.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactviceBedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactviceVisitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    private void set_swipelist()
    {
        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(16);
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        nuseingplanList.setMenuCreator(creator);

        // step 2. listener item click event
        nuseingplanList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
//                        delete(position);
                        break;
                }
                return false;
            }
        });

        // set SwipeListener
        nuseingplanList.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });
    }

    @OnClick({R.id.titlebar_docadvice_img_back, R.id.nuseingplan_add})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.titlebar_docadvice_img_back:
                finish();
                break;
            case R.id.nuseingplan_add:
                showActivity(aty, Act_NursingPlan_Add.class);
                break;
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
