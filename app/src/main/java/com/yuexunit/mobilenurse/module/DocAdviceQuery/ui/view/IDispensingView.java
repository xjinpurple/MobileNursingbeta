package com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view;


import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Dispensing;

import java.util.ArrayList;



/**
 * Created by work-jx on 2016/2/3.
 */
public interface IDispensingView {
    /**
     * 医嘱列表列表
     */
    void showDispensingList(ArrayList<DocAdvice_Dispensing> list);
}
