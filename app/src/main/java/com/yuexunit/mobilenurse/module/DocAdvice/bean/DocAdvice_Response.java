package com.yuexunit.mobilenurse.module.DocAdvice.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class DocAdvice_Response {

    private String orderlist;
    private String orderdetail;
    private String orderexec;

    public String getOrderlist() {
        return orderlist;
    }

    public void setOrderlist(String orderlist) {
        this.orderlist = orderlist;
    }

    public String getOrderdetail() {
        return orderdetail;
    }

    public void setOrderdetail(String orderdetail) {
        this.orderdetail = orderdetail;
    }

    public String getOrderexec() {
        return orderexec;
    }

    public void setOrderexec(String orderexec) {
        this.orderexec = orderexec;
    }
}
