package com.yuexunit.mobilenurse.module.PatientDetail.bean;


/**
 * Created by 姚平 on 2015/12/8.
 */

//数据返回区(response)
public class Patient_Response {
    public Patient_Patinfo patinfo;

    public Patient_Patinfo getPatinfo() {
        return patinfo;
    }

    public void setPatinfo(Patient_Patinfo patinfo) {
        this.patinfo = patinfo;
    }

    @Override
    public String toString() {
        return "Patient_Response{" +
                "patinfo=" + patinfo +
                '}';
    }
}
