package com.yuexunit.mobilenurse.module.HospitalInfo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.HospitalInfo.Act_HospitalInfo;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;
import com.yuexunit.mobilenurse.module.SignQuery.model.impl.SignModel;
import com.yuexunit.mobilenurse.module.SignQuery.presenter.impl.SignPresenter;
import com.yuexunit.mobilenurse.module.SignQuery.ui.view.ISignQueryView;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/8/9.
 */
public class Frag_SignQueryFive_Two extends Fragment implements ISignQueryView {
    @Bind(R.id.signquery_date_01)
    TextView signqueryDate01;
    @Bind(R.id.signquery_date_02)
    TextView signqueryDate02;
    @Bind(R.id.signquery_date_03)
    TextView signqueryDate03;
    @Bind(R.id.signquery_date_04)
    TextView signqueryDate04;
    @Bind(R.id.position_4_one)
    TextView position4One;
    @Bind(R.id.position_4_two)
    TextView position4Two;
    @Bind(R.id.position_4_three)
    TextView position4Three;
    @Bind(R.id.position_4_four)
    TextView position4Four;
    @Bind(R.id.position_8_one)
    TextView position8One;
    @Bind(R.id.position_8_two)
    TextView position8Two;
    @Bind(R.id.position_8_three)
    TextView position8Three;
    @Bind(R.id.position_8_four)
    TextView position8Four;
    @Bind(R.id.position_12_one)
    TextView position12One;
    @Bind(R.id.position_12_two)
    TextView position12Two;
    @Bind(R.id.position_12_three)
    TextView position12Three;
    @Bind(R.id.position_12_four)
    TextView position12Four;
    @Bind(R.id.position_16_one)
    TextView position16One;
    @Bind(R.id.position_16_two)
    TextView position16Two;
    @Bind(R.id.position_16_three)
    TextView position16Three;
    @Bind(R.id.position_16_four)
    TextView position16Four;
    @Bind(R.id.position_20_one)
    TextView position20One;
    @Bind(R.id.position_20_two)
    TextView position20Two;
    @Bind(R.id.position_20_three)
    TextView position20Three;
    @Bind(R.id.position_20_four)
    TextView position20Four;
    @Bind(R.id.position_24_one)
    TextView position24One;
    @Bind(R.id.position_24_two)
    TextView position24Two;
    @Bind(R.id.position_24_three)
    TextView position24Three;
    @Bind(R.id.position_24_four)
    TextView position24Four;
    @Bind(R.id.two_sign_multi_ll)
    LinearLayout twoSignMultiLl;
    @Bind(R.id.postion_one_am)
    TextView postionOneAm;
    @Bind(R.id.postion_two_am)
    TextView postionTwoAm;
    @Bind(R.id.postion_three_am)
    TextView postionThreeAm;
    @Bind(R.id.postion_four_am)
    TextView postionFourAm;
    @Bind(R.id.postion_one_pm)
    TextView postionOnePm;
    @Bind(R.id.postion_two_pm)
    TextView postionTwoPm;
    @Bind(R.id.postion_three_pm)
    TextView postionThreePm;
    @Bind(R.id.postion_four_pm)
    TextView postionFourPm;
    @Bind(R.id.two_sign_twice_ll)
    LinearLayout twoSignTwiceLl;
    @Bind(R.id.postion_one_all)
    TextView postionOneAll;
    @Bind(R.id.postion_two_all)
    TextView postionTwoAll;
    @Bind(R.id.postion_three_all)
    TextView postionThreeAll;
    @Bind(R.id.postion_four_all)
    TextView postionFourAll;
    @Bind(R.id.two_sign_one_ll)
    LinearLayout twoSignOneLl;
    @Bind(R.id.sign_tw)
    TextView signTw;
    @Bind(R.id.sign_mb)
    TextView signMb;
    @Bind(R.id.sign_ttcd)
    TextView signTtcd;
    @Bind(R.id.sign_hx)
    TextView signHx;
    @Bind(R.id.sign_xy)
    TextView signXy;
    @Bind(R.id.sign_sg)
    TextView signSg;
    @Bind(R.id.sign_tz)
    TextView signTz;
    @Bind(R.id.sign_dbcs)
    TextView signDbcs;
    @Bind(R.id.sign_rl)
    TextView signRl;
    @Bind(R.id.sign_cl)
    TextView signCl;
    @Bind(R.id.sign_nl)
    TextView signNl;
    private SignPresenter presenter;
    private String inpNo;
    private static final String DATE_NUM = "1";//提供几天的数据过来
    private String interval = "1";
    private SimpleDateFormat format;
    private int first_count = 4;

    Calendar calendar_start = Calendar.getInstance();
    Calendar calendar = Calendar.getInstance();

    private Act_HospitalInfo aty;
    protected View mMainView;
    protected Context mContext;

    //距离入院天数
    private int day = 0;
    private String admissiondate;

    private ArrayList<ArrayList<YXSignQuery.SignQueryEntity>> signQuery_all = new ArrayList<ArrayList<YXSignQuery.SignQueryEntity>>();

    private int select = 0;//0:体温,1:脉搏，2:疼痛，3:呼吸，4:血压，5:身高，6:体重，7:大便，8:入量，9:出量，10:尿量
    private int position = 0;

    private TextView[] text;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        aty = (Act_HospitalInfo) getActivity();
        mMainView = inflater.inflate(R.layout.act_signqueryfive_two, container, false);
        ButterKnife.bind(this, mMainView);
        text =new TextView[]{signTw,signMb,signTtcd,signHx,signXy,signSg,signTz,signDbcs,signRl,signCl,signNl};
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        presenter = new SignPresenter(new SignModel(), this);
        admissiondate = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_DATE);
        calendar_start.setTime(new Date());//获取当前时间
        String str = format.format(calendar_start.getTime());
        try {
            day = Frag_Cost.daysBetween(admissiondate, str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        ;
        presenter.showSignInfoFive(inpNo, DATE_NUM, format.format(calendar.getTime()));
    }


    @Override
    public void showSignInfo(ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities) {
        signQuery_all.add(new ArrayList<YXSignQuery.SignQueryEntity>(signQueryEntities));
        first_count--;
        day--;
        if (first_count > 0 && day > -1) {
            calendar.add(Calendar.DATE, -1);
            presenter.showSignInfoFive(inpNo, interval, format.format(calendar.getTime()));
        } else {
            set_data();
        }
    }

    private void set_data() {
        switch (select) {
            case 0:
                Visibility(0);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("体温".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setMultiData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 1:
                Visibility(0);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("脉搏".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setMultiData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 2:
                Visibility(0);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("疼痛强度".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setMultiData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 3:
                Visibility(0);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("呼吸".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setMultiData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 4:
                Visibility(1);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("血压".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setTwiceData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 5:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("身高".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 6:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("体重".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 7:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("大便次数".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 8:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("入量".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 9:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("出量".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;
            case 10:
                Visibility(2);
                for (int i = position, j = 0; i < (position + 4) && i < signQuery_all.size(); i++, j++) {
                    for (YXSignQuery.SignQueryEntity entity : signQuery_all.get(i)) {
                        String[] time = entity.getTime_point().split(" ");
                        String[] timepoints = time[0].split("/");
                        String day = timepoints[1] + "." + timepoints[2];
                        setTime(day, j);

                        String vital_signs = entity.getVital_signs();
                        if ("尿量".equals(vital_signs)) {
                            String[] timepoints_2 = time[1].split(":");
                            String times = timepoints_2[0];
                            setOneData(j, times, entity.getVital_signs_values_c());
                        }
                    }
                }
                break;

        }
    }

    private void setTime(String day, int position) {
        switch (position) {
            case 0:
                signqueryDate01.setText(day);
                break;
            case 1:
                signqueryDate02.setText(day);
                break;
            case 2:
                signqueryDate03.setText(day);
                break;
            case 3:
                signqueryDate04.setText(day);
                break;
        }
    }

    private void setOneData(int day, String time, String value) {
        switch (day) {
            case 0:
                postionOneAll.setText(value);
                break;
            case 1:
                postionTwoAll.setText(value);
                break;
            case 2:
                postionThreeAll.setText(value);
                break;
            case 3:
                postionFourAll.setText(value);
                break;
        }

    }

    private void setTwiceData(int day, String time, String value) {
        int times = Integer.valueOf(time);
        switch (day) {
            case 0:
                if (times > 10) {
                    postionOneAm.setText(value);
                } else {
                    postionOnePm.setText(value);
                }
                break;
            case 1:
                if (times > 10) {
                    postionTwoAm.setText(value);
                } else {
                    postionTwoPm.setText(value);
                }
                break;
            case 2:
                if (times > 10) {
                    postionThreeAm.setText(value);
                } else {
                    postionThreePm.setText(value);
                }
                break;
            case 3:
                if (times > 10) {
                    postionFourAm.setText(value);
                } else {
                    postionFourPm.setText(value);
                }
                break;
        }

    }

    private void setMultiData(int day, String time, String value) {
        switch (day) {
            case 0:
                switch (time) {
                    case "2":
                        position4One.setText(value);
                        break;
                    case "6":
                        position8One.setText(value);
                        break;
                    case "10":
                        position12One.setText(value);
                        break;
                    case "14":
                        position16One.setText(value);
                        break;
                    case "18":
                        position20One.setText(value);
                        break;
                    case "22":
                        position24One.setText(value);
                        break;
                }
                break;
            case 1:
                switch (time) {
                    case "2":
                        position4Two.setText(value);
                        break;
                    case "6":
                        position8Two.setText(value);
                        break;
                    case "10":
                        position12Two.setText(value);
                        break;
                    case "14":
                        position16Two.setText(value);
                        break;
                    case "18":
                        position20Two.setText(value);
                        break;
                    case "22":
                        position24Two.setText(value);
                        break;
                }
                break;
            case 2:
                switch (time) {
                    case "2":
                        position4Three.setText(value);
                        break;
                    case "6":
                        position8Three.setText(value);
                        break;
                    case "10":
                        position12Three.setText(value);
                        break;
                    case "14":
                        position16Three.setText(value);
                        break;
                    case "18":
                        position20Three.setText(value);
                        break;
                    case "22":
                        position24Three.setText(value);
                        break;
                }
                break;
            case 3:
                switch (time) {
                    case "2":
                        position4Four.setText(value);
                        break;
                    case "6":
                        position8Four.setText(value);
                        break;
                    case "10":
                        position12Four.setText(value);
                        break;
                    case "14":
                        position16Four.setText(value);
                        break;
                    case "18":
                        position20Four.setText(value);
                        break;
                    case "22":
                        position24Four.setText(value);
                        break;
                }
                break;
        }
    }


    private void Visibility(int type) {
        switch (type) {
            case 0:
                twoSignMultiLl.setVisibility(View.VISIBLE);
                twoSignTwiceLl.setVisibility(View.GONE);
                twoSignOneLl.setVisibility(View.GONE);
                clear_Multi();
                break;
            case 1:
                twoSignMultiLl.setVisibility(View.GONE);
                twoSignTwiceLl.setVisibility(View.VISIBLE);
                twoSignOneLl.setVisibility(View.GONE);
                clear_Twice();
                break;
            case 2:
                twoSignMultiLl.setVisibility(View.GONE);
                twoSignTwiceLl.setVisibility(View.GONE);
                twoSignOneLl.setVisibility(View.VISIBLE);
                clear_One();
                break;
        }
    }


    private void clear_Multi() {
        position4One.setText("");
        position4Two.setText("");
        position4Three.setText("");
        position4Four.setText("");

        position8One.setText("");
        position8Two.setText("");
        position8Three.setText("");
        position8Four.setText("");

        position12One.setText("");
        position12Two.setText("");
        position12Three.setText("");
        position12Four.setText("");

        position16One.setText("");
        position16Two.setText("");
        position16Three.setText("");
        position16Four.setText("");

        position20One.setText("");
        position20Two.setText("");
        position20Three.setText("");
        position20Four.setText("");

        position24One.setText("");
        position24Two.setText("");
        position24Three.setText("");
        position24Four.setText("");

    }

    private void clear_Twice() {
        postionOneAm.setText("");
        postionOnePm.setText("");

        postionTwoAm.setText("");
        postionTwoPm.setText("");

        postionThreeAm.setText("");
        postionThreePm.setText("");

        postionFourAm.setText("");
        postionFourPm.setText("");
    }

    private void clear_One() {
        postionOneAll.setText("");
        postionTwoAll.setText("");
        postionThreeAll.setText("");
        postionFourAll.setText("");
    }


    @OnClick({R.id.query_left, R.id.query_right, R.id.sign_tw, R.id.sign_mb, R.id.sign_ttcd, R.id.sign_hx, R.id.sign_xy, R.id.sign_sg, R.id.sign_tz, R.id.sign_dbcs, R.id.sign_rl, R.id.sign_cl, R.id.sign_nl})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.query_left:
                if (position == 0) {
                    ViewInject.toast("已经到最新一天");
                } else {
                    signQuery_all.remove(signQuery_all.size() - 1);
                    day++;
                    position--;
                    calendar.add(Calendar.DATE, 1);
                    set_data();
                }
                break;
            case R.id.query_right:
                if (day > -1) {
                    position++;
                    calendar.add(Calendar.DATE, -1);
                    presenter.showSignInfoFive(inpNo, interval, format.format(calendar.getTime()));
                } else {
                    ViewInject.toast("已经到最后一天");
                }
                break;
            case R.id.sign_tw:
                select = 0;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_mb:
                select = 1;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_ttcd:
                select = 2;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_hx:
                select = 3;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_xy:
                select = 4;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_sg:
                select = 5;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_tz:
                select = 6;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_dbcs:
                select = 7;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_rl:
                select = 8;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_cl:
                select = 9;
                setText_Status(select);
                set_data();
                break;
            case R.id.sign_nl:
                select = 10;
                setText_Status(select);
                set_data();
                break;
        }
    }

    private void setText_Status(int select)
    {
        for (int i = 0;i<11;i++)
        {
            if(select == i)
            {
//                text[i].setTextColor(getResources().getColor(R.color.app_text_four));
                text[i].setBackgroundColor(getResources().getColor(R.color.white));
            }
            else {
//                text[i].setTextColor(getResources().getColor(R.color.test_04));
                text[i].setBackgroundColor(getResources().getColor(R.color.app_bg_three));
            }
        }
    }

}
