package com.yuexunit.mobilenurse.module.Test.bean;

import java.util.ArrayList;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class TestDetail_Response {

    public TestDetail_Asrptlist asrptlist;
    public ArrayList<TestDetail_Asrptdetail> asrptdetail;

    public TestDetail_Asrptlist getAsrptlist() {
        return asrptlist;
    }

    public void setAsrptlist(TestDetail_Asrptlist asrptlist) {
        this.asrptlist = asrptlist;
    }

    public ArrayList<TestDetail_Asrptdetail> getAsrptdetail() {
        return asrptdetail;
    }

    public void setAsrptdetail(ArrayList<TestDetail_Asrptdetail> asrptdetail) {
        this.asrptdetail = asrptdetail;
    }
}
