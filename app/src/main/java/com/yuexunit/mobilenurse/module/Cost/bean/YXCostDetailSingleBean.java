package com.yuexunit.mobilenurse.module.Cost.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/21.
 */
public class YXCostDetailSingleBean {

        public YXCostDetailSingleBean body;
        public Head head;
        public CostDetail_Single_Response response;

        public Head getHead() {
            return head;
        }

        public void setHead(Head head) {
            this.head = head;
        }

    public CostDetail_Single_Response getResponse() {
        return response;
    }

    public void setResponse(CostDetail_Single_Response response) {
        this.response = response;
    }

    public YXCostDetailSingleBean getBody() {
        return body;
    }

    public void setBody(YXCostDetailSingleBean body) {
        this.body = body;
    }
}
