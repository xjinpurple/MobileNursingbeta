package com.yuexunit.mobilenurse.module.DocAdvice.bean;

/**
 * Created by work-jx on 2017/3/23.
 */
public class Dispensing_List_New {
    private String execfreq;
    private String execway;
    private String num;
    private String dosage;
    private String doseusnit;
    private String medname;
    public String execdate;
    public String execempid;
    public String exectime;

    public String getExecfreq() {
        return execfreq;
    }

    public void setExecfreq(String execfreq) {
        this.execfreq = execfreq;
    }

    public String getExecway() {
        return execway;
    }

    public void setExecway(String execway) {
        this.execway = execway;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getDoseusnit() {
        return doseusnit;
    }

    public void setDoseusnit(String doseusnit) {
        this.doseusnit = doseusnit;
    }

    public String getMedname() {
        return medname;
    }

    public void setMedname(String medname) {
        this.medname = medname;
    }

    public String getExecdate() {
        return execdate;
    }

    public void setExecdate(String execdate) {
        this.execdate = execdate;
    }

    public String getExecempid() {
        return execempid;
    }

    public void setExecempid(String execempid) {
        this.execempid = execempid;
    }

    public String getExectime() {
        return exectime;
    }

    public void setExectime(String exectime) {
        this.exectime = exectime;
    }
}
