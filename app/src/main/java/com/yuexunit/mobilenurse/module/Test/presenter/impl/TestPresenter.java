package com.yuexunit.mobilenurse.module.Test.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Test.bean.TestList_Asreqlist;
import com.yuexunit.mobilenurse.module.Test.bean.YXTestListBean;
import com.yuexunit.mobilenurse.module.Test.bean.YXTestListSingleBean;
import com.yuexunit.mobilenurse.module.Test.model.ITestModel;
import com.yuexunit.mobilenurse.module.Test.presenter.ITestPresenter;
import com.yuexunit.mobilenurse.module.Test.ui.view.ITestView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/1.
 */
public class TestPresenter implements ITestPresenter{
    private ITestView view;
    private ITestModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    ArrayList<TestList_Asreqlist> testList = new ArrayList<TestList_Asreqlist>();

    public TestPresenter(ITestView view,ITestModel model)
    {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showTestList(String visitno) {
        compositeSubscription.add(
                model.getTestListData(visitno)
                      .subscribeOn(Schedulers.io())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Subscriber<String>() {

                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {
                              ViewInject.toast("暂无数据");
                              view.showTestList(testList);
                          }

                          @Override
                          public void onNext(String result) {
                              try {
                                  YXTestListSingleBean Total = JSON.parseObject(result, YXTestListSingleBean.class);
                                  YXTestListSingleBean TestData = Total.getBody();

                                  if ("0".equals(TestData.getHead().getRet_code())) {
                                      testList.add(TestData.getResponse().getAsreqlist());
                                      view.showTestList(testList);
                                  } else {
                                      view.showTestList(testList);
                                      ViewInject.toast("获取化验单列表失败：" + TestData.getHead().getRet_info());
                                  }

                              }
                              catch (Exception e)
                              {
                                  YXTestListBean Total = JSON.parseObject(result, YXTestListBean.class);
                                  YXTestListBean TestData = Total.getBody();

                                  if ("0".equals(TestData.getHead().getRet_code())) {
                                      testList = TestData.getResponse().getAsreqlist();
                                      view.showTestList(testList);

                                  } else {
                                      view.showTestList(testList);
                                      ViewInject.toast("获取化验单列表失败：" + TestData.getHead().getRet_info());
                                  }
                              }
                          }
                      })
        );

    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
