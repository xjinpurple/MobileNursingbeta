package com.yuexunit.mobilenurse.module.HealthArticles.model.impl;

import com.yuexunit.mobilenurse.module.HealthArticles.api.ArticleApi;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.YXArticleContent;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.YXArticleList;
import com.yuexunit.mobilenurse.module.HealthArticles.model.IArticleModel;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by sslcjy on 16/1/25.
 */
public class ArticleModel implements IArticleModel {

    private ArticleApi api;
    public ArrayList articleList = new ArrayList();
    //记录Log
    private final Logger log = ProUtil.getLogger(ArticleModel.class);

    @Override
    public Observable<ArrayList<ArticleTitle>> getArticleList(String areaId) {
        api = ApiInstance();
        return api.ArticleList(areaId)
                .filter(new Func1<YXArticleList, Boolean>() {
                    @Override
                    public Boolean call(YXArticleList yxArticleList) {
                        log.debug("getArticleList_call:" + yxArticleList.toString());
                        return 200 == yxArticleList.getCode();
                    }
                }).map(new Func1<YXArticleList, ArrayList<ArticleTitle>>() {
                           @Override
                           public ArrayList<ArticleTitle> call(YXArticleList yxArticleList) {
                               articleList = yxArticleList.getData();
                               Collections.sort(articleList, new RankComparator());
                               return articleList;
                           }
                       }
                );
    }

    @Override
    public Observable<String> getArticleContent(int articleId) {
        api = ApiInstance();
        return api.ArticleContent(articleId)
                .filter(new Func1<YXArticleContent, Boolean>() {
                    @Override
                    public Boolean call(YXArticleContent yxArticleContent) {
                        log.debug("getArticleContent_call:" + yxArticleContent.toString());
                        return 200 == yxArticleContent.getCode();
                    }
                })
                .map(new Func1<YXArticleContent, String>() {
                    @Override
                    public String call(YXArticleContent yxArticleContent) {
                        return yxArticleContent.getData().getContent();
                    }
                });
    }

    // 按rank排序
    public static class RankComparator implements Comparator {
        public int compare(Object object1, Object object2) {// 实现接口中的方法
            ArticleTitle p1 = (ArticleTitle) object1; // 强制转换
            ArticleTitle p2 = (ArticleTitle) object2;
            return new Double(p1.getRank()).compareTo(new Double(p2.getRank()));
        }
    }

    public ArticleApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi(ArticleApi.class);
        }
    }
}