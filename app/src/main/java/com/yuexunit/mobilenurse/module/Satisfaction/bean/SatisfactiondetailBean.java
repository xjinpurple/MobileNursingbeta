package com.yuexunit.mobilenurse.module.Satisfaction.bean;

/**
 * Created by work-jx on 2016/6/6.
 */
public class SatisfactiondetailBean {
    private String PJBBH;
    private String PJXBH;
    private String PJXNR;
    private String PFXBH;
    private String PFXNR;
    private String PFXMR;

    public String getPJBBH() {
        return PJBBH;
    }

    public void setPJBBH(String PJBBH) {
        this.PJBBH = PJBBH;
    }

    public String getPJXBH() {
        return PJXBH;
    }

    public void setPJXBH(String PJXBH) {
        this.PJXBH = PJXBH;
    }

    public String getPJXNR() {
        return PJXNR;
    }

    public void setPJXNR(String PJXNR) {
        this.PJXNR = PJXNR;
    }

    public String getPFXBH() {
        return PFXBH;
    }

    public void setPFXBH(String PFXBH) {
        this.PFXBH = PFXBH;
    }

    public String getPFXNR() {
        return PFXNR;
    }

    public void setPFXNR(String PFXNR) {
        this.PFXNR = PFXNR;
    }

    public String getPFXMR() {
        return PFXMR;
    }

    public void setPFXMR(String PFXMR) {
        this.PFXMR = PFXMR;
    }
}
