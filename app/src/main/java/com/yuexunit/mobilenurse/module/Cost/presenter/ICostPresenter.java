package com.yuexunit.mobilenurse.module.Cost.presenter;

import java.util.Calendar;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/26.
 */
public interface ICostPresenter {

    /*
    收费数据
     */
    public void showCostList(String visitno,Calendar calendar);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
