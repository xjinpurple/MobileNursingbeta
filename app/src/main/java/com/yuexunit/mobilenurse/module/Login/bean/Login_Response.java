package com.yuexunit.mobilenurse.module.Login.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
//数据返回区(response)
public class Login_Response {

    //登陆人操作病区列表
    public String wardlist;
    //操作人员
    public Login_Empinfo empinfo;

    public String getWardlist() {
        return wardlist;
    }

    public void setWardlist(String wardlist) {
        this.wardlist = wardlist;
    }

    public Login_Empinfo getEmpinfo() {
        return empinfo;
    }

    public void setEmpinfo(Login_Empinfo empinfo) {
        this.empinfo = empinfo;
    }

    @Override
    public String toString() {
        return "Login_Response{" +
                "wardlist='" + wardlist + '\'' +
                ", empinfo=" + empinfo +
                '}';
    }
}
