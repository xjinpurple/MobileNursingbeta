package com.yuexunit.mobilenurse.module.BloodCollection.bean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2017/5/4.
 */
public class BloodCollection_Response {
    public ArrayList<Specinfo> specinfo;

    public ArrayList<Specinfo> getSpecinfo() {
        return specinfo;
    }

    public void setSpecinfo(ArrayList<Specinfo> specinfo) {
        this.specinfo = specinfo;
    }
}
