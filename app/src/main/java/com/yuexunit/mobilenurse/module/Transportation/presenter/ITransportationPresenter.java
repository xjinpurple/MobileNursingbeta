package com.yuexunit.mobilenurse.module.Transportation.presenter;

/**
 * Created by work-jx on 2016/9/29.
 */
public interface ITransportationPresenter {
    /*
   手术转运
   */
    void opsExec(String requestNo,String patState,String empId,String empName,String inputEmpId);

    /**
     * rfid
     */
    void getRfidInfo(String rfid);
}
