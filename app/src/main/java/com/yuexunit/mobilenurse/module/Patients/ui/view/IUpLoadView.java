package com.yuexunit.mobilenurse.module.Patients.ui.view;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientSign;
import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/3/25.
 */
public interface IUpLoadView {
    /**
     * 一键上传功能
     */
    void oneKeyUpload(ActionBean bean,All_Sqlite_SignInputBean signInputBean);

    /**
     * 病人体征
     */
    void showPatientSign(ArrayList<PatientSign> patientsignlist_detail);
}
