package com.yuexunit.mobilenurse.module.Patients.bean;

import java.util.ArrayList;

/**
 * Created by 姚平 on 2015/12/8.
 */
////数据返回区(response) 病区病人列表
public class PatientList_Response {

    public ArrayList<PatientList_Patinfo> patinfo;

    public ArrayList<PatientList_Patinfo> getPatinfo() {
        return patinfo;
    }

    public void setPatinfo(ArrayList<PatientList_Patinfo> patinfo) {
        this.patinfo = patinfo;
    }
}
