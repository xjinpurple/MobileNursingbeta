package com.yuexunit.mobilenurse.module.SignQuery.presenter;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignPresenter {

    /**
     * 显示所有体征项列表
     *
     */
    void showSignInfo(String inpNo, int interval, String date);

    /**
     * 显示所有体征项列表,HIS5.0
     *
     */
    void showSignInfoFive(String inpNo, String interval, String date);
}
