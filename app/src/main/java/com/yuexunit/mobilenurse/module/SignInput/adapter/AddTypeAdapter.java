package com.yuexunit.mobilenurse.module.SignInput.adapter;

import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single_Check;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by 姚平 on 2015/11/30.
 */
public class AddTypeAdapter extends KJAdapter<Sign_Single_Check> {


    public ArrayList<Sign_Single_Check> arrayList = new ArrayList<Sign_Single_Check>();
//    private ArrayList<CheckType> list;
//    private CheckType a;

    public AddTypeAdapter(AbsListView view, Collection<Sign_Single_Check> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        arrayList =(ArrayList<Sign_Single_Check>)mDatas;
//        initData(((ArrayList<Sign_Single_Check>) mDatas).size());
    }

    @Override
    public void convert(AdapterHolder adapterHolder, Sign_Single_Check signsInput_data, boolean b) {

    }

//    /**
//     * 存储刚开始各个体征中的checkbox选中情况
//     */
//    private void initData(int size) {
//        list = new ArrayList<CheckType>();
//        for (int i = 0; i < size; i++) {
//            a = new CheckType(i + "号位", CheckType.TYPE_NOCHECKED);
//            list.add(a);
//        }
//    }

    @Override
    public void convert(AdapterHolder adapterHolder, final Sign_Single_Check sign_single_check, boolean isScrolling, final int position) {
        super.convert(adapterHolder, sign_single_check, isScrolling, position);

        adapterHolder.setText(R.id.type_name, sign_single_check.getName());
        final CheckBox type_check = adapterHolder.getView(R.id.type_check);
        type_check.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type_check.isChecked()) {
//                    sign_single_check.setIscheck(true);
//                    arrayList.add(sign_single_check);
//                    list.get(position).type = CheckType.TYPE_CHECKED;
                    arrayList.get(position).setIscheck(true);
                } else {
//                    if (arrayList.contains(sign_single_check)) {
//                        arrayList.remove(sign_single_check);
////                        list.get(position).type = CheckType.TYPE_NOCHECKED;
//                    }
                    arrayList.get(position).setIscheck(false);
                }
            }
        });
        if (arrayList.get(position).getIscheck()) {
            type_check.setChecked(true);
        } else {
            type_check.setChecked(false);
        }
    }

    public ArrayList<Sign_Single_Check> getArrayList() {
        return arrayList;
    }

//    class CheckType {
//        public static final int TYPE_CHECKED = 1;
//        public static final int TYPE_NOCHECKED = 0;
//        String name;
//        int type;
//
//        public CheckType(String name, int type) {
//            this.name = name;
//            this.type = type;
//        }
//    }
}
