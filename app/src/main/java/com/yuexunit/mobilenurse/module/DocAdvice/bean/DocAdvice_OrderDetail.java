package com.yuexunit.mobilenurse.module.DocAdvice.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class DocAdvice_OrderDetail {

    //医嘱组号
    public long ordergroupno;
    //医嘱序号
    public long orderseq;
    //药品代码
    public long medid;
    //药品名称
    public String medname;
    //用量
    public double dosage;
    //用量单位
    public String doseusnit;
    //数量
    public String num;

    public String medbarcode;

    public String infusionbarcode;

    public long getOrdergroupno() {
        return ordergroupno;
    }

    public void setOrdergroupno(long ordergroupno) {
        this.ordergroupno = ordergroupno;
    }

    public long getOrderseq() {
        return orderseq;
    }

    public void setOrderseq(long orderseq) {
        this.orderseq = orderseq;
    }

    public long getMedid() {
        return medid;
    }

    public void setMedid(long medid) {
        this.medid = medid;
    }

    public String getMedname() {
        return medname;
    }

    public void setMedname(String medname) {
        this.medname = medname;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

    public String getDoseusnit() {
        return doseusnit;
    }

    public void setDoseusnit(String doseusnit) {
        this.doseusnit = doseusnit;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getMedbarcode() {
        return medbarcode;
    }

    public void setMedbarcode(String medbarcode) {
        this.medbarcode = medbarcode;
    }

    public String getInfusionbarcode() {
        return infusionbarcode;
    }

    public void setInfusionbarcode(String infusionbarcode) {
        this.infusionbarcode = infusionbarcode;
    }
}
