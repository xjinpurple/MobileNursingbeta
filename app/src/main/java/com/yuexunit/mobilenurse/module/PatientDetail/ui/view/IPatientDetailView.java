package com.yuexunit.mobilenurse.module.PatientDetail.ui.view;

import com.yuexunit.mobilenurse.module.PatientDetail.bean.Patient_Patinfo;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/24.
 */
public interface IPatientDetailView {

    void showPatientDetail(Patient_Patinfo currentPatient);

    void loadingDialogStatus(int status);

    void NoPatientDialog();

    void bloodDialog(int status);

    void saveSingleTypes(ArrayList<Sign_Single> sign_singles);
}
