package com.yuexunit.mobilenurse.module.Inspect.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//病人检查报告单
public class YXInjectionDetailBean {
    public YXInjectionDetailBean body;
    public Head head;
    public InjectionDetail_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public InjectionDetail_Response getResponse() {
        return response;
    }

    public void setResponse(InjectionDetail_Response response) {
        this.response = response;
    }

    public YXInjectionDetailBean getBody() {
        return body;
    }

    public void setBody(YXInjectionDetailBean body) {
        this.body = body;
    }
}
