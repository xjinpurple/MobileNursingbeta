package com.yuexunit.mobilenurse.module.Satisfaction.presenter.impl;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBackBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionIsCommitBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionLastBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionPFXBean;
import com.yuexunit.mobilenurse.module.Satisfaction.model.ISatisfactionModel;
import com.yuexunit.mobilenurse.module.Satisfaction.presenter.ISatisfactionPresenter;
import com.yuexunit.mobilenurse.module.Satisfaction.ui.view.ISatisfactionView;
import com.yuexunit.mobilenurse.module.Satisfaction.xml.IsSatisfactionCommitXml;
import com.yuexunit.mobilenurse.module.Satisfaction.xml.SatisfactionCommitXml;
import com.yuexunit.mobilenurse.module.Satisfaction.xml.SatisfactionXml;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/6/3.
 */
public class SatisfactionPresenter implements ISatisfactionPresenter{
    private ISatisfactionView view;
    private ISatisfactionModel model;
    ArrayList<SatisfactionLastBean> satisfactionLastBeans  = new ArrayList<SatisfactionLastBean>();
    SatisfactionCommitBackBean satisfactionCommitBackBean = new SatisfactionCommitBackBean();
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    SatisfactionIsCommitBean satisfactionIsCommitBean = new SatisfactionIsCommitBean();

    public SatisfactionPresenter(ISatisfactionView view,ISatisfactionModel model)
    {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showSatisfactionList() {
        compositeSubscription.add(
                model.getSatisfactionListData()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("暂无数据");
                                view.showSatisfactionList(satisfactionLastBeans);
                            }

                            @Override
                            public void onNext(String s) {
                                SatisfactionBean satisfactionBean = SatisfactionXml.getSatisfactionData(s);
                               if("0".equals(satisfactionBean.getRet_code()))
                               {
                                   for(int i = 0;i<satisfactionBean.getSatisfactiondetailBean_list().size();i=i+3)
                                   {
                                       SatisfactionLastBean satisfactionLastBean = new SatisfactionLastBean();
                                       satisfactionLastBean.setPJBBH(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPJBBH());
                                       satisfactionLastBean.setPJXBH(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPJXBH());
                                       satisfactionLastBean.setPJXNR(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPJXNR());
                                       List<SatisfactionPFXBean> satisfactionPFXBeans =new ArrayList<SatisfactionPFXBean>();

                                       SatisfactionPFXBean satisfactionPFXBean = new SatisfactionPFXBean();
                                       satisfactionPFXBean.setPFXBH(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPFXBH());
                                       satisfactionPFXBean.setPFXNR(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPFXNR());
                                       satisfactionPFXBean.setPFXMR(satisfactionBean.getSatisfactiondetailBean_list().get(i).getPFXMR());
                                       satisfactionPFXBeans.add(satisfactionPFXBean);
                                       satisfactionPFXBean = new SatisfactionPFXBean();
                                       satisfactionPFXBean.setPFXBH(satisfactionBean.getSatisfactiondetailBean_list().get(i + 1).getPFXBH());
                                       satisfactionPFXBean.setPFXNR(satisfactionBean.getSatisfactiondetailBean_list().get(i + 1).getPFXNR());
                                       satisfactionPFXBean.setPFXMR(satisfactionBean.getSatisfactiondetailBean_list().get(i + 1).getPFXMR());
                                       satisfactionPFXBeans.add(satisfactionPFXBean);
                                       satisfactionPFXBean = new SatisfactionPFXBean();
                                       satisfactionPFXBean.setPFXBH(satisfactionBean.getSatisfactiondetailBean_list().get(i + 2).getPFXBH());
                                       satisfactionPFXBean.setPFXNR(satisfactionBean.getSatisfactiondetailBean_list().get(i + 2).getPFXNR());
                                       satisfactionPFXBean.setPFXMR(satisfactionBean.getSatisfactiondetailBean_list().get(i + 2).getPFXMR());
                                       satisfactionPFXBeans.add(satisfactionPFXBean);

                                       satisfactionLastBean.setSatisfactionPFXBeans(satisfactionPFXBeans);
                                       satisfactionLastBeans.add(satisfactionLastBean);
                                   }
                                   view.showSatisfactionList(satisfactionLastBeans);
                               }
                            }
                        })
          );
    }

    @Override
    public void commitSatisfactionDate(ArrayList<SatisfactionCommitBean> list) {
        for (final SatisfactionCommitBean satisfactionCommitBean : list)
        {
            model.commitSatisfactionData(satisfactionCommitBean.getPJBBH(),satisfactionCommitBean.getPJXBH(),satisfactionCommitBean.getPFXBH(),
                    satisfactionCommitBean.getMZBRBH(),satisfactionCommitBean.getZYBRBH(),satisfactionCommitBean.getPFSJ())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
//                            log.info("PatientListPresenter_oneKeyUpload_onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.commitSatisfactionDate(satisfactionCommitBackBean);
//                            ViewInject.toast("一键上传体征信息失败...");
//                            log.info("PatientListPresenter_oneKeyUpload_onError:"+e);
                        }

                        @Override
                        public void onNext(String s) {
//                            log.info("PatientListPresenter_oneKeyUpload_onNext:"+actionBean.toString());
                            satisfactionCommitBackBean = SatisfactionCommitXml.getSatisfactionData(s);
                            view.commitSatisfactionDate(satisfactionCommitBackBean);
                        }
                    });
        }
    }

    @Override
    public void IscommitSatisfactionDate(String visitno) {
        compositeSubscription.add(
                model.IsSatisfactionCommitDate(visitno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
//                                String xmls =  ss.replaceAll("COUNT\\(1\\)","COUNT");
//                                ViewInject.toast(xmls);
                                satisfactionIsCommitBean.setCOUNT("0");
                                view.iscommitSatisfaction(satisfactionIsCommitBean);
                            }

                            @Override
                            public void onNext(String s) {
                                String xmls =  s.replaceAll("COUNT\\(1\\)","COUNT");
//                                ViewInject.toast(xmls);
                                satisfactionIsCommitBean = IsSatisfactionCommitXml.getSatisfactionData(xmls);
                                view.iscommitSatisfaction(satisfactionIsCommitBean);

                            }
                        })
        );
    }


    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
