package com.yuexunit.mobilenurse.module.Patients.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientSign;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by work-jx on 2016/3/28.
 */
public class PatinetSignListAdapter extends KJAdapter<PatientSign>{
    private ArrayList<PatientSign> patientSigns = new ArrayList<PatientSign>();

    public PatinetSignListAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        patientSigns = (ArrayList<PatientSign>) ((ArrayList)mDatas).clone();
    }

    @Override
    public void convert(AdapterHolder adapterHolder, PatientSign item, boolean isScrolling) {
        adapterHolder.setText(R.id.patientsign_name,item.getName());
        adapterHolder.setText(R.id.patientsign_visitno,"住院号:"+item.getVisitno());
        adapterHolder.setText(R.id.patientsign_type,item.getType()+":"+item.getCount()+item.getUnit());
        adapterHolder.setText(R.id.patientsign_time,item.getTime());
    }
}
