package com.yuexunit.mobilenurse.module.Inspect.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionDetail_Exrptdetail;
import com.yuexunit.mobilenurse.module.Inspect.bean.YXInjectionDetailBean;
import com.yuexunit.mobilenurse.module.Inspect.model.IInspectDetailModel;
import com.yuexunit.mobilenurse.module.Inspect.presenter.IInspectDetailPresenter;
import com.yuexunit.mobilenurse.module.Inspect.ui.view.IInspectDetailView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/29.
 */
public class InspectDetailPresenter implements IInspectDetailPresenter{
    private IInspectDetailModel model;
    private IInspectDetailView view;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private InjectionDetail_Exrptdetail injectionDetail_exrptdetail;

    public InspectDetailPresenter(IInspectDetailView view,IInspectDetailModel model)
    {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showInspectDetail(String requestno) {
        compositeSubscription.add(
                model.getInspectDetailData(requestno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("暂无数据");
                            }

                            @Override
                            public void onNext(String result) {
                                YXInjectionDetailBean Total = JSON.parseObject(result, YXInjectionDetailBean.class);
                                YXInjectionDetailBean InjectionDetailData = Total.getBody();

                                if ("0".equals(InjectionDetailData.getHead().getRet_code())) {
                                    injectionDetail_exrptdetail = InjectionDetailData.getResponse().getExrptdetail();
                                    view.showInspectDetail(injectionDetail_exrptdetail.getMani(), injectionDetail_exrptdetail.getConclusion(), injectionDetail_exrptdetail.getReportno() + "");

                                } else {
                                    ViewInject.toast("获取检验报告单失败：" + InjectionDetailData.getHead().getRet_info());
                                }
                            }
                        })
        );
    }

    @Override
    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    @Override
    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}
