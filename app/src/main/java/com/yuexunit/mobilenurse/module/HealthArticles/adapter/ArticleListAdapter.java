package com.yuexunit.mobilenurse.module.HealthArticles.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by 姚平 on 2015/12/5.
 */
public class ArticleListAdapter extends KJAdapter<ArticleTitle> {


    public ArticleListAdapter(AbsListView view, Collection<ArticleTitle> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, ArticleTitle article, boolean b) {
            adapterHolder.setText(R.id.article_title,article.getTitle());
    }
}
