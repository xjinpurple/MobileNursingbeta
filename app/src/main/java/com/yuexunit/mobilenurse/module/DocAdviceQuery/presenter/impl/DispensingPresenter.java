package com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.impl;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Dispensing;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_OrderDetail;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Orderlist;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.YXDocAdviceBean;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.model.IDispensingModel;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.IDispensingPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view.IDispensingView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/3.
 */
public class DispensingPresenter implements IDispensingPresenter {
    private IDispensingModel model;
    private IDispensingView view;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    ArrayList<DocAdvice_Dispensing> docAdvice_all = new ArrayList<DocAdvice_Dispensing>();

    public DispensingPresenter(IDispensingView view,IDispensingModel model)
    {
        this.view = view;
        this.model = model;
    }


    @Override
    public void showDispensingList(String visitno) {
        compositeSubscription.add(
                model.getDispensingListData(visitno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("暂无数据");
                                view.showDispensingList(docAdvice_all);
                            }

                            @Override
                            public void onNext(String result) {
                                YXDocAdviceBean Total = JSON.parseObject(result, YXDocAdviceBean.class);
                                YXDocAdviceBean DocAdviceData = Total.getBody();
                                Log.v("jx", DocAdviceData.getHead().getRet_code());
                                if ("0".equals(DocAdviceData.getHead().getRet_code())) {

                                    List<DocAdvice_Orderlist> docadvice_orderlist;
                                    List<DocAdvice_OrderDetail> docadvice_orderdetail;

                                    String orderlist = DocAdviceData.getResponse().getOrderlist();
                                    String orderlist_first = orderlist.substring(0, 1);
                                    if (!orderlist_first.equals("[")) {
                                        orderlist = "[" + orderlist + "]";
                                        docadvice_orderlist = JSON.parseArray(orderlist, DocAdvice_Orderlist.class);
                                    } else {
                                        docadvice_orderlist = JSON.parseArray(orderlist, DocAdvice_Orderlist.class);
                                    }

                                    String orderdetail = DocAdviceData.getResponse().getOrderdetail();
                                    String orderdetail_first = orderdetail.substring(0, 1);
                                    if (!orderdetail_first.equals("[")) {
                                        orderdetail = "[" + orderdetail + "]";
                                        docadvice_orderdetail = JSON.parseArray(orderdetail, DocAdvice_OrderDetail.class);
                                    } else {
                                        docadvice_orderdetail = JSON.parseArray(orderdetail, DocAdvice_OrderDetail.class);
                                    }

                                    for (int i = 0; i < docadvice_orderlist.size(); i++) {
                                        DocAdvice_Dispensing docAdvice_dispensing = new DocAdvice_Dispensing();
                                        Log.v("jx", docadvice_orderlist.get(i).getInjectflag());
                                        if ("1".equals(docadvice_orderlist.get(i).getOrdertype())) {
                                            docAdvice_dispensing.setDocAdvice_orderlist(docadvice_orderlist.get(i));
                                            ArrayList<DocAdvice_OrderDetail> docAdvice_orderDetails = new ArrayList<DocAdvice_OrderDetail>();
                                            for (int j = 0; j < docadvice_orderdetail.size(); j++) {
                                                if (docAdvice_dispensing.getDocAdvice_orderlist().getOrdergroupno() == docadvice_orderdetail.get(j).getOrdergroupno()) {
                                                   boolean ishave = false;
                                                    for(int z = 0; z<docAdvice_orderDetails.size();z++) {
                                                       if(docadvice_orderdetail.get(j).getOrderseq() == docAdvice_orderDetails.get(z).getOrderseq())
                                                       {
                                                           ishave = true;
                                                       }
                                                   }
                                                    if(!ishave)
                                                    {
                                                        docAdvice_orderDetails.add(docadvice_orderdetail.get(j));
                                                    }
                                                }
                                            }
                                            docAdvice_dispensing.setDespensing_detail(docAdvice_orderDetails);
                                            docAdvice_all.add(docAdvice_dispensing);
                                        } else {
                                            Log.v("jx", "over");
                                        }
                                    }
                                }
                                view.showDispensingList(docAdvice_all);
                            }
                        }));
    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
