package com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.BloodCollection.bean.Specinfo;
import com.yuexunit.mobilenurse.module.BloodCollection.bean.YXBloodCollectionBean;
import com.yuexunit.mobilenurse.module.BloodCollection.bean.YXBloodCollectionSingleBean;
import com.yuexunit.mobilenurse.module.BloodCollection.model.IBloodCollectionSearchModel;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionSearchModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionSearchPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionSearchView;
import com.yuexunit.mobilenurse.module.Cost.ui.Act_Cost;

import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by work-jx on 2017/5/4.
 */
public class BloodCollectionSearchPresenter implements IBloodCollectionSearchPresenter{
    //记录Log
    private final Logger log = LoggerFactory.getLogger(BloodCollectionSearchModel.class);

    private IBloodCollectionSearchView view;
    private IBloodCollectionSearchModel model;

    ArrayList<Specinfo> specinfos = new ArrayList<Specinfo>();

    public BloodCollectionSearchPresenter(IBloodCollectionSearchView view, IBloodCollectionSearchModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showBloodCollectionList(String wardId) {
        model.getBloodSearchListData(wardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {

                               @Override
                               public void onCompleted() {

                               }

                               @Override
                               public void onError(Throwable e) {
                                   log.error("Exception:" + e);
                                   ViewInject.toast("暂无数据");
                                   view.showBloodSearchList(specinfos);
                                   view.dialogState(Act_Cost.DISMISS_DIALOG);
                               }

                               @Override
                               public void onNext(String result) {
                                   try {
                                       YXBloodCollectionSingleBean Total = JSON.parseObject(result, YXBloodCollectionSingleBean.class);
                                       YXBloodCollectionSingleBean bloodCollectionData = Total.getBody();

                                       if ("0".equals(bloodCollectionData.getHead().getRet_code())) {
                                           specinfos.add(bloodCollectionData.getResponse().getSpecinfo());
                                           view.showBloodSearchList(specinfos);
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);
                                       } else {
                                           view.showBloodSearchList(specinfos);
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);
                                           ViewInject.toast("获取当日采血列表失败：" + bloodCollectionData.getHead().getRet_info());
                                       }

                                   }
                                   catch (Exception e)
                                   {
                                       YXBloodCollectionBean Total = JSON.parseObject(result, YXBloodCollectionBean.class);
                                       YXBloodCollectionBean bloodCollectionData = Total.getBody();

                                       if ("0".equals(bloodCollectionData.getHead().getRet_code())) {
                                           specinfos = bloodCollectionData.getResponse().getSpecinfo();
                                           view.showBloodSearchList(specinfos);
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);

                                       } else {
                                           view.showBloodSearchList(specinfos);
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);
                                           ViewInject.toast("获取当日采血列表失败：" + bloodCollectionData.getHead().getRet_info());
                                       }
                                   }
                               }
                           }
                );
    }
}
