package com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/3.
 */
public interface IInjectionPresenter {
    /**
     * 医嘱列表信息
     */
    void showInjectionList(String visitno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
