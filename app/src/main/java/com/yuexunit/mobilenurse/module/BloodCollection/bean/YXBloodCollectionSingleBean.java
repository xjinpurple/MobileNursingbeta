package com.yuexunit.mobilenurse.module.BloodCollection.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2017/5/4.
 */
public class YXBloodCollectionSingleBean {
    public YXBloodCollectionSingleBean body;
    public Head head;
    public BloodCollectionSingle_Response response;

    public YXBloodCollectionSingleBean getBody() {
        return body;
    }

    public void setBody(YXBloodCollectionSingleBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public BloodCollectionSingle_Response getResponse() {
        return response;
    }

    public void setResponse(BloodCollectionSingle_Response response) {
        this.response = response;
    }
}
