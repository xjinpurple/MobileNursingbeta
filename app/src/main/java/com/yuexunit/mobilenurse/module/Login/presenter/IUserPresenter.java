package com.yuexunit.mobilenurse.module.Login.presenter;

/**
 * Created by sslcjy on 16/1/20.
 */
public interface IUserPresenter {


    /**
     * 用户登陆操作
     */
    public void doInLogin(final String uid, final String pwd);


    /**
     * 下载服务器上所有配置
     */
    //获取JCI配置信息
    public void getListParams();

}
