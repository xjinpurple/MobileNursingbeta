package com.yuexunit.mobilenurse.module.Transportation.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/9/27.
 */
public interface ISelectOperModel {
    /**
     * 获取手术转运状态
     */
    Observable<String> getOpsListData(String visitno);
}
