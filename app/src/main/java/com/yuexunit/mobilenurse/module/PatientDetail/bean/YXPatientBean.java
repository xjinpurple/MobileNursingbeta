package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/11/22.
 */


//单个病人信息

public class YXPatientBean {

    public YXPatientBean body;
    public Head head;
    public Patient_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Patient_Response getResponse() {
        return response;
    }

    public void setResponse(Patient_Response response) {
        this.response = response;
    }

    public YXPatientBean getBody() {
        return body;
    }

    public void setBody(YXPatientBean body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "YXPatientBean{" +
                "body=" + body +
                ", head=" + head +
                ", response=" + response +
                '}';
    }
}
