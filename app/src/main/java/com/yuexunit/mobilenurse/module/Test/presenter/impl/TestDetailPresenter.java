package com.yuexunit.mobilenurse.module.Test.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Test.bean.TestDetail_Asrptdetail;
import com.yuexunit.mobilenurse.module.Test.bean.YXTestDetailBean;
import com.yuexunit.mobilenurse.module.Test.bean.YXTestDetailSingleBean;
import com.yuexunit.mobilenurse.module.Test.model.ITestDetailModel;
import com.yuexunit.mobilenurse.module.Test.presenter.ITestDetaiPresenter;
import com.yuexunit.mobilenurse.module.Test.ui.view.ITestDetailView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/1.
 */
public class TestDetailPresenter implements ITestDetaiPresenter{
    private ITestDetailView view;
    private ITestDetailModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    ArrayList<TestDetail_Asrptdetail> testDetail_asrptdetails = new ArrayList<TestDetail_Asrptdetail>();

    public TestDetailPresenter(ITestDetailView view,ITestDetailModel model)
    {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showTestDetail(String requestno) {
        compositeSubscription.add(
                model.getTestDetailData(requestno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("暂无数据");
                                view.showTestDetail(testDetail_asrptdetails,"");
                            }

                            @Override
                            public void onNext(String result) {
                                try{
                                    YXTestDetailSingleBean Total = JSON.parseObject(result, YXTestDetailSingleBean.class);
                                    YXTestDetailSingleBean TestDetailData = Total.getBody();

                                    if ("0".equals(TestDetailData.getHead().getRet_code())) {
                                        testDetail_asrptdetails.add(TestDetailData.getResponse().getAsrptdetail());
                                        view.showTestDetail(testDetail_asrptdetails,TestDetailData.getResponse().getAsrptlist().getReportno());

                                    } else {
                                        view.showTestDetail(testDetail_asrptdetails,"");
                                        ViewInject.toast("获取化验单详情失败：" + TestDetailData.getHead().getRet_info());
                                    }
                                }
                                catch (Exception e)
                                {
                                    YXTestDetailBean Total = JSON.parseObject(result, YXTestDetailBean.class);
                                    YXTestDetailBean TestDetailData = Total.getBody();

                                    if ("0".equals(TestDetailData.getHead().getRet_code())) {
                                        testDetail_asrptdetails = TestDetailData.getResponse().getAsrptdetail();
                                        view.showTestDetail(testDetail_asrptdetails,TestDetailData.getResponse().getAsrptlist().getReportno());

                                    } else {
                                        view.showTestDetail(testDetail_asrptdetails,"");
                                        ViewInject.toast("获取化验单详情失败：" + TestDetailData.getHead().getRet_info());
                                    }
                                }
                            }
                        })
            );
    }

    @Override
    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    @Override
    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}
