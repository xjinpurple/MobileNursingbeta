package com.yuexunit.mobilenurse.module.NursingPlan.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.module.NursingPlan.bean.NursingPlanBean;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2017/1/22.
 */
public class NursingPlanAdapter extends KJAdapter<NursingPlanBean>{

    public NursingPlanAdapter(AbsListView view, Collection<NursingPlanBean> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, NursingPlanBean nursingPlanBean, boolean b,final int position) {

    }
}
