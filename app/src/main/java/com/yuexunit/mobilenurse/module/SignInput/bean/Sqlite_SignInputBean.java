package com.yuexunit.mobilenurse.module.SignInput.bean;

import java.io.Serializable;

/**
 * Created by sslcjy on 15/12/24.
 */
public class Sqlite_SignInputBean implements Serializable{

    private int id;
    //住院号
    private String inpNo;
    //测量时的unix时间戳
    private String timestamp;
    //体征id和体征内容
    private String signData;
    //记录人员姓名
    private String recorder;
    //应当录入的时间
    private String timepoint;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInpNo() {
        return inpNo;
    }

    public void setInpNo(String inpNo) {
        this.inpNo = inpNo;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSignData() {
        return signData;
    }

    public void setSignData(String signData) {
        this.signData = signData;
    }

    public String getRecorder() {
        return recorder;
    }

    public void setRecorder(String recorder) {
        this.recorder = recorder;
    }

    public String getTimepoint() {
        return timepoint;
    }

    public void setTimepoint(String timepoint) {
        this.timepoint = timepoint;
    }
}
