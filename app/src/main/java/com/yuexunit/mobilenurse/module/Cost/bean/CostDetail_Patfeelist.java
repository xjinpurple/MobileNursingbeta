package com.yuexunit.mobilenurse.module.Cost.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/11.
 */
public class CostDetail_Patfeelist {

    //住院号
    public String visit_no;
    //费用序号
    public int feeseqno;
    //费用日期
    @JSONField(format = "yyyy-MM-dd")
    public Date feedate;
    //费用代码
    public String feecode;
    //费用名称
    public String feename;
    //费用数量
    public double feenum;
    //费用金额
    public double feeamount;

    public String getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(String visit_no) {
        this.visit_no = visit_no;
    }

    public int getFeeseqno() {
        return feeseqno;
    }

    public void setFeeseqno(int feeseqno) {
        this.feeseqno = feeseqno;
    }

    public Date getFeedate() {
        return feedate;
    }

    public void setFeedate(Date feedate) {
        this.feedate = feedate;
    }

    public String getFeecode() {
        return feecode;
    }

    public void setFeecode(String feecode) {
        this.feecode = feecode;
    }

    public String getFeename() {
        return feename;
    }

    public void setFeename(String feename) {
        this.feename = feename;
    }

    public double getFeenum() {
        return feenum;
    }

    public void setFeenum(double feenum) {
        this.feenum = feenum;
    }

    public double getFeeamount() {
        return feeamount;
    }

    public void setFeeamount(double feeamount) {
        this.feeamount = feeamount;
    }
}
