package com.yuexunit.mobilenurse.module.Transportation.presenter.impl;

import com.yuexunit.mobilenurse.module.Transportation.bean.RfidBean;
import com.yuexunit.mobilenurse.module.Transportation.model.ITransportationModel;
import com.yuexunit.mobilenurse.module.Transportation.presenter.ITransportationPresenter;
import com.yuexunit.mobilenurse.module.Transportation.ui.view.ITransportationView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by work-jx on 2016/9/29.
 */
public class TransportationPresenter implements ITransportationPresenter {

    private ITransportationModel model;
    private ITransportationView view;
    private final Logger log = LoggerFactory.getLogger(TransportationPresenter.class);
//    ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities = new ArrayList<YXSignQuery.SignQueryEntity>();

    public TransportationPresenter(ITransportationModel model,ITransportationView view){
        this.model = model;
        this.view = view;
    }

    @Override
    public void opsExec(String requestNo, String patState, String empId, String empName, String inputEmpId) {
        model.opsExec(requestNo,patState,empId,empName,inputEmpId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error(e + "");
                        view.execOps("");
                    }

                    @Override
                    public void onNext(String s) {
                        view.execOps(s);
                    }
                });
    }

    @Override
    public void getRfidInfo(String rfid) {
        model.getRfidInfo(rfid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RfidBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error(e+"");
                        RfidBean rfidBean = new RfidBean();
                        view.showRfidInfo(rfidBean);
                    }

                    @Override
                    public void onNext(RfidBean rfidBean) {
                        view.showRfidInfo(rfidBean);
                    }
                });
    }
}
