package com.yuexunit.mobilenurse.module.SignInput.bean;

/**
 * Created by work-jx on 2016/3/28.
 */
public class All_Sqlite_SignInputBean {
    private int id;
    //住院号
    private String inpNo;
    //测量时的unix时间戳
    private String timestamp;
    //体征id和体征内容
    private String signData;
    //记录人员姓名
    private String recorder;
    //应当录入的时间
    private String timepoint;
    //病人姓名
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInpNo() {
        return inpNo;
    }

    public void setInpNo(String inpNo) {
        this.inpNo = inpNo;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSignData() {
        return signData;
    }

    public void setSignData(String signData) {
        this.signData = signData;
    }

    public String getRecorder() {
        return recorder;
    }

    public void setRecorder(String recorder) {
        this.recorder = recorder;
    }

    public String getTimepoint() {
        return timepoint;
    }

    public void setTimepoint(String timepoint) {
        this.timepoint = timepoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
