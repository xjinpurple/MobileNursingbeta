package com.yuexunit.mobilenurse.module.HealthArticles.ui;

import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.HealthArticles.model.impl.ArticleModel;
import com.yuexunit.mobilenurse.module.HealthArticles.presenter.impl.ArticlePresenter;
import com.yuexunit.mobilenurse.module.HealthArticles.ui.view.IArticleContentView;

import org.kymjs.kjframe.KJActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 姚平 on 2015/12/5.
 * <p/>
 * 具体某篇健康宣教内容界面
 */
public class Act_ArticleContent extends KJActivity implements IArticleContentView {

    @Bind(R.id.article_text)
    public TextView article_text;
    @Bind(R.id.article_title)
    public TextView article_title;

    //健康宣教的标题
    private String title;
    private int articleId;
    private ArticlePresenter presenter;

    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_ArticleContent.class);

    @OnClick(R.id.img_back)
    public void out() {
        finish();
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.act_article_content);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        articleId = getIntent().getExtras().getInt("id");
        title = getIntent().getExtras().getString("title");
        presenter = new ArticlePresenter(new ArticleModel(), this);
    }

    @Override
    public void initWidget() {
        super.initWidget();
        article_title.setText(title);
        presenter.showArticleContent(articleId);
    }

    @Override
    public void showArticleContent(String Content) {
        article_text.setText(Content);
    }
}