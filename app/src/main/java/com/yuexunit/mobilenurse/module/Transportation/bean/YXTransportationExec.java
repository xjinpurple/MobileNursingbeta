package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/10/17.
 */
public class YXTransportationExec {

    /**
     * head : {"ret_code":"0","ret_info":"成功"}
     * response : {"opsExecInfo":{"info":"成功"}}
     */

    private BodyEntity body;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class BodyEntity {
        /**
         * ret_code : 0
         * ret_info : 成功
         */

        private HeadEntity head;
        /**
         * opsExecInfo : {"info":"成功"}
         */

        private ResponseEntity response;

        public void setHead(HeadEntity head) {
            this.head = head;
        }

        public void setResponse(ResponseEntity response) {
            this.response = response;
        }

        public HeadEntity getHead() {
            return head;
        }

        public ResponseEntity getResponse() {
            return response;
        }

        public static class HeadEntity {
            private String ret_code;
            private String ret_info;

            public void setRet_code(String ret_code) {
                this.ret_code = ret_code;
            }

            public void setRet_info(String ret_info) {
                this.ret_info = ret_info;
            }

            public String getRet_code() {
                return ret_code;
            }

            public String getRet_info() {
                return ret_info;
            }
        }

        public static class ResponseEntity {
            /**
             * info : 成功
             */

            private OpsExecInfoEntity opsExecInfo;

            public void setOpsExecInfo(OpsExecInfoEntity opsExecInfo) {
                this.opsExecInfo = opsExecInfo;
            }

            public OpsExecInfoEntity getOpsExecInfo() {
                return opsExecInfo;
            }

            public static class OpsExecInfoEntity {
                private String info;

                public void setInfo(String info) {
                    this.info = info;
                }

                public String getInfo() {
                    return info;
                }
            }
        }
    }
}
