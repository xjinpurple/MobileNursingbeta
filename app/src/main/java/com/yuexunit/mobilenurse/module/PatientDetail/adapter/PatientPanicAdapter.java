package com.yuexunit.mobilenurse.module.PatientDetail.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Panic.ui.Act_PanicKT;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.PatientPanicBean;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2016/6/16.
 */
public class PatientPanicAdapter extends KJAdapter<PatientPanicBean>{

    public PatientPanicAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, PatientPanicBean item, boolean isScrolling) {
        adapterHolder.setText(R.id.patientpanic_name,item.getName());

        //对item设置点击事件，跳转到病人详情界面
        onPicClick(adapterHolder.getView(R.id.patientpanic_btn));
    }

    private void onPicClick(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCxt, Act_PanicKT.class);
                mCxt.startActivity(intent);
            }
        });
    }
}
