package com.yuexunit.mobilenurse.module.Satisfaction.bean;

/**
 * Created by work-jx on 2016/6/7.
 */
public class SatisfactionCommitBackBean {
    private String ret_code;
    private String ret_info;

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_info() {
        return ret_info;
    }

    public void setRet_info(String ret_info) {
        this.ret_info = ret_info;
    }
}
