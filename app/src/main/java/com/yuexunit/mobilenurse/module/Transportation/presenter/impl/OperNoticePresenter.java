package com.yuexunit.mobilenurse.module.Transportation.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Transportation.bean.OperNotice_OpsList;
import com.yuexunit.mobilenurse.module.Transportation.bean.YXOperNoticeBean;
import com.yuexunit.mobilenurse.module.Transportation.model.IOperNoticeModel;
import com.yuexunit.mobilenurse.module.Transportation.presenter.IOperNoticePresenter;
import com.yuexunit.mobilenurse.module.Transportation.ui.view.IOperNoticeView;

import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by work-jx on 2016/12/2.
 */
public class OperNoticePresenter implements IOperNoticePresenter{
    private IOperNoticeModel model;
    private IOperNoticeView view;
    private final Logger log = LoggerFactory.getLogger(OperNoticePresenter.class);
    ArrayList<OperNotice_OpsList> operNotice_opsLists = new ArrayList<OperNotice_OpsList>();

    public OperNoticePresenter(IOperNoticeModel model,IOperNoticeView view){
        this.model = model;
        this.view = view;
    }

    @Override
    public void showOpsNoticeList(String requestno) {
        model.getOpsNoticeDate(requestno)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        ViewInject.toast("暂无数据");
                        view.showOpsNoticeList(operNotice_opsLists);
                    }

                    @Override
                    public void onNext(String result) {
                        YXOperNoticeBean Total = JSON.parseObject(result, YXOperNoticeBean.class);
                        YXOperNoticeBean OpsData = Total.getBody();

                        if ("0".equals(OpsData.getHead().getRet_code())) {
                            List<OperNotice_OpsList> opsinfos;

                            String opsinfo = OpsData.getResponse().getOpslist();
                            String opsinfo_first = opsinfo.substring(0, 1);
                            if (!opsinfo_first.equals("[")) {
                                opsinfo = "[" + opsinfo + "]";
                                opsinfos = JSON.parseArray(opsinfo, OperNotice_OpsList.class);
                            } else {
                                opsinfos = JSON.parseArray(opsinfo, OperNotice_OpsList.class);
                            }

                            for (int i = 0;i<opsinfos.size();i++)
                            {
                                operNotice_opsLists.add(opsinfos.get(i));
                            }

                            view.showOpsNoticeList(operNotice_opsLists);

                        } else {
                            view.showOpsNoticeList(operNotice_opsLists);
                            ViewInject.toast("获取转运列表失败：" + OpsData.getHead().getRet_info());
                        }
                    }
                });
    }
}
