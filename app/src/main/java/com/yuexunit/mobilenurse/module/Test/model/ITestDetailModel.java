package com.yuexunit.mobilenurse.module.Test.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestDetailModel {
    /**
     * 获取病人化验单详情
     */
    Observable<String> getTestDetailData(String requestno);
}
