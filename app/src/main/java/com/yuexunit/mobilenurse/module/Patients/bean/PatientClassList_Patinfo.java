package com.yuexunit.mobilenurse.module.Patients.bean;

/**
 * Created by work-jx on 2016/10/9.
 */
public class PatientClassList_Patinfo {

    /**
     * visitno : 453420
     * name : 林正友
     * sex : 1
     * age : 48
     * birth : 1968/3/2 0:00:00
     * carelevel : 2
     * carename : 二级
     * deptcode : 1003
     * deptname : 脑血管病介入
     * wardcode : 19
     * wardname : 内分泌病区
     * bedcode : 40028
     * totalfee : 14810.68
     * prepay : 14810.68
     * admissiondate : 2016-09-25
     * feetypecode : 8
     * feetypename : 仿医保
     * mdoctorid : 20
     * mdoctorname : 曹庆华
     */

    private String visitno;
    private String name;
    private String sex;
    private int age;
    private String birth;
    private String carelevel;
    private String carename;
    private String deptcode;
    private String deptname;
    private String wardcode;
    private String wardname;
    private String bedcode;
    private String totalfee;
    private String prepay;
    private String admissiondate;
    private String feetypecode;
    private String feetypename;
    private String mdoctorid;
    private String mdoctorname;

    public void setVisitno(String visitno) {
        this.visitno = visitno;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public void setCarelevel(String carelevel) {
        this.carelevel = carelevel;
    }

    public void setCarename(String carename) {
        this.carename = carename;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public void setWardcode(String wardcode) {
        this.wardcode = wardcode;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public void setTotalfee(String totalfee) {
        this.totalfee = totalfee;
    }

    public void setPrepay(String prepay) {
        this.prepay = prepay;
    }

    public void setAdmissiondate(String admissiondate) {
        this.admissiondate = admissiondate;
    }

    public void setFeetypecode(String feetypecode) {
        this.feetypecode = feetypecode;
    }

    public void setFeetypename(String feetypename) {
        this.feetypename = feetypename;
    }

    public void setMdoctorid(String mdoctorid) {
        this.mdoctorid = mdoctorid;
    }

    public void setMdoctorname(String mdoctorname) {
        this.mdoctorname = mdoctorname;
    }

    public String getVisitno() {
        return visitno;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public String getBirth() {
        return birth;
    }

    public String getCarelevel() {
        return carelevel;
    }

    public String getCarename() {
        return carename;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public String getWardcode() {
        return wardcode;
    }

    public String getWardname() {
        return wardname;
    }

    public String getBedcode() {
        return bedcode;
    }

    public String getTotalfee() {
        return totalfee;
    }

    public String getPrepay() {
        return prepay;
    }

    public String getAdmissiondate() {
        return admissiondate;
    }

    public String getFeetypecode() {
        return feetypecode;
    }

    public String getFeetypename() {
        return feetypename;
    }

    public String getMdoctorid() {
        return mdoctorid;
    }

    public String getMdoctorname() {
        return mdoctorname;
    }
}
