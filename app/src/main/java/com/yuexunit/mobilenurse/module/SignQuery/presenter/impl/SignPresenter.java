package com.yuexunit.mobilenurse.module.SignQuery.presenter.impl;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignInputListBean;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;
import com.yuexunit.mobilenurse.module.SignQuery.model.ISignModel;
import com.yuexunit.mobilenurse.module.SignQuery.presenter.ISignPresenter;
import com.yuexunit.mobilenurse.module.SignQuery.ui.view.ISignQueryView;

import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by sslcjy on 16/2/25.
 */
public class SignPresenter implements ISignPresenter {

    private ISignModel model;
    private ISignQueryView view;
    private final Logger log = LoggerFactory.getLogger(SignPresenter.class);
    ArrayList<YXSignQuery.SignQueryEntity>  signQueryEntities = new ArrayList<YXSignQuery.SignQueryEntity>();

    public SignPresenter(ISignModel model,ISignQueryView view){
        this.model = model;
        this.view = view;
    }

    @Override
    public void showSignInfo(String inpNo,int interval,String date) {
        model.getShowSignInfo(inpNo,interval,date)
                .map(new Func1<YXSignQuery, ArrayList<YXSignQuery.SignQueryEntity>>() {
                    @Override
                    public ArrayList<YXSignQuery.SignQueryEntity> call(YXSignQuery yxSignQuery) {
                        return yxSignQuery.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<YXSignQuery.SignQueryEntity>>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_showSignInfo:onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error("SignPresenter_showSignInfo:onError:"+e);
                        Log.v("dyp","SignPresenter_showSignInfo:onError:"+e);

                    }

                    @Override
                    public void onNext(ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities) {
                        view.showSignInfo(signQueryEntities);
                    }
                });
    }

    @Override
    public void showSignInfoFive(String inpNo, String interval, String date) {
        model.getShowSignInfoFive(inpNo, interval, date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {

                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_showSignInfo:onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error("SignPresenter_showSignInfo:onError:"+e);
                        Log.v("jx","SignPresenter_showSignInfo:onError:"+e);
                        if (signQueryEntities != null) {
                            signQueryEntities.clear();
                        }
                        view.showSignInfo(signQueryEntities);
                    }

                    @Override
                    public void onNext(String result) {
                        YXSignInputListBean Total = JSON.parseObject(result, YXSignInputListBean.class);
                        YXSignInputListBean SignDate = Total.getBody();
                        if ("0".equals(SignDate.getHead().getRet_code())) {
                            if (signQueryEntities != null) {
                                signQueryEntities.clear();
                            }

                            List<YXSignQuery.SignQueryEntity> signQueryEntityList;

                            String signdates = SignDate.getResponse().getEnrlist();
                            String signdates_first = signdates.substring(0, 1);
                            if (!signdates_first.equals("[")) {
                                signdates = "[" + signdates + "]";
                                signQueryEntityList = JSON.parseArray(signdates, YXSignQuery.SignQueryEntity.class);
                            } else {
                                signQueryEntityList = JSON.parseArray(signdates, YXSignQuery.SignQueryEntity.class);
                            }

                            for (int i = 0; i < signQueryEntityList.size(); i++) {
                                YXSignQuery.SignQueryEntity signQueryEntity = new YXSignQuery.SignQueryEntity();
                                signQueryEntity = signQueryEntityList.get(i);
                                signQueryEntities.add(signQueryEntity);
                            }
                            view.showSignInfo(signQueryEntities);
                        }
                            else {
                                ViewInject.toast("暂无数据");
                                view.showSignInfo(signQueryEntities);
                            }

                    }
                });
    }
    }
