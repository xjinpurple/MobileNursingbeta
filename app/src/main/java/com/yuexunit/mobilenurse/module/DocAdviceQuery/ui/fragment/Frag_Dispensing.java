package com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter.DispensingListAdapter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Dispensing;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.model.impl.DispensingModel;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.IDispensingPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.impl.DispensingPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.Act_DocAdvice;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view.IDispensingView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.ui.KJFragment;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class Frag_Dispensing extends KJFragment implements IDispensingView {

    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;
    @Bind(R.id.dispensing_list)
    public ListView dispensing_list;
    private BaseAdapter adapter;
    Act_DocAdvice aty;
    Context context;
    //病人住院号
    private  String visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    LoadingDialog loadingDialog;

    IDispensingPresenter presenter;

    @Override
    protected View inflaterView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        aty = (Act_DocAdvice) getActivity();
        context = getActivity();
        View layout = View.inflate(aty, R.layout.frag_dispensing,null);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    protected void initWidget(View parentView) {
        super.initWidget(parentView);
        presenter = new DispensingPresenter(this,new DispensingModel());
        visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        mEmptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });

        if(SystemTool.checkNet(aty)) {
           presenter.showDispensingList(visitno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }

    @Override
    public void showDispensingList(ArrayList<DocAdvice_Dispensing> list) {
        adapter = new DispensingListAdapter(aty,list);
        dispensing_list.setAdapter(adapter);
        mEmptyLayout.dismiss();
    }
}
