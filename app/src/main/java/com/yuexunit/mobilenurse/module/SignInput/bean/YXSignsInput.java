package com.yuexunit.mobilenurse.module.SignInput.bean;

import java.util.ArrayList;

/**
 * Created by sslcjy on 15/12/22.
 *
 * 体征录取功能
 */
public class YXSignsInput {

    public int code;
    public String message;
    public ArrayList<SignsInput_Data> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SignsInput_Data> getData() {
        return data;
    }

    public void setData(ArrayList<SignsInput_Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "YXSignsInput{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
