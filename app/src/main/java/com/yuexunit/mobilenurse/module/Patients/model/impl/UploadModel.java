package com.yuexunit.mobilenurse.module.Patients.model.impl;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.Patients.api.PatientListApi;
import com.yuexunit.mobilenurse.module.Patients.model.IUploadModel;
import com.yuexunit.mobilenurse.util.ApiUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import rx.Observable;

/**
 * Created by work-jx on 2016/3/25.
 */
public class UploadModel implements IUploadModel{
    private PatientListApi api;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(UploadModel.class);

    @Override
    public Observable<ActionBean> uploadTypesData(Map<String, String> praise) {
        api = ApiInstance();
        return api.oneKeyUpload(praise);
    }

    public PatientListApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi(PatientListApi.class);
        }
    }
}
