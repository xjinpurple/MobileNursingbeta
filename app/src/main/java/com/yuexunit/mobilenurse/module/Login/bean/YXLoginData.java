package com.yuexunit.mobilenurse.module.Login.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/11/20.
 */

//登陆接口
public class YXLoginData {

    public YXLoginData body;

    //交易返回区(head)
    public Head head;
    //数据返回区(response)
    public Login_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Login_Response getResponse() {
        return response;
    }

    public void setResponse(Login_Response response) {
        this.response = response;
    }

    public  void setBody(YXLoginData body){
        this.body = body;
    }

    public YXLoginData getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "YXLoginData{" +
                "body=" + body +
                ", head=" + head +
                ", response=" + response +
                '}';
    }
}
