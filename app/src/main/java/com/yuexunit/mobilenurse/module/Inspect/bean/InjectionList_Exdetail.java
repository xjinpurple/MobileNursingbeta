package com.yuexunit.mobilenurse.module.Inspect.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class InjectionList_Exdetail {

    //申请单号
    public long requestno;
    //申请单序号
    public long requestseqno;

    public String itemcode;

    public String itemname;

    public long getRequestno() {
        return requestno;
    }

    public void setRequestno(long requestno) {
        this.requestno = requestno;
    }

    public long getRequestseqno() {
        return requestseqno;
    }

    public void setRequestseqno(long requestseqno) {
        this.requestseqno = requestseqno;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }
}
