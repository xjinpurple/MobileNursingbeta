package com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_OrderDetail;

import java.util.ArrayList;



/**
 * Created by jx on 2015/11/22.
 */
public class DispensingListListAdapter extends BaseAdapter{

    private Context context;
//    protected LayoutInflater mInflater;
    private ArrayList<DocAdvice_OrderDetail> mDatas;

    public DispensingListListAdapter(Context context,ArrayList<DocAdvice_OrderDetail> mDatas){
        this.context = context;
        this.mDatas = mDatas;
    }


    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.dispensinglist_itemlist, parent, false);
            holder = new ViewHolder();
            holder.dispensing_list_item_dosage = (TextView)convertView.findViewById(R.id.dispensing_list_item_dosage);
            holder.dispensing_list_item_name = (TextView)convertView.findViewById(R.id.dispensing_list_item_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

            holder.dispensing_list_item_name.setText(mDatas.get(position).getMedname() + " * " + mDatas.get(position).getNum());
            holder.dispensing_list_item_dosage.setText(mDatas.get(position).getDosage()+mDatas.get(position).getDoseusnit());

        return convertView;
    }

    class ViewHolder{
        TextView dispensing_list_item_name,dispensing_list_item_dosage;
    }

}
