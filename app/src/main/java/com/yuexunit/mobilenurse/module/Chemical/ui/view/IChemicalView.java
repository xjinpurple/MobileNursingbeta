package com.yuexunit.mobilenurse.module.Chemical.ui.view;

import com.yuexunit.mobilenurse.module.Chemical.bean.ChemicalDetailBean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface IChemicalView {
    void showChemicalDate(ArrayList<ChemicalDetailBean> list);

    void loadingDialogStatus(int status);

    void isExecSuccess(boolean issuccess);
}
