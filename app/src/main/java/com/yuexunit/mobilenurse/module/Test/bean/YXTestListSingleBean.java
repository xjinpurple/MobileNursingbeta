package com.yuexunit.mobilenurse.module.Test.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/22.
 */
public class YXTestListSingleBean {
    public YXTestListSingleBean body;
    public Head head;
    public TestList_Single_Response response;

    public YXTestListSingleBean getBody() {
        return body;
    }

    public void setBody(YXTestListSingleBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public TestList_Single_Response getResponse() {
        return response;
    }

    public void setResponse(TestList_Single_Response response) {
        this.response = response;
    }
}
