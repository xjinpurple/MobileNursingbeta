package com.yuexunit.mobilenurse.module.Panic.bean;

/**
 * Created by work-jx on 2016/6/15.
 */
public class PanicBean {
    //住院号
    public String visit_no;
    //病人姓名
    public String name;
    //病人年龄
    public int age;
    //病人性别
    public String sex;
    //床位号
    public String bedcode;
    //危急值
    public String panicinfo;

    public String getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(String visit_no) {
        this.visit_no = visit_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBedcode() {
        return bedcode;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public String getPanicinfo() {
        return panicinfo;
    }

    public void setPanicinfo(String panicinfo) {
        this.panicinfo = panicinfo;
    }
}
