package com.yuexunit.mobilenurse.module.Transportation.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/12/2.
 */
public interface IOperNoticeModel {
    Observable<String> getOpsNoticeDate(String requestno);
}
