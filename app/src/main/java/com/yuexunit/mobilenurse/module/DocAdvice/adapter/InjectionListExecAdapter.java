package com.yuexunit.mobilenurse.module.DocAdvice.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdvice.bean.DocAdvice_OrderExec;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2017/3/30.
 */
public class InjectionListExecAdapter extends KJAdapter<DocAdvice_OrderExec>{

    public InjectionListExecAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, DocAdvice_OrderExec docAdvice_orderExec, boolean b) {
        adapterHolder.setText(R.id.injection_list_exec_name,docAdvice_orderExec.getExec_empname());
        adapterHolder.setText(R.id.injection_list_exec_time,docAdvice_orderExec.getExectime());
    }
}
