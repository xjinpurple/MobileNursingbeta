package com.yuexunit.mobilenurse.module.SignInput.model;

import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignModel {
    Observable<ArrayList<Sign_Single>> getBaseTypes(String areaId);
    Observable<ArrayList<SignsInput_Data>> getAllTypes(String areaId);
    Observable<String> uploadTypesValue(Map<String, String> praise);
}
