package com.yuexunit.mobilenurse.module.Test.bean;

import java.util.ArrayList;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class TestList_Response {

    public ArrayList<TestList_Asreqlist>  asreqlist;

    public ArrayList<TestList_Asreqlist> getAsreqlist() {
        return asreqlist;
    }

    public void setAsreqlist(ArrayList<TestList_Asreqlist> asreqlist) {
        this.asreqlist = asreqlist;
    }
}
