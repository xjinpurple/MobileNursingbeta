package com.yuexunit.mobilenurse.module.Test.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestPresenter {
    /*
    化验单列表
     */
    public void showTestList(String visitno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
