package com.yuexunit.mobilenurse.module.SignInput.api;

import com.yuexunit.mobilenurse.base.bean.ActionBean;

import java.util.Map;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by work-jx on 2016/5/12.
 */
public interface AddSignApi {
    @FormUrlEncoded
    @POST("ydhl/api/patient/update")
    Observable<ActionBean> addsinglesign(@FieldMap Map<String, String> praise);
}
