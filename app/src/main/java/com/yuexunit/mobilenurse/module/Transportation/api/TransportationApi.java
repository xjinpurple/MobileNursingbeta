package com.yuexunit.mobilenurse.module.Transportation.api;

import com.yuexunit.mobilenurse.module.Transportation.bean.RfidBean;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by work-jx on 2016/10/14.
 */
public interface TransportationApi {
    @GET("ydhl/api/card/loadRfidMsg")
    Observable<RfidBean> getRfidInfo(@Query("rfid") String rfid);
}
