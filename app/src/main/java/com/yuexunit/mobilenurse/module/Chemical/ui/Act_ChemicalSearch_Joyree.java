package com.yuexunit.mobilenurse.module.Chemical.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.EditText;

import com.android.joyree.JoyreeScannerInterface;
import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/6/14.
 */
public class Act_ChemicalSearch_Joyree extends KJActivity {
    @Bind(R.id.chemicalsearch_edt)
    EditText chemicalsearchEdt;

    private IntentFilter mBarcodeFilter = null;

    public static final String BARCODE_DATA_ACTION = "android.intent.action.BARCODEDATA";
    public static final String BARCODE_RESULT = "barcode_result";

    public JoyreeScannerInterface mScanner = null;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_chemiaclsearch);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            unregisterReceiver(mBarcodeReceiver);
        } catch (IllegalArgumentException e) {

        }

        mScanner = null;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(mBarcodeReceiver);
        } catch (IllegalArgumentException e) {

        }

        mScanner = null;
    }

    @OnClick({R.id.chemicalsearch_img_back, R.id.chemicalsearch_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chemicalsearch_img_back:
                finish();
                break;
            case R.id.chemicalsearch_btn:
                if(chemicalsearchEdt.getText().toString().length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE))) {
                    String type_tag = chemicalsearchEdt.getText().toString().substring(8, 9);
                    if (type_tag.equals(AppConfig.TAG_INJECTION)) {
                        Intent intent_new = new Intent();
                        intent_new.setClass(aty, Act_ChemicalDetail.class);
                        intent_new.putExtra("barcode", chemicalsearchEdt.getText().toString());
//                    intent_new.putExtra("barcode", "20160628100176");
                        startActivity(intent_new);
                        chemicalsearchEdt.setText("");
                    } else {
                        ViewInject.toast("非化药条码");
                    }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
                break;
        }
    }

    private BroadcastReceiver mBarcodeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            if (intent != null && intent.getStringExtra(BARCODE_RESULT) != null) {
                String mBarcodeData = intent.getStringExtra(BARCODE_RESULT);
                if (!mBarcodeData.equals("")) {
                    if (mBarcodeData.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE)))
                    {
                        String type_tag = mBarcodeData.substring(8, 9);
                        if(type_tag.equals(AppConfig.TAG_INJECTION)) {
                            Intent intent_new = new Intent();
                            intent_new.setClass(aty, Act_ChemicalDetail.class);
                            intent_new.putExtra("barcode", mBarcodeData);
                            startActivity(intent_new);
                            chemicalsearchEdt.setText("");
                        }
                        else {
                            ViewInject.toast("非化药条码");
                        }
                    }
                    else{
                        ViewInject.toast("非化药条码");
                    }
                }
            }
        }

    };

    private void Register_Receiver()
    {
        mScanner = new JoyreeScannerInterface(getApplication());
        mScanner.enableBeep(true);
        mScanner.enableVibrate(true);
        mScanner.setOutputMode(0);

        mBarcodeFilter = new IntentFilter();
        mBarcodeFilter.addAction(BARCODE_DATA_ACTION);

        registerReceiver(mBarcodeReceiver, mBarcodeFilter);
    }

}
