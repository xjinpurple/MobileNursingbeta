package com.yuexunit.mobilenurse.module.BloodCollection.ui.view;

import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;

/**
 * Created by work-jx on 2017/5/5.
 */
public interface IBloodCollectionView {
    void collectSend(Boolean isSuccess, Collectinfo collectinfo);
}
