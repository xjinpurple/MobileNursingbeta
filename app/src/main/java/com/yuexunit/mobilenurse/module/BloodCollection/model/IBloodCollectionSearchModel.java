package com.yuexunit.mobilenurse.module.BloodCollection.model;

import rx.Observable;

/**
 * Created by work-jx on 2017/5/4.
 */
public interface IBloodCollectionSearchModel {
    Observable<String> getBloodSearchListData(String wardId);
}
