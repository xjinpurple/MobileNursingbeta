package com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view;


import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Injection;

import java.util.ArrayList;



/**
 * Created by work-jx on 2016/2/3.
 */
public interface IInjectionView {
    /**
     * 医嘱列表列表
     */
    void showInjectionList(ArrayList<DocAdvice_Injection> list);
}
