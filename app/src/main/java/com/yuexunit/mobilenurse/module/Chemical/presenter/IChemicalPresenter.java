package com.yuexunit.mobilenurse.module.Chemical.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface IChemicalPresenter {
    /*
   化药列表
   */
    public void showChemicalDate(String barcode);

    /*
   执行化药
   */
    public void execChemicalDate(String barcode,String nurseid);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
