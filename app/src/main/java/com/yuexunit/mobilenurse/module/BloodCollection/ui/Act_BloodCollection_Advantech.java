package com.yuexunit.mobilenurse.module.BloodCollection.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.adapter.BloodCollectionAdapter;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl.BloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionView;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/5/8.
 */
public class Act_BloodCollection_Advantech extends KJActivity implements IBloodCollectionView {

    @Bind(R.id.bloodcollection_list)
    ListView bloodcollectionList;

    BaseAdapter adapter;
    private LoadingDialog dialog;

    ArrayList<Collectinfo> collectinfos = new ArrayList<Collectinfo>();

    IBloodCollectionPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_bloodcollection);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        dialog = new LoadingDialog(aty);
        presenter = new BloodCollectionPresenter(new BloodCollectionModel(),this);
        adapter = new BloodCollectionAdapter(bloodcollectionList, collectinfos, R.layout.item_bloodcollection);
        bloodcollectionList.setAdapter(adapter);
    }

    @OnClick({R.id.bloodcollection_img_back, R.id.bloodcollection_img_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bloodcollection_img_back:
                finish();
                break;
            case R.id.bloodcollection_img_search:
                Intent intent = new Intent(aty,Act_BloodCollection_Serach.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Intent sendIntent = new Intent("df.scanservice.toapp");
        sendBroadcast(sendIntent);

        IntentFilter ItFilter = new IntentFilter();
        ItFilter.addAction("df.scanservice.result");
        this.registerReceiver(myReceiver, ItFilter);
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            this.unregisterReceiver(myReceiver);
//            Intent sendIntent = new Intent("df.scanservice.cancelapp");
//            sendBroadcast(sendIntent);
        } catch (IllegalArgumentException e) {

        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            this.unregisterReceiver(myReceiver);
//            Intent sendIntent = new Intent("df.scanservice.cancelapp");
//            sendBroadcast(sendIntent);
        } catch (IllegalArgumentException e) {

        }
    }
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            if (arg1.getAction().equalsIgnoreCase("df.scanservice.result")) {
                String codes = arg1.getStringExtra("result");
                codes.trim();
                if (!codes.equals("")) {
                    if(codes.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_CX_RULE))) {
                        dialog.show();
                        presenter.collect("", codes, PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID));
                    }
                    else {
                        Toast.makeText(aty, "条码无效!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    @Override
    public void collectSend(Boolean isSuccess ,Collectinfo collectinfo) {
        if(isSuccess)
        {
            collectinfos.add(new Collectinfo());
            adapter.notifyDataSetChanged();
        }
        else {
            ViewInject.toast("发送失败");
        }
        dialog.dismiss();
    }
}
