package com.yuexunit.mobilenurse.module.HealthArticles.model;

import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/25.
 */
public interface IArticleModel {

    /**
     * 获取健康宣教列表
     */
    Observable<ArrayList<ArticleTitle>> getArticleList(String areaId);

    /**
     * 获取健康宣教某篇文章
     */
    Observable<String> getArticleContent(int articleId);

}
