package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/14.
 */
public class Collectinfo {

    //执行条码
    private String barcode;
    //采集医嘱名称
    private String ordername;
    //执行人员
    private  int execempid;
    //执行时间
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date exectime;
    //标本名称
    private String specname;
    //标本状态
    private int state;
    //执行人姓名
    private String execempname;
    //发送人员
    private int sendempid;
    //发送时间
    private String sendtime;
    //发送人员姓名
    private String sendempname;
    //病人姓名
    private String reg_name;


    public String getSpecname() {
        return specname;
    }

    public void setSpecname(String specname) {
        this.specname = specname;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getExecempname() {
        return execempname;
    }

    public void setExecempname(String execempname) {
        this.execempname = execempname;
    }

    public int getSendempid() {
        return sendempid;
    }

    public void setSendempid(int sendempid) {
        this.sendempid = sendempid;
    }

    public String getSendempname() {
        return sendempname;
    }

    public void setSendempname(String sendempname) {
        this.sendempname = sendempname;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public int getExecempid() {
        return execempid;
    }

    public void setExecempid(int execempid) {
        this.execempid = execempid;
    }

    public Date getExectime() {
        return exectime;
    }

    public void setExectime(Date exectime) {
        this.exectime = exectime;
    }

    public String getSendtime() {
        return sendtime;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public String getReg_name() {
        return reg_name;
    }

    public void setReg_name(String reg_name) {
        this.reg_name = reg_name;
    }
}
