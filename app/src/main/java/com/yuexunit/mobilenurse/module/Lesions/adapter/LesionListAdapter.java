package com.yuexunit.mobilenurse.module.Lesions.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Lesions.ui.Act_LesionList;
import com.yuexunit.mobilenurse.module.Login.bean.Login_Bq;
import com.yuexunit.mobilenurse.module.Patients.ui.Act_Main_Normal;

import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class LesionListAdapter extends KJAdapter<Login_Bq> {
    private Act_LesionList aty;
    public LesionListAdapter(AbsListView view, Collection mDatas, int itemLayoutId,Act_LesionList aty) {
        super(view, mDatas, itemLayoutId);
        this.aty = aty;
    }

    @Override
    public void convert(AdapterHolder adapterHolder, final Login_Bq Bq, boolean isScrolling) {
        adapterHolder.setText(R.id.bq_name, Bq.getWardname());
        onPicClick(adapterHolder.getConvertView(), Bq.getWardno(), Bq.getWardname());
    }

    private void onPicClick(View view, final String Wardno, final String WardnoName) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceHelper.write(mCxt, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_ID, Wardno);
                PreferenceHelper.write(mCxt, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_NAME, WardnoName);
                //Intent intent = new Intent(mCxt,Act_Main_Moto.class);
//                Intent intent = new Intent(mCxt, Act_Main_HoneyWell.class);
                Intent intent = new Intent(mCxt, Act_Main_Normal.class);
//                Intent intent = new Intent(mCxt, Act_Main_Advantech.class);
//                Intent intent = new Intent(mCxt, Act_Main_Lachsis.class);
//                Intent intent = new Intent(mCxt, Act_Main_BayNexus.class);
//                Intent intent = new Intent(mCxt, Act_Main_M80.class);
//                Intent intent = new Intent(aty, Act_Main_Joyree.class);
                mCxt.startActivity(intent);

            }
        });
    }
}
