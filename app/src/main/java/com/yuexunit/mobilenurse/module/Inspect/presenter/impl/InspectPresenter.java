package com.yuexunit.mobilenurse.module.Inspect.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exdetail;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exreqlist;
import com.yuexunit.mobilenurse.module.Inspect.bean.YXInjectionListBean;
import com.yuexunit.mobilenurse.module.Inspect.model.IInspectModel;
import com.yuexunit.mobilenurse.module.Inspect.presenter.IInspectPresenter;
import com.yuexunit.mobilenurse.module.Inspect.ui.view.IInspectView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/29.
 */
public class InspectPresenter implements IInspectPresenter{

    private IInspectView view;
    private IInspectModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    ArrayList<InjectionList_Exreqlist> injectlist  = new ArrayList<InjectionList_Exreqlist>();

    public InspectPresenter(IInspectView view,IInspectModel model)
    {
        this.view = view;
        this.model = model;
    }


    @Override
    public void showInspectList(String visitno) {
        compositeSubscription.add(
                model.getInspectListData(visitno)
                      .subscribeOn(Schedulers.io())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Subscriber<String>() {
                                     @Override
                                     public void onCompleted() {

                                     }

                                     @Override
                                     public void onError(Throwable e) {
                                         ViewInject.toast("暂无数据");
                                         view.showInspectList(injectlist);
                                     }

                                     @Override
                                     public void onNext(String result) {

                                         YXInjectionListBean Total = JSON.parseObject(result, YXInjectionListBean.class);
                                         YXInjectionListBean InspectData = Total.getBody();

                                         if ("0".equals(InspectData.getHead().getRet_code())) {

                                             if (injectlist != null) {
                                                 injectlist.clear();
                                             }

                                             List<InjectionList_Exdetail> injectionList_exdetails;
                                             List<InjectionList_Exreqlist> injectionList_exreqlists;

                                             String exreqlists = InspectData.getResponse().getExreqlist();
                                             String exreqlists_first = exreqlists.substring(0, 1);
                                             if (!exreqlists_first.equals("[")) {
                                                 exreqlists = "[" + exreqlists + "]";
                                                 injectionList_exreqlists = JSON.parseArray(exreqlists, InjectionList_Exreqlist.class);
                                             } else {
                                                 injectionList_exreqlists = JSON.parseArray(exreqlists, InjectionList_Exreqlist.class);
                                             }

                                             String exdetails = InspectData.getResponse().getExreqdetail();
                                             String exdetails_first = exdetails.substring(0, 1);
                                             if (!exdetails_first.equals("[")) {
                                                 exdetails = "[" + exdetails + "]";
                                                 injectionList_exdetails = JSON.parseArray(exdetails, InjectionList_Exdetail.class);
                                             } else {
                                                 injectionList_exdetails = JSON.parseArray(exdetails, InjectionList_Exdetail.class);
                                             }

                                             for (int i = 0; i < injectionList_exreqlists.size(); i++) {

                                                 InjectionList_Exreqlist injectionList_exreqlist = new InjectionList_Exreqlist();
                                                 injectionList_exreqlist = injectionList_exreqlists.get(i);

                                                 String name = new String(injectionList_exreqlists.get(i).getClassname());
                                                 for (int j = 0; j < injectionList_exdetails.size(); j++) {
                                                     if (injectionList_exreqlists.get(i).getRequestno() == injectionList_exdetails.get(j).getRequestno()) {
                                                         name = name + " " + injectionList_exdetails.get(j).getItemname();
                                                     }
                                                 }
                                                 injectionList_exreqlist.setClassname(name);
                                                 injectlist.add(injectionList_exreqlist);
                                             }
                                             view.showInspectList(injectlist);
                                         } else {
                                             ViewInject.toast("暂无数据");
                                             view.showInspectList(injectlist);
                                         }

                                     }
                                 }
                      )
        );

    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
