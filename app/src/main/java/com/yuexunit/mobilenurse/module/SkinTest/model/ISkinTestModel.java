package com.yuexunit.mobilenurse.module.SkinTest.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface ISkinTestModel {
    /**
     * 获取病人皮试信息
     */
    Observable<String> getSkinTestDate(String visitno);

    /**
     * 获取皮试药品批次
     */
    Observable<String> getMedicineDate(String ypid);

    /**
     * 执行皮试
     */
    Observable<String> execSkinTest(String sqdh,String czry,String kssj,String pssc,String pcnm,String psph);
}
