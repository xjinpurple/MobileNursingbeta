package com.yuexunit.mobilenurse.module.SignInput.bean;

import java.io.Serializable;

/**
 * Created by work-jx on 2016/5/11.
 */
public class Sign_Single_Check implements Serializable {
    private String code;

    private String name;

    private String unit;

    private Boolean ischeck;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getIscheck() {
        return ischeck;
    }

    public void setIscheck(Boolean ischeck) {
        this.ischeck = ischeck;
    }
}
