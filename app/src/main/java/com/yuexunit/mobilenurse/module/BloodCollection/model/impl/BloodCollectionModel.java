package com.yuexunit.mobilenurse.module.BloodCollection.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.model.IBloodCollectionModel;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by work-jx on 2017/5/5.
 */
public class BloodCollectionModel implements IBloodCollectionModel{
    //记录Log
    private final Logger log = ProUtil.getLogger(BloodCollectionModel.class);

    @Override
    public Observable collect(final String Visitno,final String Code,final String NurseId) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD;
                    String url = AppConfig.WEB_CONTENT_EXEC;
                    SoapObject request = new SoapObject(nameSpace, methodName);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                    String str = formatter.format(curDate);
                    request.addProperty("XmlString", CreateJson.Ascollect_Json(Visitno, Code,
                            Integer.valueOf(NurseId), str, AppConfig.EXECTYPE_SEND));

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true; //very important for compatibility
                    HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                    Object response = null;
                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();
                        //记录传输到服务器及传回的数据
                        log.info(CreateJson.LOG_JSON(url + ",mhs_as_collect", "2",
                                Visitno + "," + Code + "," + Integer.valueOf(NurseId) + "," + str + "," + AppConfig.EXECTYPE_SEND
                                , response.toString()));
                        isConnect = false;
                        if (response == null) {
                            subscriber.onError(new Exception());
                        }
                    } catch (Exception e) {
                        count++;
                        log.error("Exception:", e.toString());
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
                subscriber.onCompleted();
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }
}
