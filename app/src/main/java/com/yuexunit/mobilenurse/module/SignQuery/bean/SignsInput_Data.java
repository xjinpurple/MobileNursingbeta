package com.yuexunit.mobilenurse.module.SignQuery.bean;

import java.io.Serializable;

/**
 * Created by sslcjy on 15/12/22.
 */
public class SignsInput_Data implements Serializable {

    public int sign_id;
    public String code;
    public String name;
    public String unit;
    public String range;
    public String voice_code;
    public int rank;

    public int getSign_id() {
        return sign_id;
    }

    public void setSign_id(int sign_id) {
        this.sign_id = sign_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getVoice_code() {
        return voice_code;
    }

    public void setVoice_code(String voice_code) {
        this.voice_code = voice_code;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "SignsInput_Data{" +
                "sign_id=" + sign_id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", range='" + range + '\'' +
                ", voice_code='" + voice_code + '\'' +
                ", rank=" + rank +
                '}';
    }
}
