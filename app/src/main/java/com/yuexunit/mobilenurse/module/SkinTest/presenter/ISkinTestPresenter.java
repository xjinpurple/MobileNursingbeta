package com.yuexunit.mobilenurse.module.SkinTest.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface ISkinTestPresenter {
    /*
      皮试列表
     */
    public void showSkinTestDate(String visitno);

    /*
    皮试列表
     */
    public void getMedicineDate(String ypid);

      /*
      皮试执行
       */
     public void execSkinTestDate(String sqdh,String czry,String kssj,String pssc,String pcnm,String psph);


    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
