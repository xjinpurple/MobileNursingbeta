package com.yuexunit.mobilenurse.module.SignInput.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.AppContext;
import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.SignInput.adapter.AddTypeAdapter;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single_Check;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;
import com.yuexunit.mobilenurse.module.SignInput.model.impl.AddSignModel;
import com.yuexunit.mobilenurse.module.SignInput.model.impl.SignModel;
import com.yuexunit.mobilenurse.module.SignInput.presenter.impl.SignPresenter;
import com.yuexunit.mobilenurse.module.SignInput.ui.view.IAddSignType;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sslcjy on 16/1/27.
 */
public class Act_AddType extends KJActivity implements IAddSignType{

    @Bind(R.id.add_type)
    ListView addList;

    public ArrayList<SignsInput_Data> types = new ArrayList<SignsInput_Data>();
    public ArrayList<Sign_Single> singletypes = new ArrayList<Sign_Single>();
    public ArrayList<Sign_Single_Check> typelist = new ArrayList<Sign_Single_Check>();
    public AddTypeAdapter addTypeAdapter;
    private SignPresenter presenter,presenter_new;

    private String visitno,Wardno;

    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_AddType.class);

    @Override
    public void setRootView() {
        setContentView(R.layout.act_addtype);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new SignPresenter(new SignModel(),this);
        presenter_new = new SignPresenter(new AddSignModel(),this);
        visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        Wardno = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_ID);
        initAllTypes();
    }

    @Override
    public void initWidget() {
        super.initWidget();
    }

    @OnClick(R.id.left_text)
    public void out(){
        finish();
    }

    @OnClick(R.id.right_text)
    public void complete(){
        if(addTypeAdapter != null){
            ArrayList<Sign_Single_Check> addtypes = addTypeAdapter.getArrayList();
            String codelist ="";
            for(int i = 0;i<addtypes.size();i++)
            {
                if(addtypes.get(i).getIscheck())
                codelist += addtypes.get(i).getCode()+",";
            }
            if(codelist.length() > 0)
            codelist = codelist.substring(0,codelist.length()-1);

            Map<String, String> params = new HashMap<>();
            params.put("patientId",visitno);
            params.put("signs",codelist);
            presenter_new.SaveSingleSign(params);
        }
        else
        {
            Map<String, String> params = new HashMap<>();
            params.put("patientId",visitno);
            params.put("signs","");
            presenter_new.SaveSingleSign(params);
        }
    }

    @Override
    public void showAllType(ArrayList<SignsInput_Data> baselist) {
        for(int i = 0;i<baselist.size();i++)
        {
            Boolean issave =false;
            Sign_Single_Check sign_single_check = new Sign_Single_Check();
            for(int j = 0;j<singletypes.size();j++)
            {
                if(baselist.get(i).getCode().equals(singletypes.get(j).getCode()))
                {
                    sign_single_check.setCode(baselist.get(i).getCode());
                    sign_single_check.setName(baselist.get(i).getName());
                    sign_single_check.setUnit(baselist.get(i).getUnit());
                    sign_single_check.setIscheck(true);
                    issave =true;
                }
            }
            if (issave)
            {
                sign_single_check.setCode(baselist.get(i).getCode());
                sign_single_check.setName(baselist.get(i).getName());
                sign_single_check.setUnit(baselist.get(i).getUnit());
                sign_single_check.setIscheck(false);
            }

            typelist.add(sign_single_check);
        }

        addTypeAdapter = new AddTypeAdapter(addList, typelist, R.layout.item_add_type);
        addList.setAdapter(addTypeAdapter);
    }

    @Override
    public void isSave(ActionBean actionBean) {
        if(actionBean.getCode() == 200)
        {
            AddTypeForBase();
        }
        else
        {
            ViewInject.toast("保存体征失败");
        }
    }

    public void AddTypeForBase() {
        Bundle bundle = new Bundle();
        ArrayList lists = new ArrayList();
        Intent intent = new Intent();
        if(addTypeAdapter != null){
            ArrayList<Sign_Single_Check> addtypes = addTypeAdapter.getArrayList();
            lists.add(addtypes);
            bundle.putParcelableArrayList("addtypelist", lists);
            intent.putExtras(bundle);
        }
        setResult(RESULT_OK, intent);
        finish();
    }

    private void initAllTypes() {
        types = ((AppContext) getApplication()).getAllTypes();
        singletypes = ((AppContext) getApplication()).getSingleTypes();
        if (types.size() > 0) {
            for(int i = 0;i<types.size();i++)
            {
                Boolean issave =true;
                Sign_Single_Check sign_single_check = new Sign_Single_Check();
                for(int j = 0;j<singletypes.size();j++)
                {
                    if(types.get(i).getCode().equals(singletypes.get(j).getCode()))
                    {
                        sign_single_check.setCode(types.get(i).getCode());
                        sign_single_check.setName(types.get(i).getName());
                        sign_single_check.setUnit(types.get(i).getUnit());
                        sign_single_check.setIscheck(true);
                        issave =false;
                    }
                }
                if (issave)
                {
                    sign_single_check.setCode(types.get(i).getCode());
                    sign_single_check.setName(types.get(i).getName());
                    sign_single_check.setUnit(types.get(i).getUnit());
                    sign_single_check.setIscheck(false);
                }

                typelist.add(sign_single_check);
            }
            addTypeAdapter = new AddTypeAdapter(addList, typelist, R.layout.item_add_type);
            addList.setAdapter(addTypeAdapter);
        } else {
            presenter.showAllTypes(Wardno);
        }
    }
}
