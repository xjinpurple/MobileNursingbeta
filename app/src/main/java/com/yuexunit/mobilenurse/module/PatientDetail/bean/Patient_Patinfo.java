package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by 姚平 on 2015/12/17.
 */
public class Patient_Patinfo {


    //住院号
    public String visit_no;
    //病人姓名
    public String name;
    //病人性别
    public String sex;
    //病人年龄
    public int age;
    //病人出生日期
    @JSONField(format = "yyyy-MM-dd")
    public Date birth;
    //护理等级代码
    public String gradecode;
    //护理等级
    public String gradename;
    //病人科室
    public String deptcode;
    //病人科室名称
    public String deptname;
    //病人病区
    public String wardcode;
    //病区名称
    public String wardname;
    //床位号
    public String bedcode;
    //入院诊断
    public String diagnosis;
    //费用总额
    public double totalfee;
    //预付款金额
    public double prepay;
    //入院日期
    public String admissiondate;
    //费别性质代码
    public String feetypecode;
    //费别名称
    public String feetypename;
    //主治医生
    public int mdoctorid;
    //主治医生姓名
    public String mdoctorname;
    //住院
    public String ops_time;

    public String getFeetypecode() {
        return feetypecode;
    }

    public void setFeetypecode(String feetypecode) {
        this.feetypecode = feetypecode;
    }

    public String getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(String visit_no) {
        this.visit_no = visit_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGradecode() {
        return gradecode;
    }

    public void setGradecode(String gradecode) {
        this.gradecode = gradecode;
    }

    public String getGradename() {
        return gradename;
    }

    public void setGradename(String gradename) {
        this.gradename = gradename;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getWardcode() {
        return wardcode;
    }

    public void setWardcode(String wardcode) {
        this.wardcode = wardcode;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }

    public String getBedcode() {
        return bedcode;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public double getTotalfee() {
        return totalfee;
    }

    public void setTotalfee(double totalfee) {
        this.totalfee = totalfee;
    }

    public double getPrepay() {
        return prepay;
    }

    public void setPrepay(double prepay) {
        this.prepay = prepay;
    }

    public String getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(String admissiondate) {
        this.admissiondate = admissiondate;
    }

    public String getFeetypename() {
        return feetypename;
    }

    public void setFeetypename(String feetypename) {
        this.feetypename = feetypename;
    }

    public int getMdoctorid() {
        return mdoctorid;
    }

    public void setMdoctorid(int mdoctorid) {
        this.mdoctorid = mdoctorid;
    }

    public String getMdoctorname() {
        return mdoctorname;
    }

    public void setMdoctorname(String mdoctorname) {
        this.mdoctorname = mdoctorname;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getOps_time() {
        return ops_time;
    }

    public void setOps_time(String ops_time) {
        this.ops_time = ops_time;
    }
}
