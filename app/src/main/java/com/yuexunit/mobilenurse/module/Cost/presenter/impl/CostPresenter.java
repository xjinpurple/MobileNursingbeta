package com.yuexunit.mobilenurse.module.Cost.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Cost.bean.CostDetail_Patfeelist;
import com.yuexunit.mobilenurse.module.Cost.bean.YXCostDetailBean;
import com.yuexunit.mobilenurse.module.Cost.bean.YXCostDetailSingleBean;
import com.yuexunit.mobilenurse.module.Cost.model.ICostModel;
import com.yuexunit.mobilenurse.module.Cost.model.impl.CostModel;
import com.yuexunit.mobilenurse.module.Cost.presenter.ICostPresenter;
import com.yuexunit.mobilenurse.module.Cost.ui.Act_Cost;
import com.yuexunit.mobilenurse.module.Cost.ui.view.ICostView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/26.
 */
public class CostPresenter implements ICostPresenter {
    //记录Log
    private final Logger log = LoggerFactory.getLogger(CostModel.class);

    private ICostView view;
    private ICostModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    //费用列表
    ArrayList<CostDetail_Patfeelist> costlist = new ArrayList<CostDetail_Patfeelist>();

    public CostPresenter(ICostView view, ICostModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showCostList(String visitno, Calendar calendar) {
        compositeSubscription.add(
                model.getCostListData(visitno, calendar)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                                       @Override
                                       public void onCompleted() {

                                       }

                                       @Override
                                       public void onError(Throwable e) {
                                           log.error("Exception:" + e);
                                           ViewInject.toast("暂无数据");
                                           view.showCostList(costlist);
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);
                                       }

                                       @Override
                                       public void onNext(String result) {
                                           double today = 0;
                                           java.text.DecimalFormat today_type = new java.text.DecimalFormat("#.##");
                                           try {
                                               YXCostDetailSingleBean Total = JSON.parseObject(result, YXCostDetailSingleBean.class);
                                               YXCostDetailSingleBean CostData = Total.getBody();
                                               if ("0".equals(CostData.getHead().getRet_code())) {
                                                   if (costlist != null) {
                                                       costlist.clear();
                                                   }
                                                   costlist.add(CostData.getResponse().getPatfeelist());

                                                   for (int i = 0; i < costlist.size(); i++) {
                                                       today += (costlist.get(i).getFeeamount());
                                                   }
                                               }
                                           } catch (Exception e) {
                                               YXCostDetailBean Total = JSON.parseObject(result, YXCostDetailBean.class);
                                               YXCostDetailBean CostData = Total.getBody();
                                               if ("0".equals(CostData.getHead().getRet_code())) {
                                                   if (costlist != null) {
                                                       costlist.clear();
                                                   }
                                                   costlist = CostData.getResponse().getPatfeelist();

                                                   for (int i = 0; i < costlist.size(); i++) {
                                                       today += (costlist.get(i).getFeeamount());
                                                   }
                                               }
                                           }

                                           if (costlist != null) {
                                               view.showCostList(costlist);
                                               view.showCostToday(today_type.format(today));
                                           }
                                           view.dialogState(Act_Cost.DISMISS_DIALOG);
                                       }
                                   }
                        )
        );

    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
