package com.yuexunit.mobilenurse.module.PatientDetail.model;

import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/24.
 */
public interface IPatientDetailModel {

    /**
     * 查询病人详情
     */
    Observable getPatientDetail(String Visitno);

    /**
     * 执行采血操作
     */
    Observable collect(String Visitno,String Code,String NurseId);

    //获取基本体征项
    public Observable<ArrayList<Sign_Single>> getSingleTypes(String visitno);
}
