package com.yuexunit.mobilenurse.module.Chemical.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface IChemicalModel {
    /**
     * 获取病人化药信息
     */
    Observable<String> getChemicalDate(String barcode);

    /**
     * 执行化药
     */
    Observable<String> execChemicalDate(String barcode,String nurseid);
}
