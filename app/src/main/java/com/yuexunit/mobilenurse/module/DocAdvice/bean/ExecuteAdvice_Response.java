package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class ExecuteAdvice_Response {
    //执行条码
    public String barcode;
    //执行日期
    @JSONField(format = "yyyy-MM-dd")
    public Date execdate;
    //执行次数
    public int execseq;
    //执行人员
    public String execempid;
    //执行时间
    @JSONField(format = "yyyy-MM-dd")
    public Date exectime;
    //住院号
    public int visit_no;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Date getExecdate() {
        return execdate;
    }

    public void setExecdate(Date execdate) {
        this.execdate = execdate;
    }

    public int getExecseq() {
        return execseq;
    }

    public void setExecseq(int execseq) {
        this.execseq = execseq;
    }

    public String getExecempid() {
        return execempid;
    }

    public void setExecempid(String execempid) {
        this.execempid = execempid;
    }

    public Date getExectime() {
        return exectime;
    }

    public void setExectime(Date exectime) {
        this.exectime = exectime;
    }

    public int getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(int visit_no) {
        this.visit_no = visit_no;
    }
}
