package com.yuexunit.mobilenurse.module.Login.bean;

import java.io.Serializable;

/**
 * Created by 姚平 on 2015/12/8.
 */
//登陆人操作病区列表（键名）
public class Login_Bq implements Serializable {
    //病区号
    public String wardno;
    //病区名称
    public String wardname;

    public String getWardno() {
        return wardno;
    }

    public void setWardno(String wardno) {
        this.wardno = wardno;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }
}
