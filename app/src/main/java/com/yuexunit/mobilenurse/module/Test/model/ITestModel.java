package com.yuexunit.mobilenurse.module.Test.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestModel {
    /**
     * 获取病人化验单
     */
    Observable<String> getTestListData(String visitno);
}
