package com.yuexunit.mobilenurse.module.Login.bean;

/**
 * Created by work-jx on 2015/12/19.
 */
public class SystemConfig {
    private String jci;
    private String byj;
    private String wd_rule;
    private String cx_rule;
    private String ptsm;
    private String ptsm_rule;
    private String wd;
    private String enabled;
    private String byj_rule;

    public String getJci() {
        return jci;
    }

    public void setJci(String jci) {
        this.jci = jci;
    }

    public String getByj() {
        return byj;
    }

    public void setByj(String byj) {
        this.byj = byj;
    }

    public String getWd_rule() {
        return wd_rule;
    }

    public void setWd_rule(String wd_rule) {
        this.wd_rule = wd_rule;
    }

    public String getCx_rule() {
        return cx_rule;
    }

    public void setCx_rule(String cx_rule) {
        this.cx_rule = cx_rule;
    }

    public String getPtsm() {
        return ptsm;
    }

    public void setPtsm(String ptsm) {
        this.ptsm = ptsm;
    }

    public String getPtsm_rule() {
        return ptsm_rule;
    }

    public void setPtsm_rule(String ptsm_rule) {
        this.ptsm_rule = ptsm_rule;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getByj_rule() {
        return byj_rule;
    }

    public void setByj_rule(String byj_rule) {
        this.byj_rule = byj_rule;
    }

    @Override
    public String toString() {
        return "SystemConfig{" +
                "jci='" + jci + '\'' +
                ", byj='" + byj + '\'' +
                ", wd_rule='" + wd_rule + '\'' +
                ", cx_rule='" + cx_rule + '\'' +
                ", ptsm='" + ptsm + '\'' +
                ", ptsm_rule='" + ptsm_rule + '\'' +
                ", wd='" + wd + '\'' +
                ", enabled='" + enabled + '\'' +
                ", byj_rule='" + byj_rule + '\'' +
                '}';
    }
}
