package com.yuexunit.mobilenurse.module.DocAdviceQuery.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.fragment.Frag_Dispensing;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.fragment.Frag_Injection;

import org.kymjs.kjframe.ui.KJFragment;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Act_DocAdvice extends TitleBar_DocAdvice {

    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;
    @Bind(R.id.doc_injection_tv)
    public TextView doc_injection_tv;
    @Bind(R.id.doc_dispensing_tv)
    public TextView doc_dispensing_tv;
    @Bind(R.id.doc_dispensing_img)
    public ImageView doc_dispensing_img;
    @Bind(R.id.doc_dispensing_img_01)
    public ImageView doc_dispensing_img_01;
    @Bind(R.id.doc_injection_img)
    public ImageView doc_injection_img;
    @Bind(R.id.doc_injection_img_01)
    public ImageView doc_injection_img_01;

    private Frag_Dispensing contentFragment1;
    private Frag_Injection contentFragment2;
    private KJFragment contentFragment;


    @Override
    public void initData() {
        super.initData();
        contentFragment1 = new Frag_Dispensing();
        contentFragment2 = new Frag_Injection();
            change_frag_02();
            change_frag_01();
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.act_docadvice);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dispensing_ll)
    public void dispensing_ll() {
        change_frag_01();
    }

    @OnClick(R.id.injection_ll)
    public void injection_ll() {
        change_frag_02();
    }

    public void changeFragment(KJFragment targetFragment) {
        contentFragment = targetFragment;
        super.changeFragment(R.id.docadvice_frame, targetFragment);

    }
    @Override
    public  void onBackClick()
    {
        finish();
    }

    public void change_frag_01()
    {
        doc_dispensing_tv.setTextColor(getResources().getColor(R.color.doc_advice_01));
        doc_dispensing_img.setVisibility(View.VISIBLE);
        doc_dispensing_img_01.setVisibility(View.GONE);
        doc_injection_tv.setTextColor(getResources().getColor(R.color.doc_advice_03));
        doc_injection_img_01.setVisibility(View.VISIBLE);
        doc_injection_img.setVisibility(View.GONE);
        changeFragment(contentFragment1);
    }

    public void change_frag_02()
    {
        doc_dispensing_tv.setTextColor(getResources().getColor(R.color.doc_advice_03));
        doc_dispensing_img.setVisibility(View.GONE);
        doc_dispensing_img_01.setVisibility(View.VISIBLE);
        doc_injection_tv.setTextColor(getResources().getColor(R.color.doc_advice_01));
        doc_injection_img_01.setVisibility(View.GONE);
        doc_injection_img.setVisibility(View.VISIBLE);
        changeFragment(contentFragment2);
    }

    public void setTitle()
    {
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }
}
