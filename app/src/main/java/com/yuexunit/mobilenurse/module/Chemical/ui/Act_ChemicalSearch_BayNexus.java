package com.yuexunit.mobilenurse.module.Chemical.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.EditText;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/8/8.
 */
public class Act_ChemicalSearch_BayNexus extends KJActivity {
    @Bind(R.id.chemicalsearch_edt)
    EditText chemicalsearchEdt;

    public String action;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_chemiaclsearch);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            unregisterReceiver(Reciever);
        } catch (IllegalArgumentException e) {

        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(Reciever);
        } catch (IllegalArgumentException e) {

        }
    }

    @OnClick({R.id.chemicalsearch_img_back, R.id.chemicalsearch_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chemicalsearch_img_back:
                finish();
                break;
            case R.id.chemicalsearch_btn:
                if(chemicalsearchEdt.getText().toString().length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE))) {
                    String type_tag = chemicalsearchEdt.getText().toString().substring(8, 9);
                    if (type_tag.equals(AppConfig.TAG_INJECTION)) {
                        Intent intent_new = new Intent();
                        intent_new.setClass(aty, Act_ChemicalDetail.class);
                        intent_new.putExtra("barcode", chemicalsearchEdt.getText().toString());
//                    intent_new.putExtra("barcode", "20160628100176");
                        startActivity(intent_new);
                        chemicalsearchEdt.setText("");
                    } else {
                        ViewInject.toast("非化药条码");
                    }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
                break;
        }
    }

    //注册条码Receiver
    private void Register_Receiver()
    {
        IntentFilter filter=new IntentFilter();
        filter.addAction(AppConfig.BAR_READ_ACTION);
        registerReceiver(Reciever, filter);
    }

    //条码Receiver
    private BroadcastReceiver Reciever=new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            action = intent.getAction();
            if (action.equals(AppConfig.BAR_READ_ACTION)) {
                String BAR_value = intent.getStringExtra("BAR_VALUE");
                if (BAR_value.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE)))
                {
                    String type_tag = BAR_value.substring(8, 9);
                    if(type_tag.equals(AppConfig.TAG_INJECTION)) {
                        Intent intent_new = new Intent();
                        intent_new.setClass(aty, Act_ChemicalDetail.class);
                        intent_new.putExtra("barcode", BAR_value);
                        startActivity(intent_new);
                        chemicalsearchEdt.setText("");
                    }
                    else {
                        ViewInject.toast("非化药条码");
                    }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
            }
        }
    };


}
