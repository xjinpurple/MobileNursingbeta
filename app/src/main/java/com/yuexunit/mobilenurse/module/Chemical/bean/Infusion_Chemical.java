package com.yuexunit.mobilenurse.module.Chemical.bean;

/**
 * Created by work-jx on 2016/6/29.
 */
public class Infusion_Chemical {
    private String infusion;

    public String getInfusion() {
        return infusion;
    }

    public void setInfusion(String infusion) {
        this.infusion = infusion;
    }
}
