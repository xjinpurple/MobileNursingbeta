package com.yuexunit.mobilenurse.module.Inspect.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectModel {
    /**
     * 获取病人检查单
     */
    Observable<String> getInspectListData(String visitno);
}
