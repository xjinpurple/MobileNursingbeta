package com.yuexunit.mobilenurse.module.HealthArticles.bean;

/**
 * Created by sslcjy on 16/1/26.
 */
public class YXArticleContent {
    public int code;
    public String message;
    public Article data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "YXArticleContent{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}