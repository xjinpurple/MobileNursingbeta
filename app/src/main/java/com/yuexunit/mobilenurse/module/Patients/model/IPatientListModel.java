package com.yuexunit.mobilenurse.module.Patients.model;

import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/22.
 */
public interface IPatientListModel {

    /**
     * 获取病人列表信息
     */
    Observable getPatientListData(String Wardno);

    /**
     * 获取病人列表信息_分类
     */
    Observable getPatientClassifyListData(String wardId,String careLevel,String isKF,String isZS,String isLAB);

    /**
     * 一键上传体征数据
     */
    Observable<String> uploadTypesData(Map<String, String> praise);

    //获取基本体征项
    public Observable<ArrayList<SignsInput_Data>> getBaseTypes(String areaId);

    //获取全部体征项
    public Observable<ArrayList<SignsInput_Data>> getAllTypes();

}
