package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2015/12/23.
 */
public class YXPatientSingleBean {
    public YXPatientSingleBean body;

    //交易返回区(head)
    public Head head;
    //数据返回区(response)
    public PatientSingle_Response response;

    public PatientSingle_Response getResponse() {
        return response;
    }

    public void setResponse(PatientSingle_Response response) {
        this.response = response;
    }

    public YXPatientSingleBean getBody() {
        return body;
    }

    public void setBody(YXPatientSingleBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }
}
