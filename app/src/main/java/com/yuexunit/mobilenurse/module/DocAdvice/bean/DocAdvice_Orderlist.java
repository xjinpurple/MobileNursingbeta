package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by Administrator on 2015-12-13.
 */
public class DocAdvice_Orderlist {

    //医嘱组号
    public long ordergroupno;
    //医嘱类型
    public String ordertype;
    //用法
    public String execfreq;
    //给药方式
    public String execway;
    //注射标记
    public  String injectflag;
    //开嘱时间
    @JSONField(format = "yyyy-MM-dd")
    public Date ordertime;
    //医生工号
    public int doctorid;
    //医生姓名
    public String doctorname;

    public long getOrdergroupno() {
        return ordergroupno;
    }

    public void setOrdergroupno(long ordergroupno) {
        this.ordergroupno = ordergroupno;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getExecfreq() {
        return execfreq;
    }

    public void setExecfreq(String execfreq) {
        this.execfreq = execfreq;
    }

    public String getExecway() {
        return execway;
    }

    public void setExecway(String execway) {
        this.execway = execway;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public int getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(int doctorid) {
        this.doctorid = doctorid;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }


    public String getInjectflag() {
        return injectflag;
    }

    public void setInjectflag(String injectflag) {
        this.injectflag = injectflag;
    }
}
