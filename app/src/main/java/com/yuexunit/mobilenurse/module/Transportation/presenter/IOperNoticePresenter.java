package com.yuexunit.mobilenurse.module.Transportation.presenter;

/**
 * Created by work-jx on 2016/12/2.
 */
public interface IOperNoticePresenter {
    public void showOpsNoticeList(String requestno);
}
