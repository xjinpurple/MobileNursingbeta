package com.yuexunit.mobilenurse.module.NursingDocuments.ui;

import android.content.Context;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.NursingDocuments.model.impl.DocumentRecordDetailModel;
import com.yuexunit.mobilenurse.module.NursingDocuments.presenter.impl.DocumentRecordDetailPresenter;
import com.yuexunit.mobilenurse.module.NursingDocuments.ui.view.IDocumentRecordDetailView;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/10/13.
 */
public class Act_DocumentRecordDetail extends KJActivity implements IDocumentRecordDetailView {

    @Bind(R.id.signinput_name)
    TextView signinputName;
    @Bind(R.id.signinput_bedno)
    TextView signinputBedno;
    @Bind(R.id.signinput_visitno)
    TextView signinputVisitno;
    @Bind(R.id.documentrecord_time_day)
    TextView documentrecordTimeDay;
    @Bind(R.id.documentrecord_time_hour)
    Spinner documentrecordTimeHour;
    @Bind(R.id.documentrecord_temp_edt)
    EditText documentrecordTempEdt;
    @Bind(R.id.documentrecord_bp_01)
    EditText documentrecordBp01;
    @Bind(R.id.documentrecord_bp_02)
    EditText documentrecordBp02;
    @Bind(R.id.documentrecord_pulse_edit)
    EditText documentrecordPulseEdit;
    @Bind(R.id.documentrecord_heart_edit)
    EditText documentrecordHeartEdit;
    @Bind(R.id.documentrecord_breathe_edit)
    EditText documentrecordBreatheEdit;
    @Bind(R.id.documentrecord_bos_edit)
    EditText documentrecordBosEdit;
    @Bind(R.id.documentrecord_ache_edit)
    EditText documentrecordAcheEdit;
    @Bind(R.id.documentrecord_pupilleft_edit)
    EditText documentrecordPupilleftEdit;
    @Bind(R.id.documentrecord_pupilright_edit)
    EditText documentrecordPupilrightEdit;
    @Bind(R.id.documentrecord_lightleft_edit)
    Spinner documentrecordLightleftEdit;
    @Bind(R.id.documentrecord_lightright_edit)
    Spinner documentrecordLightrightEdit;
    @Bind(R.id.documentrecord_aware_edit)
    Spinner documentrecordAwareEdit;
    @Bind(R.id.documentrecord_health_edit)
    Spinner documentrecordHealthEdit;
    @Bind(R.id.documentrecord_healthcustom_edit)
    EditText documentrecordHealthcustomEdit;
    @Bind(R.id.documentrecord_nursed_edit)
    Spinner documentrecordNursedEdit;
    @Bind(R.id.documentrecord_nursed_e_edit)
    EditText documentrecordNursedEEdit;
    @Bind(R.id.documentrecord_nursed_f_edit)
    EditText documentrecordNursedFEdit;
    @Bind(R.id.documentrecord_nursed_g_edit)
    EditText documentrecordNursedGEdit;
    @Bind(R.id.documentrecord_nursed_h_edit)
    EditText documentrecordNursedHEdit;

    public String inpNo;//住院号
    public String recorder;//记录人员id

    private DocumentRecordDetailPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_documentrecorddetail);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new DocumentRecordDetailPresenter(new DocumentRecordDetailModel(), this);
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        recorder = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID);
    }

    @Override
    public void initWidget() {
        super.initWidget();
        signinputName.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        signinputBedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        signinputVisitno.setText(inpNo);
        documentrecordTimeDay.setText(StringUtils.getDataTime("yyyy-MM-dd"));
        documentrecordTempEdt.setInputType(InputType.TYPE_CLASS_PHONE);
    }

    @OnClick({R.id.titlebar_img_back, R.id.right_text, R.id.titlebar_img_menu})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.titlebar_img_back:
                finish();
                break;
            case R.id.right_text:
                buildValue();
                break;
            case R.id.titlebar_img_menu:
                buildValue();
                break;
        }
    }

    private void buildValue()
    {
        StringBuilder builder = new StringBuilder();

        if (!StringUtils.isEmpty(documentrecordTempEdt.getText())) {
            builder.append("424" + ":" + documentrecordTempEdt.getText());
        }

        if (!(StringUtils.isEmpty(documentrecordBp01.getText()) && StringUtils.isEmpty(documentrecordBp02.getText()))) {
            if (builder.length() != 0) {
                builder.append(",427:" + documentrecordBp01.getText() + "/" + documentrecordBp02.getText());
            } else {
                builder.append("427:" + documentrecordBp01.getText() + "/" + documentrecordBp02.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordPulseEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",425:" + documentrecordPulseEdit.getText() );
            } else {
                builder.append("425:" + documentrecordPulseEdit.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordHeartEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",170:" + documentrecordHeartEdit.getText() );
            } else {
                builder.append("170:" + documentrecordHeartEdit.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordBreatheEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",426:" + documentrecordBreatheEdit.getText() );
            } else {
                builder.append("426:" + documentrecordBreatheEdit.getText());
            }
        }

//        if (!StringUtils.isEmpty(documentrecordBosEdit.getText())) {
//            if (builder.length() != 0) {
//                builder.append(",435:" + documentrecordBosEdit.getText() );
//            } else {
//                builder.append("435:" + documentrecordBosEdit.getText());
//            }
//        }

        if (!StringUtils.isEmpty(documentrecordAcheEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",535:" + documentrecordAcheEdit.getText() );
            } else {
                builder.append("535:" + documentrecordAcheEdit.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordPupilleftEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",38:" + documentrecordPupilleftEdit.getText() );
            } else {
                builder.append("38:" + documentrecordPupilleftEdit.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordPupilrightEdit.getText())) {
            if (builder.length() != 0) {
                builder.append(",39:" + documentrecordPupilrightEdit.getText() );
            } else {
                builder.append("39:" + documentrecordPupilrightEdit.getText());
            }
        }

        if (!StringUtils.isEmpty(documentrecordLightleftEdit.getSelectedItem().toString())) {
            if (builder.length() != 0) {
                builder.append(",495:" + documentrecordLightleftEdit.getSelectedItem().toString());
            } else {
                builder.append("495:" + documentrecordLightleftEdit.getSelectedItem().toString());
            }
        }

        if (!StringUtils.isEmpty(documentrecordLightrightEdit.getSelectedItem().toString())) {
            if (builder.length() != 0) {
                builder.append(",499:" + documentrecordLightrightEdit.getSelectedItem().toString());
            } else {
                builder.append("499:" + documentrecordLightrightEdit.getSelectedItem().toString());
            }
        }

//        if (!StringUtils.isEmpty(documentrecordAwareEdit.getSelectedItem().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",452:" + documentrecordAwareEdit.getSelectedItem().toString());
//            } else {
//                builder.append("452:" + documentrecordAwareEdit.getSelectedItem().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordHealthEdit.getSelectedItem().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",442:" + documentrecordHealthEdit.getSelectedItem().toString());
//            } else {
//                builder.append("442:" + documentrecordHealthEdit.getSelectedItem().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordHealthcustomEdit.getText().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",443:" + documentrecordHealthcustomEdit.getText().toString());
//            } else {
//                builder.append("443:" + documentrecordHealthcustomEdit.getText().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordNursedEdit.getSelectedItem().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",444:" + documentrecordNursedEdit.getSelectedItem().toString());
//            } else {
//                builder.append("444:" + documentrecordNursedEdit.getSelectedItem().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordNursedEEdit.getText().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",445:" + documentrecordNursedEEdit.getText().toString());
//            } else {
//                builder.append("445:" + documentrecordNursedEEdit.getText().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordNursedFEdit.getText().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",446:" + documentrecordNursedFEdit.getText().toString());
//            } else {
//                builder.append("446:" + documentrecordNursedFEdit.getText().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordNursedGEdit.getText().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",447:" + documentrecordNursedGEdit.getText().toString());
//            } else {
//                builder.append("447:" + documentrecordNursedGEdit.getText().toString());
//            }
//        }
//
//        if (!StringUtils.isEmpty(documentrecordNursedHEdit.getText().toString())) {
//            if (builder.length() != 0) {
//                builder.append(",448:" + documentrecordNursedHEdit.getText().toString());
//            } else {
//                builder.append("448:" + documentrecordNursedHEdit.getText().toString());
//            }
//        }

        Map<String, String> params = new HashMap<>();
        params.put("inpNo", inpNo);
        params.put("timestamp", System.currentTimeMillis() + "");
        params.put("timepoint", StringUtils.getDataTime("yyyy-MM-dd") + " " + documentrecordTimeHour.getSelectedItem().toString() + ":00:00");
        params.put("templateId", "36");
        params.put("signData", builder.toString());
        params.put("recorder", recorder);


        presenter.uploadRecord(params);
    }


    /**
     * 通过dispatchTouchEvent每次ACTION_DOWN事件中动态判断非EditText本身区域的点击事件，然后在事件中进行屏蔽。然后当点击不是EditText时候，进行隐藏键盘操作
     *
     * @param ev
     * @return
     */

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {

        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件\
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public void uploadRecord() {
        finish();
    }
}
