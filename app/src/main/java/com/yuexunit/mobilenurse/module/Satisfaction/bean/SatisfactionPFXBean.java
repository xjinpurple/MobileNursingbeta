package com.yuexunit.mobilenurse.module.Satisfaction.bean;

/**
 * Created by work-jx on 2016/6/6.
 */
public class SatisfactionPFXBean {
    private String PFXBH;
    private String PFXNR;
    private String PFXMR;

    public String getPFXBH() {
        return PFXBH;
    }

    public void setPFXBH(String PFXBH) {
        this.PFXBH = PFXBH;
    }

    public String getPFXNR() {
        return PFXNR;
    }

    public void setPFXNR(String PFXNR) {
        this.PFXNR = PFXNR;
    }

    public String getPFXMR() {
        return PFXMR;
    }

    public void setPFXMR(String PFXMR) {
        this.PFXMR = PFXMR;
    }
}
