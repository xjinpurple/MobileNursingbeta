package com.yuexunit.mobilenurse.module.Patients.ui.view;

import com.yuexunit.mobilenurse.module.Patients.bean.PatientClassList_Patinfo;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientList_Patinfo;
import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/22.
 */
public interface IPatientListView {

    /**
     * 一键上传功能
     */
    void oneKeyUpload(String bean,String info,All_Sqlite_SignInputBean signInputBean);

    /**
     * 加载病人列表
     */
    void showPatientList(ArrayList<PatientList_Patinfo> list);
    /**
     * 加载病人列表-分类
     */
    void showPatientClassList(ArrayList<PatientClassList_Patinfo> list);
    /**
     * 病人列表加载过程中状态提示
     */
    void PullDownRefreshState(int state);

    /**
     * 进入病人列表界面的时候预先加载体征录入功能中的基本体征项及全部体征项
     * @param baseList
     */
    void saveBaseTypes(ArrayList<SignsInput_Data> baseList);
    void saveAllTypes(ArrayList<SignsInput_Data> allList);

}
