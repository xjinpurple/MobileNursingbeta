package com.yuexunit.mobilenurse.module.Patients.bean;

/**
 * Created by work-jx on 2016/3/28.
 */
public class PatientSign {
    //姓名
    private String name;
    //住院号
    private String visitno;
    //体征类型
    private String type;
    //体征值
    private String count;
    //体征单位
    private String unit;
    //体征输入时间
    private String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVisitno() {
        return visitno;
    }

    public void setVisitno(String visitno) {
        this.visitno = visitno;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
