package com.yuexunit.mobilenurse.module.Patients.bean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/10/9.
 */
public class PatientClassList_Response {
    public ArrayList<PatientClassList_Patinfo> patclass;

    public ArrayList<PatientClassList_Patinfo> getPatclass() {
        return patclass;
    }

    public void setPatclass(ArrayList<PatientClassList_Patinfo> patclass) {
        this.patclass = patclass;
    }
}
