package com.yuexunit.mobilenurse.module.HospitalInfo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.HospitalInfo.Act_HospitalInfo;
import com.yuexunit.mobilenurse.module.Inspect.adapter.InspectAdapter;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exreqlist;
import com.yuexunit.mobilenurse.module.Inspect.model.impl.InspectModel;
import com.yuexunit.mobilenurse.module.Inspect.presenter.IInspectPresenter;
import com.yuexunit.mobilenurse.module.Inspect.presenter.impl.InspectPresenter;
import com.yuexunit.mobilenurse.module.Inspect.ui.view.IInspectView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by work-jx on 2016/7/13.
 */
public class Frag_Inspect extends Fragment implements IInspectView{
    @Bind(R.id.act_inspect_list)
    public ListView act_inspect_list;
    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;

    private Act_HospitalInfo aty;
    private BaseAdapter adapter;
    //病人住院号
    private  String Visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Frag_Inspect.class);
    IInspectPresenter presenter;

    protected View mMainView;
    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        aty = (Act_HospitalInfo)getActivity();
        mMainView = inflater.inflate(R.layout.act_inspect, container, false);
        ButterKnife.bind(this, mMainView);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter = new InspectPresenter(this,new InspectModel());
        Visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        mEmptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });
        if(SystemTool.checkNet(aty)) {
            presenter.showInspectList(Visitno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void showInspectList(ArrayList<InjectionList_Exreqlist> list) {
        adapter = new InspectAdapter(act_inspect_list, list, R.layout.testlist_item);
        act_inspect_list.setAdapter(adapter);
        mEmptyLayout.dismiss();
    }
}
