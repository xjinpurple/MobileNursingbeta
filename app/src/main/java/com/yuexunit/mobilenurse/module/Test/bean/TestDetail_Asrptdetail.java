package com.yuexunit.mobilenurse.module.Test.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */

//检验结果明细(asrptdetail)
public class TestDetail_Asrptdetail {

    //报告单编号
    public String reportno;
    //报告单序号
    public String reportseqno;
    //结果序号
    public String seqno;
    //项目代码
    public String itemcode;
    //项目名称
    public String itemname;
    //项目结果
    public String resultdata;
    //参考范围
    public String resultrange;
    //结果描述
    public String resultdesc;

    public String getReportno() {
        return reportno;
    }

    public void setReportno(String reportno) {
        this.reportno = reportno;
    }

    public String getReportseqno() {
        return reportseqno;
    }

    public void setReportseqno(String reportseqno) {
        this.reportseqno = reportseqno;
    }

    public String getSeqno() {
        return seqno;
    }

    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getResultdata() {
        return resultdata;
    }

    public void setResultdata(String resultdata) {
        this.resultdata = resultdata;
    }

    public String getResultrange() {
        return resultrange;
    }

    public void setResultrange(String resultrange) {
        this.resultrange = resultrange;
    }

    public String getResultdesc() {
        return resultdesc;
    }

    public void setResultdesc(String resultdesc) {
        this.resultdesc = resultdesc;
    }
}
