package com.yuexunit.mobilenurse.module.SignQuery.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.SignQuery.api.SignQueryApi;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;
import com.yuexunit.mobilenurse.module.SignQuery.model.ISignModel;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;




/**
 * Created by sslcjy on 16/1/26.
 */
public class SignModel implements ISignModel {

    private SignQueryApi api;
    //记录Log
    private final Logger log = ProUtil.getLogger(SignModel.class);

    @Override
    public Observable<YXSignQuery> getShowSignInfo(String inpNo,int interval,String date) {
        api = ApiInstance();
        return api.getShowSignInfo(inpNo, interval, date);
    }

    @Override
    public Observable<String> getShowSignInfoFive(final String inpNo,final String interval, final String date) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT;

                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.Signlist_Json(inpNo, interval, date));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht =  new HttpTransportSE(url,AppConfig.TIMEOUT);
//                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url+",kmhs_enr_list", "2", inpNo+","+interval+","+date, response.toString()));
                    isConnect = false;
                    if (response != null) {
                        JSONObject all, body;
                        try {
                            all = new JSONObject(response.toString());
                            body = all.getJSONObject("body");
                            result = body.getString("response");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                            subscriber.onError(e);
                        }
                        if ("null".equals(result))
                        {
                            subscriber.onError(new Exception());
                        }
                    } else {
                        subscriber.onError(new Exception());
                    }

                } catch (Exception e) {
                    count++;
                    log.error("Error", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);

            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }

    public SignQueryApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi(SignQueryApi.class);
        }
    }


}
