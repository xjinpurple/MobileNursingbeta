package com.yuexunit.mobilenurse.module.Transportation.adapter;

import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Transportation.bean.TransportationRecord_Opsinfo;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2016/9/29.
 */
public class TransportationRecordAdapter  extends KJAdapter<TransportationRecord_Opsinfo> {
    public TransportationRecordAdapter(AbsListView view, Collection<TransportationRecord_Opsinfo> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, TransportationRecord_Opsinfo item, boolean isScrolling) {
        adapterHolder.setText(R.id.item_transportation_number,"手术单号:"+item.getRequestno());
        adapterHolder.setText(R.id.item_transportation_level,"手术等级:"+item.getOpsgrand());
        adapterHolder.setText(R.id.item_transportation_name,item.getOpsname());

        LinearLayout item_outward_ll = adapterHolder.getView(R.id.item_outward_ll);
        LinearLayout item_inops_ll = adapterHolder.getView(R.id.item_inops_ll);
        LinearLayout item_outops_ll = adapterHolder.getView(R.id.item_outops_ll);
        LinearLayout item_inward_ll = adapterHolder.getView(R.id.item_inward_ll);

        if(item.getOutwardtime()==null)
        {
            item_outward_ll.setVisibility(View.GONE);
        }
        else {
            item_outward_ll.setVisibility(View.VISIBLE);
            adapterHolder.setText(R.id.item_outward_time, item.getOutwardtime());
            adapterHolder.setText(R.id.item_outward_worker, item.getOutWardOper());
            adapterHolder.setText(R.id.item_outward_worker_num, item.getOutWardOperId());
            adapterHolder.setText(R.id.item_outward_nurse, item.getOutwardemp());
            adapterHolder.setText(R.id.item_outward_nurse_num, item.getOutWardEmpId());
        }

        if(item.getInOpsTime()==null)
        {
            item_inops_ll.setVisibility(View.GONE);
        }
        else {
            item_inops_ll.setVisibility(View.VISIBLE);
            adapterHolder.setText(R.id.item_inops_time, item.getInOpsTime());
            adapterHolder.setText(R.id.item_inops_worker, item.getInOpsOper());
            adapterHolder.setText(R.id.item_inops_worker_num, item.getInOpsOperId());
            adapterHolder.setText(R.id.item_inops_nurse, item.getInOpsEmp());
            adapterHolder.setText(R.id.item_inops_nurse_num, item.getInOpsEmpId());
        }

        if(item.getOutOpsTime()==null)
        {
            item_outops_ll.setVisibility(View.GONE);
        }
        else {
            item_outops_ll.setVisibility(View.VISIBLE);
            adapterHolder.setText(R.id.item_outops_time, item.getOutOpsTime());
            adapterHolder.setText(R.id.item_outops_worker, item.getOutOpsOper());
            adapterHolder.setText(R.id.item_outops_worker_num, item.getOutOpsOperId());
            adapterHolder.setText(R.id.item_outops_nurse, item.getOutOpsEmp());
            adapterHolder.setText(R.id.item_outops_nurse_num, item.getOutOpsEmpId());
        }

        if(item.getInwardtime()==null)
        {
            item_inward_ll.setVisibility(View.GONE);
        }
        else {
            item_inward_ll.setVisibility(View.VISIBLE);
            adapterHolder.setText(R.id.item_inward_time, item.getInwardtime());
            adapterHolder.setText(R.id.item_inward_worker, item.getInWardOpsOper());
            adapterHolder.setText(R.id.item_inward_worker_num, item.getInWardOpsOper());
            adapterHolder.setText(R.id.item_inward_nurse, item.getInwardemp());
            adapterHolder.setText(R.id.item_inward_nurse_num, item.getInWardEmpId());
        }
    }
}
