package com.yuexunit.mobilenurse.module.Test.bean;

/**
 * Created by work-jx on 2015/12/11.
 */
public class TestDetail_Asrptlist {

    //报告单编号
    public String reportno;
    //报告单序号
    public String reportseqno;
    //检验人员
    public int entryid;
    //检验人员姓名
    public String entryname;
    //检验时间
    public String entrytime;
    //审核人员
    public int auditid;
    //审核人员姓名
    public String auditname;
    //审核时间
    public String audittime;
    //复审人员
    public int reauditid;
    //复审人员姓名
    public String reauditname;
    //复审时间
    public String reaudittime;
    //报告单名称
    public String reportname;



    public String getReportno() {
        return reportno;
    }

    public void setReportno(String reportno) {
        this.reportno = reportno;
    }

    public String getReportseqno() {
        return reportseqno;
    }

    public void setReportseqno(String reportseqno) {
        this.reportseqno = reportseqno;
    }

    public int getEntryid() {
        return entryid;
    }

    public void setEntryid(int entryid) {
        this.entryid = entryid;
    }

    public String getEntryname() {
        return entryname;
    }

    public void setEntryname(String entryname) {
        this.entryname = entryname;
    }

    public String getEntrytime() {
        return entrytime;
    }

    public void setEntrytime(String entrytime) {
        this.entrytime = entrytime;
    }

    public int getAuditid() {
        return auditid;
    }

    public void setAuditid(int auditid) {
        this.auditid = auditid;
    }

    public String getAuditname() {
        return auditname;
    }

    public void setAuditname(String auditname) {
        this.auditname = auditname;
    }

    public String getAudittime() {
        return audittime;
    }

    public void setAudittime(String audittime) {
        this.audittime = audittime;
    }

    public int getReauditid() {
        return reauditid;
    }

    public void setReauditid(int reauditid) {
        this.reauditid = reauditid;
    }

    public String getReauditname() {
        return reauditname;
    }

    public void setReauditname(String reauditname) {
        this.reauditname = reauditname;
    }

    public String getReaudittime() {
        return reaudittime;
    }

    public void setReaudittime(String reaudittime) {
        this.reaudittime = reaudittime;
    }

    public String getReportname() {
        return reportname;
    }

    public void setReportname(String reportname) {
        this.reportname = reportname;
    }
}
