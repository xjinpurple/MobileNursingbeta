package com.yuexunit.mobilenurse.module.Login.model;

import com.yuexunit.mobilenurse.module.Login.bean.SystemConfig;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/20.
 */
public interface ILoginModel {

    //获取JCI配置信息
    public Observable<SystemConfig> getListParams();

    //查询账号密码是否能登陆
    public Observable<Object> doInLogin(final String uid, final String pwd);
}
