package com.yuexunit.mobilenurse.module.Test.ui;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Test.adapter.TestDetailAdapter;
import com.yuexunit.mobilenurse.module.Test.bean.TestDetail_Asrptdetail;
import com.yuexunit.mobilenurse.module.Test.model.impl.TestDetaiModel;
import com.yuexunit.mobilenurse.module.Test.presenter.impl.TestDetailPresenter;
import com.yuexunit.mobilenurse.module.Test.ui.view.ITestDetailView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by work-jx on 2015/11/24.
 // */
public class Act_TestDetail extends TitleBar_DocAdvice implements ITestDetailView
{
    @Bind(R.id.act_testdetail_list)
    public ListView act_testdetail_list;
    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;
    @Bind(R.id.act_testdetail_num)
    public TextView act_testdetail_num;
    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;

    private BaseAdapter adapter;
    //申请单号
    private  String requestno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_TestDetail.class);

    private TestDetailPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_testdetail);
        ButterKnife.bind(this);
    }

    @Override
    public void showTestDetail(ArrayList<TestDetail_Asrptdetail> list,String requestno) {
        adapter = new TestDetailAdapter(act_testdetail_list, list, R.layout.testdetaillist_item);
        act_testdetail_list.setAdapter(adapter);
        set_text(requestno);
        mEmptyLayout.dismiss();
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new TestDetailPresenter(this,new TestDetaiModel());
        requestno = getIntent().getExtras().getString("requestno");
        mEmptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });

        if(SystemTool.checkNet(aty)) {
           presenter.showTestDetail(requestno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }

    }

    private void set_text(String reportno)
    {
        act_testdetail_num.setText("单号:"+reportno);
    }

    @Override
    public void setTitle()
    {
        titlebar_docadvice_tv_title.setText("化验报告单");
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    @Override
    public  void onBackClick()
    {
        finish();
    }
}

