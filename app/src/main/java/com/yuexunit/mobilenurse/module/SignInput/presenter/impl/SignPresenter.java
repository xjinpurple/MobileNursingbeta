package com.yuexunit.mobilenurse.module.SignInput.presenter.impl;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single_Check;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;
import com.yuexunit.mobilenurse.module.SignInput.model.IAddSignModel;
import com.yuexunit.mobilenurse.module.SignInput.model.ISignModel;
import com.yuexunit.mobilenurse.module.SignInput.presenter.ISignPresenter;
import com.yuexunit.mobilenurse.module.SignInput.ui.view.IAddSignType;
import com.yuexunit.mobilenurse.module.SignInput.ui.view.ISignInput;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.KJDB;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.StringUtils;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sslcjy on 16/1/26.
 */
public class SignPresenter implements ISignPresenter {

    private IAddSignType addview;
    private ISignInput view;
    private ISignModel model;
    private IAddSignModel addmodel;
    //记录Log
    private final Logger log = ProUtil.getLogger(SignPresenter.class);

    public SignPresenter(ISignModel model, ISignInput view) {
        this.model = model;
        this.view = view;
    }

    public SignPresenter(ISignModel model, IAddSignType addview) {
        this.model = model;
        this.addview = addview;
    }

    public SignPresenter(IAddSignModel model, IAddSignType addview) {
        this.addmodel = model;
        this.addview = addview;
    }

    @Override
    public void showBaseTypes(String areaId) {
        model.getBaseTypes(areaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Sign_Single>>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_showBaseTypes_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error("SignPresenter_showBaseTypes_onError:" + e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ArrayList<Sign_Single> signsInput_datas) {
                        log.info("SignPresenter_showBaseTypes_onNext:" + signsInput_datas.toString());
                        view.showSignInputType(signsInput_datas);
                    }
                });
    }

    @Override
    public void showAllTypes(String areaId) {
        model.getAllTypes(areaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<SignsInput_Data>>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_showAllTypes_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error("SignPresenter_showAllTypes_onError:" + e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ArrayList<SignsInput_Data> signsInput_datas) {
                        log.info("SignPresenter_showAllTypes_onNext:" + signsInput_datas.toString());
                        addview.showAllType(signsInput_datas);
                    }
                });
    }

    @Override
    public void buildUploadSignValue(HashMap signValue,HashMap booldSignValue,ArrayList<Sign_Single_Check> all) {
        StringBuilder builder = new StringBuilder();
        String booldValue = "";
        Iterator iter = signValue.entrySet().iterator();
        Iterator boolditer = booldSignValue.entrySet().iterator();
        while (boolditer.hasNext()) {
            Map.Entry entry = (Map.Entry) boolditer.next();
            int key = (int) entry.getKey();
            String val = (String) entry.getValue();
            if (!StringUtils.isEmpty(val)) {
                booldValue += val;
                if (boolditer.hasNext()) {
                    booldValue += "/";
                }
            }
        }
        if (!StringUtils.isEmpty(booldValue)) {
            builder.append("0:").append(booldValue).append(",");
        }
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            int key = (int) entry.getKey();
            String val = (String) entry.getValue();
            if (!StringUtils.isEmpty(val)) {
                builder.append(all.get(key).getCode() + ":" + val);
                if (iter.hasNext()) {
                    builder.append(",");
                }
            }
        }
        log.info("SignPresenter_buildUploadSignValue_builder:" + builder.toString());
        view.uploadSignValue(builder.toString());
    }

    @Override
    public void uploadTypesInfo(Map<String, String> praise,String name) {
        if(StringUtils.isEmpty(praise.get("signData"))){
            ViewInject.toast("未填写任何体征上传信息");
            return;
        }
        final All_Sqlite_SignInputBean currentbean = new All_Sqlite_SignInputBean();
        currentbean.setSignData(praise.get("signData"));
        currentbean.setInpNo(praise.get("inpNo"));
        currentbean.setRecorder(praise.get("recorder"));
        currentbean.setTimestamp(praise.get("timestamp"));
        currentbean.setTimepoint(praise.get("timepoint"));
        currentbean.setName(name);
        final KJDB kjdb = KJDB.create();
        kjdb.saveBindId(currentbean);

        model.uploadTypesValue(praise)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_uploadTypesInfo_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        ViewInject.toast("上传体征信息失败,已缓存");
                        log.error("SignPresenter_uploadTypesInfo_onError:"+e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(String actionBean) {
                        JSONObject all, body,reponse,head;
                        String ret_code = null,ret_info = null;
                        try {
                            all = new JSONObject(actionBean.toString());
                            body = all.getJSONObject("body");
                            reponse = body.getJSONObject("response");
                            head = reponse.getJSONObject("head");
                            ret_code = head.getString("ret_code");
                            ret_info = head.getString("ret_info");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                        }

                        if("0".equals(ret_code)){
                            kjdb.delete(currentbean);
                            ViewInject.toast("上传体征信息成功");
                        }else{
                            kjdb.delete(currentbean);
                            ViewInject.toast("当前时间点体征信息已录入");
                        }
                    }
                });
    }

    @Override
    public void SaveSingleSign(Map<String, String> praise) {
        addmodel.addsign(praise)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionBean>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_showAllTypes_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.error("SignPresenter_showAllTypes_onError:" + e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ActionBean actionBean) {
                        log.info("SignPresenter_showAllTypes_onNext:" + actionBean.toString());
                        addview.isSave(actionBean);
                    }
                });
    }
}
