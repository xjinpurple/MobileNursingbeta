package com.yuexunit.mobilenurse.module.Inspect.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectDetailModel {
    /**
     * 获取病人检查单详情
     */
    Observable<String> getInspectDetailData(String requestno);
}
