package com.yuexunit.mobilenurse.module.Patients.bean;


import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/1.
 */

//病人列表
public class YXPatientListBean {

    public YXPatientListBean body;

    //交易返回区(head)
    public Head head;
    //数据返回区(response)
    public PatientList_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public PatientList_Response getResponse() {
        return response;
    }

    public void setResponse(PatientList_Response response) {
        this.response = response;
    }

    public YXPatientListBean getBody() {
        return body;
    }

    public void setBody(YXPatientListBean body) {
        this.body = body;
    }
}
