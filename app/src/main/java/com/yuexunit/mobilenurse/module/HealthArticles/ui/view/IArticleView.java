package com.yuexunit.mobilenurse.module.HealthArticles.ui.view;

import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/25.
 */
public interface IArticleView {

    /**
     * 显示健康宣教文章列表
     */
    void showArticleList(ArrayList<ArticleTitle> list);

}
