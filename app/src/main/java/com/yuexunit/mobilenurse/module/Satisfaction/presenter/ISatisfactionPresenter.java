package com.yuexunit.mobilenurse.module.Satisfaction.presenter;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBean;

import java.util.ArrayList;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/6/1.
 */
public interface ISatisfactionPresenter {
    /*
    满意度列表
     */
    public void showSatisfactionList();

    /*
    提交满意度
     */
    public void commitSatisfactionDate(ArrayList<SatisfactionCommitBean> list);

     /*
    判断是否提交过
     */
     public void IscommitSatisfactionDate(String visitno);


    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
