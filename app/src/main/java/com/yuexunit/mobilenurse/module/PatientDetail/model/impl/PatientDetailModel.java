package com.yuexunit.mobilenurse.module.PatientDetail.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.PatientDetail.api.PatientDetailApi;
import com.yuexunit.mobilenurse.module.PatientDetail.model.IPatientDetailModel;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsSingleInput;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by sslcjy on 16/1/24.
 */
public class PatientDetailModel implements IPatientDetailModel {
    private PatientDetailApi api;

    //记录Log
    private final Logger log = ProUtil.getLogger(PatientDetailModel.class);

    @Override
    public Observable<Object> getPatientDetail(final String Visitno) {

        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                try {
                    Boolean isConnect = true;
                    int count = 0;
                    Object response = null;

                    do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD;
                    String url = AppConfig.WEB_CONTENT;
                    SoapObject request = new SoapObject(nameSpace, methodName);
                    request.addProperty("XmlString", CreateJson.Patinfo_Json(Visitno));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true; //very important for compatibility
                    HttpTransportSE ht = new HttpTransportSE(url);
//                    Object response = null;

                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();

                        //记录传输到服务器及传回的数据
                        log.info("getPatientDetail:  " + CreateJson.LOG_JSON(url + ",mhs_patinfo", "2", Visitno, response.toString()));
                        isConnect = false;
                    } catch (Exception e) {
                        count++;
                        log.error("getPatientDetail:" + e);
                        e.printStackTrace();
                    }
                    } while (isConnect && count < 5);
                    subscriber.onNext(response);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                    log.error("getPatientDetail_Exception:", e.toString());
                }
            }
        });
    }


    @Override
    public Observable collect(final String Visitno, final String Code, final String NurseId) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD;
                    String url = AppConfig.WEB_CONTENT_EXEC;
                    SoapObject request = new SoapObject(nameSpace, methodName);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                    String str = formatter.format(curDate);
                    request.addProperty("XmlString", CreateJson.Ascollect_Json(Visitno, Code,
                            Integer.valueOf(NurseId), str, AppConfig.EXECTYPE));

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true; //very important for compatibility
                    HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                    Object response = null;
                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();
                        //记录传输到服务器及传回的数据
                        log.info(CreateJson.LOG_JSON(url + ",mhs_as_collect", "2",
                                Visitno + "," + Code + "," + Integer.valueOf(NurseId) + "," + str + "," + AppConfig.EXECTYPE
                                , response.toString()));
                        isConnect = false;
                        if (response == null) {
                            subscriber.onError(new Exception());
                        }
                    } catch (Exception e) {
                        count++;
                        log.error("Exception:", e.toString());
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                } while (isConnect && count < 5);
                    subscriber.onNext(response);
                    subscriber.onCompleted();
                }
            }

            ).

            map(new Func1<Object, String>() {
                @Override
                public String call (Object response){
                    return (String) response.toString();
                }
            }

            );
        }

    public PatientDetailApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi_new(PatientDetailApi.class);
        }
    }

    //获取病人体征项
    @Override
    public Observable<ArrayList<Sign_Single>> getSingleTypes(String visitno) {
        api = ApiInstance();
        return api.SingleTypes(visitno).filter(new Func1<YXSignsSingleInput, Boolean>() {
            @Override
            public Boolean call(YXSignsSingleInput yxSignsSingleInput) {
                log.debug("getBaseTypes:"+yxSignsSingleInput.toString());
                return (yxSignsSingleInput.getCode() == 200) || (yxSignsSingleInput.getCode() == 400);
            }
        }).map(new Func1<YXSignsSingleInput, ArrayList<Sign_Single>>() {

            @Override
            public ArrayList<Sign_Single> call(YXSignsSingleInput yxSignsSingleInput) {
                return yxSignsSingleInput.getData();
            }
        });
    }
}

