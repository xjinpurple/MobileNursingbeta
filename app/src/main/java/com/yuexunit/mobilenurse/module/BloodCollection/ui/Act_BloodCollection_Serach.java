package com.yuexunit.mobilenurse.module.BloodCollection.ui;

import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.adapter.BloodCollectionSearchAdapter;
import com.yuexunit.mobilenurse.module.BloodCollection.bean.Specinfo;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionSearchModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionSearchPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl.BloodCollectionSearchPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionSearchView;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/5/3.
 */
public class Act_BloodCollection_Serach extends KJActivity implements IBloodCollectionSearchView{

    @Bind(R.id.bloodcollection_search_list)
    ListView bloodcollectionSearchList;

    BaseAdapter adapter;


    public static final int SHOW_DIALOG = 1;
    public static final int DISMISS_DIALOG = -1;

    LoadingDialog dialog;
    IBloodCollectionSearchPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_bloodcollection_search);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new BloodCollectionSearchPresenter(this, new BloodCollectionSearchModel());
        this.dialogState(SHOW_DIALOG);
        presenter.showBloodCollectionList(PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_ID));
    }


    @OnClick(R.id.bloodcollection_img_back)
    public void onClick() {
        finish();
    }


    @Override
    public void showBloodSearchList(ArrayList<Specinfo> list) {
        adapter = new BloodCollectionSearchAdapter(bloodcollectionSearchList,list,R.layout.item_bloodcollection_search);
        bloodcollectionSearchList.setAdapter(adapter);
    }

    @Override
    public void dialogState(int status) {
        switch (status){
            case SHOW_DIALOG:
                dialog = new LoadingDialog(this);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                break;
            case DISMISS_DIALOG:
                dialog.dismiss();
                break;
        }
    }
}
