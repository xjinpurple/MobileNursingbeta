package com.yuexunit.mobilenurse.module.Chemical.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Chemical.bean.ChemicalDetailBean;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2016/6/17.
 */
public class ChemicalDetailAdapter extends KJAdapter<ChemicalDetailBean>{

    public ChemicalDetailAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, ChemicalDetailBean chemicalDetailBean, boolean b,final int position) {
        adapterHolder.setText(R.id.chemical_id,(position+1)+"");
        adapterHolder.setText(R.id.chemical_name,chemicalDetailBean.getYpmc());
        adapterHolder.setText(R.id.chemical_spec,chemicalDetailBean.getJl()+chemicalDetailBean.getJldw());
        adapterHolder.setText(R.id.chemical_percount,chemicalDetailBean.getSl());
    }
}
