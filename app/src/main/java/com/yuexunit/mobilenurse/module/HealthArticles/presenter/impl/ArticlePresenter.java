package com.yuexunit.mobilenurse.module.HealthArticles.presenter.impl;

import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;
import com.yuexunit.mobilenurse.module.HealthArticles.model.IArticleModel;
import com.yuexunit.mobilenurse.module.HealthArticles.ui.view.IArticleContentView;
import com.yuexunit.mobilenurse.module.HealthArticles.ui.view.IArticleView;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.slf4j.Logger;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sslcjy on 16/1/25.
 */
public class ArticlePresenter {

    private IArticleModel model;
    private IArticleView view;
    private IArticleContentView contentView;
    //记录Log
    private final Logger log = ProUtil.getLogger(ArticlePresenter.class);

    public ArticlePresenter(IArticleModel model, IArticleView view) {
        this.model = model;
        this.view = view;
    }

    public ArticlePresenter(IArticleModel model, IArticleContentView view) {
        this.model = model;
        this.contentView = view;
    }


    public void showArticleList(String areaId) {
        model.getArticleList(areaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<ArticleTitle>>() {
                    @Override
                    public void onCompleted() {
                        log.info("ArticlePresenter_showArticleList_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.info("ArticlePresenter_showArticleList_onError:"+e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ArrayList<ArticleTitle> articleTitles) {
                        log.info("ArticlePresenter_showArticleList_onNext:"+articleTitles.toString());
                        view.showArticleList(articleTitles);
                    }
                });
    }


    public void showArticleContent(int articleId) {
        model.getArticleContent(articleId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        log.info("ArticlePresenter_showArticleContent_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.info("ArticlePresenter_showArticleContent_onError:"+e);
                    }

                    @Override
                    public void onNext(String content) {
                        log.info("ArticlePresenter_showArticleContent_onNext:"+content);
                        contentView.showArticleContent(content);
                    }
                });
    }
}
