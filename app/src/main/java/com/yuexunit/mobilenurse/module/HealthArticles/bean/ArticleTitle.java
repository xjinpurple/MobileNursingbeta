package com.yuexunit.mobilenurse.module.HealthArticles.bean;

/**
 * Created by hbprotoss on 12/1/15.
 */

//健康宣教的id，标题和链接

public class ArticleTitle {
    private int article_id;

    private String title;

    private int rank;

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "ArticleTitle{" +
                "article_id=" + article_id +
                ", title='" + title + '\'' +
                ", rank=" + rank +
                '}';
    }
}
