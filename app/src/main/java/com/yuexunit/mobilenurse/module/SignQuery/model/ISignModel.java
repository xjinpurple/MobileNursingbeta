package com.yuexunit.mobilenurse.module.SignQuery.model;


import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;

import rx.Observable;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignModel {

    Observable<YXSignQuery> getShowSignInfo(String inpNo, int interval, String date);

    /**
     * 体征查询 HIS5.0
     */
    Observable<String> getShowSignInfoFive(String inpNo, String interval, String date);

}
