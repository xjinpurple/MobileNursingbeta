package com.yuexunit.mobilenurse.module.Inspect.ui.view;

import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exreqlist;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectView {
    //检查单列表列表
    void showInspectList(ArrayList<InjectionList_Exreqlist> list);
}
