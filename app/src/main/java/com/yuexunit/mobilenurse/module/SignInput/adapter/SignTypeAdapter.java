package com.yuexunit.mobilenurse.module.SignInput.adapter;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single_Check;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sslcjy on 15/12/22.
 */
public class SignTypeAdapter extends KJAdapter<Sign_Single_Check> {

    private TextView name;
    private EditText edit;
    private TextView unit;

    private EditText edit_in, edit_out;


    public ArrayList<Sign_Single_Check> lists = new ArrayList<Sign_Single_Check>();

    public ArrayList<TypeContent> contentlists = new ArrayList<TypeContent>();
    TypeContent a;

    //定义一个HashMap，用来存放EditText的值，Key是position
    HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
    HashMap<Integer, String> hashMap_blood = new HashMap<Integer, String>();

    public Map getEditBooldValue() {
        return hashMap_blood;
    }

    public Map getEditValue() {
        return hashMap;
    }

    public void cleatMap() {
        hashMap.clear();
        hashMap_blood.clear();
    }

    public SignTypeAdapter(AbsListView view, Collection<Sign_Single_Check> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);

        lists = (ArrayList<Sign_Single_Check>) mDatas;
        initData(30);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, Sign_Single_Check sign_single_check, boolean b) {

    }


    private void initData(int size) {
        for (int i = 0; i < size; i++) {
            a = new TypeContent(i + "号位", "");
            contentlists.add(a);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

//        if (position == 0) {
//            convertView = View.inflate(mCxt, R.layout.item_type_blood, null);
//            name = (TextView) convertView.findViewById(R.id.type_name);
//            unit = (TextView) convertView.findViewById(R.id.type_unit);
//            edit_in = (EditText) convertView.findViewById(R.id.type_edit_in);
//            edit_out = (EditText) convertView.findViewById(R.id.type_edit_out);
//            name.setText(lists.get(position).getName());
//            unit.setText(lists.get(position).getUnit());
//
//            edit_in.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    hashMap_blood.put(position, s.toString());
//                }
//            });
//
//
//            edit_out.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    hashMap_blood.put(position + 1, s.toString());
//                }
//            });
//            //如果hashMap_blood不为空，就设置的editText
//
//            if (hashMap_blood.get(position) != null) {
//                edit_in.setText(hashMap_blood.get(position));
//            }
//
//            if (hashMap_blood.get(position + 1) != null) {
//                edit_out.setText(hashMap_blood.get(position + 1));
//            }
//
//        } else {


            convertView = View.inflate(mCxt, R.layout.item_type, null);
            name = (TextView) convertView.findViewById(R.id.type_name);
            unit = (TextView) convertView.findViewById(R.id.type_unit);
            edit = (EditText) convertView.findViewById(R.id.type_edit);
            if(!lists.get(position).getCode().equals("559"))
            edit.setInputType(InputType.TYPE_CLASS_PHONE);

            name.setText(lists.get(position).getName());
            unit.setText(lists.get(position).getUnit());
//            final String range = lists.get(position).getRange();
            edit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {


                    hashMap.put(position, s.toString());
                }
            });


//            edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View v, boolean hasFocus) {
//                    EditText textView = (EditText) v;
//                    String s = textView.getText().toString();
//                    if (!hasFocus) {
//                        if (!StringUtils.isEmpty(range) && !StringUtils.isEmpty(s)) {
//                            String[] MinMax = range.split("-");
//                            Double Max = Double.valueOf(MinMax[1]);
//                            Double Min = Double.valueOf(MinMax[0]);
//                            if (Min > Double.valueOf(s.toString())) {
//                                ViewInject.toast("小于“ " + lists.get(position).getName() + " ”所允许的最小值范围,请核对。");
//                            }
//                            if (Double.valueOf(s.toString()) > Max) {
//                                ViewInject.toast("大于“ " + lists.get(position).getName() + " “所允许的最大值范围,请核对。");
//                            }
//                        }
//                    }
//                }
//            });


            //如果hashMap不为空，就设置的editText
            if (hashMap.get(position) != null) {
                edit.setText(hashMap.get(position));
            }
//        }

        return convertView;

    }

    class TypeContent {
        String name;
        String content;
        public TypeContent(String name, String content) {
            this.name = name;
            this.content = content;
        }
    }
}