package com.yuexunit.mobilenurse.module.BloodCollection.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.joyree.JoyreeScannerInterface;
import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.adapter.BloodCollectionAdapter;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl.BloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionView;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/6/14.
 */
public class Act_BloodCollection_Joyree extends KJActivity implements IBloodCollectionView {
    @Bind(R.id.bloodcollection_list)
    ListView bloodcollectionList;

    BaseAdapter adapter;
    private LoadingDialog dialog;

    ArrayList<Collectinfo> collectinfos = new ArrayList<Collectinfo>();

    IBloodCollectionPresenter presenter;

    private IntentFilter mBarcodeFilter = null;

    public static final String BARCODE_DATA_ACTION = "android.intent.action.BARCODEDATA";
    public static final String BARCODE_RESULT = "barcode_result";

    public JoyreeScannerInterface mScanner = null;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_bloodcollection);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        dialog = new LoadingDialog(aty);
        presenter = new BloodCollectionPresenter(new BloodCollectionModel(),this);
        adapter = new BloodCollectionAdapter(bloodcollectionList, collectinfos, R.layout.item_bloodcollection);
        bloodcollectionList.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            unregisterReceiver(mBarcodeReceiver);
        } catch (IllegalArgumentException e) {

        }
        mScanner = null;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(mBarcodeReceiver);
        } catch (IllegalArgumentException e) {

        }
        mScanner = null;
    }


    @OnClick({R.id.bloodcollection_img_back, R.id.bloodcollection_img_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bloodcollection_img_back:
                finish();
                break;
            case R.id.bloodcollection_img_search:
                Intent intent = new Intent(aty,Act_BloodCollection_Serach.class);
                startActivity(intent);
                break;
        }
    }


    private void Register_Receiver()
    {
        mScanner = new JoyreeScannerInterface(getApplication());
        mScanner.enableBeep(true);
        mScanner.enableVibrate(true);
        mScanner.setOutputMode(0);

        mBarcodeFilter = new IntentFilter();
        mBarcodeFilter.addAction(BARCODE_DATA_ACTION);

        registerReceiver(mBarcodeReceiver, mBarcodeFilter);
    }

    private BroadcastReceiver mBarcodeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            if (intent != null && intent.getStringExtra(BARCODE_RESULT) != null) {
                String mBarcodeData = intent.getStringExtra(BARCODE_RESULT);
                if (!mBarcodeData.equals("")) {
                    if(!mBarcodeData.equals(""))
                    {
                        if(mBarcodeData.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_CX_RULE))) {
                            dialog.show();
                            presenter.collect("", mBarcodeData, PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID));
                        }
                        else {
                            Toast.makeText(aty, "条码无效!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }

    };

    @Override
    public void collectSend(Boolean isSuccess ,Collectinfo collectinfo) {
        if(isSuccess)
        {
            if(collectinfo != null) {
                collectinfos.add(collectinfo);
                adapter.notifyDataSetChanged();
            }
            else {
                ViewInject.toast("该标本已发送");
            }
        }
        else {
            ViewInject.toast("发送失败");
        }
        dialog.dismiss();
    }
}
