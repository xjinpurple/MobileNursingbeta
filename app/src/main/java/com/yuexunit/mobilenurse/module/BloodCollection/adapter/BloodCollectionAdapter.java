package com.yuexunit.mobilenurse.module.BloodCollection.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2017/5/3.
 */
public class BloodCollectionAdapter extends KJAdapter<Collectinfo>{

    public BloodCollectionAdapter(AbsListView view, Collection<Collectinfo> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, Collectinfo collectinfo, boolean b,final int position) {
        adapterHolder.setText(R.id.bloodcollection_name,collectinfo.getReg_name());
        adapterHolder.setText(R.id.bloodcollection_time,collectinfo.getSendtime());
        adapterHolder.setText(R.id.bloodcollection_ordername,collectinfo.getOrdername());
    }
}
