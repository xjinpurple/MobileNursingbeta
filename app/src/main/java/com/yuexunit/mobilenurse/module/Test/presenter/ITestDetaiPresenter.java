package com.yuexunit.mobilenurse.module.Test.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestDetaiPresenter {
    /*
    化验单详情列表
     */
    public void showTestDetail(String requestno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
