package com.yuexunit.mobilenurse.module.NursingDocuments.model;

import java.util.Map;

import rx.Observable;

/**
 * Created by work-jx on 2016/10/13.
 */
public interface IDocumentRecordDetailModel {
    Observable<String> uploadRecord(Map<String, String> praise);
}
