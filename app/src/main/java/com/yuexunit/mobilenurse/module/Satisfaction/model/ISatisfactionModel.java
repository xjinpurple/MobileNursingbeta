package com.yuexunit.mobilenurse.module.Satisfaction.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/6/1.
 */
public interface ISatisfactionModel{
    /**
     * 满意度调查列表
     */
    Observable<String> getSatisfactionListData();

    /**
     * 提交满意度
     */
    Observable<String> commitSatisfactionData(String PJBBH,String PJXBH,String PFXBH,String MZBRBH,String ZYBRBH,String PFSJ);

    /**
     * 判断是否提交过满意度
     */
    Observable<String> IsSatisfactionCommitDate(String BRBZ);
}
