package com.yuexunit.mobilenurse.module.PatientDetail.presenter;

/**
 * Created by sslcjy on 16/1/24.
 */
public interface IPatientDetailPresenter {

    /**
     * 获取病人详情信息
     */
    void showPatientDetail(String Visitno);

    /**
     * 执行采血操作
     */
    void collect(String Visitno,String Code,String NurseId);

    //获取病人体征项
    public void getSingleTypes(String visitno);
}
