package com.yuexunit.mobilenurse.module.Satisfaction.bean;

import java.util.List;

/**
 * Created by work-jx on 2016/6/6.
 */
public class SatisfactionLastBean {
    private String PJBBH;
    private String PJXBH;
    private String PJXNR;
    private List<SatisfactionPFXBean> satisfactionPFXBeans;

    public String getPJBBH() {
        return PJBBH;
    }

    public void setPJBBH(String PJBBH) {
        this.PJBBH = PJBBH;
    }

    public String getPJXBH() {
        return PJXBH;
    }

    public void setPJXBH(String PJXBH) {
        this.PJXBH = PJXBH;
    }

    public String getPJXNR() {
        return PJXNR;
    }

    public void setPJXNR(String PJXNR) {
        this.PJXNR = PJXNR;
    }

    public List<SatisfactionPFXBean> getSatisfactionPFXBeans() {
        return satisfactionPFXBeans;
    }

    public void setSatisfactionPFXBeans(List<SatisfactionPFXBean> satisfactionPFXBeans) {
        this.satisfactionPFXBeans = satisfactionPFXBeans;
    }
}
