package com.yuexunit.mobilenurse.module.SkinTest.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Chemical.model.impl.ChemicalModel;
import com.yuexunit.mobilenurse.module.SkinTest.model.ISkinTestModel;
import com.yuexunit.mobilenurse.util.CreateJson;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by work-jx on 2016/6/29.
 */
public class SkinTestModel implements ISkinTestModel{
    //记录Log
    private final Logger log = LoggerFactory.getLogger(ChemicalModel.class);

    @Override
    public Observable<String> getSkinTestDate(final String visitno) {
        return Observable.create(new Observable.OnSubscribe<Object>() {

            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT;

                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.SkinTest_Json(visitno));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht =  new HttpTransportSE(url,AppConfig.TIMEOUT);
//                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url + ",kmhs_st_info", "2", visitno, response.toString()));
                    isConnect = false;
                    if (response != null) {
                        JSONObject all, body;
                        try {
                            all = new JSONObject(response.toString());
                            body = all.getJSONObject("body");
                            result = body.getString("response");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                            subscriber.onError(e);
                        }
                        if ("null".equals(result))
                        {
                            subscriber.onError(new Exception());
                        }
                    } else {
                        subscriber.onError(new Exception());
                    }

                } catch (Exception e) {
                    count++;
                    log.error("Exception", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }

    @Override
    public Observable<String> getMedicineDate(final String ypid) {
        return Observable.create(new Observable.OnSubscribe<Object>() {

            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD;
                    String url = AppConfig.WEB_CONTENT;

                    SoapObject request = new SoapObject(nameSpace, methodName);
                    request.addProperty("XmlString", CreateJson.SkinTest_getcode_Json(ypid));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true;
                    HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                    Object response = null;
                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();
                        //记录传输到服务器及传回的数据
                        log.info(CreateJson.LOG_JSON(url + ",kmhs_st_batch", "2", ypid, response.toString()));
                        isConnect = false;
                        if (response != null) {
                            JSONObject all, body;
                            try {
                                all = new JSONObject(response.toString());
                                body = all.getJSONObject("body");
                                result = body.getString("response");
                            } catch (JSONException e) {
                                log.error("JSONException:", e);
                                subscriber.onError(e);
                            }
                            if ("null".equals(result)) {
                                subscriber.onError(new Exception());
                            }
                        } else {
                            subscriber.onError(new Exception());
                        }

                    } catch (Exception e) {
                        count++;
                        log.error("Exception", e);
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                } while (isConnect && count < 5);
                    subscriber.onNext(response);
                }
            }

            ).

            map(new Func1<Object, String>() {
                @Override
                public String call (Object response){
                    return (String) response.toString();
                }
            }

            );
        }

        @Override
    public Observable<String> execSkinTest(final String sqdh,final String czry,final String kssj,final String pssc,final String pcnm,final String psph) {
        return Observable.create(new Observable.OnSubscribe<Object>() {

            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;

                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT_EXEC;

                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.SkinTest_Exec_Json(sqdh,czry,kssj,pssc,pcnm,psph));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht =  new HttpTransportSE(url,AppConfig.TIMEOUT);
                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url+",kmhs_st_exec", "2", sqdh+","+czry+""+kssj+""+pssc+""+pcnm+""+psph, response.toString()));
                    if (response != null) {
                        JSONObject all, body;
                        try {
                            all = new JSONObject(response.toString());
                            body = all.getJSONObject("body");
//                            result = body.getString("response");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                            subscriber.onError(e);
                        }
//                        if ("null".equals(result))
//                        {
//                            subscriber.onError(new Exception());
//                        }
                    } else {
                        subscriber.onError(new Exception());
                    }

                } catch (Exception e) {
                    log.error("Exception", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                subscriber.onNext(response);
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }
}
