package com.yuexunit.mobilenurse.module.Transportation.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Transportation.bean.TransportationRecord_Opsinfo;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2016/9/29.
 */
public class SelectOperAdapter extends KJAdapter<TransportationRecord_Opsinfo> {

    public SelectOperAdapter(AbsListView view, Collection<TransportationRecord_Opsinfo> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, TransportationRecord_Opsinfo item, boolean isScrolling) {
        adapterHolder.setText(R.id.item_selectoper, item.getOpsname());
    }
}
