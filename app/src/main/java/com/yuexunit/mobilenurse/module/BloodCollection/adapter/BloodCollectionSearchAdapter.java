package com.yuexunit.mobilenurse.module.BloodCollection.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.BloodCollection.bean.Specinfo;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by work-jx on 2017/5/4.
 */
public class BloodCollectionSearchAdapter extends KJAdapter<Specinfo>{
    public BloodCollectionSearchAdapter(AbsListView view, Collection<Specinfo> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, Specinfo specinfo, boolean b,final int position) {
        adapterHolder.setText(R.id.bloodcollection_search_name,specinfo.getReg_name());
        adapterHolder.setText(R.id.bloodcollection_search_ordername,specinfo.getPrn_name());
        if(Integer.valueOf(specinfo.getState()) >= 5)
        {
            adapterHolder.setText(R.id.bloodcollection_search_time,specinfo.getSend_time());
            adapterHolder.setText(R.id.bloodcollection_search_state,"已发送");
        }
    }
}
