package com.yuexunit.mobilenurse.module.SkinTest.bean;

/**
 * Created by work-jx on 2016/6/28.
 */
public class SkinTestDetailBean {

    //病人姓名
    private String hzxm;
    //病人年龄
    private String hznl;
    //病人性别
    private String hzxb;
    //病人id
    private String hzid;
    //皮试药品id
    private String ypid;
    //皮试药品名称
    private String psypmc;
    //申请单号
    private String sqdh;
    //申请医生
    private String sqys;
    //申请时间
    private String sqsj;
    //申请单状态
    private String sqdzt;
    //皮试默认时长
    private String mrsc;

    public String getYpid() {
        return ypid;
    }

    public void setYpid(String ypid) {
        this.ypid = ypid;
    }

    public String getMrsc() {
        return mrsc;
    }

    public void setMrsc(String mrsc) {
        this.mrsc = mrsc;
    }

    public String getHzxm() {
        return hzxm;
    }

    public void setHzxm(String hzxm) {
        this.hzxm = hzxm;
    }

    public String getHznl() {
        return hznl;
    }

    public void setHznl(String hznl) {
        this.hznl = hznl;
    }

    public String getHzxb() {
        return hzxb;
    }

    public void setHzxb(String hzxb) {
        this.hzxb = hzxb;
    }

    public String getHzid() {
        return hzid;
    }

    public void setHzid(String hzid) {
        this.hzid = hzid;
    }

    public String getPsypmc() {
        return psypmc;
    }

    public void setPsypmc(String psypmc) {
        this.psypmc = psypmc;
    }

    public String getSqdh() {
        return sqdh;
    }

    public void setSqdh(String sqdh) {
        this.sqdh = sqdh;
    }

    public String getSqys() {
        return sqys;
    }

    public void setSqys(String sqys) {
        this.sqys = sqys;
    }

    public String getSqsj() {
        return sqsj;
    }

    public void setSqsj(String sqsj) {
        this.sqsj = sqsj;
    }

    public String getSqdzt() {
        return sqdzt;
    }

    public void setSqdzt(String sqdzt) {
        this.sqdzt = sqdzt;
    }
}
