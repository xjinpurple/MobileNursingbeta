package com.yuexunit.mobilenurse.module.HealthArticles.presenter;

/**
 * Created by sslcjy on 16/1/25.
 */
public interface IArticlePresenter {
    /**
     * 显示健康宣教文章列表
     */
    void showArticleList(String areaId);

    /**
     * 显示健康宣教文章内容
     */
    void showArticleContent(String articleId);
}
