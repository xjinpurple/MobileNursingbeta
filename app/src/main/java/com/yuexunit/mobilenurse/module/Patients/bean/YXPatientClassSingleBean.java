package com.yuexunit.mobilenurse.module.Patients.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/10/9.
 */
public class YXPatientClassSingleBean {
    public YXPatientClassSingleBean body;

    //交易返回区(head)
    public Head head;
    //数据返回区(response)
    public PatientClassSingle_Response response;

    public YXPatientClassSingleBean getBody() {
        return body;
    }

    public void setBody(YXPatientClassSingleBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public PatientClassSingle_Response getResponse() {
        return response;
    }

    public void setResponse(PatientClassSingle_Response response) {
        this.response = response;
    }
}
