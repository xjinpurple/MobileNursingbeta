package com.yuexunit.mobilenurse.module.SignInput.model.impl;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.SignInput.api.AddSignApi;
import com.yuexunit.mobilenurse.module.SignInput.model.IAddSignModel;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.slf4j.Logger;

import java.util.Map;

import rx.Observable;

/**
 * Created by work-jx on 2016/5/12.
 */
public class AddSignModel implements IAddSignModel{
    private AddSignApi api;
    //记录Log
    private final Logger log = ProUtil.getLogger(AddSignModel.class);

    @Override
    public Observable<ActionBean> addsign(Map<String, String> praise) {
        api = ApiInstance();
        return api.addsinglesign(praise);
    }

    public AddSignApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi_new(AddSignApi.class);
        }
    }
}
