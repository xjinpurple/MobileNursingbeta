package com.yuexunit.mobilenurse.module.Inspect.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/11.
 */
public class InjectionDetail_Exrptdetail {

    //报告单
    public long reportno;
    //诊断表现
    public String mani;
    //诊断结论
    public String conclusion;
    //报告时间
    @JSONField(format = "yyyy-MM-dd")
    public Date reporttime;
    //报告人员工号
    public int entryid;
    //报告人员姓名
    public String entryname;

    public long getReportno() {
        return reportno;
    }

    public void setReportno(long reportno) {
        this.reportno = reportno;
    }

    public String getMani() {
        return mani;
    }

    public void setMani(String mani) {
        this.mani = mani;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public Date getReporttime() {
        return reporttime;
    }

    public void setReporttime(Date reporttime) {
        this.reporttime = reporttime;
    }

    public int getEntryid() {
        return entryid;
    }

    public void setEntryid(int entryid) {
        this.entryid = entryid;
    }

    public String getEntryname() {
        return entryname;
    }

    public void setEntryname(String entryname) {
        this.entryname = entryname;
    }
}
