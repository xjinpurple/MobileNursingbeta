package com.yuexunit.mobilenurse.module.Login.bean;

/**
 * Created by work-jx on 2015/12/19.
 */
public class SystemConfigDate {
    private String sysvar_id;
    private String name;
    private String type;
    private String value;

    public String getSysvar_id() {
        return sysvar_id;
    }

    public void setSysvar_id(String sysvar_id) {
        this.sysvar_id = sysvar_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
