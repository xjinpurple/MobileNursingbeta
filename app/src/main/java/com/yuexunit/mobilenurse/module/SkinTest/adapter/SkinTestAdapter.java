package com.yuexunit.mobilenurse.module.SkinTest.adapter;

import android.widget.AbsListView;
import android.widget.CheckBox;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.SkinTest.bean.SkinTestCheckBean;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by work-jx on 2016/6/28.
 */
public class SkinTestAdapter extends KJAdapter<SkinTestCheckBean>{
    public ArrayList<SkinTestCheckBean> arrayList = new ArrayList<SkinTestCheckBean>();

    public SkinTestAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        arrayList  =(ArrayList<SkinTestCheckBean>)mDatas;
    }

    @Override
    public void convert(AdapterHolder adapterHolder, SkinTestCheckBean skinTestCheckBean, boolean b) {
        adapterHolder.setText(R.id.skintest_name,skinTestCheckBean.getPsypmc());
        adapterHolder.setText(R.id.skintest_doctor, skinTestCheckBean.getSqys());

        final CheckBox skintest_check = adapterHolder.getView(R.id.skintest_check);


        if (skinTestCheckBean.ischeck()) {
            skintest_check.setChecked(true);
        } else {
            skintest_check.setChecked(false);
        }
    }

    public ArrayList<SkinTestCheckBean> getArrayList() {
        return arrayList;
    }
}
