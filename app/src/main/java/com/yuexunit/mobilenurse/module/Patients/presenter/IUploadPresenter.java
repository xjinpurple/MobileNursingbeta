package com.yuexunit.mobilenurse.module.Patients.presenter;

import com.yuexunit.mobilenurse.module.SignInput.bean.All_Sqlite_SignInputBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/3/25.
 */
public interface IUploadPresenter {
    /**
     * 一键上传功能
     */
    void oneKeyUpload(ArrayList<All_Sqlite_SignInputBean> list);

    /**
     *  病人体征
     */
    void showPatientSign(ArrayList<All_Sqlite_SignInputBean> list, ArrayList<SignsInput_Data>  allTypes);
}
