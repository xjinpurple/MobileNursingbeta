package com.yuexunit.mobilenurse.module.Inspect.ui.view;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectDetailView {
    //检查单详情
    void showInspectDetail(String content,String judge,String reportno);
}
