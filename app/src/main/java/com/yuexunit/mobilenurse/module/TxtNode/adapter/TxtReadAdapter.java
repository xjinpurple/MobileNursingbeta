package com.yuexunit.mobilenurse.module.TxtNode.adapter;

import android.view.View;
import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.TxtNode.bean.TxtWirteBean;
import com.yuexunit.mobilenurse.module.TxtNode.ui.Act_TxtRead;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by work-jx on 2016/6/19.
 */
public class TxtReadAdapter extends KJAdapter<TxtWirteBean>{
    private ArrayList<TxtWirteBean> txtWirteBeans = new ArrayList<TxtWirteBean>();
    private Act_TxtRead activity;

    public TxtReadAdapter(Act_TxtRead act_txtRead,AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        activity = act_txtRead;
        txtWirteBeans = (ArrayList<TxtWirteBean>) ((ArrayList)mDatas).clone();
    }

    @Override
    public void convert(AdapterHolder adapterHolder, TxtWirteBean item, boolean isScrolling) {
        adapterHolder.setText(R.id.txtread_name,item.getName());
        adapterHolder.setText(R.id.txtread_hospital,item.getVisitno());
        adapterHolder.setText(R.id.txtread_bed,item.getBedcode());
        adapterHolder.setText(R.id.txtread_content, item.getTxtcontent());

        onPicClick(adapterHolder.getView(R.id.txtread_delete), item);
    }

    private void onPicClick(View view, final TxtWirteBean item) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.deletenote(item);
            }
        });
    }
}
