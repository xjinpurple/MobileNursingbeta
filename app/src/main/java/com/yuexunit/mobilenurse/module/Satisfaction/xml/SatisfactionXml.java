package com.yuexunit.mobilenurse.module.Satisfaction.xml;

import android.util.Xml;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactiondetailBean;

import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by work-jx on 2016/6/6.
 */
public class SatisfactionXml {

    public static SatisfactionBean getSatisfactionData(String s)
    {
        if(s == null)
            return null;
        XmlPullParser parser = Xml.newPullParser();
        SatisfactionBean satisfactionBean = null;
        SatisfactiondetailBean satisfactiondetailBean =null;
        List<SatisfactiondetailBean> list_detail = null;
        ByteArrayInputStream tInputStringStream = null;
        try
        {
            if (s != null && !s.trim().equals("")) {
                tInputStringStream = new ByteArrayInputStream(s.getBytes());
            }
        }
        catch (Exception e) {
        }
        try
        {
            parser.setInput(tInputStringStream,"utf-8");

            int type = parser.getEventType();
            while(type != XmlPullParser.END_DOCUMENT)
            {
                switch (type)
                {
                    case XmlPullParser.START_TAG:
                        if("body".equals(parser.getName()))
                        {
                        }else if("response".equals(parser.getName()))
                        {
                        }else if("ret_code".equals(parser.getName()))
                        {
                            list_detail = new ArrayList<SatisfactiondetailBean>();
                            satisfactionBean =new SatisfactionBean();
                            satisfactionBean.setRet_code(parser.nextText());
                        }else if("ret_info".equals(parser.getName()))
                        {
                            satisfactionBean.setRet_info(parser.nextText());
                        }else if("patinfo".equals(parser.getName()))
                        {
                           satisfactiondetailBean = new SatisfactiondetailBean();
                        }
                        else  if("PJBBH".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPJBBH(parser.nextText());
                        }
                        else  if("PJXBH".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPJXBH(parser.nextText());
                        }
                        else  if("PJXNR".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPJXNR(parser.nextText());
                        }
                        else  if("PFXBH".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPFXBH(parser.nextText());
                        }
                        else  if("PFXNR".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPFXNR(parser.nextText());
                        }
                        else  if("PFXMR".equals(parser.getName()))
                        {
                            satisfactiondetailBean.setPFXMR(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if("patinfo".equals(parser.getName()))
                        {
                            list_detail.add(satisfactiondetailBean);
                            satisfactiondetailBean = null;
                        }
                        else if("response".equals(parser.getName()))
                        {
                            satisfactionBean.setSatisfactiondetailBean_list(list_detail);
                        }
                        break;

                    default:
                        break;
                }
                type = parser.next();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return satisfactionBean;
    }
}
