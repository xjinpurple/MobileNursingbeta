package com.yuexunit.mobilenurse.module.Inspect.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectPresenter {
    /*
    检查单列表
     */
    public void showInspectList(String visitno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
