package com.yuexunit.mobilenurse.module.Inspect.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/11.
 */
public class InjectionList_Exreqlist {

    //申请单号
    public long requestno;
    //加急标志
    public String emergflag;
    //状态代码
    public String statuscode;
    //状态名称
    public String statusname;
    //检验医嘱代码
    public int classcode;
    //检验医嘱名称
    public String classname;
    //开单医生
    public String doctorid;
    //开单医生姓名
    public String doctorname;
    //开单时间
    @JSONField(format = "yyyy-MM-dd")
    public Date entrytime;
    //病人科室
    public String deptcode;
    //病人科室名称
    public String deptname;
    //病人病区
    public String wardcode;
    //病区名称
    public String wardname;

    public long getRequestno() {
        return requestno;
    }

    public void setRequestno(long requestno) {
        this.requestno = requestno;
    }

    public String getEmergflag() {
        return emergflag;
    }

    public void setEmergflag(String emergflag) {
        this.emergflag = emergflag;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public int getClasscode() {
        return classcode;
    }

    public void setClasscode(int classcode) {
        this.classcode = classcode;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(String doctorid) {
        this.doctorid = doctorid;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public Date getEntrytime() {
        return entrytime;
    }

    public void setEntrytime(Date entrytime) {
        this.entrytime = entrytime;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getWardcode() {
        return wardcode;
    }

    public void setWardcode(String wardcode) {
        this.wardcode = wardcode;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }
}
