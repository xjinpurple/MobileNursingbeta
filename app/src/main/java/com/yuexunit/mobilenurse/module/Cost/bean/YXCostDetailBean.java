package com.yuexunit.mobilenurse.module.Cost.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//病人费用清单查询
public class YXCostDetailBean {
    public YXCostDetailBean body;
    public Head head;
    public CostDetail_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public CostDetail_Response getResponse() {
        return response;
    }

    public void setResponse(CostDetail_Response response) {
        this.response = response;
    }

    public YXCostDetailBean getBody() {
        return body;
    }

    public void setBody(YXCostDetailBean body) {
        this.body = body;
    }
}
