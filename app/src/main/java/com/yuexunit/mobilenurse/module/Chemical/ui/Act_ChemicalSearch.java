package com.yuexunit.mobilenurse.module.Chemical.ui;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.EditText;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/17.
 */
public class Act_ChemicalSearch extends KJActivity {
    @Bind(R.id.chemicalsearch_edt)
    EditText chemicalsearchEdt;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_chemiaclsearch);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            unregisterReceiver(PatientReceiver);
        } catch (IllegalArgumentException e) {

        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(PatientReceiver);
        } catch (IllegalArgumentException e) {

        }
    }

    @OnClick({R.id.chemicalsearch_img_back, R.id.chemicalsearch_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chemicalsearch_img_back:
                finish();
                break;
            case R.id.chemicalsearch_btn:
                if(chemicalsearchEdt.getText().toString().length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE))) {
                    String type_tag = chemicalsearchEdt.getText().toString().substring(8, 9);
                        if (type_tag.equals(AppConfig.TAG_INJECTION)) {
                            Intent intent_new = new Intent();
                            intent_new.setClass(aty, Act_ChemicalDetail.class);
                            intent_new.putExtra("barcode", chemicalsearchEdt.getText().toString());
//                    intent_new.putExtra("barcode", "20160628100176");
                            startActivity(intent_new);
                            chemicalsearchEdt.setText("");
                        } else {
                            ViewInject.toast("非化药条码");
                        }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
                break;
        }
    }

    //注册条码Receiver
    private void Register_Receiver()
    {
        //注册条码Receiver
        IntentFilter scanDataIntentFilter = new IntentFilter();
        scanDataIntentFilter.addAction("com.android.scancontext");
        scanDataIntentFilter.addAction("com.android.scanservice.scancontext");
        registerReceiver(PatientReceiver, scanDataIntentFilter);

        if(!isServiceRunning("com.android.scanservice.ScanService")) {
            ViewInject.toast("未检测到扫描服务");
        }
    }

    //条码Receiver
    private BroadcastReceiver PatientReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent){
            String action = intent.getAction();
            int keycode = intent.getIntExtra("Scan_Keycode", 0);
            if (action.equals("com.android.scanservice.scancontext")) {
                String result = intent.getStringExtra("Scan_context");
                String type = intent.getStringExtra("Scan_type");
                if (result.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_PTSM_RULE)))
                {
                    String type_tag = result.substring(8, 9);
                    if(type_tag.equals(AppConfig.TAG_INJECTION)) {
                        Intent intent_new = new Intent();
                        intent_new.setClass(aty, Act_ChemicalDetail.class);
                        intent_new.putExtra("barcode", result);
                        startActivity(intent_new);
                        chemicalsearchEdt.setText("");
                    }
                    else {
                        ViewInject.toast("非化药条码");
                    }
                }
                else{
                    ViewInject.toast("非化药条码");
                }
            }
        }
    };

    // 检查Service是否运行
    private boolean isServiceRunning(String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager.getRunningServices(100);

        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }
}
