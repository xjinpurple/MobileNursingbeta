package com.yuexunit.mobilenurse.module.TxtNode.ui;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.TxtNode.bean.TxtWirteBean;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.KJDB;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/17.
 */
public class Act_TxtWirte extends KJActivity {
    @Bind(R.id.txtwrite_edt)
    EditText txtwriteEdt;
    @Bind(R.id.patient_name)
    TextView patientName;
    @Bind(R.id.patient_bedno)
    TextView patientBedno;
    @Bind(R.id.patient_visitno)
    TextView patientVisitno;

    //护士id
    private String nurse_id;
    //病人姓名，床位号，住院号
    private String name, bedcode, visit_no;

    KJDB kjdb;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_txtwrite);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        nurse_id = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID);
        name = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME);
        bedcode = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE);
        visit_no = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);

        set_title();
    }

    private void set_title()
    {
        patientName.setText(name);
        patientBedno.setText(bedcode);
        patientVisitno.setText(visit_no);
    }


    private void savetxt() {
        TxtWirteBean txtWirteBean = new TxtWirteBean();
        txtWirteBean.setName(name);
        txtWirteBean.setNurseid(nurse_id);
        txtWirteBean.setBedcode(bedcode);
        txtWirteBean.setVisitno(visit_no);
        txtWirteBean.setTxtcontent(txtwriteEdt.getText().toString());

        kjdb = KJDB.create();
        kjdb.save(txtWirteBean);
        ViewInject.toast("保存成功");
        finish();
    }

    @OnClick({R.id.txtwrite_img_back, R.id.txtwirte_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtwrite_img_back:
                finish();
                break;
            case R.id.txtwirte_btn:
                savetxt();
                break;
        }
    }
}
