package com.yuexunit.mobilenurse.module.SignInput.ui.view;

import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignInput {
    /**
     * 显示基本体征项
     */
    void showSignInputType(ArrayList<Sign_Single> baselist);

    /**
     * 上传体征信息内容
     */
    void uploadSignValue(String buildItem);
}
