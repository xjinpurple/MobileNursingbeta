package com.yuexunit.mobilenurse.module.Satisfaction.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Satisfaction.model.ISatisfactionModel;
import com.yuexunit.mobilenurse.util.CreateJson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by work-jx on 2016/6/1.
 */
public class SatisfactionModel implements ISatisfactionModel{
    //记录Log
    private final Logger log = LoggerFactory.getLogger(SatisfactionModel.class);

    @Override
    public Observable<String> getSatisfactionListData() {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD_TWO;
                String url = AppConfig.WEB_CONTENT;
                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", AppConfig.TEST_WEBSERVICE);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht = new HttpTransportSE(url,AppConfig.TIMEOUT);
//                Object response = null;

                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();

                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url + ",getSatisfactionListData", "2", AppConfig.TEST_WEBSERVICE, response.toString()));
                    isConnect = false;
                    if (response == null) {
                        subscriber.onError(new Exception());
                    }
                }
                catch (Exception e)
                {
                    count++;
                    log.error("Exception", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }

    @Override
    public Observable<String> commitSatisfactionData(final String PJBBH,final String PJXBH, final String PFXBH, final String MZBRBH,final String ZYBRBH,final String PFSJ) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD_TWO;
                    String url = AppConfig.WEB_CONTENT;
                    SoapObject request = new SoapObject(nameSpace, methodName);
                    request.addProperty("XmlString", AppConfig.SATISFACTION_COMMIT(PJBBH, PJXBH, PFXBH, MZBRBH, ZYBRBH, PFSJ));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true;
                    HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                    Object response = null;

                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();

                        //记录传输到服务器及传回的数据
                        log.info(CreateJson.LOG_JSON(url + ",commitSatisfactionData", "2", AppConfig.SATISFACTION_COMMIT(PJBBH, PJXBH, PFXBH, MZBRBH, ZYBRBH, PFSJ), response.toString()));
                        isConnect = false;
                        if (response == null) {
                            subscriber.onError(new Exception());
                        }
                    } catch (Exception e) {
                        count++;
                        log.error("Exception", e);
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                } while (isConnect && count < 5);
                    subscriber.onNext(response);
                }
            }

            ).

            map(new Func1<Object, String>() {
                    @Override
                    public String call(Object response) {
                        return (String) response.toString();
                    }
                }

            );
        }

        @Override
    public Observable<String> IsSatisfactionCommitDate(final String BRBZ) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                    String nameSpace = AppConfig.WEB_NAME_SPACE;
                    String methodName = AppConfig.WEB_NAME_METHOD_TWO;
                    String url = AppConfig.WEB_CONTENT;
                    SoapObject request = new SoapObject(nameSpace, methodName);
                    request.addProperty("XmlString", AppConfig.Is_Commit_Satisfaction(BRBZ));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.bodyOut = request;
                    envelope.dotNet = true;
                    HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                    Object response = null;

                    try {
                        ht.call(nameSpace + methodName, envelope);
                        response = envelope.getResponse();

                        //记录传输到服务器及传回的数据
                        log.info(CreateJson.LOG_JSON(url + ",IsSatisfactionCommitDate", "2", AppConfig.Is_Commit_Satisfaction(BRBZ), response.toString()));
                        isConnect = false;
                        if (response == null) {
                            subscriber.onError(new Exception());
                        }
                    } catch (Exception e) {
                        count++;
                        log.error("Exception", e);
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                } while (isConnect && count < 5);
                    subscriber.onNext(response);
                }
            }

            ).

            map(new Func1<Object, String>() {
                @Override
                public String call (Object response){
                    return (String) response.toString();
                }
            }

            );
        }

        }
