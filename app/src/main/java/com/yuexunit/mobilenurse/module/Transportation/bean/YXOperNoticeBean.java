package com.yuexunit.mobilenurse.module.Transportation.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/12/2.
 */
public class YXOperNoticeBean {
    public YXOperNoticeBean body;
    public Head head;
    public OperNotice_Response response;

    public YXOperNoticeBean getBody() {
        return body;
    }

    public void setBody(YXOperNoticeBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public OperNotice_Response getResponse() {
        return response;
    }

    public void setResponse(OperNotice_Response response) {
        this.response = response;
    }
}
