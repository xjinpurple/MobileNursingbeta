package com.yuexunit.mobilenurse.module.SignQuery.adapter;

import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.SignQuery.bean.SignsInput_Data;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by 姚平 on 2015/11/30.
 */
public class AddTypeAdapter extends KJAdapter<SignsInput_Data> {


    public ArrayList<SignsInput_Data> arrayList = new ArrayList<SignsInput_Data>();
    private ArrayList<CheckType> list;
    private CheckType a;

    public AddTypeAdapter(AbsListView view, Collection<SignsInput_Data> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        initData(((ArrayList<SignsInput_Data>) mDatas).size());
    }

    @Override
    public void convert(AdapterHolder adapterHolder, SignsInput_Data signsInput_data, boolean b) {

    }

    /**
     * 存储刚开始各个体征中的checkbox选中情况
     */
    private void initData(int size) {
        list = new ArrayList<CheckType>();
        for (int i = 0; i < size; i++) {
            a = new CheckType(i + "号位", CheckType.TYPE_NOCHECKED);
            list.add(a);
        }
    }

    @Override
    public void convert(AdapterHolder adapterHolder, final SignsInput_Data signsInput_data, boolean isScrolling, final int position) {
        super.convert(adapterHolder, signsInput_data, isScrolling, position);

        adapterHolder.setText(R.id.type_name, signsInput_data.getName());
        final CheckBox type_check = adapterHolder.getView(R.id.type_check);
        type_check.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type_check.isChecked()) {
                    arrayList.add(signsInput_data);
                    list.get(position).type = CheckType.TYPE_CHECKED;
                } else {
                    if (arrayList.contains(signsInput_data)) {
                        arrayList.remove(signsInput_data);
                        list.get(position).type = CheckType.TYPE_NOCHECKED;
                    }
                }
            }
        });
        if (list.get(position).type == CheckType.TYPE_CHECKED) {
            type_check.setChecked(true);
        } else {
            type_check.setChecked(false);
        }
    }

    public ArrayList<SignsInput_Data> getArrayList() {
        return arrayList;
    }

    class CheckType {
        public static final int TYPE_CHECKED = 1;
        public static final int TYPE_NOCHECKED = 0;
        String name;
        int type;

        public CheckType(String name, int type) {
            this.name = name;
            this.type = type;
        }
    }
}
