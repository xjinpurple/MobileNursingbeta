package com.yuexunit.mobilenurse.module.DocAdvice.ui.view;

import com.yuexunit.mobilenurse.module.DocAdvice.bean.DocAdvice_Dispensing_New;
import com.yuexunit.mobilenurse.module.DocAdvice.bean.YXExecuteOrder;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/2/3.
 */
public interface IDispensingView {
    /**
     * 医嘱列表列表
     */
    void showDispensingList(ArrayList<DocAdvice_Dispensing_New> list);

    /**
     * 医嘱执行状态
     */
    void execDialog(int status,String message);

    /**
     * 医嘱执行信息返回
     */
    void getExecData(YXExecuteOrder executeOrderData);
}
