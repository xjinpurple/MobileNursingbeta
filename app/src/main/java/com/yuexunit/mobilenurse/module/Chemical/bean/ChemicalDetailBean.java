package com.yuexunit.mobilenurse.module.Chemical.bean;

/**
 * Created by work-jx on 2016/6/17.
 */
public class ChemicalDetailBean {
    //条码
    private String tm;
    //医嘱组号
    private String yzzh;
    //医嘱状态
    private String yzzt;
    //用法
    private String yf;
    //给药方式
    private String gyfs;
    //开嘱时间
    private String kzsj;
    //医生工号
    private String ysgh;
    //医生姓名
    private String ysxm;
    //药品代码
    private String ypdm;
    //药品名称
    private String ypmc;
    //剂量
    private String jl;
    //剂量单位
    private String jldw;
    //数量
    private String sl;
    //病人姓名
    private String brxm;
    //病人性别
    private String brxb;
    //病人年龄
    private String brnl;
    //床位号
    private String cwh;
    //住院号
    private String zyh;


    public String getTm() {
        return tm;
    }

    public void setTm(String tm) {
        this.tm = tm;
    }

    public String getYzzh() {
        return yzzh;
    }

    public void setYzzh(String yzzh) {
        this.yzzh = yzzh;
    }

    public String getYzzt() {
        return yzzt;
    }

    public void setYzzt(String yzzt) {
        this.yzzt = yzzt;
    }

    public String getYf() {
        return yf;
    }

    public void setYf(String yf) {
        this.yf = yf;
    }

    public String getGyfs() {
        return gyfs;
    }

    public void setGyfs(String gyfs) {
        this.gyfs = gyfs;
    }

    public String getKzsj() {
        return kzsj;
    }

    public void setKzsj(String kzsj) {
        this.kzsj = kzsj;
    }

    public String getYsgh() {
        return ysgh;
    }

    public void setYsgh(String ysgh) {
        this.ysgh = ysgh;
    }

    public String getYsxm() {
        return ysxm;
    }

    public void setYsxm(String ysxm) {
        this.ysxm = ysxm;
    }

    public String getYpdm() {
        return ypdm;
    }

    public void setYpdm(String ypdm) {
        this.ypdm = ypdm;
    }

    public String getYpmc() {
        return ypmc;
    }

    public void setYpmc(String ypmc) {
        this.ypmc = ypmc;
    }

    public String getJl() {
        return jl;
    }

    public void setJl(String jl) {
        this.jl = jl;
    }

    public String getJldw() {
        return jldw;
    }

    public void setJldw(String jldw) {
        this.jldw = jldw;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getBrxm() {
        return brxm;
    }

    public void setBrxm(String brxm) {
        this.brxm = brxm;
    }

    public String getBrxb() {
        return brxb;
    }

    public void setBrxb(String brxb) {
        this.brxb = brxb;
    }

    public String getBrnl() {
        return brnl;
    }

    public void setBrnl(String brnl) {
        this.brnl = brnl;
    }

    public String getCwh() {
        return cwh;
    }

    public void setCwh(String cwh) {
        this.cwh = cwh;
    }

    public String getZyh() {
        return zyh;
    }

    public void setZyh(String zyh) {
        this.zyh = zyh;
    }
}
