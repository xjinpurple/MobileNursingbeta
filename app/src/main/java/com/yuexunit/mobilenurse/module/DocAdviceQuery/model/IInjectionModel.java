package com.yuexunit.mobilenurse.module.DocAdviceQuery.model;

import rx.Observable;

/**
 * Created by work-jx on 2016/2/3.
 */
public interface IInjectionModel {
    /**
     * 获取医嘱列表信息
     */
    Observable getInjectionListData(String visitno);
}
