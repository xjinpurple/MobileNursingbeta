package com.yuexunit.mobilenurse.module.Panic.ui;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Panic.adapter.PanicAdapter;
import com.yuexunit.mobilenurse.module.Panic.bean.PanicBean;
import com.yuexunit.mobilenurse.widget.Dialog_Success;
import com.yuexunit.mobilenurse.widget.EmptyLayout;
import com.yuexunit.mobilenurse.widget.FooterLoadingLayout;
import com.yuexunit.mobilenurse.widget.PullToRefreshBase;
import com.yuexunit.mobilenurse.widget.PullToRefreshList;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/13.
 */
public class Act_Panic extends KJActivity {


    @Bind(R.id.nurse_name)
    TextView nurseName;
    @Bind(R.id.nurse_id)
    TextView nurseId;
    @Bind(R.id.titlebar_text_title)
    TextView titlebarTextTitle;
    @Bind(R.id.panic_list)
    PullToRefreshList panicList;
    @Bind(R.id.empty_layout)
    EmptyLayout emptyLayout;

    //病区号及病区名称
    private String Wardno, WardName;
    //护士相关信息
    private String NurseName, NurseId;

    private ListView mList;
    private ArrayList<PanicBean> panicBeans = new ArrayList<PanicBean>();
    private BaseAdapter adapter;

    private int position;
    Dialog_Success.Builder builder_success;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_panic);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        NurseId = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID);
        NurseName = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGNAME);
        WardName = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_NAME);
        Wardno = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_ID);
        listViewPreference();
        showNurseInfo(NurseId, NurseName, WardName);


        PanicBean panicBean = new PanicBean();
        panicBean.setAge(34);
        panicBean.setSex("1");
        panicBean.setBedcode("999");
        panicBean.setPanicinfo("CMK1234567");
        panicBean.setVisit_no("123456");
        panicBean.setName("叶思宇");

        panicBeans.add(panicBean);
        panicBeans.add(panicBean);
        panicBeans.add(panicBean);
        panicBeans.add(panicBean);

        adapter = new PanicAdapter(this,this,panicBeans);
        mList.setAdapter(adapter);
        emptyLayout.dismiss();
    }

    public void showNurseInfo(String NurseId, String NurseName, String WardName) {
        nurseId.setText(NurseId);
        nurseName.setText(NurseName);
        titlebarTextTitle.setText(WardName);
    }

    public void DeleteDate(int position)
    {
        this.position = position;
        showExecDialog();
    }

    public void showExecDialog()
    {
        builder_success = new Dialog_Success.Builder(this);
        builder_success.setMessage("是否删除?");
        builder_success.setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        panicBeans.remove(position);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

        builder_success.setNegativeButton("否",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder_success.create().show();
    }

    /**
     * 初始化ListView样式
     */
    private void listViewPreference() {
        mList = panicList.getRefreshView();
        mList.setDivider(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        mList.setOverscrollFooter(null);
        mList.setOverscrollHeader(null);
        mList.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        panicList.setPullLoadEnabled(false);//设置上拉刷新功能关闭
        panicList.setPullRefreshEnabled(false);//设置下拉刷新功能关闭
        ((FooterLoadingLayout) panicList.getFooterLoadingLayout())
                .setNoMoreDataText("已经是最新状态");

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        });

        panicList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(
                    PullToRefreshBase<ListView> refreshView) {
                AppConfig.ISOVER_PATIENT = false;
            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<ListView> refreshView) {
                panicList.setHasMoreData(false);
            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });
    }

    @OnClick(R.id.panic_back)
    public void onClick() {
        finish();
    }
}
