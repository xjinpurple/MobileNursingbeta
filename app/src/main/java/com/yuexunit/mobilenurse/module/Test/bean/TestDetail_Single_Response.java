package com.yuexunit.mobilenurse.module.Test.bean;

/**
 * Created by work-jx on 2015/12/22.
 */
public class TestDetail_Single_Response {
    public TestDetail_Asrptlist asrptlist;
    public TestDetail_Asrptdetail asrptdetail;

    public TestDetail_Asrptlist getAsrptlist() {
        return asrptlist;
    }

    public void setAsrptlist(TestDetail_Asrptlist asrptlist) {
        this.asrptlist = asrptlist;
    }

    public TestDetail_Asrptdetail getAsrptdetail() {
        return asrptdetail;
    }

    public void setAsrptdetail(TestDetail_Asrptdetail asrptdetail) {
        this.asrptdetail = asrptdetail;
    }
}
