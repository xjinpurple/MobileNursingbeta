package com.yuexunit.mobilenurse.module.Panic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Panic.bean.PanicBean;
import com.yuexunit.mobilenurse.module.Panic.ui.Act_Panic;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/6/15.
 */
public class PanicAdapter extends BaseAdapter{

    private Context context;
    private Act_Panic activity;
    private ArrayList<PanicBean> mDatas;

    public PanicAdapter(Act_Panic activity,Context context,ArrayList<PanicBean> mDatas){
        this.context = context;
        this.activity =activity;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_panic, parent, false);
            holder = new ViewHolder();
            holder.panic_pic = (ImageView)convertView.findViewById(R.id.panic_pic);
            holder.panic_name = (TextView)convertView.findViewById(R.id.panic_name);
            holder.panic_hospital = (TextView)convertView.findViewById(R.id.panic_hospital);
            holder.panic_bed = (TextView)convertView.findViewById(R.id.panic_bed);
            holder.panic_panic = (TextView)convertView.findViewById(R.id.panic_panic);
            holder.panic_delete = (TextView)convertView.findViewById(R.id.panic_delete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.panic_name.setText(mDatas.get(position).getName());
        holder.panic_hospital.setText(mDatas.get(position).getVisit_no());
        holder.panic_bed.setText(mDatas.get(position).getBedcode());
        holder.panic_panic.setText(mDatas.get(position).getPanicinfo());

        //设置病人头像
        if ("1".equals(mDatas.get(position).getSex()) && mDatas.get(position).getAge() > AppConfig.AGE_LIMIT) {
            holder.panic_pic.setImageResource(R.drawable.panic_men);
        } else if ("1".equals(mDatas.get(position).getSex()) && mDatas.get(position).getAge() < AppConfig.AGE_LIMIT) {
            holder.panic_pic.setImageResource(R.drawable.panic_boy);
        } else if ("2".equals(mDatas.get(position).getSex()) && mDatas.get(position).getAge() > AppConfig.AGE_LIMIT) {
            holder.panic_pic.setImageResource(R.drawable.panic_women);
        } else {
            holder.panic_pic.setImageResource(R.drawable.panic_girl);
        }

        holder.panic_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.DeleteDate(position);
            }
        });

        return convertView;
    }

    class ViewHolder{
        ImageView panic_pic;
        TextView panic_name,panic_hospital,panic_bed,panic_panic,panic_delete;
    }
}
