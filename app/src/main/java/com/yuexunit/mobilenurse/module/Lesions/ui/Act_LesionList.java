package com.yuexunit.mobilenurse.module.Lesions.ui;

import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.AppContext;
import com.yuexunit.mobilenurse.module.Lesions.adapter.LesionListAdapter;
import com.yuexunit.mobilenurse.module.Lesions.presenter.impl.LesionPresenter;
import com.yuexunit.mobilenurse.module.Lesions.ui.view.ILesionView;
import com.yuexunit.mobilenurse.module.Login.model.impl.LoginModel;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.kymjs.kjframe.KJActivity;
import org.slf4j.Logger;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sslcjy on 16/1/20.
 */
public class Act_LesionList extends KJActivity implements ILesionView{

    @Bind(R.id.lesionlist)
    ListView lesionList;

    public LesionListAdapter adapter;
    LesionPresenter presenter;
    //记录Log
    private final Logger log = ProUtil.getLogger(Act_LesionList.class);

    @Override
    public void setRootView() {
        setContentView(R.layout.act_lesionlist);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new LesionPresenter(this);
        presenter.showLesionList(((AppContext) getApplication()).getBqlist());
    }

    @Override
    public void showLesionList(ArrayList list) {
        log.info("Act_LesionList_showLesionList:"+list.toString());
        adapter = new LesionListAdapter(lesionList, list, R.layout.item_lesionlist,(Act_LesionList)aty);
        lesionList.setAdapter(adapter);
    }
}