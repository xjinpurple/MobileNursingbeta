package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/9/23.
 */
public class TransportationRecord_Response {
    private String opsinfo;

    public String getOpsinfo() {
        return opsinfo;
    }

    public void setOpsinfo(String opsinfo) {
        this.opsinfo = opsinfo;
    }
}
