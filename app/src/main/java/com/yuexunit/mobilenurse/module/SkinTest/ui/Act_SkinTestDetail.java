package com.yuexunit.mobilenurse.module.SkinTest.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.SkinTest.adapter.SkinTestAdapter;
import com.yuexunit.mobilenurse.module.SkinTest.bean.SkinTestCheckBean;
import com.yuexunit.mobilenurse.module.SkinTest.bean.SkinTestDetailBean;
import com.yuexunit.mobilenurse.module.SkinTest.model.impl.SkinTestModel;
import com.yuexunit.mobilenurse.module.SkinTest.presenter.ISkinTestPresenter;
import com.yuexunit.mobilenurse.module.SkinTest.presenter.impl.SkinTestPresenter;
import com.yuexunit.mobilenurse.module.SkinTest.ui.view.ISkinTestView;
import com.yuexunit.mobilenurse.receiver.SkinTestReceiver;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/27.
 */
public class Act_SkinTestDetail extends KJActivity implements ISkinTestView {

    @Bind(R.id.skintestdetail_code)
    TextView skintestdetailCode;
    @Bind(R.id.skintestdetail_recipe)
    TextView skintestdetailRecipe;
    @Bind(R.id.skintestdetail_visit)
    TextView skintestdetailVisit;
    @Bind(R.id.skintestdetail_sex)
    TextView skintestdetailSex;
    @Bind(R.id.skintestdetail_age)
    TextView skintestdetailAge;
    @Bind(R.id.skintestdetail_bedcode)
    TextView skintestdetailBedcode;
    @Bind(R.id.skintestdetail_visitno)
    TextView skintestdetailVisitno;
    @Bind(R.id.skintestdetail_doctor)
    TextView skintestdetailDoctor;
    @Bind(R.id.skintestdetail_list)
    ListView skintestdetailList;
    @Bind(R.id.skintestdetail_selecttime_btn_01)
    TextView skintestdetailSelecttimeBtn01;
    @Bind(R.id.skintestdetail_selecttime_btn_02)
    TextView skintestdetailSelecttimeBtn02;
    @Bind(R.id.skintestdetail_selecttime_btn_03)
    TextView skintestdetailSelecttimeBtn03;
    @Bind(R.id.skintestdetail_sex_name)
    TextView skintestdetailSexName;

    private BaseAdapter adapter;
    private ArrayList<SkinTestDetailBean> skinTestDetailBeans = new ArrayList<SkinTestDetailBean>();
    private ArrayList<SkinTestCheckBean> skinTestCheckBeans = new ArrayList<SkinTestCheckBean>();

    //当前选中项
    SkinTestCheckBean skinTestCheckBean = new SkinTestCheckBean();

    ISkinTestPresenter presenter;
    private LoadingDialog dialog;

    private int time = 0;

    //闹钟计数
    private SharedPreferences sharedPreferences;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_skintestdetail);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        presenter = new SkinTestPresenter(this, new SkinTestModel());
        dialog = new LoadingDialog(aty);
        sharedPreferences = this.getSharedPreferences("skintest", Context.MODE_WORLD_READABLE);
        presenter.showSkinTestDate(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
        Set_Data();
        skintestdetailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (SkinTestCheckBean bean : skinTestCheckBeans) {//全部设为未选中
                    bean.setIscheck(false);
                }
                skinTestCheckBeans.get(position).setIscheck(true);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @OnClick({R.id.skintestdetail_img_back, R.id.skintestdetail_selecttime_btn_01, R.id.skintestdetail_selecttime_btn_02, R.id.skintestdetail_selecttime_btn_03, R.id.skintestdetail_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.skintestdetail_img_back:
                finish();
                break;
            case R.id.skintestdetail_selecttime_btn_01:
                time = 15;
                skintestdetailSelecttimeBtn01.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_selected);
                skintestdetailSelecttimeBtn01.setTextColor(getResources().getColor(R.color.white));
                skintestdetailSelecttimeBtn02.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn02.setTextColor(getResources().getColor(R.color.test_05));
                skintestdetailSelecttimeBtn03.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn03.setTextColor(getResources().getColor(R.color.test_05));
                break;
            case R.id.skintestdetail_selecttime_btn_02:
                skintestdetailSelecttimeBtn01.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn01.setTextColor(getResources().getColor(R.color.test_05));
                skintestdetailSelecttimeBtn02.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_selected);
                skintestdetailSelecttimeBtn02.setTextColor(getResources().getColor(R.color.white));
                skintestdetailSelecttimeBtn03.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn03.setTextColor(getResources().getColor(R.color.test_05));
                time = 20;
                break;
            case R.id.skintestdetail_selecttime_btn_03:
                skintestdetailSelecttimeBtn01.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn01.setTextColor(getResources().getColor(R.color.test_05));
                skintestdetailSelecttimeBtn02.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_normal);
                skintestdetailSelecttimeBtn02.setTextColor(getResources().getColor(R.color.test_05));
                skintestdetailSelecttimeBtn03.setBackgroundResource(R.drawable.skintestdetail_selecttime_btn_selected);
                skintestdetailSelecttimeBtn03.setTextColor(getResources().getColor(R.color.white));
                time = 30;
                break;
            case R.id.skintestdetail_btn:

                for (int i = 0;i<skinTestCheckBeans.size();i++)
                {
                    if ((skinTestCheckBeans.get(i).ischeck()))
                    {
                        skinTestCheckBean = skinTestCheckBeans.get(i);
                    }
                }
                if(null!=skinTestCheckBean.getHzid())
                {
                    if(time != 0 ) {
//                    presenter.getMedicineDate(skinTestCheckBean.getYpid());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                        String str = formatter.format(curDate);
                        presenter.execSkinTestDate(skinTestCheckBean.getSqdh(),
                                PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID),
                                str, time + "", "", "");
                    }
                    else {
                        ViewInject.toast("请选择皮试时长");
                    }
                }
                else {
                    ViewInject.toast("请选择皮试内容");
                }
                break;
        }
    }

    @Override
    public void showSkinTestDate(ArrayList<SkinTestDetailBean> list) {
        for (int i = 0; i < list.size(); i++) {
            SkinTestCheckBean skinTestCheckBean = new SkinTestCheckBean();
            skinTestCheckBean.setHzid(list.get(i).getHzid());
            skinTestCheckBean.setHznl(list.get(i).getHznl());
            skinTestCheckBean.setHzxb(list.get(i).getHzxb());
            skinTestCheckBean.setHzxm(list.get(i).getHzxm());
            skinTestCheckBean.setYpid(list.get(i).getYpid());
            skinTestCheckBean.setPsypmc(list.get(i).getPsypmc());
            skinTestCheckBean.setSqdh(list.get(i).getSqdh());
            skinTestCheckBean.setSqdzt(list.get(i).getSqdzt());
            skinTestCheckBean.setSqsj(list.get(i).getSqsj());
            skinTestCheckBean.setSqys(list.get(i).getSqys());
            skinTestCheckBean.setMrsc(list.get(i).getMrsc());
            skinTestCheckBean.setIscheck(false);

            skinTestCheckBeans.add(skinTestCheckBean);
        }
        adapter = new SkinTestAdapter(skintestdetailList, skinTestCheckBeans, R.layout.item_skintestdetail);
        skintestdetailList.setAdapter(adapter);
    }

    private void Set_Data() {
        skintestdetailVisit.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        skintestdetailSex.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_SEX));
        skintestdetailAge.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_AGE));
        skintestdetailBedcode.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        skintestdetailVisitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    @Override
    public void loadingDialogStatus(int status) {
        switch (status) {
            case AppConfig.SHOW_DIALOG:
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                break;
            case AppConfig.DISMISS_DIALOG:
                dialog.dismiss();
                break;
        }
    }

    @Override
    public void getMedicineStatus(boolean issuccess) {
        if(issuccess){
//            presenter.execSkinTestDate();
        }else
        {
            ViewInject.toast("皮试执行失败");
        }
    }

    @Override
    public void ExecStatus(boolean issuccess) {
        if(issuccess){
            int count = sharedPreferences.getInt("count",0);
            count++;
            setReminder(true,count);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("count",count);
            editor.commit();
            ViewInject.toast("皮试执行成功");
        }
        else {
            ViewInject.toast("皮试执行失败");
        }
    }


    /**
     * Set the alarm
     *
     * @param b whether enable the Alarm clock or not
     */
    private void setReminder(boolean b,int barcode) {

        // get the AlarmManager instance
        AlarmManager am= (AlarmManager) getSystemService(ALARM_SERVICE);
        // create a PendingIntent that will perform a broadcast
//        Intent intent = new Intent(i+"");
//        PendingIntent pi= PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);
        Intent intent =new Intent(this,SkinTestReceiver.class);
        Bundle bundle = new Bundle();
        bundle.putString("id", barcode+"");
        bundle.putString("title", skinTestCheckBean.getPsypmc());
        bundle.putString("name", skinTestCheckBean.getHzxm());
        bundle.putString("subtxt", "皮试结束");
        intent.putExtras(bundle);
        PendingIntent pi= PendingIntent.getBroadcast(Act_SkinTestDetail.this, barcode, intent, 0);

        if(b){
            // just use current time + 10s as the Alarm time.
            Calendar c=Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
            //可以根据项目要求修改，秒、分钟、提前、延后
            c.add(Calendar.MINUTE, time);
            // schedule an alarm
            am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),pi);
        }
        else{
            // cancel current alarm
            am.cancel(pi);
        }

    }

}
