package com.yuexunit.mobilenurse.module.Test.bean;


import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//病人检验申请单列表
public class YXTestListBean {

    public YXTestListBean body;
    public Head head;
    public TestList_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public TestList_Response getResponse() {
        return response;
    }

    public void setResponse(TestList_Response response) {
        this.response = response;
    }

    public YXTestListBean getBody() {
        return body;
    }

    public void setBody(YXTestListBean body) {
        this.body = body;
    }
}
