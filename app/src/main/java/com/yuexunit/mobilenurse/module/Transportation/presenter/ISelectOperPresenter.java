package com.yuexunit.mobilenurse.module.Transportation.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/9/27.
 */
public interface ISelectOperPresenter {
    /*
   手术转运状态
    */
    public void showOpsList(String visitno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
