package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/12/2.
 */
public class OperNotice_OpsList {
    private String ARRANGE_OPS_TIME;
    private String VISIT_NO;
    private String PAT_NAME;
    private String DATE_OF_BIRTH;
    private String DEPT_NAME;
    private String EME_LEVEL;
    private String DOCTOR;
    private String NURSE1;
    private String NURSE2;
    private String NURSE3;
    private String OPS_REQUIRE;
    private String BED_CODE;
    private String DIAG;
    private String OPS_NAME;
    private String OPS_ROOM;


    public String getARRANGE_OPS_TIME() {
        return ARRANGE_OPS_TIME;
    }

    public void setARRANGE_OPS_TIME(String ARRANGE_OPS_TIME) {
        this.ARRANGE_OPS_TIME = ARRANGE_OPS_TIME;
    }

    public String getVISIT_NO() {
        return VISIT_NO;
    }

    public void setVISIT_NO(String VISIT_NO) {
        this.VISIT_NO = VISIT_NO;
    }

    public String getPAT_NAME() {
        return PAT_NAME;
    }

    public void setPAT_NAME(String PAT_NAME) {
        this.PAT_NAME = PAT_NAME;
    }

    public String getDATE_OF_BIRTH() {
        return DATE_OF_BIRTH;
    }

    public void setDATE_OF_BIRTH(String DATE_OF_BIRTH) {
        this.DATE_OF_BIRTH = DATE_OF_BIRTH;
    }

    public String getDEPT_NAME() {
        return DEPT_NAME;
    }

    public void setDEPT_NAME(String DEPT_NAME) {
        this.DEPT_NAME = DEPT_NAME;
    }

    public String getEME_LEVEL() {
        return EME_LEVEL;
    }

    public void setEME_LEVEL(String EME_LEVEL) {
        this.EME_LEVEL = EME_LEVEL;
    }

    public String getDOCTOR() {
        return DOCTOR;
    }

    public void setDOCTOR(String DOCTOR) {
        this.DOCTOR = DOCTOR;
    }

    public String getNURSE1() {
        return NURSE1;
    }

    public void setNURSE1(String NURSE1) {
        this.NURSE1 = NURSE1;
    }

    public String getNURSE2() {
        return NURSE2;
    }

    public void setNURSE2(String NURSE2) {
        this.NURSE2 = NURSE2;
    }

    public String getNURSE3() {
        return NURSE3;
    }

    public void setNURSE3(String NURSE3) {
        this.NURSE3 = NURSE3;
    }

    public String getOPS_REQUIRE() {
        return OPS_REQUIRE;
    }

    public void setOPS_REQUIRE(String OPS_REQUIRE) {
        this.OPS_REQUIRE = OPS_REQUIRE;
    }

    public String getBED_CODE() {
        return BED_CODE;
    }

    public void setBED_CODE(String BED_CODE) {
        this.BED_CODE = BED_CODE;
    }

    public String getDIAG() {
        return DIAG;
    }

    public void setDIAG(String DIAG) {
        this.DIAG = DIAG;
    }

    public String getOPS_NAME() {
        return OPS_NAME;
    }

    public void setOPS_NAME(String OPS_NAME) {
        this.OPS_NAME = OPS_NAME;
    }

    public String getOPS_ROOM() {
        return OPS_ROOM;
    }

    public void setOPS_ROOM(String OPS_ROOM) {
        this.OPS_ROOM = OPS_ROOM;
    }
}
