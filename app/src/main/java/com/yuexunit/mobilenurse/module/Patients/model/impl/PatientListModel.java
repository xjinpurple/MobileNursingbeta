package com.yuexunit.mobilenurse.module.Patients.model.impl;

import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Patients.api.PatientListApi;
import com.yuexunit.mobilenurse.module.Patients.model.IPatientListModel;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;
import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsInput;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.CreateJson;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by sslcjy on 16/1/22.
 */
public class PatientListModel implements IPatientListModel{
    private PatientListApi api;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(PatientListModel.class);

    @Override
    public Observable getPatientListData(final String Wardno) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT;
                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.Patlist_Json(Wardno));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true; //very important for compatibility
                envelope.bodyOut = request;
                HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url + ",mhs_patlist", "2", Wardno, response.toString()));
                    isConnect = false;
                } catch (Exception e) {
                    count++;
                    log.error("Exception:", e);
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
                subscriber.onCompleted();
            }

        }).filter(new Func1<Object, Boolean>() {
            @Override
            public Boolean call(Object response) {
                return response != null;
            }
        });
    }

    @Override
    public Observable getPatientClassifyListData(final String wardId, final String careLevel, final String isKF, final String isZS, final String isLAB) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT;
                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.PatlistClassify_Json(wardId,careLevel,isKF,isZS,isLAB));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true; //very important for compatibility
                envelope.bodyOut = request;
                HttpTransportSE ht = new HttpTransportSE(url, AppConfig.TIMEOUT);
//                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url + ",kmhs_pat_class", "2", wardId+","+careLevel+","+isKF+","+isZS+","+isLAB, response.toString()));
                    isConnect = false;
                } catch (Exception e) {
                    count++;
                    log.error("Exception:", e);
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
                subscriber.onCompleted();
            }

        }).filter(new Func1<Object, Boolean>() {
            @Override
            public Boolean call(Object response) {
                return response != null;
            }
        });
    }

    @Override
    public Observable<String> uploadTypesData(final Map<String, String> praise) {
        return Observable.create(new Observable.OnSubscribe<Object>() {

            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String result = null;
                Boolean isConnect = true;
                int count = 0;
                Object response = null;

                do {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT_EXEC;

                SoapObject request = new SoapObject(nameSpace, methodName);
                request.addProperty("XmlString", CreateJson.Sign_Json(praise.get("inpNo"),praise.get("timestamp"),praise.get("recorder"),praise.get("signData"),praise.get("timepoint")));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.bodyOut = request;
                envelope.dotNet = true;
                HttpTransportSE ht =  new HttpTransportSE(url,AppConfig.TIMEOUT);
//                Object response = null;
                try {
                    ht.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.info(CreateJson.LOG_JSON(url+",kmhs_enr_record", "2", praise.get("inpNo")+praise.get("timestamp")+praise.get("recorder")+praise.get("signData")+praise.get("timepoint"), response.toString()));
                    isConnect = false;
                    if (response != null) {
                        JSONObject all, body;
                        try {
                            all = new JSONObject(response.toString());
                            body = all.getJSONObject("body");
                            result = body.getString("response");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                            subscriber.onError(e);
                        }
                        if ("null".equals(result))
                        {
                            subscriber.onError(new Exception());
                        }
                    } else {
                        subscriber.onError(new Exception());
                    }

                } catch (Exception e) {
                    count++;
                    log.error("Exception", e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                } while (isConnect && count < 5);
                subscriber.onNext(response);
            }
        }).map(new Func1<Object, String>() {
            @Override
            public String call(Object response) {
                return (String) response.toString();
            }
        });
    }

    public PatientListApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi(PatientListApi.class);
        }
    }

    //获取基本体征项
    @Override
    public Observable<ArrayList<SignsInput_Data>> getBaseTypes(String areaId) {
        api = ApiInstance();
        return api.BaseTypes(areaId).filter(new Func1<YXSignsInput, Boolean>() {
            @Override
            public Boolean call(YXSignsInput yxSignsInput) {
                log.debug("getBaseTypes:"+yxSignsInput.toString());
                return yxSignsInput.getCode() == 200;
            }
        }).map(new Func1<YXSignsInput, ArrayList<SignsInput_Data>>() {

            @Override
            public ArrayList<SignsInput_Data> call(YXSignsInput yxSignsInput) {
                return yxSignsInput.getData();
            }
        });
    }

    //获取全部体征项
    @Override
    public Observable<ArrayList<SignsInput_Data>> getAllTypes() {
        api = ApiInstance();
        return api.AllTypes().map(new Func1<YXSignsInput, ArrayList<SignsInput_Data>>() {
            @Override
            public ArrayList<SignsInput_Data> call(YXSignsInput yxSignsInput) {
                log.debug("getAllTypes:"+yxSignsInput.toString());
                return yxSignsInput.getData();
            }
        });
    }
}
