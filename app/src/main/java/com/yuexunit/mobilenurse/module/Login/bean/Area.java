package com.yuexunit.mobilenurse.module.Login.bean;

/**
 * Created by hbprotoss on 12/1/15.
 */

//获取病区信息

public class Area {
    public Integer area_id;
    public String offical_id;
    public String name;
    public boolean enabled;
    public String digital_case_version;
    public int wd_type;

    public Integer getArea_id() {
        return area_id;
    }

    public void setArea_id(Integer area_id) {
        this.area_id = area_id;
    }

    public String getOffical_id() {
        return offical_id;
    }

    public void setOffical_id(String offical_id) {
        this.offical_id = offical_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDigital_case_version() {
        return digital_case_version;
    }

    public void setDigital_case_version(String digital_case_version) {
        this.digital_case_version = digital_case_version;
    }

    public int getWd_type() {
        return wd_type;
    }

    public void setWd_type(int wd_type) {
        this.wd_type = wd_type;
    }
}
