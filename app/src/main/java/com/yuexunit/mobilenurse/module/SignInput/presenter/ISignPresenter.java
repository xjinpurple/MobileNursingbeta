package com.yuexunit.mobilenurse.module.SignInput.presenter;

import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single_Check;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignPresenter {

    /**
     * 显示基本体征项列表
     * @param areaId
     */
    void showBaseTypes(String areaId);

    /**
     * 显示所有体征项列表
     * @param areaId
     */
    void showAllTypes(String areaId);

    /**
     * 组织要上传的体征项和体征值拼接的信息
     */
    void buildUploadSignValue(HashMap signValue,HashMap booldSignValue,ArrayList<Sign_Single_Check> all);

    /**
     * 上传体征数据信息
     */
    void uploadTypesInfo(Map<String, String> praise,String name);

    /**
     * 保存病人体征项
     */
    void SaveSingleSign(Map<String, String> praise);
}
