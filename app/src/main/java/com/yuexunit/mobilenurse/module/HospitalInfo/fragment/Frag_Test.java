package com.yuexunit.mobilenurse.module.HospitalInfo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.HospitalInfo.Act_HospitalInfo;
import com.yuexunit.mobilenurse.module.Test.adapter.TestAdapter;
import com.yuexunit.mobilenurse.module.Test.bean.TestList_Asreqlist;
import com.yuexunit.mobilenurse.module.Test.model.impl.TestModel;
import com.yuexunit.mobilenurse.module.Test.presenter.ITestPresenter;
import com.yuexunit.mobilenurse.module.Test.presenter.impl.TestPresenter;
import com.yuexunit.mobilenurse.module.Test.ui.view.ITestView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by work-jx on 2016/7/13.
 */
public class Frag_Test extends Fragment implements ITestView{
    @Bind(R.id.act_test_list)
    ListView actTestList;
    @Bind(R.id.empty_layout)
    EmptyLayout emptyLayout;

    private Act_HospitalInfo aty;

    private BaseAdapter adapter;
    //病人住院号
    private  String Visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Frag_Test.class);
    ITestPresenter presenter;

    protected View mMainView;
    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        aty = (Act_HospitalInfo)getActivity();
        mMainView = inflater.inflate(R.layout.act_test, container, false);
        ButterKnife.bind(this, mMainView);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter = new TestPresenter(this, new TestModel());
        Visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });
        if(SystemTool.checkNet(aty)) {
            presenter.showTestList(Visitno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void showTestList(ArrayList<TestList_Asreqlist> list) {
        adapter = new TestAdapter(actTestList, list, R.layout.testlist_item);
        actTestList.setAdapter(adapter);
        emptyLayout.dismiss();
    }


}
