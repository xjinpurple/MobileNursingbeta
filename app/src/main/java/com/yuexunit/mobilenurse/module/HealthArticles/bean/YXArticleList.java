package com.yuexunit.mobilenurse.module.HealthArticles.bean;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/25.
 */
public class YXArticleList {
    public int code;
    public String message;
    public ArrayList<ArticleTitle> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ArticleTitle> getData() {
        return data;
    }

    public void setData(ArrayList<ArticleTitle> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "YXArticleList{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
