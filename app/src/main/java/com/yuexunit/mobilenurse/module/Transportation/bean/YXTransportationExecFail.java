package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/10/17.
 */
public class YXTransportationExecFail {

    /**
     * response : {"head":{"ret_code":"1","ret_info":"失败：OPERATE-018133: 更新手术病人状态信息时,影响行数不匹配！\r\n"}}
     */

    private BodyEntity body;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class BodyEntity {
        /**
         * head : {"ret_code":"1","ret_info":"失败：OPERATE-018133: 更新手术病人状态信息时,影响行数不匹配！\r\n"}
         */

        private ResponseEntity response;

        public void setResponse(ResponseEntity response) {
            this.response = response;
        }

        public ResponseEntity getResponse() {
            return response;
        }

        public static class ResponseEntity {
            /**
             * ret_code : 1
             * ret_info : 失败：OPERATE-018133: 更新手术病人状态信息时,影响行数不匹配！

             */

            private HeadEntity head;

            public void setHead(HeadEntity head) {
                this.head = head;
            }

            public HeadEntity getHead() {
                return head;
            }

            public static class HeadEntity {
                private String ret_code;
                private String ret_info;

                public void setRet_code(String ret_code) {
                    this.ret_code = ret_code;
                }

                public void setRet_info(String ret_info) {
                    this.ret_info = ret_info;
                }

                public String getRet_code() {
                    return ret_code;
                }

                public String getRet_info() {
                    return ret_info;
                }
            }
        }
    }
}
