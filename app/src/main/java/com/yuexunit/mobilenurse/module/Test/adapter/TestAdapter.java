package com.yuexunit.mobilenurse.module.Test.adapter;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Test.bean.TestList_Asreqlist;
import com.yuexunit.mobilenurse.module.Test.ui.Act_TestDetail;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Created by jx on 2015/11/22.
 */
public class TestAdapter extends KJAdapter<TestList_Asreqlist> {

    public TestAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, TestList_Asreqlist item, boolean isScrolling) {
        TextView testlist_item_statu = (TextView)adapterHolder.getView(R.id.testlist_item_statu);
        ImageView ic_arrow = (ImageView)adapterHolder.getView(R.id.ic_arrow);
        adapterHolder.setText(R.id.testlist_item_name,item.getOrdername());
        adapterHolder.setText(R.id.testlist_item_statu, item.getStatusname());
        if(item.getReportno() >0)
        {
            testlist_item_statu.setTextColor(mCxt.getResources().getColor(R.color.test_04));
            ic_arrow.setVisibility(View.VISIBLE);
        }
        else
        {
            testlist_item_statu.setTextColor(mCxt.getResources().getColor(R.color.test_05));
            ic_arrow.setVisibility(View.INVISIBLE);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",java.util.Locale.getDefault());
        Date curDate = item.getEntrytime();
        String str= formatter.format(curDate);
        adapterHolder.setText(R.id.testlist_item_data, str);
        adapterHolder.setText(R.id.testlist_item_num, item.getRequestno()+"");
        onPicClick(adapterHolder.getConvertView(),item.getRequestno()+"",item.getReportno());
    }

    private void onPicClick(View view,final String Requestno,final long Reportno) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Reportno > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString("requestno", Requestno);
                    Intent intent = new Intent(mCxt, Act_TestDetail.class);
                    intent.putExtras(bundle);
                    mCxt.startActivity(intent);
                }
                else {
                    ViewInject.toast("未出报告单!");
                }
            }
        });
    }

}
