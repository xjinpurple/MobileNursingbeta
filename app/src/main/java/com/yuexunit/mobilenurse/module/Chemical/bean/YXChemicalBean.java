package com.yuexunit.mobilenurse.module.Chemical.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/6/29.
 */
public class YXChemicalBean {
    public YXChemicalBean body;
    public Head head;
    public Infusion_Chemical response;

    public YXChemicalBean getBody() {
        return body;
    }

    public void setBody(YXChemicalBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Infusion_Chemical getResponse() {
        return response;
    }

    public void setResponse(Infusion_Chemical response) {
        this.response = response;
    }
}
