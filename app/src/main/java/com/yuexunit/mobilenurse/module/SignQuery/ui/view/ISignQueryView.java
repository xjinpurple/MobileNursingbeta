package com.yuexunit.mobilenurse.module.SignQuery.ui.view;


import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;

import java.util.ArrayList;



/**
 * Created by sslcjy on 16/1/26.
 */
public interface ISignQueryView {
    /**
     * 显示体征信息
     */
    void showSignInfo(ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities);

}
