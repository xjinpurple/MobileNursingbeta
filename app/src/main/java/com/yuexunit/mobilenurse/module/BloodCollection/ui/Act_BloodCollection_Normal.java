package com.yuexunit.mobilenurse.module.BloodCollection.ui;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.adapter.BloodCollectionAdapter;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl.BloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionView;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/5/3.
 */
public class Act_BloodCollection_Normal extends KJActivity implements IBloodCollectionView{


    @Bind(R.id.bloodcollection_list)
    ListView bloodcollectionList;

    BaseAdapter adapter;
    private LoadingDialog dialog;

    ArrayList<Collectinfo> collectinfos = new ArrayList<Collectinfo>();

    IBloodCollectionPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_bloodcollection);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        dialog = new LoadingDialog(aty);
        presenter = new BloodCollectionPresenter(new BloodCollectionModel(),this);
        adapter = new BloodCollectionAdapter(bloodcollectionList, collectinfos, R.layout.item_bloodcollection);
        bloodcollectionList.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
        try {
            unregisterReceiver(PatientReceiver);
        } catch (IllegalArgumentException e) {

        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            unregisterReceiver(PatientReceiver);
        } catch (IllegalArgumentException e) {

        }
    }


    @OnClick({R.id.bloodcollection_img_back, R.id.bloodcollection_img_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bloodcollection_img_back:
                finish();
                break;
            case R.id.bloodcollection_img_search:
                Intent intent = new Intent(aty,Act_BloodCollection_Serach.class);
                startActivity(intent);
                break;
        }
    }


    //注册条码Receiver
    private void Register_Receiver()
    {
        //注册条码Receiver
        IntentFilter scanDataIntentFilter = new IntentFilter();
        scanDataIntentFilter.addAction("com.android.scancontext");
        scanDataIntentFilter.addAction("com.android.scanservice.scancontext");
        registerReceiver(PatientReceiver, scanDataIntentFilter);

        if(!isServiceRunning("com.android.scanservice.ScanService")) {
            ViewInject.toast("未检测到扫描服务");
        }
    }

    //条码Receiver
    private BroadcastReceiver PatientReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent){
            String action = intent.getAction();
            int keycode = intent.getIntExtra("Scan_Keycode", 0);
            if (action.equals("com.android.scanservice.scancontext")) {
                String result = intent.getStringExtra("Scan_context");
                String type = intent.getStringExtra("Scan_type");
                if(!result.equals(""))
                {
                    if(result.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_CX_RULE))) {
                        dialog.show();
                        presenter.collect("", result, PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID));
                    }
                    else {
                        Toast.makeText(aty, "条码无效!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    // 检查Service是否运行
    private boolean isServiceRunning(String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager.getRunningServices(100);

        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    @Override
    public void collectSend(Boolean isSuccess ,Collectinfo collectinfo) {
        if(isSuccess)
        {
            if(collectinfo != null) {
                collectinfos.add(collectinfo);
                adapter.notifyDataSetChanged();
            }
            else {
                ViewInject.toast("该标本已发送");
            }
        }
        else {
            ViewInject.toast("发送失败");
        }
        dialog.dismiss();
    }
}
