package com.yuexunit.mobilenurse.module.NursingPlan.ui;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.widget.CustomDatePicker;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2017/1/23.
 */
public class Act_NursingPlan_Add extends KJActivity {
    @Bind(R.id.docactvice_name)
    TextView docactviceName;
    @Bind(R.id.docactvice_bedno)
    TextView docactviceBedno;
    @Bind(R.id.docactvice_visitno)
    TextView docactviceVisitno;
    @Bind(R.id.nursingplan_add_title_edt)
    EditText nursingplanAddTitleEdt;
    @Bind(R.id.nursingplan_add_date)
    TextView nursingplanAddDate;
    @Bind(R.id.nursingplan_add_question)
    TextView nursingplanAddQuestion;
    @Bind(R.id.nursingplan_add_target)
    TextView nursingplanAddTarget;
    @Bind(R.id.nursingplan_add_method)
    TextView nursingplanAddMethod;
    @Bind(R.id.nursingplan_add_comment)
    TextView nursingplanAddComment;

    private CustomDatePicker customDatePicker;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_nursingplan_add);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        setContent();
        initDatePicker();
    }

    public void setContent()
    {
        docactviceName.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactviceBedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactviceVisitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    @OnClick({R.id.nuseingplan_submit, R.id.nursingplan_add_date_ll, R.id.nursingplan_add_question_ll, R.id.nursingplan_add_target_ll, R.id.nursingplan_add_method_ll, R.id.nursingplan_add_comment_ll})
    public void onClick(View view) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        String now = sdf.format(new Date());
        switch (view.getId()) {
            case R.id.nuseingplan_submit:
                break;
            case R.id.nursingplan_add_date_ll:
                customDatePicker.show(now);
                break;
            case R.id.nursingplan_add_question_ll:
                break;
            case R.id.nursingplan_add_target_ll:
                break;
            case R.id.nursingplan_add_method_ll:
                break;
            case R.id.nursingplan_add_comment_ll:
                break;
        }
    }


    private void initDatePicker() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
        String now = sdf.format(new Date());

        customDatePicker = new CustomDatePicker(this, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                nursingplanAddDate.setText(time.substring(0,10));
            }
        }, "2010-01-01 00:00", now); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker.showSpecificTime(false); // 不显示时和分
        customDatePicker.setIsLoop(false); // 不允许循环滚动
    }
}
