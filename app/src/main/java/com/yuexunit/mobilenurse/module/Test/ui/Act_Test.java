package com.yuexunit.mobilenurse.module.Test.ui;

import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Test.adapter.TestAdapter;
import com.yuexunit.mobilenurse.module.Test.bean.TestList_Asreqlist;
import com.yuexunit.mobilenurse.module.Test.presenter.ITestPresenter;
import com.yuexunit.mobilenurse.module.Test.ui.view.ITestView;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.utils.PreferenceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by work-jx on 2015/11/24.
 // */
public class Act_Test extends TitleBar_DocAdvice implements ITestView
{
    @Bind(R.id.act_test_list)
    public ListView act_test_list;
    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;
    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;

    private BaseAdapter adapter;
    //病人住院号
    private  String Visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_Test.class);
    ITestPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_test);
        ButterKnife.bind(this);
    }

    @Override
    public void initWidget() {
        super.initWidget();

    }

    @Override
    public void initData() {
        super.initData();

    }

    @Override
    public void setTitle()
    {
        titlebar_docadvice_tv_title.setText("化验报告单");
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));

    }

    @Override
    public  void onBackClick()
    {
        finish();
    }

    @Override
    public void showTestList(ArrayList<TestList_Asreqlist> list) {
        adapter = new TestAdapter(act_test_list, list, R.layout.testlist_item);
        act_test_list.setAdapter(adapter);
        mEmptyLayout.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.UnSubObservers();
    }
}

