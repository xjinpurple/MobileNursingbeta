package com.yuexunit.mobilenurse.module.PatientDetail.bean;

import com.yuexunit.mobilenurse.module.Patients.bean.PatientList_Patinfo;

/**
 * Created by work-jx on 2015/12/23.
 */
public class PatientSingle_Response {

    public PatientList_Patinfo patinfo;

    public PatientList_Patinfo getPatinfo() {
        return patinfo;
    }

    public void setPatinfo(PatientList_Patinfo patinfo) {
        this.patinfo = patinfo;
    }
}
