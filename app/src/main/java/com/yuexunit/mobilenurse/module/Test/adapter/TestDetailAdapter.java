package com.yuexunit.mobilenurse.module.Test.adapter;

import android.widget.AbsListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Test.bean.TestDetail_Asrptdetail;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.Collection;

/**
 * Created by jx on 2015/11/22.
 */
public class TestDetailAdapter extends KJAdapter<TestDetail_Asrptdetail> {


    public TestDetailAdapter(AbsListView view, Collection<TestDetail_Asrptdetail> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
    }

    @Override
    public void convert(AdapterHolder adapterHolder, TestDetail_Asrptdetail item, boolean isScrolling) {
            adapterHolder.setText(R.id.test_detail_adapter_name, item.getItemname());
            String current = item.getResultdata().replace("&lt;","<").replace("&gt;",">");
            adapterHolder.setText(R.id.test_detail_adapter_current,current);
        if(!(null==item.getResultdesc()))
        {
            adapterHolder.setText(R.id.test_detail_adapter_status,item.getResultdesc().replace("*",""));
        }
        else
        {
            adapterHolder.setText(R.id.test_detail_adapter_status,"");
        }

        if(!(null==item.getResultrange()))
        {
            String standard = item.getResultrange().replace("&lt;","<").replace("&gt;",">");

            adapterHolder.setText(R.id.test_detail_adapter_standard,standard);
        }
        else
        {
            adapterHolder.setText(R.id.test_detail_adapter_standard,"");
        }


    }
}
