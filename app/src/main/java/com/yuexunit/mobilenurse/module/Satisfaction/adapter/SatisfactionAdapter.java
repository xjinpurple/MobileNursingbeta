package com.yuexunit.mobilenurse.module.Satisfaction.adapter;

import android.widget.AbsListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionLastBean;

import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by work-jx on 2016/5/27.
 */
public class SatisfactionAdapter extends KJAdapter<SatisfactionLastBean>{

    public ArrayList<CheckType> list;
    private CheckType a;

    public SatisfactionAdapter(AbsListView view, Collection<SatisfactionLastBean> mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        initData(((ArrayList<SatisfactionLastBean>) mDatas).size());
    }

        /**
     * 存储刚开始radio选中情况
     */
    private void initData(int size) {
        list = new ArrayList<CheckType>();
        for (int i = 0; i < size; i++) {
            a = new CheckType(i + "号位", CheckType.TYPE_SECOND);
            list.add(a);
        }
    }

    @Override
    public void convert(AdapterHolder adapterHolder, SatisfactionLastBean satisfactionLastBean, boolean b,final int position) {
        final RadioGroup radioGroup = adapterHolder.getView(R.id.group);
        final RadioButton radioButton_01 = adapterHolder.getView(R.id.answer_01);
        final RadioButton radioButton_02 = adapterHolder.getView(R.id.answer_02);
        final RadioButton radioButton_03 = adapterHolder.getView(R.id.answer_03);
        adapterHolder.setText(R.id.answer_tv,satisfactionLastBean.getPJXNR());

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.answer_01:
                        list.get(position).type = CheckType.TYPE_FIRST;
                        break;
                    case R.id.answer_02:
                        list.get(position).type = CheckType.TYPE_SECOND;
                        break;
                    case R.id.answer_03:
                        list.get(position).type = CheckType.TYPE_THIRD;
                        break;
                }
            }
        });

        if(list.get(position).type == CheckType.TYPE_FIRST)
        {
            radioGroup.check(R.id.answer_01);
        }
        else if(list.get(position).type == CheckType.TYPE_SECOND)
        {
            radioGroup.check(R.id.answer_02);
        }
        else if(list.get(position).type == CheckType.TYPE_THIRD)
        {
            radioGroup.check(R.id.answer_03);
        }
    }

    public ArrayList<CheckType> getArrayList() {
        return list;
    }

        public class CheckType {
        public static final int TYPE_FIRST = 0;
        public static final int TYPE_SECOND = 1;
        public static final int TYPE_THIRD = 2;
        public String name;
        public int type;

        public CheckType(String name, int type) {
            this.name = name;
            this.type = type;
        }
    }
}
