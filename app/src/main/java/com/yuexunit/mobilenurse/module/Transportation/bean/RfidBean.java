package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/10/14.
 */
public class RfidBean {

    /**
     * code : 1
     * desc : null
     * object : {"rfid_sn":"0001010001000019","name":"小卡1","create_time":"ss","status":1,"area_code":"ss","area_name":"ss","first_row":"ss","row_size":"ss","order_by":"ss","sort_by":"ss"}
     */

    private int code;
    private Object desc;
    /**
     * rfid_sn : 0001010001000019
     * name : 小卡1
     * jt_id: 223
     * create_time : ss
     * status : 1
     * area_code : ss
     * area_name : ss
     * first_row : ss
     * row_size : ss
     * order_by : ss
     * sort_by : ss
     */

    private ObjectEntity object;

    public void setCode(int code) {
        this.code = code;
    }

    public void setDesc(Object desc) {
        this.desc = desc;
    }

    public void setObject(ObjectEntity object) {
        this.object = object;
    }

    public int getCode() {
        return code;
    }

    public Object getDesc() {
        return desc;
    }

    public ObjectEntity getObject() {
        return object;
    }

    public static class ObjectEntity {
        private String rfid_sn;
        private String name;
        private String jt_id;
        private String create_time;
        private int status;
        private String area_code;
        private String area_name;
        private String first_row;
        private String row_size;
        private String order_by;
        private String sort_by;

        public void setRfid_sn(String rfid_sn) {
            this.rfid_sn = rfid_sn;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public void setArea_code(String area_code) {
            this.area_code = area_code;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public void setFirst_row(String first_row) {
            this.first_row = first_row;
        }

        public void setRow_size(String row_size) {
            this.row_size = row_size;
        }

        public void setOrder_by(String order_by) {
            this.order_by = order_by;
        }

        public void setSort_by(String sort_by) {
            this.sort_by = sort_by;
        }

        public String getRfid_sn() {
            return rfid_sn;
        }

        public String getName() {
            return name;
        }

        public String getCreate_time() {
            return create_time;
        }

        public int getStatus() {
            return status;
        }

        public String getArea_code() {
            return area_code;
        }

        public String getArea_name() {
            return area_name;
        }

        public String getFirst_row() {
            return first_row;
        }

        public String getRow_size() {
            return row_size;
        }

        public String getOrder_by() {
            return order_by;
        }

        public String getSort_by() {
            return sort_by;
        }

        public String getJt_id() {
            return jt_id;
        }

        public void setJt_id(String jt_id) {
            this.jt_id = jt_id;
        }
    }
}
