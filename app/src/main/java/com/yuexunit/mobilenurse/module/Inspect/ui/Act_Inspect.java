package com.yuexunit.mobilenurse.module.Inspect.ui;

import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Inspect.adapter.InspectAdapter;
import com.yuexunit.mobilenurse.module.Inspect.bean.InjectionList_Exreqlist;
import com.yuexunit.mobilenurse.module.Inspect.model.impl.InspectModel;
import com.yuexunit.mobilenurse.module.Inspect.presenter.IInspectPresenter;
import com.yuexunit.mobilenurse.module.Inspect.presenter.impl.InspectPresenter;
import com.yuexunit.mobilenurse.module.Inspect.ui.view.IInspectView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by work-jx on 2015/11/24.
 // */
public class Act_Inspect extends TitleBar_DocAdvice implements IInspectView
{
    @Bind(R.id.act_inspect_list)
    public ListView act_inspect_list;
    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;
    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;

    private BaseAdapter adapter;
    //病人住院号
    private  String Visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_Inspect.class);
    IInspectPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_inspect);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new InspectPresenter(this,new InspectModel());
        Visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        mEmptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });
        act_inspect_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                showActivity(Act_Inspect.this, Act_InspectDetail.class);

            }
        });
        if(SystemTool.checkNet(aty)) {
            presenter.showInspectList(Visitno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }




    @Override
    public void setTitle()
    {
        titlebar_docadvice_tv_title.setText("检查报告单");
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    @Override
    public  void onBackClick()
    {
        finish();
    }

    @Override
    public void showInspectList(ArrayList<InjectionList_Exreqlist> list) {
        adapter = new InspectAdapter(act_inspect_list, list, R.layout.testlist_item);
        act_inspect_list.setAdapter(adapter);
        mEmptyLayout.dismiss();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.UnSubObservers();
    }
}

