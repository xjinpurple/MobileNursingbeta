package com.yuexunit.mobilenurse.module.SkinTest.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.SkinTest.bean.SkinTestDetailBean;
import com.yuexunit.mobilenurse.module.SkinTest.bean.YXSkinTestBean;
import com.yuexunit.mobilenurse.module.SkinTest.model.ISkinTestModel;
import com.yuexunit.mobilenurse.module.SkinTest.presenter.ISkinTestPresenter;
import com.yuexunit.mobilenurse.module.SkinTest.ui.view.ISkinTestView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/6/29.
 */
public class SkinTestPresenter implements ISkinTestPresenter{
    private ISkinTestView view;
    private ISkinTestModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    ArrayList<SkinTestDetailBean> skinTestDetailBeans  = new ArrayList<SkinTestDetailBean>();

    public SkinTestPresenter(ISkinTestView view, ISkinTestModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showSkinTestDate(String visitno) {
        compositeSubscription.add(
                model.getSkinTestDate(visitno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("该病人没有皮试数据");
                            }

                            @Override
                            public void onNext(String result) {
                                YXSkinTestBean Total = JSON.parseObject(result, YXSkinTestBean.class);
                                YXSkinTestBean SkinTestData = Total.getBody();

                                if ("0".equals(SkinTestData.getHead().getRet_code())) {
                                    List<SkinTestDetailBean> skintestBeanlist;
                                    String skintest = SkinTestData.getResponse().getStinfo();
                                    String skintest_first = skintest.substring(0, 1);
                                    if (!skintest_first.equals("[")) {
                                        skintest = "[" + skintest + "]";
                                        skintestBeanlist = JSON.parseArray(skintest, SkinTestDetailBean.class);
                                    } else {
                                        skintestBeanlist = JSON.parseArray(skintest, SkinTestDetailBean.class);
                                    }

                                    for(int i = 0;i<skintestBeanlist.size();i++)
                                    {
                                        skinTestDetailBeans.add(skintestBeanlist.get(i));
                                    }

                                    view.showSkinTestDate(skinTestDetailBeans);
                                }
                                else {
                                    ViewInject.toast("网络异常，请重新加载");
                                }
                            }
                        }
                        )
        );
    }

    @Override
    public void getMedicineDate(String ypid) {
        compositeSubscription.add(
                model.getMedicineDate(ypid)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(new Action0() {
                            @Override
                            public void call() {
                                view.loadingDialogStatus(AppConfig.SHOW_DIALOG);
                            }
                        })
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                                       @Override
                                       public void onCompleted() {

                                       }

                                       @Override
                                       public void onError(Throwable e) {
                                           view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
                                           view.getMedicineStatus(false);
                                       }

                                       @Override
                                       public void onNext(String result) {

                                       }
                                   }
                        )
        );
    }

    @Override
    public void execSkinTestDate(String sqdh, String czry, String kssj, String pssc, String pcnm, String psph) {
        compositeSubscription.add(
                model.execSkinTest(sqdh,czry,kssj,pssc,pcnm,psph)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(new Action0() {
                            @Override
                            public void call() {
                                view.loadingDialogStatus(AppConfig.SHOW_DIALOG);
                            }
                        })
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                                       @Override
                                       public void onCompleted() {

                                       }

                                       @Override
                                       public void onError(Throwable e) {
                                           view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
                                           view.ExecStatus(false);
                                       }

                                       @Override
                                       public void onNext(String result) {
                                           JSONObject all, body, response,head;
                                           String ret_code = "-1";
                                           try {
                                               all = new JSONObject(result.toString());
                                               body = all.getJSONObject("body");
                                               response=body.getJSONObject("response");
                                               head = response.getJSONObject("head");
                                               ret_code = head.getString("ret_code");
                                           } catch (JSONException e) {
                                           }

                                           if ("0".equals(ret_code)) {
                                               view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
                                               view.ExecStatus(true);
                                           } else {
                                               view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
                                               view.ExecStatus(false);
                                           }
                                       }
                                       }
                        )
        );
    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
