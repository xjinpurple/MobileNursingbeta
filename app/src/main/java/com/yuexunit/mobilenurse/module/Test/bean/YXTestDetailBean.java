package com.yuexunit.mobilenurse.module.Test.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//病人检验报告单

public class YXTestDetailBean {

    public YXTestDetailBean body;
    public Head head;
    public TestDetail_Response response;


    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public TestDetail_Response getResponse() {
        return response;
    }

    public void setResponse(TestDetail_Response response) {
        this.response = response;
    }

    public YXTestDetailBean getBody() {
        return body;
    }

    public void setBody(YXTestDetailBean body) {
        this.body = body;
    }
}
