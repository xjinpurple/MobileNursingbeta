package com.yuexunit.mobilenurse.module.Satisfaction.bean;

import java.util.List;

/**
 * Created by work-jx on 2016/5/27.
 */
public class SatisfactionBean {
    private String ret_code;
    private String ret_info;
    private List<SatisfactiondetailBean> satisfactiondetailBean_list;

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_info() {
        return ret_info;
    }

    public void setRet_info(String ret_info) {
        this.ret_info = ret_info;
    }

    public List<SatisfactiondetailBean> getSatisfactiondetailBean_list() {
        return satisfactiondetailBean_list;
    }

    public void setSatisfactiondetailBean_list(List<SatisfactiondetailBean> satisfactiondetailBean_list) {
        this.satisfactiondetailBean_list = satisfactiondetailBean_list;
    }
}
