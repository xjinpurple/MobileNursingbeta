package com.yuexunit.mobilenurse.module.NursingDocuments.ui.view;

/**
 * Created by work-jx on 2016/10/13.
 */
public interface IDocumentRecordDetailView {
    void uploadRecord();
}
