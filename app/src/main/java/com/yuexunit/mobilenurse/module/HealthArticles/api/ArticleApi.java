package com.yuexunit.mobilenurse.module.HealthArticles.api;

import com.yuexunit.mobilenurse.module.HealthArticles.bean.YXArticleContent;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.YXArticleList;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by sslcjy on 16/1/25.
 */
public interface ArticleApi {
    /**
     * 获取健康宣教列表数据
     */
    @GET("ydhl/api/article/titles")
    Observable<YXArticleList> ArticleList(@Query("areaId")String areaId);

    /**
     * 获取健康宣教某篇文章的具体内容
     */
    @GET("ydhl/api/article")
    Observable<YXArticleContent> ArticleContent(@Query("articleId")int articleId);
}