package com.yuexunit.mobilenurse.module.Patients.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/10/9.
 */
public class YXPatientClassListBean {
    public YXPatientClassListBean body;

    //交易返回区(head)
    public Head head;
    //数据返回区(response)
    public PatientClassList_Response response;

    public YXPatientClassListBean getBody() {
        return body;
    }

    public void setBody(YXPatientClassListBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public PatientClassList_Response getResponse() {
        return response;
    }

    public void setResponse(PatientClassList_Response response) {
        this.response = response;
    }
}
