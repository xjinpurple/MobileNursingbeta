package com.yuexunit.mobilenurse.module.Transportation.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Transportation.adapter.SelectOperAdapter;
import com.yuexunit.mobilenurse.module.Transportation.bean.TransportationRecord_Opsinfo;
import com.yuexunit.mobilenurse.module.Transportation.model.impl.SelectOperModel;
import com.yuexunit.mobilenurse.module.Transportation.presenter.ISelectOperPresenter;
import com.yuexunit.mobilenurse.module.Transportation.presenter.impl.SelectOperPresenter;
import com.yuexunit.mobilenurse.module.Transportation.ui.view.ISelectOperView;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/9/23.
 */
public class Act_SelectOper extends KJActivity implements ISelectOperView{

    @Bind(R.id.selectoper_list)
    ListView selectoperList;
    @Bind(R.id.empty_layout)
    EmptyLayout emptyLayout;

    //病人住院号
    private  String Visitno;
    ISelectOperPresenter presenter;
    private BaseAdapter adapter;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_SelectOper.class);

    ArrayList<TransportationRecord_Opsinfo> transportationRecord_opsinfos = new ArrayList<TransportationRecord_Opsinfo>();

    @Override
    public void setRootView() {
        setContentView(R.layout.act_selectoper);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new SelectOperPresenter(this, new SelectOperModel());
        Visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });

        if(SystemTool.checkNet(aty)) {
            presenter.showOpsList(Visitno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }

        selectoperList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent();
                bundle.putSerializable("opsinfo",transportationRecord_opsinfos.get(position));
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @OnClick(R.id.back)
    public void onClick() {
        finish();
    }


    @Override
    public void showOpsList(ArrayList<TransportationRecord_Opsinfo> list) {
        transportationRecord_opsinfos = list;
        adapter = new SelectOperAdapter(selectoperList, transportationRecord_opsinfos, R.layout.item_selectoper);
        selectoperList.setAdapter(adapter);
        emptyLayout.dismiss();
    }


}
