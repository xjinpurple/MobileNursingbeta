package com.yuexunit.mobilenurse.module.Test.ui.view;

import com.yuexunit.mobilenurse.module.Test.bean.TestList_Asreqlist;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestView {
    //检查单列表列表
    void showTestList(ArrayList<TestList_Asreqlist> list);
}
