package com.yuexunit.mobilenurse.module.Inspect.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class InjectionList_Response {

    public String exreqdetail;

    public String exreqlist;

    public String getExreqdetail() {
        return exreqdetail;
    }

    public void setExreqdetail(String exreqdetail) {
        this.exreqdetail = exreqdetail;
    }

    public String getExreqlist() {
        return exreqlist;
    }

    public void setExreqlist(String exreqlist) {
        this.exreqlist = exreqlist;
    }
}
