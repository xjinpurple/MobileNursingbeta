package com.yuexunit.mobilenurse.module.SignInput.api;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsInput;
import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsSingleInput;

import java.util.Map;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface SignInputApi {
    @GET("ydhl/api/patient")
    Observable<YXSignsSingleInput> getBaseTypes(@Query("areaId") String areaId);

    @GET("ydhl/api/sign/all")
    Observable<YXSignsInput> getAllTypes(@Query("areaId")String areaId);

    @FormUrlEncoded
    @POST("ydhl/api/sign/offical/add")
    Observable<ActionBean> uploadSignInput(@FieldMap Map<String, String> praise);


}
