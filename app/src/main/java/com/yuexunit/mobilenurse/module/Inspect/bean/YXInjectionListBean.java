package com.yuexunit.mobilenurse.module.Inspect.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//病人检查申请单列表
public class YXInjectionListBean {

    public YXInjectionListBean body;
    public Head head;
    public InjectionList_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public InjectionList_Response getResponse() {
        return response;
    }

    public void setResponse(InjectionList_Response response) {
        this.response = response;
    }

    public YXInjectionListBean getBody() {
        return body;
    }

    public void setBody(YXInjectionListBean body) {
        this.body = body;
    }
}
