package com.yuexunit.mobilenurse.module.DocAdvice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdvice.bean.DocAdvice_Dispensing_New;
import com.yuexunit.mobilenurse.module.DocAdvice.ui.fragment.Frag_Dispensing;

import java.util.ArrayList;

/**
 * Created by jx on 2015/11/22.
 */
public class DispensingListAdapter extends BaseAdapter{

    private Context context;
    private Frag_Dispensing activity;
    private ArrayList<DocAdvice_Dispensing_New> mDatas;
//    protected LayoutInflater mInflater;

    public DispensingListAdapter(Frag_Dispensing activity,Context context,ArrayList<DocAdvice_Dispensing_New> mDatas){
        this.context = context;
        this.activity =activity;
        this.mDatas = mDatas;
    }


    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.dispensinglist_item, parent, false);
            holder = new ViewHolder();
            holder.dispensinglist_list = (ListView)convertView.findViewById(R.id.dispensinglist_list);
            holder.dispensinglist_statu_btn = (Button)convertView.findViewById(R.id.dispensinglist_statu_btn);
            holder.dispensinglist_plan_usage = (TextView)convertView.findViewById(R.id.dispensinglist_plan_usage);
            holder.dispensinglist_statu_tv = (TextView)convertView.findViewById(R.id.dispensinglist_statu_tv);
            holder.dispensinglist_count = (TextView)convertView.findViewById(R.id.dispensinglist_count);
            holder.dispensinglist_ll = (RelativeLayout)convertView.findViewById(R.id.dispensinglist_ll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.dispensinglist_statu_tv.setText(mDatas.get(position).getStatus());
        holder.dispensinglist_count.setText(mDatas.get(position).getCount());
        if(mDatas.get(position).getStatus().equals("等待发药"))
        {
            holder.dispensinglist_ll.setBackgroundResource(R.color.white);
            holder.dispensinglist_statu_tv.setTextColor(context.getResources().getColor(R.color.doc_advice_01));
            holder.dispensinglist_statu_btn.setVisibility(View.VISIBLE);
            holder.dispensinglist_statu_btn.setEnabled(true);
        }
        else
        {
            holder.dispensinglist_ll.setBackgroundResource(R.color.doc_advice_04);
            holder.dispensinglist_statu_tv.setTextColor(context.getResources().getColor(R.color.doc_advice_01));
            holder.dispensinglist_statu_btn.setVisibility(View.INVISIBLE);
            holder.dispensinglist_statu_btn.setEnabled(false);
        }

        BaseAdapter adapter  = new DispensingListListAdapter(context,mDatas.get(position).getDispensing_list_news());
        holder.dispensinglist_list.setAdapter(adapter);

        holder.dispensinglist_statu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String barcode = "-1";
//                for(int i = 0;i<mDatas.get(position).getDespensing_exec().size();i++)
//                {
//                    if(((Double.valueOf(barcode) > Double.valueOf(mDatas.get(position).getDespensing_exec().get(i).getBarcode()))
//                            || Double.valueOf(barcode)==-1)&& mDatas.get(position).getDespensing_exec().get(i).getExecempid() == null)
//                    {
//                        barcode = mDatas.get(position).getDespensing_exec().get(i).getBarcode();
//
//                    }
//                }
                activity.ExecuteOrder_inner(false,mDatas.get(position).getBarcode(),position);
            }
        });

        return convertView;
    }

    class ViewHolder{
        ListView dispensinglist_list;
        Button dispensinglist_statu_btn;
        TextView dispensinglist_statu_tv,dispensinglist_plan_usage,dispensinglist_count;
        RelativeLayout dispensinglist_ll;

    }


}
