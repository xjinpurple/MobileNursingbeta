package com.yuexunit.mobilenurse.module.NursingDocuments.ui;

import android.view.View;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;

import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/10/13.
 */
public class Act_Document extends TitleBar_DocAdvice {

    @Bind(R.id.titlebar_docadvice_tv_title)
    TextView titlebarDocadviceTvTitle;
    @Bind(R.id.docactvice_name)
    TextView docactviceName;
    @Bind(R.id.docactvice_bedno)
    TextView docactviceBedno;
    @Bind(R.id.docactvice_visitno)
    TextView docactviceVisitno;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_document);
        ButterKnife.bind(this);
        setContent();
    }


    @OnClick({R.id.titlebar_docadvice_img_back, R.id.document_ll_15})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.titlebar_docadvice_img_back:
                finish();
                break;
            case R.id.document_ll_15:
                showActivity(aty, Act_DocumentRecordDetail.class);
                break;
        }
    }

    public void setContent()
    {
        titlebarDocadviceTvTitle.setText("护理文书");
        docactviceName.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactviceBedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactviceVisitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }
}
