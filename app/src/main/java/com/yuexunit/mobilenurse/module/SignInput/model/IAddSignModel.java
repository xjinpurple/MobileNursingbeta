package com.yuexunit.mobilenurse.module.SignInput.model;

import com.yuexunit.mobilenurse.base.bean.ActionBean;

import java.util.Map;

import rx.Observable;

/**
 * Created by work-jx on 2016/5/12.
 */
public interface IAddSignModel {
    Observable<ActionBean> addsign(Map<String, String> praise);
}
