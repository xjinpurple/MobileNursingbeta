package com.yuexunit.mobilenurse.module.BloodCollection.bean;

/**
 * Created by work-jx on 2017/5/4.
 */
public class Specinfo {


    /**
     * efid : 112500382064
     * prn_name : 急诊血常规+CRP
     * state : 90
     * collect_time : 2017/5/4 1:53:36
     * collect_empid : 281
     * collect_empname : 唐静芳
     * send_time : 2017/5/4 1:56:02
     * send_empid : 281
     * send_empname : 唐静芳
     * report_state : 1
     */

    private String efid;
    private String prn_name;
    private String state;
    private String collect_time;
    private String collect_empid;
    private String collect_empname;
    private String send_time;
    private String send_empid;
    private String send_empname;
    private String report_state;
    private String reg_name;

    public void setEfid(String efid) {
        this.efid = efid;
    }

    public void setPrn_name(String prn_name) {
        this.prn_name = prn_name;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCollect_time(String collect_time) {
        this.collect_time = collect_time;
    }

    public void setCollect_empid(String collect_empid) {
        this.collect_empid = collect_empid;
    }

    public void setCollect_empname(String collect_empname) {
        this.collect_empname = collect_empname;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public void setSend_empid(String send_empid) {
        this.send_empid = send_empid;
    }

    public void setSend_empname(String send_empname) {
        this.send_empname = send_empname;
    }

    public void setReport_state(String report_state) {
        this.report_state = report_state;
    }

    public String getEfid() {
        return efid;
    }

    public String getPrn_name() {
        return prn_name;
    }

    public String getState() {
        return state;
    }

    public String getCollect_time() {
        return collect_time;
    }

    public String getCollect_empid() {
        return collect_empid;
    }

    public String getCollect_empname() {
        return collect_empname;
    }

    public String getSend_time() {
        return send_time;
    }

    public String getSend_empid() {
        return send_empid;
    }

    public String getSend_empname() {
        return send_empname;
    }

    public String getReport_state() {
        return report_state;
    }

    public String getReg_name() {
        return reg_name;
    }

    public void setReg_name(String reg_name) {
        this.reg_name = reg_name;
    }
}
