package com.yuexunit.mobilenurse.module.DocAdviceQuery.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/14.
 */
public class Orderexecinfo {

    //执行条码
    private String barcode;
    //执行日期
    @JSONField(format = "yyyy-MM-dd")
    public Date execdate;
    //执行次数
    private  int execseq;
    //执行人员
    private  int execempid;
    //执行时间
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date exectime;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Date getExecdate() {
        return execdate;
    }

    public void setExecdate(Date execdate) {
        this.execdate = execdate;
    }

    public int getExecseq() {
        return execseq;
    }

    public void setExecseq(int execseq) {
        this.execseq = execseq;
    }

    public int getExecempid() {
        return execempid;
    }

    public void setExecempid(int execempid) {
        this.execempid = execempid;
    }

    public Date getExectime() {
        return exectime;
    }

    public void setExectime(Date exectime) {
        this.exectime = exectime;
    }
}
