package com.yuexunit.mobilenurse.module.DocAdvice.bean;

/**
 * Created by work-jx on 2015/12/14.
 */
public class Orderexecinfo {

    //执行条码
    private String barcode;
    //执行日期
    public String execdate;
    //执行次数
    private  int execseq;
    //执行人员
    private  int execempid;
    //执行时间
    public String exectime;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getExecdate() {
        return execdate;
    }

    public void setExecdate(String execdate) {
        this.execdate = execdate;
    }

    public int getExecseq() {
        return execseq;
    }

    public void setExecseq(int execseq) {
        this.execseq = execseq;
    }

    public int getExecempid() {
        return execempid;
    }

    public void setExecempid(int execempid) {
        this.execempid = execempid;
    }

    public String getExectime() {
        return exectime;
    }

    public void setExectime(String exectime) {
        this.exectime = exectime;
    }
}
