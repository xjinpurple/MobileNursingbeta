package com.yuexunit.mobilenurse.module.SignQuery.ui;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;
import com.yuexunit.mobilenurse.module.SignQuery.model.impl.SignModel;
import com.yuexunit.mobilenurse.module.SignQuery.presenter.impl.SignPresenter;
import com.yuexunit.mobilenurse.module.SignQuery.ui.view.ISignQueryView;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sslcjy on 16/2/25.
 */
public class Act_SignQuery extends KJActivity implements ISignQueryView {

    private SignPresenter presenter;
    private String inpNo;
    private static final int DATE_NUM = 1;//提供几天的数据过来
    private int interval = 1;
    private SimpleDateFormat format;
    //距离当日天数
    private int countday = 0;

    Calendar calendar = Calendar.getInstance();
    int[] typesid = {
            R.id.sign_tw, R.id.sign_mb, R.id.sign_ttcd, R.id.sign_hx, R.id.sign_xy, R.id.sign_sg, R.id.sign_tz,
            R.id.sign_dbcs, R.id.sign_rl, R.id.sign_cl, R.id.sign_nl
    };
    int[] positionid = {
            R.id.position_one, R.id.position_two, R.id.position_three, R.id.position_four, R.id.position_six
    };
    @Bind(R.id.query_date)
    public TextView query_date;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_signs_query);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        super.initData();
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        presenter = new SignPresenter(new SignModel(), this);
    }

    @Override
    public void initWidget() {
        super.initWidget();
        query_date.setText(format.format(calendar.getTime()));
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        ;
        presenter.showSignInfo(inpNo, DATE_NUM, format.format(new Date(System.currentTimeMillis())));
    }

    @OnClick(R.id.query_left)
    public void QueryBefore() {
        if (countday < 7) {
            countday = countday + 1;
            calendar.add(Calendar.DATE, -1);
            query_date.setText(format.format(calendar.getTime()));
            clearText();
            presenter.showSignInfo(inpNo, interval, format.format(calendar.getTime()));
        }
    }

    @OnClick(R.id.query_right)
    public void QueryAfter() {

        if (countday > 0) {
            countday = countday - 1;
            calendar.add(Calendar.DATE, 1);
            query_date.setText(format.format(calendar.getTime()));
            clearText();
            presenter.showSignInfo(inpNo, interval, format.format(calendar.getTime()));
        }
    }

    @OnClick(R.id.titlebar_img_back)
    public void OnBack() {
        finish();
    }

    @Override
    public void showSignInfo(ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities) {
//        for (YXSignQuery.SignQueryEntity entity : signQueryEntities) {
//            Log.v("dyp", "entity:" + entity.toString());
//            String timepoint = entity.getTime_point().substring(11, 13);
//            String vital_signs = entity.getVital_signs();
//            View view = null;
//            if("口表".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_kb), timepoint, entity);
//            }
//            else if("腋表".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_yb), timepoint, entity);
//            }
//            else if("肛表".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_gb), timepoint, entity);
//            }
//            else if("物理降温".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_wljw), timepoint, entity);
//            }
//            else if("耳温".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_ew), timepoint, entity);
//            }
//            else if("脉搏".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_mb), timepoint, entity);
//            }
//            else if("呼吸".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_hx), timepoint, entity);
//            }
//            else if("血压".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_xy), timepoint, entity);
//            }
//            else if("大便次数".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_dbcs), timepoint, entity);
//            }
//            else if("体重".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_tz), timepoint, entity);
//            }
//            else if("总入量".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_rl), timepoint, entity);
//            }
//            else if("总出量".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_cl), timepoint, entity);
//            }
//            else if("尿量".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_nl), timepoint, entity);
//            }
//            else if("心率".equals(vital_signs))
//            {
//            	setMultiSignValue(findViewById(R.id.sign_xl), timepoint, entity);
//            }
//        }
    }

    public void setMultiSignValue(View view, String timepoint, YXSignQuery.SignQueryEntity entity) {
        Log.v("dyp", "setMultiSignValue:" + entity.getVital_signs_values_c());
        if ("02".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_one)).setText(entity.getVital_signs_values_c());
        } else if ("06".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_two)).setText(entity.getVital_signs_values_c());
        } else if ("10".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_three)).setText(entity.getVital_signs_values_c());
        } else if ("14".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_four)).setText(entity.getVital_signs_values_c());
        } else if ("18".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_five)).setText(entity.getVital_signs_values_c());
        } else if ("24".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_six)).setText(entity.getVital_signs_values_c());
        }
    }

    public void clearText() {
        View view = null;
        for (int i = 0; i < typesid.length; i++) {
            view = findViewById(typesid[i]);

            for (int j = 0; j < positionid.length; j++) {
                TextView tx = (TextView) view.findViewById(positionid[j]);
                if (tx != null) {
                    tx.setText("");
                }
            }
        }
    }

}
