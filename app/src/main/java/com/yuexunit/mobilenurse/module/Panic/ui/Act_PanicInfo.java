package com.yuexunit.mobilenurse.module.Panic.ui;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;

import org.kymjs.kjframe.KJActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/5/26.
 */
public class Act_PanicInfo extends KJActivity {

    @Bind(R.id.panicinfo_time)
    TextView panicinfoTime;
    @Bind(R.id.panicinfo_name)
    TextView panicinfoName;
    @Bind(R.id.panicinfo_bed)
    TextView panicinfoBed;
    @Bind(R.id.panicinfo_hospital)
    TextView panicinfoHospital;
    @Bind(R.id.panicinfo_painic)
    TextView panicinfoPainic;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_panicinfo);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @OnClick({R.id.panicinfo_img_back, R.id.panicinfo_detail_ll})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.panicinfo_img_back:
                finish();
                break;
            case R.id.panicinfo_detail_ll:
                Intent intent = new Intent();
                intent.setClass(aty, Act_PanicKT.class);
                aty.startActivity(intent);
                break;
        }
    }
}
