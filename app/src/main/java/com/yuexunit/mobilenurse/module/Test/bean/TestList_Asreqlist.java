package com.yuexunit.mobilenurse.module.Test.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/11.
 */
public class TestList_Asreqlist {

    //申请单号
    public long requestno;
    //加急标志
    public String emergflag;
    //检验医嘱代码
    public int orderid;
    //检验医嘱名称
    public String ordername;
    //检验标本代码
    public int ordertypeid;
    //检验标本名称
    public String ordertypename;
    //状态代码
    public String statuscode;
    //状态名称
    public String statusname;
    //报告单号
    public long reportno;
    //开单医生
    public int doctorid;
    //开单医生姓名
    public String doctorname;
    //开单时间
    @JSONField(format = "yyyy-MM-dd")
    public Date entrytime;
    //病人科室
    public String deptcode;
    //病人科室名称
    public String deptname;
    //病人病区
    public String wardcode;
    //病区名称
    public String wardname;

    public long getRequestno() {
        return requestno;
    }

    public void setRequestno(long requestno) {
        this.requestno = requestno;
    }

    public String getEmergflag() {
        return emergflag;
    }

    public void setEmergflag(String emergflag) {
        this.emergflag = emergflag;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public int getOrdertypeid() {
        return ordertypeid;
    }

    public void setOrdertypeid(int ordertypeid) {
        this.ordertypeid = ordertypeid;
    }

    public String getOrdertypename() {
        return ordertypename;
    }

    public void setOrdertypename(String ordertypename) {
        this.ordertypename = ordertypename;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public long getReportno() {
        return reportno;
    }

    public void setReportno(long reportno) {
        this.reportno = reportno;
    }

    public int getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(int doctorid) {
        this.doctorid = doctorid;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public Date getEntrytime() {
        return entrytime;
    }

    public void setEntrytime(Date entrytime) {
        this.entrytime = entrytime;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getWardcode() {
        return wardcode;
    }

    public void setWardcode(String wardcode) {
        this.wardcode = wardcode;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }
}
