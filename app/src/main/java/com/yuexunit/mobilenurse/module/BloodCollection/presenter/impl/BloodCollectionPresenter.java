package com.yuexunit.mobilenurse.module.BloodCollection.presenter.impl;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.BloodCollection.model.IBloodCollectionModel;
import com.yuexunit.mobilenurse.module.BloodCollection.model.impl.BloodCollectionModel;
import com.yuexunit.mobilenurse.module.BloodCollection.presenter.IBloodCollectionPresenter;
import com.yuexunit.mobilenurse.module.BloodCollection.ui.view.IBloodCollectionView;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.Collectinfo;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.YXCollectBean;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONObject;
import org.slf4j.Logger;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by work-jx on 2017/5/5.
 */
public class BloodCollectionPresenter implements IBloodCollectionPresenter{
    private final Logger log = ProUtil.getLogger(BloodCollectionModel.class);
    private IBloodCollectionModel model;
    private IBloodCollectionView view;

    public BloodCollectionPresenter(IBloodCollectionModel model, IBloodCollectionView view) {
        this.model = model;
        this.view = view;
    }

    Collectinfo collectinfo;

    @Override
    public void collect(String Visitno, String Code, String NurseId) {
        model.collect(Visitno, Code, NurseId)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        collectinfo = new Collectinfo();
                        view.collectSend(false,collectinfo);
                    }

                    @Override
                    public void onNext(String result) {
                        JSONObject all, body;
                        String reponse = null;
                        try {
                            all = new JSONObject(result.toString());
                            body = all.getJSONObject("body");
                            reponse = body.getString("response");
                        } catch (Exception e) {
                            log.error("Exception:"+e);
                            view.collectSend(false, collectinfo);
                        }

                        if (result.toString().indexOf(AppConfig.ORACLEERROR) != -1) {
                            view.collectSend(false, collectinfo);
                        }
                        else {
                            if (!("null".equals(reponse))) {
                                YXCollectBean Total = JSON.parseObject(((String) result).toString(), YXCollectBean.class);
                                YXCollectBean Collectdate = Total.getBody();
                                if ("0".equals(Collectdate.getHead().getRet_code())) {
                                    view.collectSend(true, Collectdate.getResponse().getCollectinfo());
                                } else {
                                    Log.v("jx", "Error:" + Collectdate.getHead().getRet_info());
                                    view.collectSend(false, collectinfo);
                                }
                            }
                            else {
                                view.collectSend(false, collectinfo);
                            }
                        }
                    }
                });
    }
}
