package com.yuexunit.mobilenurse.module.Cost.ui.view;

import com.yuexunit.mobilenurse.module.Cost.bean.CostDetail_Patfeelist;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/1/26.
 */
public interface ICostView {
    //费用列表
    void showCostList(ArrayList<CostDetail_Patfeelist> list);
    //当日小计
    void showCostToday(String today);
    //加载过程中进度界面的显示与消失
    void dialogState(int status);
}
