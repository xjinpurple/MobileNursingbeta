package com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Injection;

import java.util.ArrayList;



/**
 * Created by work-jx on 2015/11/24.
 */
public class InjectionListAdapter extends BaseAdapter
{
    private Context context;
//    private Frag_Injection activity;
    private ArrayList<DocAdvice_Injection> mDatas;

    public InjectionListAdapter(Context context,ArrayList<DocAdvice_Injection> mDatas){
        this.context = context;
//        this.activity =activity;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId( int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.injectionlist_query_item, parent, false);
            holder = new ViewHolder();
            holder.injectionlist_list = (ListView)convertView.findViewById(R.id.injectionlist_list);
            holder.injectionlist_statu_btn = (Button)convertView.findViewById(R.id.injectionlist_statu_btn);
            holder.injectionlist_plan_usage = (TextView)convertView.findViewById(R.id.injectionlist_plan_usage);
            holder.injectionlist_time = (TextView)convertView.findViewById(R.id.injectionlist_time);
            holder.injectionlist_ll = (RelativeLayout)convertView.findViewById(R.id.injectionlist_ll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.injectionlist_plan_usage.setText(mDatas.get(position).getDocAdvice_orderlist().getExecfreq() + "  "+ mDatas.get(position).getDocAdvice_orderlist().getExecway());
        holder.injectionlist_time.setText("医嘱时间："+mDatas.get(position).getDocAdvice_orderlist().getOrdertime());
        BaseAdapter adapter  = new InjectionListListAdapter(context,mDatas.get(position).getDespensing_detail());
        holder.injectionlist_list.setAdapter(adapter);

        return convertView;
    }

    class ViewHolder{
        ListView injectionlist_list;
        Button injectionlist_statu_btn;
        TextView injectionlist_plan_usage,injectionlist_time;
        RelativeLayout injectionlist_ll;
    }
}
