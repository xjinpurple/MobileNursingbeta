package com.yuexunit.mobilenurse.module.DocAdvice.bean;

/**
 * Created by 姚平 on 2015/12/8.
 */
public class DocAdvice_OrderExec {

    //医嘱组号
    public long ordergroupno;
    //执行条码
    public String barcode;
    //执行日期
    public String execdate;
    //执行次数
    public int execseq;
    //执行人员
    public Integer execempid;
    //执行时间
    public String exectime;

    public String exec_empname;

    public long getOrdergroupno() {
        return ordergroupno;
    }

    public void setOrdergroupno(long ordergroupno) {
        this.ordergroupno = ordergroupno;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }


    public int getExecseq() {
        return execseq;
    }

    public void setExecseq(int execseq) {
        this.execseq = execseq;
    }

    public Integer getExecempid() {
        return execempid;
    }

    public void setExecempid(Integer execempid) {
        this.execempid = execempid;
    }

    public String getExectime() {
        return exectime;
    }

    public void setExectime(String exectime) {
        this.exectime = exectime;
    }

    public String getExecdate() {
        return execdate;
    }

    public void setExecdate(String execdate) {
        this.execdate = execdate;
    }

    public String getExec_empname() {
        return exec_empname;
    }

    public void setExec_empname(String exec_empname) {
        this.exec_empname = exec_empname;
    }
}
