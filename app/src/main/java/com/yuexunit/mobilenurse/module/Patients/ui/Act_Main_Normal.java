package com.yuexunit.mobilenurse.module.Patients.ui;


import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Login.ui.Act_Login;
import com.yuexunit.mobilenurse.module.PatientDetail.ui.Act_PatientDetail_Normal;
import com.yuexunit.mobilenurse.module.Patients.ui.fragment.Frag_PatientList_Normal;
import com.yuexunit.mobilenurse.module.Patients.ui.fragment.Frag_Upload;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.KJFragment;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.List;

public class Act_Main_Normal extends KJActivity {
    @BindView(id = R.id.bottombar_content1, click = true)
    private RadioButton mRbtnContent1;
    @BindView(id = R.id.bottombar_content2, click = true)
    private RadioButton mRbtnContent2;
    @BindView(id = R.id.bottombar_content3, click = true)
    private RadioButton mRbtnContent3;

    private Frag_PatientList_Normal contentFragment1;
    private Frag_Upload contentFragment2;
    private Frag_Upload contentFragment3;
    private KJFragment currentFragment;

    @Override
    public void initData() {
        super.initData();
        AppConfig.ISOVER_PATIENT=false;
        contentFragment1 = new Frag_PatientList_Normal();
//        contentFragment2 = new Frag_Upload();
        contentFragment3 = new Frag_Upload();
    }

    @Override
    public void initWidget() {
        super.initWidget();
        changeFragment(contentFragment1);
    }

    @Override
    public void onResume(){
        super.onResume();
        Register_Receiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();     
            try {
                unregisterReceiver(PatientReceiver);
            } catch (IllegalArgumentException e) {

            }      
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
            try {
                unregisterReceiver(PatientReceiver);
            } catch (IllegalArgumentException e) {

            }       
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.act_main);
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.bottombar_content1:
                changeFragment(contentFragment1);
                break;
            case R.id.bottombar_content2:
                //changeFragment(contentFragment2);
                break;
            case R.id.bottombar_content3:
                 changeFragment(contentFragment3);
                break;
            default:
                break;
        }
    }

    public void changeFragment(KJFragment targetFragment) {
        currentFragment = targetFragment;
        super.changeFragment(R.id.main_content, targetFragment);
    }

    /**
     * 通过dispatchTouchEvent每次ACTION_DOWN事件中动态判断非EditText本身区域的点击事件，然后在事件中进行屏蔽。然后当点击不是EditText时候，进行隐藏键盘操作
     *
     * @param ev
     * @return
     */

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {

        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件\
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    //注册条码Receiver
    private void Register_Receiver()
    {
        //注册条码Receiver
        IntentFilter scanDataIntentFilter = new IntentFilter();
        scanDataIntentFilter.addAction("com.android.scancontext");
        scanDataIntentFilter.addAction("com.android.scanservice.scancontext");
        registerReceiver(PatientReceiver, scanDataIntentFilter);

        if(!isServiceRunning("com.android.scanservice.ScanService")) {
            ViewInject.toast("未检测到扫描服务");
        }
    }

    //条码Receiver
    private BroadcastReceiver PatientReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent){
            String action = intent.getAction();
            int keycode = intent.getIntExtra("Scan_Keycode", 0);
            if (action.equals("com.android.scanservice.scancontext")) {
                String result = intent.getStringExtra("Scan_context");
                String type = intent.getStringExtra("Scan_type");
                if(!result.equals(""))
                {
                    if(AppConfig.ISOVER_PATIENT) {
                        if (result.length() == Integer.valueOf(PreferenceHelper.readString(aty, AppConfig.JCI_INFO, AppConfig.JCI_WD_RULE))) {
                            if(contentFragment1.JudgeVisitNo(result)) {
                                PreferenceHelper.write(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO, result);
                                AppConfig.VISITNO = result;
                                showActivity(aty, Act_PatientDetail_Normal.class);
                            }
                            else {
                                Toast.makeText(aty, "非本病区病人!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(aty, "请扫描病人腕带!", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Toast.makeText(aty, "病人列表尚未加载完!请加载完毕后,再扫描!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    // 检查Service是否运行
    private boolean isServiceRunning(String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager.getRunningServices(100);

        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    public void onBackPressed() {
        dialog_exit();
    }

    private void dialog_exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(aty);  //先得到构造器
        builder.setTitle("提示"); //设置标题
        builder.setMessage("是否确认退出?"); //设置内容
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() { //设置确定按钮
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss(); //关闭dialog
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setClass(aty, Act_Login.class);
                aty.startActivity(intent);
                aty.finish();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() { //设置取消按钮
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //参数都设置完成了，创建并显示出来
        builder.create().show();
    }
}
