package com.yuexunit.mobilenurse.module.SignInput.ui.view;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.SignsInput_Data;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface IAddSignType {

    /**
     * 显示基本体征项
     */
    void showAllType(ArrayList<SignsInput_Data> baselist);

    /**
     * 病人体征项存储
     */
    void isSave(ActionBean actionBean);

}
