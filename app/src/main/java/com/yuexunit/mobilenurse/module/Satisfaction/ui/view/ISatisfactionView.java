package com.yuexunit.mobilenurse.module.Satisfaction.ui.view;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBackBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionIsCommitBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionLastBean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/5/27.
 */
public interface ISatisfactionView {
    //满意度列表
    void showSatisfactionList(ArrayList<SatisfactionLastBean> list);

    //满意度提交
    void commitSatisfactionDate(SatisfactionCommitBackBean satisfactionCommitBackBean);

    //是否评价过满意度
    void iscommitSatisfaction(SatisfactionIsCommitBean satisfactionIsCommitBean);
}
