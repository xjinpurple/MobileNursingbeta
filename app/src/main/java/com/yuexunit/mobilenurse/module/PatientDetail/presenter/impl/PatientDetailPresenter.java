package com.yuexunit.mobilenurse.module.PatientDetail.presenter.impl;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.YXCollectBean;
import com.yuexunit.mobilenurse.module.PatientDetail.bean.YXPatientBean;
import com.yuexunit.mobilenurse.module.PatientDetail.model.IPatientDetailModel;
import com.yuexunit.mobilenurse.module.PatientDetail.model.impl.PatientDetailModel;
import com.yuexunit.mobilenurse.module.PatientDetail.presenter.IPatientDetailPresenter;
import com.yuexunit.mobilenurse.module.PatientDetail.ui.view.IPatientDetailView;
import com.yuexunit.mobilenurse.module.SignInput.bean.Sign_Single;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONObject;
import org.slf4j.Logger;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by sslcjy on 16/1/24.
 */
public class PatientDetailPresenter implements IPatientDetailPresenter {
    //记录Log
    private final Logger log = ProUtil.getLogger(PatientDetailModel.class);
    private IPatientDetailModel model;
    private IPatientDetailView view;
    public PatientDetailPresenter(IPatientDetailModel model, IPatientDetailView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void showPatientDetail(String Visitno) {
        model.getPatientDetail(Visitno)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.loadingDialogStatus(AppConfig.SHOW_DIALOG);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<Object, Boolean>() {
                    @Override
                    public Boolean call(Object response) {
                        log.info("PatientDetailPresenter_showPatientDeatil:"+response.toString());
                        return response != null;
                    }
                }).filter(new Func1<Object, Boolean>() {
            @Override
            public Boolean call(Object response) {
                JSONObject all, body;
                String result = null;
                try {
                    all = new JSONObject(response.toString());
                    body = all.getJSONObject("body");
                    result = body.getString("response");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(!"null".equals(result)){
                    return true;
                }else{
                    view.NoPatientDialog();
                    return false;
                }
            }
        }).map(new Func1<Object, YXPatientBean>() {
            @Override
            public YXPatientBean call(Object response) {
                YXPatientBean total = JSON.parseObject(response.toString(), YXPatientBean.class);
                YXPatientBean bean = total.getBody();
                log.info("PatientDetailPresenter_showPatientDeatil_bean:"+bean.toString());
                return bean;
            }
        }).subscribe(new Subscriber<YXPatientBean>() {
            @Override
            public void onCompleted() {
                log.info("PatientDetailPresenter_showPatientDeatil_onCompleted");
                view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
            }

            @Override
            public void onError(Throwable e) {
                log.info("PatientDetailPresenter_showPatientDeatil_onError:"+e);
                view.loadingDialogStatus(AppConfig.DISMISS_DIALOG);
                e.printStackTrace();
            }

            @Override
            public void onNext(YXPatientBean patientBean) {
                log.info("PatientDetailPresenter_showPatientDeatil_onNext:"+patientBean.toString());
                if ("0".equals(patientBean.getHead().getRet_code())) {
                    view.showPatientDetail(patientBean.getResponse().getPatinfo());
                }
            }
        });
    }

    @Override
    public void collect(String Visitno, String Code, String NurseId) {
        model.collect(Visitno, Code, NurseId)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.bloodDialog(AppConfig.BLOOD_DIALOG_UNPASS);
                    }

                    @Override
                    public void onNext(String result) {
                        JSONObject all, body;
                        String reponse = null;
                        try {
                            all = new JSONObject(result.toString());
                            body = all.getJSONObject("body");
                            reponse = body.getString("response");
                        } catch (Exception e) {
                            log.error("Exception:"+e);
                            view.bloodDialog(AppConfig.BLOOD_DIALOG_UNPASS);
                        }

                        if (result.toString().indexOf(AppConfig.ORACLEERROR) != -1) {
                            view.bloodDialog(AppConfig.BLOOD_DIALOG_REPEAT);
                        }
                        else {
                            if (!("null".equals(reponse))) {
                                YXCollectBean Total = JSON.parseObject(((String) result).toString(), YXCollectBean.class);
                                YXCollectBean Collectdate = Total.getBody();
                                if ("0".equals(Collectdate.getHead().getRet_code())) {
                                    view.bloodDialog(AppConfig.BLOOD_DIALOG_PASS);
                                } else {
                                    Log.v("jx", "Error:" + Collectdate.getHead().getRet_info());
                                    view.bloodDialog(AppConfig.BLOOD_DIALOG_UNPASS);
                                }
                            }
                            else {
                                view.bloodDialog(AppConfig.BLOOD_DIALOG_UNPASS);
                            }
                        }
                    }
                });
    }

    @Override
    public void getSingleTypes(String visitno) {
                model.getSingleTypes(visitno)
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<ArrayList<Sign_Single>>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(ArrayList<Sign_Single> sign_singles) {
                                if(sign_singles != null) {
                                    view.saveSingleTypes(sign_singles);
                                }else
                                {
                                    sign_singles = new ArrayList<Sign_Single>();
                                    view.saveSingleTypes(sign_singles);
                                }
                            }
                        });
    }
}
