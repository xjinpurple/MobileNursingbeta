package com.yuexunit.mobilenurse.module.Cost.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.Cost.bean.CostDetail_Patfeelist;

import java.util.ArrayList;

/**
 * Created by work-jx on 2015/11/25.
 */
public class CostListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CostDetail_Patfeelist> mDatas;

    public CostListAdapter(Context context, ArrayList<CostDetail_Patfeelist> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.costlist_item, parent, false);
            holder = new ViewHolder();
            holder.costlist_name = (TextView) convertView.findViewById(R.id.costlist_name);
            holder.costlist_count = (TextView) convertView.findViewById(R.id.costlist_count);
            holder.costlist_money = (TextView) convertView.findViewById(R.id.costlist_money);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.costlist_name.setText(mDatas.get(position).getFeename() + "*" + mDatas.get(position).getFeenum());
        holder.costlist_count.setText("数量:" + mDatas.get(position).getFeenum());
        holder.costlist_money.setText("金额¥" + mDatas.get(position).getFeeamount());

        return convertView;
    }

    class ViewHolder {
        TextView costlist_name, costlist_count, costlist_money;

    }
}
