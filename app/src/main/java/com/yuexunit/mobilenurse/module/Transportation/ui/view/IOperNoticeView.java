package com.yuexunit.mobilenurse.module.Transportation.ui.view;

import com.yuexunit.mobilenurse.module.Transportation.bean.OperNotice_OpsList;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/12/2.
 */
public interface IOperNoticeView {
    void showOpsNoticeList(ArrayList<OperNotice_OpsList> list);
}
