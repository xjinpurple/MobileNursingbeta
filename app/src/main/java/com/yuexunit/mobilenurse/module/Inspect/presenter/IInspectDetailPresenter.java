package com.yuexunit.mobilenurse.module.Inspect.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/1/29.
 */
public interface IInspectDetailPresenter {
    /*
    检查单详情
     */
    public void showInspectDetail(String requestno);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
