package com.yuexunit.mobilenurse.module.HealthArticles.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.HealthArticles.adapter.ArticleListAdapter;
import com.yuexunit.mobilenurse.module.HealthArticles.bean.ArticleTitle;
import com.yuexunit.mobilenurse.module.HealthArticles.model.impl.ArticleModel;
import com.yuexunit.mobilenurse.module.HealthArticles.presenter.impl.ArticlePresenter;
import com.yuexunit.mobilenurse.module.HealthArticles.ui.view.IArticleView;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 姚平 on 2015/12/5.
 * <p>
 * 健康宣教列表界面
 */
public class Act_Article extends KJActivity implements AdapterView.OnItemClickListener,IArticleView {
    @Bind(R.id.article_list)
    public ListView titlelist;

    public List<ArticleTitle> articleTitles;
    public ArrayList articleList = new ArrayList();
    private ArticlePresenter presenter;
    @OnClick(R.id.img_back)
    public void out(){
        finish();
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.act_article_list);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        presenter = new ArticlePresenter(new ArticleModel(),this);
    }

    @Override
    public void initWidget() {
        super.initWidget();
        presenter.showArticleList(PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_WARDNO_ID));
        titlelist.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", articleTitles.get(position).getArticle_id());
        bundle.putString("title", articleTitles.get(position).getTitle());
        showActivity(Act_Article.this, Act_ArticleContent.class, bundle);
    }

    @Override
    public void showArticleList(ArrayList<ArticleTitle> list) {
        articleTitles = list;
        titlelist.setAdapter(new ArticleListAdapter(titlelist,list, R.layout.item_article_list));
    }
}