package com.yuexunit.mobilenurse.module.Inspect.ui;

import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Inspect.model.impl.InspectDetailModel;
import com.yuexunit.mobilenurse.module.Inspect.presenter.impl.InspectDetailPresenter;
import com.yuexunit.mobilenurse.module.Inspect.ui.view.IInspectDetailView;
import com.yuexunit.mobilenurse.util.CreateJson;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by work-jx on 2015/11/24.
 // */
public class Act_InspectDetail extends TitleBar_DocAdvice implements IInspectDetailView
{

    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind( R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;
    @Bind(R.id.inspectdetail_content)
    public TextView inspectdetail_content;
    @Bind(R.id.inspectdetail_judge)
    public TextView inspectdetail_judge;
    @Bind(R.id.inspectdetail_reportno)
    public TextView inspectdetail_reportno;
    //申请单号
    private  String requestno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Act_InspectDetail.class);

    InspectDetailPresenter presenter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_inspectdetail);
        ButterKnife.bind(this);
    }
    @Override
    public void initData() {
        super.initData();
        presenter = new InspectDetailPresenter(this,new InspectDetailModel());
        requestno = getIntent().getExtras().getString("requestno");
        if(SystemTool.checkNet(aty)) {
            presenter.showInspectDetail(requestno);
        }
        else
        {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }

    @Override
    public void showInspectDetail(String content, String judge, String reportno) {
        inspectdetail_content.setText(content);
        inspectdetail_judge.setText(judge);
        inspectdetail_reportno.setText("单号:"+reportno);
    }

    @Override
    public void setTitle()
    {
        titlebar_docadvice_tv_title.setText("检查报告单");
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));

    }


    @Override
    public  void onBackClick()
    {
        finish();
    }
}

