package com.yuexunit.mobilenurse.module.Panic.ui;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yuexunit.mobilenurse.R;

import org.kymjs.kjframe.KJActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/16.
 */
public class Act_PanicKT extends KJActivity {

    @Bind(R.id.panickt_web)
    WebView panicktWeb;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_panickt);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        initWebView(panicktWeb);
        panicktWeb.loadUrl("http://www.baidu.com");
    }

    /**
     * webview初始化
     *
     * @return
     */
    private void initWebView(WebView webview) {
        //WebSettings settings = ad_webview_01.getSettings();
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setBlockNetworkImage(true);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        // webView.getSettings().setDatabaseEnabled(true);
        //  webView.getSettings().setDomStorageEnabled(true);
        //    String cacheDirPath = getFilesDir().getAbsolutePath()+APP_CACHE_DIRNAME;
        //Log.i("path",cacheDirPath);
        //  webView.getSettings().setDatabasePath(cacheDirPath);
        //  webView.getSettings().setAppCachePath(cacheDirPath);
        //    webView.getSettings().setAppCacheEnabled(true);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                view.getSettings().setBlockNetworkImage(false);
                super.onPageFinished(view, url);
            }


        });
        webview.setSaveEnabled(false);
    }

    @OnClick(R.id.panicinfo_img_back)
    public void onClick() {
        finish();
    }
}
