package com.yuexunit.mobilenurse.module.NursingPlan.bean;

/**
 * Created by work-jx on 2017/1/22.
 */
public class NursingPlanBean {
    private String title;
    private String date;
    private String name;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
