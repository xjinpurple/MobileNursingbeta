package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by 姚平 on 2015/12/7.
 */

//获取病人当日可执行医嘱（注射、发药需要分开）
public class YXDocAdviceBean {
    public YXDocAdviceBean body;
    public Head head;
    public DocAdvice_Response response;


    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public DocAdvice_Response getResponse() {
        return response;
    }

    public void setResponse(DocAdvice_Response response) {
        this.response = response;
    }

    public YXDocAdviceBean getBody() {
        return body;
    }

    public void setBody(YXDocAdviceBean body) {
        this.body = body;
    }
}
