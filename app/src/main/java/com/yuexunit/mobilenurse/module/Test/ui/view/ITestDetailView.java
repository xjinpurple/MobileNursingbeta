package com.yuexunit.mobilenurse.module.Test.ui.view;

import com.yuexunit.mobilenurse.module.Test.bean.TestDetail_Asrptdetail;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/2/1.
 */
public interface ITestDetailView {
    //化验单详情列表
    void showTestDetail(ArrayList<TestDetail_Asrptdetail> list,String requestno);
}
