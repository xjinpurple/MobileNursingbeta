package com.yuexunit.mobilenurse.module.Transportation.ui.view;

import com.yuexunit.mobilenurse.module.Transportation.bean.RfidBean;

/**
 * Created by work-jx on 2016/9/29.
 */
public interface ITransportationView {
    void showRfidInfo(RfidBean rfidBean);
    void execOps(String s);
}
