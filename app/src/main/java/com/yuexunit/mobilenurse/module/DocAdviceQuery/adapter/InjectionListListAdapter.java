package com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_OrderDetail;

import java.util.ArrayList;



/**
 * Created by 姚平 on 2015/11/22.
 */
public class InjectionListListAdapter extends BaseAdapter{

    private Context context;
//    protected LayoutInflater mInflater;
    private ArrayList<DocAdvice_OrderDetail> mDatas;

    public InjectionListListAdapter(Context context,ArrayList<DocAdvice_OrderDetail> mDatas){
        this.context = context;
        this.mDatas = mDatas;
    }


    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.injectionlist_itemlist, parent, false);
            holder = new ViewHolder();
            holder.injection_list_item_name = (TextView)convertView.findViewById(R.id.injection_list_item_name);
            holder.injection_list_item_dosage = (TextView)convertView.findViewById(R.id.injection_list_item_dosage);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.injection_list_item_name.setText(mDatas.get(position).getMedname()+" * "+mDatas.get(position).getNum());
        holder.injection_list_item_dosage.setText(mDatas.get(position).getDosage()+mDatas.get(position).getDoseusnit());

        return convertView;
    }

    class ViewHolder{
        TextView injection_list_item_name,injection_list_item_dosage;
    }

}
