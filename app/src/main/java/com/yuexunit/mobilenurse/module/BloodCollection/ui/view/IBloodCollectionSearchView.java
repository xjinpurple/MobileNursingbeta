package com.yuexunit.mobilenurse.module.BloodCollection.ui.view;

import com.yuexunit.mobilenurse.module.BloodCollection.bean.Specinfo;

import java.util.ArrayList;

/**
 * Created by work-jx on 2017/5/4.
 */
public interface IBloodCollectionSearchView {
    void showBloodSearchList(ArrayList<Specinfo> list);

    void dialogState(int status);
}
