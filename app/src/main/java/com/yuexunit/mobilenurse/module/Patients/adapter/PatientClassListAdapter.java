package com.yuexunit.mobilenurse.module.Patients.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.PatientDetail.ui.Act_PatientDetail_Normal;
import com.yuexunit.mobilenurse.module.Patients.bean.PatientClassList_Patinfo;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by work-jx on 2016/10/9.
 */
public class PatientClassListAdapter extends KJAdapter<PatientClassList_Patinfo> {
    private ArrayList<PatientClassList_Patinfo> patients = new ArrayList<PatientClassList_Patinfo>();
    ArrayList<PatientClassList_Patinfo> patients_filter = new ArrayList<PatientClassList_Patinfo>();
    public PatientClassListAdapter(AbsListView view, Collection mDatas, int itemLayoutId) {
        super(view, mDatas, itemLayoutId);
        patients = (ArrayList<PatientClassList_Patinfo>) ((ArrayList)mDatas).clone();
    }

    @Override
    public void convert(AdapterHolder adapterHolder, PatientClassList_Patinfo item, boolean isScrolling) {
        adapterHolder.setText(R.id.patient_name, item.getName());
        adapterHolder.setText(R.id.patient_bed, item.getBedcode());
        adapterHolder.setText(R.id.patient_hospital, item.getVisitno());

        //信息提醒的红点,暂时没这个功能 暂时先都隐藏
//        adapterHolder.getView(R.id.patient_message).setVisibility(View.GONE);
        //设置病人头像
        if ("1".equals(item.getSex()) && item.getAge() > AppConfig.AGE_LIMIT) {
            adapterHolder.setImageResource(R.id.patient_pic, R.drawable.pic_male_circle);
        } else if ("1".equals(item.getSex()) && item.getAge() < AppConfig.AGE_LIMIT) {
            adapterHolder.setImageResource(R.id.patient_pic, R.drawable.pic_boy_circle);
        } else if ("2".equals(item.getSex()) && item.getAge() > AppConfig.AGE_LIMIT) {
            adapterHolder.setImageResource(R.id.patient_pic, R.drawable.pic_female_circle);
        } else {
            adapterHolder.setImageResource(R.id.patient_pic, R.drawable.pic_girl_circle);
        }

        ImageView imageView = adapterHolder.getView(R.id.patient_degree);
        if ("1".equals(item.getCarelevel())) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.bg_first);
//            adapterHolder.setImageResource(R.id.patient_degree, R.drawable.bg_first);
        } else if ("2".equals(item.getCarelevel())) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.bg_second);
//            adapterHolder.setImageResource(R.id.patient_degree, R.drawable.bg_second);
        } else {
//            adapterHolder.setImageResource(R.id.patient_degree, R.drawable.yyx_transpant);
            imageView.setVisibility(View.GONE);
        }

        //对item设置点击事件，跳转到病人详情界面
        onPicClick(adapterHolder.getConvertView(), item.getVisitno(), item.getBedcode(), item.getName());
    }

    private void onPicClick(View view, final String VisitNo, final String BedNo, final String Name) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //这里是通过列表点击进入到某个病人详情，进行存储当前病人的住院号，如果是通过扫描的话，
                //要额外在扫描代码处添加存储当前病人住院号代码
                if ("0".equals(PreferenceHelper.readString(mCxt, AppConfig.JCI_INFO, AppConfig.JCI_JCI))) {
                    PreferenceHelper.write(mCxt, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO, VisitNo);
                    AppConfig.VISITNO = VisitNo;
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_BayNexus.class);
                    Intent intent = new Intent(mCxt, Act_PatientDetail_Normal.class);
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_M80.class);
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_Advantech.class);
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_HoneyWell.class);
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_Lachsis.class);
//                    Intent intent = new Intent(mCxt, Act_PatientDetail_Joyree.class);
                    mCxt.startActivity(intent);
                } else {
                    if (true){//"0".equals(PreferenceHelper.readString(mCxt, AppConfig.JCI_INFO, AppConfig.JCI_ENABLE))) {
                        PreferenceHelper.write(mCxt, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO, VisitNo);
                        AppConfig.VISITNO = VisitNo;
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_BayNexus.class);
                        Intent intent = new Intent(mCxt, Act_PatientDetail_Normal.class);
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_M80.class);
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_Advantech.class);
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_HoneyWell.class);
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_Lachsis.class);
//                        Intent intent = new Intent(mCxt, Act_PatientDetail_Joyree.class);
                        mCxt.startActivity(intent);
                    } else {
                        ViewInject.toast("已启用JCI标准，必须通过扫描进入");
                    }
                }
            }
        });
    }
}
