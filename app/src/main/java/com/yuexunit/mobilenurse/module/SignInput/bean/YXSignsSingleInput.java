package com.yuexunit.mobilenurse.module.SignInput.bean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/5/11.
 */
public class YXSignsSingleInput {
    public int code;
    public String message;
    public ArrayList<Sign_Single> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Sign_Single> getData() {
        return data;
    }

    public void setData(ArrayList<Sign_Single> data) {
        this.data = data;
    }
}
