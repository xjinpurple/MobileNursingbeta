package com.yuexunit.mobilenurse.module.PatientDetail.bean;

/**
 * Created by 姚平 on 2015/11/22.
 */
public class PatientBean {
    private String ret_code;
    private String ret_info;
    private String visit_no;
    private String name;
    private String sex;
    private String deptno;
    private String deptname;
    private String wardno;
    private String wardname;
    private String bedcode;
    private String totalfee;
    private String prepay;

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_info() {
        return ret_info;
    }

    public void setRet_info(String ret_info) {
        this.ret_info = ret_info;
    }

    public String getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(String visit_no) {
        this.visit_no = visit_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDeptno() {
        return deptno;
    }

    public void setDeptno(String deptno) {
        this.deptno = deptno;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getWardno() {
        return wardno;
    }

    public void setWardno(String wardno) {
        this.wardno = wardno;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }

    public String getBedcode() {
        return bedcode;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public String getTotalfee() {
        return totalfee;
    }

    public void setTotalfee(String totalfee) {
        this.totalfee = totalfee;
    }

    public String getPrepay() {
        return prepay;
    }

    public void setPrepay(String prepay) {
        this.prepay = prepay;
    }
}
