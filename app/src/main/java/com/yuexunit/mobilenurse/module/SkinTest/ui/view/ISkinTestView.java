package com.yuexunit.mobilenurse.module.SkinTest.ui.view;

import com.yuexunit.mobilenurse.module.SkinTest.bean.SkinTestDetailBean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/6/29.
 */
public interface ISkinTestView {
    void showSkinTestDate(ArrayList<SkinTestDetailBean> list);

    void loadingDialogStatus(int status);

    void getMedicineStatus(boolean issuccess);

    void ExecStatus(boolean issuccess);
}
