package com.yuexunit.mobilenurse.module.TxtNode.bean;

/**
 * Created by work-jx on 2016/6/19.
 */
public class TxtWirteBean {
    private int id;
    //护士id
    private String nurseid;
    //病人姓名
    private String name;
    //床位号
    private String bedcode;
    //住院号
    private String visitno;
    //笔记内容
    private String txtcontent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNurseid() {
        return nurseid;
    }

    public void setNurseid(String nurseid) {
        this.nurseid = nurseid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBedcode() {
        return bedcode;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public String getVisitno() {
        return visitno;
    }

    public void setVisitno(String visitno) {
        this.visitno = visitno;
    }

    public String getTxtcontent() {
        return txtcontent;
    }

    public void setTxtcontent(String txtcontent) {
        this.txtcontent = txtcontent;
    }
}
