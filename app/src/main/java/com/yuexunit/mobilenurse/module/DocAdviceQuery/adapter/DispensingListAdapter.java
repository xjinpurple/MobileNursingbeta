package com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Dispensing;

import java.util.ArrayList;



/**
 * Created by jx on 2015/11/22.
 */
public class DispensingListAdapter extends BaseAdapter{

    private Context context;
//    private Frag_Dispensing activity;
    private ArrayList<DocAdvice_Dispensing> mDatas;
//    protected LayoutInflater mInflater;

    public DispensingListAdapter(Context context,ArrayList<DocAdvice_Dispensing> mDatas){
        this.context = context;
//        this.activity =activity;
        this.mDatas = mDatas;
    }


    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.dispensinglist_query_item, parent, false);
            holder = new ViewHolder();
            holder.dispensinglist_list = (ListView)convertView.findViewById(R.id.dispensinglist_list);
            holder.dispensinglist_statu_btn = (Button)convertView.findViewById(R.id.dispensinglist_statu_btn);
            holder.dispensinglist_plan_usage = (TextView)convertView.findViewById(R.id.dispensinglist_plan_usage);
            holder.dispensinglist_time = (TextView)convertView.findViewById(R.id.dispensinglist_time);
            holder.dispensinglist_ll = (RelativeLayout)convertView.findViewById(R.id.dispensinglist_ll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.dispensinglist_plan_usage.setText(mDatas.get(position).getDocAdvice_orderlist().getExecfreq() + "  "+ mDatas.get(position).getDocAdvice_orderlist().getExecway());
        if(null != mDatas.get(position).getDocAdvice_orderlist().getStoptime())
        {
            holder.dispensinglist_time.setText("医嘱时间："+mDatas.get(position).getDocAdvice_orderlist().getOrdertime()+"-"+mDatas.get(position).getDocAdvice_orderlist().getStoptime());
        }
        else
        {
            holder.dispensinglist_time.setText("医嘱时间："+mDatas.get(position).getDocAdvice_orderlist().getOrdertime()+"-");
        }

        BaseAdapter adapter  = new DispensingListListAdapter(context,mDatas.get(position).getDespensing_detail());
        holder.dispensinglist_list.setAdapter(adapter);

        return convertView;
    }

    class ViewHolder{
        ListView dispensinglist_list;
        Button dispensinglist_statu_btn;
        TextView dispensinglist_plan_usage,dispensinglist_time;
        RelativeLayout dispensinglist_ll;

    }


}
