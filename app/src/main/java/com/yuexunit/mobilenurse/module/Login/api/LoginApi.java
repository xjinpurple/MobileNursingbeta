package com.yuexunit.mobilenurse.module.Login.api;


import com.yuexunit.mobilenurse.base.bean.ActionBean;

import retrofit.http.GET;
import rx.Observable;


/**
 * Created by sslcjy on 16/1/18.
 */
public interface LoginApi {

    //获取JCI标准信息
    @GET("ydhl/api/sysvar/list")
    Observable<ActionBean> ListParams();

}
