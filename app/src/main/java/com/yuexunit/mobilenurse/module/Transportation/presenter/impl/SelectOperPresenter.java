package com.yuexunit.mobilenurse.module.Transportation.presenter.impl;

import com.alibaba.fastjson.JSON;
import com.yuexunit.mobilenurse.module.Transportation.bean.TransportationRecord_Opsinfo;
import com.yuexunit.mobilenurse.module.Transportation.bean.YXTransportationRecordBean;
import com.yuexunit.mobilenurse.module.Transportation.model.ISelectOperModel;
import com.yuexunit.mobilenurse.module.Transportation.presenter.ISelectOperPresenter;
import com.yuexunit.mobilenurse.module.Transportation.ui.view.ISelectOperView;
import com.yuexunit.mobilenurse.util.RxUtils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/9/27.
 */
public class SelectOperPresenter implements ISelectOperPresenter{
    private ISelectOperView view;
    private ISelectOperModel model;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    ArrayList<TransportationRecord_Opsinfo> opsinfos_over = new ArrayList<TransportationRecord_Opsinfo>();

    public SelectOperPresenter(ISelectOperView view,ISelectOperModel model)
    {
        this.view = view;
        this.model = model;
    }

    @Override
    public void showOpsList(String visitno) {
        compositeSubscription.add(
                model.getOpsListData(visitno)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                ViewInject.toast("暂无数据");
                                view.showOpsList(opsinfos_over);
                            }

                            @Override
                            public void onNext(String result) {
                                YXTransportationRecordBean Total = JSON.parseObject(result, YXTransportationRecordBean.class);
                                YXTransportationRecordBean OpsData = Total.getBody();

                                if ("0".equals(OpsData.getHead().getRet_code())) {
                                    List<TransportationRecord_Opsinfo> opsinfos;

                                    String opsinfo = OpsData.getResponse().getOpsinfo();
                                    String opsinfo_first = opsinfo.substring(0, 1);
                                    if (!opsinfo_first.equals("[")) {
                                        opsinfo = "[" + opsinfo + "]";
                                        opsinfos = JSON.parseArray(opsinfo, TransportationRecord_Opsinfo.class);
                                    } else {
                                        opsinfos = JSON.parseArray(opsinfo, TransportationRecord_Opsinfo.class);
                                    }

                                    Map<String, ArrayList<TransportationRecord_Opsinfo>> params = new HashMap<>();
                                    for (int i = 0; i < opsinfos.size(); i++)
                                    {
                                        if(!params.containsKey(opsinfos.get(i).getRequestno()))
                                        {
                                            ArrayList<TransportationRecord_Opsinfo> opsinfos_map = new ArrayList<TransportationRecord_Opsinfo>();
                                            opsinfos_map.add(opsinfos.get(i));
                                            params.put(opsinfos.get(i).getRequestno(),opsinfos_map);
                                        }
                                        else {
                                            params.get(opsinfos.get(i).getRequestno()).add(opsinfos.get(i));
                                        }
                                    }

                                    Iterator keys = params.keySet().iterator();
                                    while (keys.hasNext())
                                    {
                                        String key = (String) keys.next();
                                        ArrayList<TransportationRecord_Opsinfo> opsinfos_merge = params.get(key);
                                        TransportationRecord_Opsinfo opsinfo_merge = new TransportationRecord_Opsinfo();
                                        String opsinfo_opsname = "";
                                        for (int i = 0;i<opsinfos_merge.size();i++)
                                        {
                                            if(opsinfo_opsname.length() > 0)
                                            {
                                                opsinfo_opsname = opsinfo_opsname +","+opsinfos_merge.get(i).getOpsname();
                                            }
                                            else {
                                                opsinfo_opsname = opsinfos_merge.get(i).getOpsname();
                                            }
                                        }
                                        opsinfo_merge = opsinfos_merge.get(0);
                                        opsinfo_merge.setOpsname(opsinfo_opsname);
                                        opsinfos_over.add(opsinfo_merge);
                                    }
                                    view.showOpsList(opsinfos_over);

                                } else {
                                    view.showOpsList(opsinfos_over);
                                    ViewInject.toast("获取转运列表失败：" + OpsData.getHead().getRet_info());
                                }
                            }
                        })
        );
    }

    public CompositeSubscription getCompositeSubscription() {
        return RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
    }

    public void UnSubObservers() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }

    }
}
