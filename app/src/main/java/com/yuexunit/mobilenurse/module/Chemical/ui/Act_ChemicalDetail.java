package com.yuexunit.mobilenurse.module.Chemical.ui;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Chemical.adapter.ChemicalDetailAdapter;
import com.yuexunit.mobilenurse.module.Chemical.bean.ChemicalDetailBean;
import com.yuexunit.mobilenurse.module.Chemical.model.impl.ChemicalModel;
import com.yuexunit.mobilenurse.module.Chemical.presenter.IChemicalPresenter;
import com.yuexunit.mobilenurse.module.Chemical.presenter.impl.ChemicalPresenter;
import com.yuexunit.mobilenurse.module.Chemical.ui.view.IChemicalView;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/17.
 */
public class Act_ChemicalDetail extends KJActivity implements IChemicalView
{

    @Bind(R.id.chemicaldetail_code)
    TextView chemicaldetailCode;
    @Bind(R.id.chemicaldetail_recipe)
    TextView chemicaldetailRecipe;
    @Bind(R.id.chemicaldetail_visit)
    TextView chemicaldetailVisit;
    @Bind(R.id.chemicaldetail_sex)
    TextView chemicaldetailSex;
    @Bind(R.id.chemicaldetail_age)
    TextView chemicaldetailAge;
    @Bind(R.id.chemicaldetail_bedcode)
    TextView chemicaldetailBedcode;
    @Bind(R.id.chemicaldetail_visitno)
    TextView chemicaldetailVisitno;
    @Bind(R.id.chemicaldetail_doctor)
    TextView chemicaldetailDoctor;
    @Bind(R.id.chemical_list)
    ListView chemicalList;

    private BaseAdapter adapter;
    private ArrayList<ChemicalDetailBean> chemicalDetailBeans = new ArrayList<ChemicalDetailBean>();

    String barcode;

    IChemicalPresenter presenter;
    private LoadingDialog dialog;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_chemicaldetail);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        barcode = getIntent().getStringExtra("barcode");
        dialog = new LoadingDialog(aty);
        presenter = new ChemicalPresenter(this,new ChemicalModel());
        presenter.showChemicalDate(barcode);
    }

    @OnClick({R.id.chemicaldetail_img_back, R.id.chemicaldetail_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chemicaldetail_img_back:
                finish();
                break;
            case R.id.chemicaldetail_btn:
                presenter.execChemicalDate(barcode, PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID));
                break;
        }
    }

    @Override
    public void showChemicalDate(ArrayList<ChemicalDetailBean> list) {
        adapter = new ChemicalDetailAdapter(chemicalList,list,R.layout.item_chemicaldetail);
        chemicalList.setAdapter(adapter);

        set_date(list.get(0));
    }

    @Override
    public void loadingDialogStatus(int status) {
        switch (status) {
            case AppConfig.SHOW_DIALOG:
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                break;
            case AppConfig.DISMISS_DIALOG:
                dialog.dismiss();
                break;
        }
    }

    @Override
    public void isExecSuccess(boolean issuccess) {
        if(issuccess)
        {
            ViewInject.toast("化药成功");
            finish();
        }
        else{
            ViewInject.toast("化药失败");
        }
    }


    private void set_date(ChemicalDetailBean chemicalDetailBean)
    {
        chemicaldetailCode.setText(barcode);
        chemicaldetailVisit.setText(chemicalDetailBean.getBrxm());
        chemicaldetailSex.setText(chemicalDetailBean.getBrxb());
        chemicaldetailAge.setText(chemicalDetailBean.getBrnl());
        chemicaldetailBedcode.setText(chemicalDetailBean.getCwh());
        chemicaldetailVisitno.setText(chemicalDetailBean.getZyh());
        chemicaldetailDoctor.setText(chemicalDetailBean.getYsxm());
    }
}
