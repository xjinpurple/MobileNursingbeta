package com.yuexunit.mobilenurse.module.SkinTest.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/6/29.
 */
public class YXSkinTestBean {
    public YXSkinTestBean body;
    public Head head;
    public Stinfo_SkinTest response;

    public YXSkinTestBean getBody() {
        return body;
    }

    public void setBody(YXSkinTestBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Stinfo_SkinTest getResponse() {
        return response;
    }

    public void setResponse(Stinfo_SkinTest response) {
        this.response = response;
    }
}
