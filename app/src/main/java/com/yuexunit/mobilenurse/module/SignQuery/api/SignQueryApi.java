package com.yuexunit.mobilenurse.module.SignQuery.api;


import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface SignQueryApi {
    @GET("ydhl/api/sign/offical/get")
    Observable<YXSignQuery> getShowSignInfo(@Query("inpNo") String inpNo, @Query("interval") int interval, @Query("date") String date);
}
