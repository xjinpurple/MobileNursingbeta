package com.yuexunit.mobilenurse.module.Satisfaction.xml;

import android.util.Xml;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBackBean;

import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;

/**
 * Created by work-jx on 2016/6/7.
 */
public class SatisfactionCommitXml {

    public static SatisfactionCommitBackBean getSatisfactionData(String s)
    {
        if(s == null)
            return null;
        XmlPullParser parser = Xml.newPullParser();
        SatisfactionCommitBackBean satisfactionCommitBackBean = null;
        ByteArrayInputStream tInputStringStream = null;
        try
        {
            if (s != null && !s.trim().equals("")) {
                tInputStringStream = new ByteArrayInputStream(s.getBytes());
            }
        }
        catch (Exception e) {
        }
        try
        {
            parser.setInput(tInputStringStream,"utf-8");

            int type = parser.getEventType();
            while(type != XmlPullParser.END_DOCUMENT)
            {
                switch (type)
                {
                    case XmlPullParser.START_TAG:
                        if("body".equals(parser.getName()))
                        {
                        }else if("response".equals(parser.getName()))
                        {
                        }else if("ret_code".equals(parser.getName()))
                        {
                            satisfactionCommitBackBean = new SatisfactionCommitBackBean();
                            satisfactionCommitBackBean.setRet_code(parser.nextText());
                        }else if("ret_info".equals(parser.getName()))
                        {
                            satisfactionCommitBackBean.setRet_info(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;

                    default:
                        break;
                }
                type = parser.next();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return satisfactionCommitBackBean;
    }
}
