package com.yuexunit.mobilenurse.module.SignQuery.bean;


import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/4/28.
 */
public class YXSignInputListBean {
    public YXSignInputListBean body;
    public Head head;
    public InputList_Response response;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public InputList_Response getResponse() {
        return response;
    }

    public void setResponse(InputList_Response response) {
        this.response = response;
    }

    public YXSignInputListBean getBody() {

        return body;
    }

    public void setBody(YXSignInputListBean body) {
        this.body = body;
    }
}
