package com.yuexunit.mobilenurse.module.TxtNode.ui;

import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.TxtNode.adapter.TxtReadAdapter;
import com.yuexunit.mobilenurse.module.TxtNode.bean.TxtWirteBean;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.KJDB;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/6/19.
 */
public class Act_TxtRead extends KJActivity {

    @Bind(R.id.txtread_list)
    ListView txtreadList;

    //KJDB访问数据库
    KJDB kjdb;

    private String Nurse_Id;
    ArrayList<TxtWirteBean> txtWirteBeans = new ArrayList<TxtWirteBean>();

    public BaseAdapter adapter;

    @Override
    public void setRootView() {
        setContentView(R.layout.act_txtread);
        ButterKnife.bind(this);
    }

    @Override
    public void initData() {
        super.initData();
        kjdb = KJDB.create();
        Nurse_Id = PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_LOGID);
        txtWirteBeans = (ArrayList<TxtWirteBean>) kjdb.findAllByWhere(TxtWirteBean.class,"nurseid='"+Nurse_Id+"'");
        ShowDatas(txtWirteBeans);
    }

    public void ShowDatas(ArrayList<TxtWirteBean> txtWirteBeans)
    {
        adapter = new TxtReadAdapter(this,txtreadList,txtWirteBeans,R.layout.item_txtread);
        txtreadList.setAdapter(adapter);
    }

    public void deletenote(TxtWirteBean txtWirteBean)
    {
        try {
            kjdb.delete(txtWirteBean);
            txtWirteBeans.remove(txtWirteBean);
            adapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            System.out.print(e.toString());
        }
    }


    @OnClick(R.id.txtwrite_img_back)
    public void onClick() {
        finish();
    }
}
