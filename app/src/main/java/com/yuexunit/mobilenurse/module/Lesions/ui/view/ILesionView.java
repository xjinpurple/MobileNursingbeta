package com.yuexunit.mobilenurse.module.Lesions.ui.view;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/21.
 */
public interface ILesionView {
    /**
     * 显示病区列表
     */
     void showLesionList(ArrayList list);

}
