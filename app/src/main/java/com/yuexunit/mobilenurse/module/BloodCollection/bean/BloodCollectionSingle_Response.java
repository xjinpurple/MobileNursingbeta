package com.yuexunit.mobilenurse.module.BloodCollection.bean;

/**
 * Created by work-jx on 2017/5/4.
 */
public class BloodCollectionSingle_Response {
    public Specinfo specinfo;

    public Specinfo getSpecinfo() {
        return specinfo;
    }

    public void setSpecinfo(Specinfo specinfo) {
        this.specinfo = specinfo;
    }
}
