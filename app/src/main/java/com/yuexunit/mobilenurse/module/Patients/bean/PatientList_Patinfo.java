package com.yuexunit.mobilenurse.module.Patients.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by work-jx on 2015/12/11.
 */
public class PatientList_Patinfo {

    //住院号
    public String visit_no;
    //病人姓名
    public String name;
    //病人年龄
    public int age;
    //病人性别
    public String sex;
    //病人出生日期
    @JSONField(format = "yyyy-MM-dd")
    public Date birth;
    //护理等级代码
    public String gradecode;
    //护理等级
    public String gradename;
    //床位号
    public String bedcode;
    //入院日期
    @JSONField(format = "yyyy-MM-dd")
    public Date admissiondate;
    //Y+s+h+
    public String YSH;

    public String getVisit_no() {
        return visit_no;
    }

    public void setVisit_no(String visit_no) {
        this.visit_no = visit_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGradecode() {
        return gradecode;
    }

    public void setGradecode(String gradecode) {
        this.gradecode = gradecode;
    }

    public String getGradename() {
        return gradename;
    }

    public void setGradename(String gradename) {
        this.gradename = gradename;
    }

    public String getBedcode() {
        return bedcode;
    }

    public void setBedcode(String bedcode) {
        this.bedcode = bedcode;
    }

    public Date getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(Date admissiondate) {
        this.admissiondate = admissiondate;
    }

    public String getYSH() {
        return YSH;
    }

    public void setYSH(String YSH) {
        this.YSH = YSH;
    }
}
