package com.yuexunit.mobilenurse.module.HospitalInfo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter.DispensingListAdapter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.adapter.InjectionListAdapter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Dispensing;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.bean.DocAdvice_Injection;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.model.impl.DispensingModel;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.model.impl.InjectionModel;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.IDispensingPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.IInjectionPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.impl.DispensingPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.presenter.impl.InjectionPresenter;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view.IDispensingView;
import com.yuexunit.mobilenurse.module.DocAdviceQuery.ui.view.IInjectionView;
import com.yuexunit.mobilenurse.module.HospitalInfo.Act_HospitalInfo;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.widget.EmptyLayout;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.SystemTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/7/14.
 */
public class Frag_DocAdviceQuery extends Fragment implements IDispensingView,IInjectionView{


    protected View mMainView;
    protected Context mContext;
    @Bind(R.id.doc_query_long)
    TextView docQueryLong;
    @Bind(R.id.doc_query_short)
    TextView docQueryShort;
    @Bind(R.id.doc_query_long_list)
    ListView docQueryLongList;
    @Bind(R.id.doc_query_short_list)
    ListView docQueryShortList;
    @Bind(R.id.empty_layout)
    EmptyLayout emptyLayout;
    private Act_HospitalInfo aty;

    IDispensingPresenter presenter_long;
    IInjectionPresenter presenter_short;
    private BaseAdapter adapter_long,adapter_short;

    //病人住院号
    private  String visitno;
    //记录Log
    private final Logger log = LoggerFactory.getLogger(Frag_DocAdviceQuery.class);

    ArrayList<DocAdvice_Dispensing> list_long = new ArrayList<DocAdvice_Dispensing>();
    ArrayList<DocAdvice_Injection> list_short = new ArrayList<DocAdvice_Injection>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        aty = (Act_HospitalInfo) getActivity();
        mMainView = inflater.inflate(R.layout.frag_docadvicequery, container, false);
        ButterKnife.bind(this, mMainView);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter_long = new DispensingPresenter(this, new DispensingModel());
        presenter_short = new InjectionPresenter(this, new InjectionModel());
        visitno = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });

        if (SystemTool.checkNet(aty)) {
            presenter_long.showDispensingList(visitno);
        } else {
            ViewInject.toast("网络异常，请检查网络是否连接!");
            log.error(CreateJson.LOG_JSON("", "1", PreferenceHelper.readString(aty, AppConfig.NURSE_INFO, AppConfig.NURSE_DEPTNO), ""));
        }
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void showDispensingList(ArrayList<DocAdvice_Dispensing> list) {
        list_long = list;
        adapter_long = new DispensingListAdapter(aty,list_long);
        docQueryLongList.setAdapter(adapter_long);
        emptyLayout.dismiss();
    }

    @Override
    public void showInjectionList(ArrayList<DocAdvice_Injection> list) {
        list_short = list;
        adapter_short = new InjectionListAdapter(aty,list_short);
        docQueryShortList.setAdapter(adapter_short);
        emptyLayout.dismiss();
    }

    @OnClick({R.id.doc_query_long, R.id.doc_query_short})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.doc_query_long:
                if(list_long != null) {
                    list_long.clear();
                    adapter_long.notifyDataSetChanged();
                }
                presenter_long.showDispensingList(visitno);
                docQueryShortList.setVisibility(View.GONE);
                docQueryLongList.setVisibility(View.VISIBLE);
                docQueryLong.setBackgroundResource(R.drawable.ic_doc_query_selected);
                docQueryLong.setTextColor(getResources().getColor(R.color.white));
                docQueryShort.setBackgroundResource(R.drawable.ic_doc_query_unselected);
                docQueryShort.setTextColor(getResources().getColor(R.color.test_01));
                break;
            case R.id.doc_query_short:
                if(adapter_short != null) {
                    list_short.clear();
                    adapter_short.notifyDataSetChanged();
                }
                presenter_short.showInjectionList(visitno);
                docQueryShortList.setVisibility(View.VISIBLE);
                docQueryLongList.setVisibility(View.GONE);
                docQueryLong.setBackgroundResource(R.drawable.ic_doc_query_unselected);
                docQueryLong.setTextColor(getResources().getColor(R.color.test_01));
                docQueryShort.setBackgroundResource(R.drawable.ic_doc_query_selected);
                docQueryShort.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }
}
