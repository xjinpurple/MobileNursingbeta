package com.yuexunit.mobilenurse.module.Login.model.impl;

import com.alibaba.fastjson.JSON;
import com.facebook.stetho.common.StringUtil;
import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Login.api.LoginApi;
import com.yuexunit.mobilenurse.module.Login.bean.SystemConfig;
import com.yuexunit.mobilenurse.module.Login.model.ILoginModel;
import com.yuexunit.mobilenurse.util.ApiUtil;
import com.yuexunit.mobilenurse.util.CreateJson;
import com.yuexunit.mobilenurse.util.HttpUtil;
import com.yuexunit.mobilenurse.util.JtConstant;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;

import java.io.ByteArrayInputStream;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by sslcjy on 16/1/18.
 */
public class LoginModel implements ILoginModel {
    private LoginApi api;

    //记录Log
    private final Logger log = ProUtil.getLogger(LoginModel.class);

    public LoginApi ApiInstance() {
        if (api != null) {
            return api;
        } else {
            return ApiUtil.getInstance().createRetrofitApi(LoginApi.class);
        }
    }

    //获取JCI配置信息
    @Override
    public Observable<SystemConfig> getListParams() {
        api = ApiInstance();
        return api.ListParams().filter(new Func1<ActionBean, Boolean>() {
            @Override
            public Boolean call(ActionBean actionBean) {
                log.debug("getListParams_getCode:"+actionBean.getCode());
                return actionBean.getCode() == 200;
            }
        }).map(new Func1<ActionBean, SystemConfig>() {
            @Override
            public SystemConfig call(ActionBean actionBean) {
                log.debug("getListParams_getData:"+actionBean.getData());
                return JSON.parseObject(actionBean.getData(), SystemConfig.class);
            }
        });
    }

    //查询账号密码是否能登陆
    @Override
    public Observable<Object> doInLogin(final String uid, final String pwd) {

        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                String nameSpace = AppConfig.WEB_NAME_SPACE;
                String methodName = AppConfig.WEB_NAME_METHOD;
                String url = AppConfig.WEB_CONTENT;
                SoapObject soapObject = new SoapObject(nameSpace, methodName);
                soapObject.addProperty("XmlString", CreateJson.Login_Json(uid, pwd));
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true; //very important for compatibility
                envelope.bodyOut = soapObject;

                HttpTransportSE htSE = new HttpTransportSE(url);
                Object response = null;
                try {

                    htSE.call(nameSpace + methodName, envelope);
                    response = envelope.getResponse();
                    //记录传输到服务器及传回的数据
                    log.error(CreateJson.LOG_JSON(url + ",mhs_login", "2", uid + "," + pwd, response.toString()));

                } catch (Exception e) {
                    log.error("doInLogin:"+e);
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                subscriber.onNext(response);
                subscriber.onCompleted();
            }
        });
    }

//    //查询账号密码是否能登陆
//    @Override
//    public Observable<Object> doInLogin(final String uid, final String pwd) {
//
//        return Observable.create(new Observable.OnSubscribe<Object>() {
//            @Override
//            public void call(Subscriber<? super Object> subscriber) {
//                try {
//                    HttpUtil.init();
//                    String requestXml = new StringBuffer().append(JtConstant.attachReqFront())
//                            .append(CreateJson.Login_Json(uid, pwd)).append(JtConstant.attachReqEnd()).toString();
//                    String respString = HttpUtil.postXml(AppConfig.WEB_CONTENT, new StringRequestEntity(requestXml,"text/xml; charset=UTF-8", JtConstant.Charset));
//                    SAXReader saxReader = new SAXReader();
//                    Document document = saxReader.read(new ByteArrayInputStream(respString.getBytes("UTF-8")));
//                    Element root = document.getRootElement();
//                    String resp = root.element("Body").element("ProcessJsonResponse").element("ProcessJsonResult").getText();
//                    ViewInject.toast(resp);
//                    subscriber.onNext(resp);
//                    subscriber.onCompleted();
//                } catch (Exception e) {
////                    logger.error("获取医护人员信息接口获取异常",e);
//                }
//
//            }
//        });
//    }
}
