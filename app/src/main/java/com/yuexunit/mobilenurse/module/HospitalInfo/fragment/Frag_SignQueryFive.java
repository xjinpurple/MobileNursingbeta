package com.yuexunit.mobilenurse.module.HospitalInfo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.HospitalInfo.Act_HospitalInfo;
import com.yuexunit.mobilenurse.module.SignQuery.bean.YXSignQuery;
import com.yuexunit.mobilenurse.module.SignQuery.model.impl.SignModel;
import com.yuexunit.mobilenurse.module.SignQuery.presenter.impl.SignPresenter;
import com.yuexunit.mobilenurse.module.SignQuery.ui.view.ISignQueryView;

import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/7/14.
 */
public class Frag_SignQueryFive  extends Fragment implements ISignQueryView {
    private SignPresenter presenter;
    private String inpNo;
    private static final String DATE_NUM = "1";//提供几天的数据过来
    private String interval = "1";
    private SimpleDateFormat format;
    //距离当日天数
    private int countday = 0;
    Calendar calendar = Calendar.getInstance();

    int[] typesid = {
            R.id.sign_tw, R.id.sign_mb, R.id.sign_ttcd, R.id.sign_hx, R.id.sign_xy, R.id.sign_sg, R.id.sign_tz,
            R.id.sign_dbcs, R.id.sign_rl, R.id.sign_cl, R.id.sign_nl
    };
    int[] positionid = {
            R.id.position_one, R.id.position_two, R.id.position_three, R.id.position_four, R.id.position_five, R.id.position_six
    };
    @Bind(R.id.query_date)
    public TextView query_date;

    private Act_HospitalInfo aty;
    protected View mMainView;
    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        aty = (Act_HospitalInfo)getActivity();
        mMainView = inflater.inflate(R.layout.act_signs_query, container, false);
        ButterKnife.bind(this, mMainView);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);
        presenter = new SignPresenter(new SignModel(), this);
        query_date.setText(format.format(calendar.getTime()));
        inpNo = PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO);;
        presenter.showSignInfoFive(inpNo, DATE_NUM, format.format(new Date(System.currentTimeMillis())));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.query_left)
    public void QueryBefore() {
        if (countday < 7) {
            countday = countday + 1;
            calendar.add(Calendar.DATE, -1);
            query_date.setText(format.format(calendar.getTime()));
            clearText();
            presenter.showSignInfoFive(inpNo, interval, format.format(calendar.getTime()));
        }
    }

    @OnClick(R.id.query_right)
    public void QueryAfter() {

        if (countday > 0) {
            countday = countday - 1;
            calendar.add(Calendar.DATE, 1);
            query_date.setText(format.format(calendar.getTime()));
            clearText();
            presenter.showSignInfoFive(inpNo, interval, format.format(calendar.getTime()));
        }
    }

    @Override
    public void showSignInfo(ArrayList<YXSignQuery.SignQueryEntity> signQueryEntities) {
        for (YXSignQuery.SignQueryEntity entity : signQueryEntities) {
            Log.v("dyp", "entity:" + entity.toString());
            String[] time = entity.getTime_point().split(" ");
            String[] timepoints = time[1].split(":");
            String timepoint = timepoints[0];
            String vital_signs = entity.getVital_signs();
            View view = null;
            //一天六次
            if ("体温".equals(vital_signs)) {
                setMultiSignValue(getActivity().findViewById(R.id.sign_tw), timepoint, entity);
            } else if ("脉搏".equals(vital_signs)) {
                setMultiSignValue(getActivity().findViewById(R.id.sign_mb), timepoint, entity);
            } else if ("疼痛程度".equals(vital_signs)) {
                setMultiSignValue(getActivity().findViewById(R.id.sign_ttcd), timepoint, entity);
            } else if ("呼吸".equals(vital_signs)) {
                setMultiSignValue(getActivity().findViewById(R.id.sign_hx), timepoint, entity);
            }
            //一天两次
            else if ("血压".equals(vital_signs)) {
                setTwiceSignValue(getActivity().findViewById(R.id.sign_xy), timepoint, entity);
            }
            //一天一次
            else if ("身高".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_sg), timepoint, entity);
            } else if ("体重".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_tz), timepoint, entity);
            } else if ("大便次数".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_dbcs), timepoint, entity);
            } else if ("入量".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_rl), timepoint, entity);
            } else if ("出量".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_cl), timepoint, entity);
            } else if ("尿量".equals(vital_signs)) {
                setOneSignValue(getActivity().findViewById(R.id.sign_nl), timepoint, entity);
            }
        }
    }

    public void setMultiSignValue(View view, String timepoint, YXSignQuery.SignQueryEntity entity) {
        if ("2".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_one)).setText(entity.getVital_signs_values_c());
        } else if ("6".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_two)).setText(entity.getVital_signs_values_c());
        } else if ("10".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_three)).setText(entity.getVital_signs_values_c());
        } else if ("14".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_four)).setText(entity.getVital_signs_values_c());
        } else if ("18".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_five)).setText(entity.getVital_signs_values_c());
        } else if ("22".equals(timepoint)) {
            ((TextView) view.findViewById(R.id.position_six)).setText(entity.getVital_signs_values_c());
        }
    }

    public void setTwiceSignValue(View view, String timepoint, YXSignQuery.SignQueryEntity entity) {
        int time = Integer.valueOf(timepoint);
        if(time > 10){
            ((TextView) view.findViewById(R.id.position_two)).setText(entity.getVital_signs_values_c());
        }
        else {
            ((TextView) view.findViewById(R.id.position_one)).setText(entity.getVital_signs_values_c());
        }
    }

    public void setOneSignValue(View view, String timepoint, YXSignQuery.SignQueryEntity entity) {
        ((TextView) view.findViewById(R.id.position_one)).setText(entity.getVital_signs_values_c());
    }

    public void clearText() {
        View view = null;
        for (int i = 0; i < typesid.length; i++) {
            view = getActivity().findViewById(typesid[i]);

            for (int j = 0; j < positionid.length; j++) {
                TextView tx = (TextView) view.findViewById(positionid[j]);
                if (tx != null) {
                    tx.setText("");
                }
            }
        }
    }
}
