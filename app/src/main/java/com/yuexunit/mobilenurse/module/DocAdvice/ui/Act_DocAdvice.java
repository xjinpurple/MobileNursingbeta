package com.yuexunit.mobilenurse.module.DocAdvice.ui;

import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.DocAdvice.ui.fragment.Frag_Dispensing;
import com.yuexunit.mobilenurse.module.DocAdvice.ui.fragment.Frag_Injection;
import com.yuexunit.mobilenurse.widget.Dialog_Pass;
import com.yuexunit.mobilenurse.widget.Dialog_Unpass;

import org.kymjs.kjframe.ui.KJFragment;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Act_DocAdvice extends TitleBar_DocAdvice {

    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;
    @Bind(R.id.doc_injection_tv)
    public TextView doc_injection_tv;
    @Bind(R.id.doc_dispensing_tv)
    public TextView doc_dispensing_tv;
    @Bind(R.id.doc_dispensing_img)
    public ImageView doc_dispensing_img;
    @Bind(R.id.doc_dispensing_img_01)
    public ImageView doc_dispensing_img_01;
    @Bind(R.id.doc_injection_img)
    public ImageView doc_injection_img;
    @Bind(R.id.doc_injection_img_01)
    public ImageView doc_injection_img_01;

    private Frag_Dispensing contentFragment1;
    private Frag_Injection contentFragment2;
    private KJFragment contentFragment;

    //指定声音池的最大音频流数目为10，声音品质为5
    private SoundPool  pool;
    //载入音频流，返回在池中的id
    private  int sourceid ;

    private Thread_Fail thread_fail;
    private Hander_Fail hander_fail = new Hander_Fail();

    private Thread_Success thread_success;
    private Hander_Success hander_success = new Hander_Success();

    Handler handler_noid;
    Timer time;
    TimerTask timetask;

    private int position = 0; //通过扫描执行医嘱position无意义，这里置为0；

    @Override
    public void initData() {
        super.initData();
        contentFragment1 = new Frag_Dispensing();
        contentFragment2 = new Frag_Injection();

        Bundle bundle = getIntent().getExtras();
        String source = bundle.getString("source");
        if(source.equals(AppConfig.SEARCH)) {
            if (bundle.getString("type").equals(AppConfig.DISPENSING)) {
                change_frag_02();
                change_frag_01();
                Dispensing_Captcha(bundle.getString("id"));

            } else if(bundle.getString("type").equals(AppConfig.INJECTION)){
                change_frag_01();
                change_frag_02();
                Injection_Captcha(bundle.getString("id"));
            }
        }
        else
        {
            change_frag_01();
            change_frag_02();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.act_docadvice);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dispensing_ll)
    public void dispensing_ll() {
        change_frag_01();
    }

    @OnClick(R.id.injection_ll)
    public void injection_ll() {
        change_frag_02();
    }

    public void changeFragment(KJFragment targetFragment) {
        contentFragment = targetFragment;
        super.changeFragment(R.id.docadvice_frame, targetFragment);

    }
    @Override
    public  void onBackClick()
    {
        finish();
    }

    public void change_frag_01()
    {
        doc_dispensing_tv.setTextColor(getResources().getColor(R.color.doc_advice_01));
        doc_dispensing_img.setVisibility(View.VISIBLE);
        doc_dispensing_img_01.setVisibility(View.GONE);
        doc_injection_tv.setTextColor(getResources().getColor(R.color.doc_advice_03));
        doc_injection_img_01.setVisibility(View.VISIBLE);
        doc_injection_img.setVisibility(View.GONE);
        changeFragment(contentFragment1);
    }

    public void change_frag_02()
    {
        doc_dispensing_tv.setTextColor(getResources().getColor(R.color.doc_advice_03));
        doc_dispensing_img.setVisibility(View.GONE);
        doc_dispensing_img_01.setVisibility(View.VISIBLE);
        doc_injection_tv.setTextColor(getResources().getColor(R.color.doc_advice_01));
        doc_injection_img_01.setVisibility(View.GONE);
        doc_injection_img.setVisibility(View.VISIBLE);
        changeFragment(contentFragment2);
    }

    private void Dispensing_Captcha(String code)
    {
        contentFragment1.ExecuteOrder(true, code, position);
    }


    private void Injection_Captcha(String code)
    {
        contentFragment2.ExecuteOrder(true, code, position);
    }

    public void setTitle()
    {
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    public void show_unpass(String message)
    {
        pool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 0);
        sourceid = pool.load(this, R.raw.fail, 1);

        final Dialog_Unpass.Builder dialog_unpass = new Dialog_Unpass.Builder(aty);
        dialog_unpass.setMessage(message);
        dialog_unpass.setPositiveButton("确认",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        pool.unload(sourceid);
                        pool.release();
                        finish();


                    }
                });
        dialog_unpass.create().show();

        thread_fail  = new Thread_Fail();
        thread_fail.start();



    }

    private class Thread_Fail extends Thread {


        @Override
        public void run() {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            Bundle bundle = new Bundle();
            msg.setData(bundle);
            hander_fail.sendMessage(msg);

        }
    }

    private class Hander_Fail extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);

            Log.v("jx", "start_pool" + sourceid);
            pool.play(sourceid, 1, 1, 0, -1, 1);
            Log.v("jx", "over_pool" + sourceid);
        }

    }

    public void show_pass()
    {

        pool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 0);
        sourceid = pool.load(this, R.raw.success, 1);

        thread_success  = new Thread_Success();
        thread_success.start();

        final Dialog_Pass.Builder dialog_pass = new Dialog_Pass.Builder(aty);
        dialog_pass.create().show();
        handler_noid = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what > 0) {
                    Log.v("time", msg.what + "");
                } else {
                    dialog_pass.dismiss();
                    finish();
                }
                super.handleMessage(msg);
            }

        };

        time = new Timer(true);
        timetask = new TimerTask() {
            int countTime = 3;

            public void run() {
                if (countTime > 0) {
                    countTime--;
                }
                Message msg = new Message();
                msg.what = countTime;
                handler_noid.sendMessage(msg);

            }

        };
        time.schedule(timetask, 0, 1000);

    }

    private class Thread_Success extends Thread {


        @Override
        public void run() {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            Bundle bundle = new Bundle();
            msg.setData(bundle);
            hander_success.sendMessage(msg);

        }
    }

    private class Hander_Success extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            pool.play(sourceid, 1, 1, 0, 0, 1);
        }

    }
}
