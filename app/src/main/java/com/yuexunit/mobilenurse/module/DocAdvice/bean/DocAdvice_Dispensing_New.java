package com.yuexunit.mobilenurse.module.DocAdvice.bean;

import java.util.ArrayList;

/**
 * Created by work-jx on 2017/3/23.
 */
public class DocAdvice_Dispensing_New {
    //状态
    public String status;
    //执行次数及总数
    public String count;

    public String barcode;

    public ArrayList<Dispensing_List_New> dispensing_list_news;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public ArrayList<Dispensing_List_New> getDispensing_list_news() {
        return dispensing_list_news;
    }

    public void setDispensing_list_news(ArrayList<Dispensing_List_New> dispensing_list_news) {
        this.dispensing_list_news = dispensing_list_news;
    }
}
