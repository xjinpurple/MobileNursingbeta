package com.yuexunit.mobilenurse.module.Cost.model;

import com.yuexunit.mobilenurse.module.Cost.bean.CostDetail_Patfeelist;

import java.util.ArrayList;
import java.util.Calendar;

import rx.Observable;

/**
 * Created by work-jx on 2016/1/26.
 */
public interface ICostModel {
    /**
     * 获取病人收费信息
     */
    Observable<String> getCostListData(String visitno,Calendar calendar);
}
