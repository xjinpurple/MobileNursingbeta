package com.yuexunit.mobilenurse.module.BloodCollection.model;

import rx.Observable;

/**
 * Created by work-jx on 2017/5/5.
 */
public interface IBloodCollectionModel {
    Observable collect(String Visitno,String Code,String NurseId);
}
