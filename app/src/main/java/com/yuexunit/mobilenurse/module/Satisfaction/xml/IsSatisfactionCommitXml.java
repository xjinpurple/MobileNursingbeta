package com.yuexunit.mobilenurse.module.Satisfaction.xml;

import android.util.Xml;

import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionIsCommitBean;

import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;

/**
 * Created by work-jx on 2016/6/23.
 */
public class IsSatisfactionCommitXml {

    public static SatisfactionIsCommitBean getSatisfactionData(String s)
    {
        if(s == null)
            return null;
        XmlPullParser parser = Xml.newPullParser();
        SatisfactionIsCommitBean satisfactionIsCommitBean = null;
        ByteArrayInputStream tInputStringStream = null;
        try
        {
            if (s != null && !s.trim().equals("")) {
                tInputStringStream = new ByteArrayInputStream(s.getBytes());
            }
        }
        catch (Exception e) {
        }
        try
        {
            parser.setInput(tInputStringStream,"utf-8");

            int type = parser.getEventType();
            while(type != XmlPullParser.END_DOCUMENT)
            {
                switch (type)
                {
                    case XmlPullParser.START_TAG:
                        if("body".equals(parser.getName()))
                        {
                        }else if("response".equals(parser.getName()))
                        {
                        }else if("ret_code".equals(parser.getName()))
                        {
                            satisfactionIsCommitBean = new SatisfactionIsCommitBean();
                            satisfactionIsCommitBean.setRet_code(parser.nextText());
                        }else if("ret_info".equals(parser.getName()))
                        {
                            satisfactionIsCommitBean.setRet_info(parser.nextText());
                        }
                        else if("COUNT".equals(parser.getName()))
                        {
                            satisfactionIsCommitBean.setCOUNT(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;

                    default:
                        break;
                }
                type = parser.next();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return satisfactionIsCommitBean;
    }
}
