package com.yuexunit.mobilenurse.module.BloodCollection.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2017/5/4.
 */
public class YXBloodCollectionBean {
    public YXBloodCollectionBean body;
    public Head head;
    public BloodCollection_Response response;

    public YXBloodCollectionBean getBody() {
        return body;
    }

    public void setBody(YXBloodCollectionBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public BloodCollection_Response getResponse() {
        return response;
    }

    public void setResponse(BloodCollection_Response response) {
        this.response = response;
    }
}
