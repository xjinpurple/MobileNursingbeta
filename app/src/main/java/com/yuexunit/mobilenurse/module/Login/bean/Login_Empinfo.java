package com.yuexunit.mobilenurse.module.Login.bean;

/**
 * Created by work-jx on 2015/12/11.
 */
public class Login_Empinfo {

    //登陆工号
    public String logid;
    //登陆人姓名
    public String logname;
    //登陆人科室代码
    public String deptno;
    //登陆人科室名称
    public String deptname;

    public String getLogid() {
        return logid;
    }

    public void setLogid(String logid) {
        this.logid = logid;
    }

    public String getLogname() {
        return logname;
    }

    public void setLogname(String logname) {
        this.logname = logname;
    }

    public String getDeptno() {
        return deptno;
    }

    public void setDeptno(String deptno) {
        this.deptno = deptno;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }
}
