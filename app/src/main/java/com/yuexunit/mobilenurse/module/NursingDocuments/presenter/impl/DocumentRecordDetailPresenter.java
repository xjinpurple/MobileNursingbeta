package com.yuexunit.mobilenurse.module.NursingDocuments.presenter.impl;


import com.yuexunit.mobilenurse.module.NursingDocuments.model.IDocumentRecordDetailModel;
import com.yuexunit.mobilenurse.module.NursingDocuments.presenter.IDocumentRecordDetailPresenter;
import com.yuexunit.mobilenurse.module.NursingDocuments.ui.view.IDocumentRecordDetailView;
import com.yuexunit.mobilenurse.util.ProUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.ui.ViewInject;
import org.slf4j.Logger;

import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by work-jx on 2016/10/13.
 */
public class DocumentRecordDetailPresenter implements IDocumentRecordDetailPresenter {
    private IDocumentRecordDetailView view;
    private IDocumentRecordDetailModel model;

    //记录Log
    private final Logger log = ProUtil.getLogger(DocumentRecordDetailPresenter.class);

    public DocumentRecordDetailPresenter(IDocumentRecordDetailModel model, IDocumentRecordDetailView addview) {
        this.model = model;
        this.view = addview;
    }


    @Override
    public void uploadRecord(Map<String, String> praise) {
        model.uploadRecord(praise)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        log.info("SignPresenter_uploadTypesInfo_onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        ViewInject.toast("上传护理记录单信息失败");
                        log.error("SignPresenter_uploadTypesInfo_onError:"+e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(String actionBean) {
                        JSONObject all, body,reponse,head;
                        String ret_code = null,ret_info = null;
                        try {
                            all = new JSONObject(actionBean.toString());
                            body = all.getJSONObject("body");
                            reponse = body.getJSONObject("response");
                            head = reponse.getJSONObject("head");
                            ret_code = head.getString("ret_code");
                            ret_info = head.getString("ret_info");
                        } catch (JSONException e) {
                            log.error("JSONException:", e);
                        }

                        if("0".equals(ret_code)){
                            ViewInject.toast("上传护理记录单信息成功");
                            view.uploadRecord();
                        }else{
//                            ViewInject.toast("当前时间点护理记录单信息已录入");
                            ViewInject.toast("上传护理记录单信息失败");
                        }
                    }
                });
    }
}
