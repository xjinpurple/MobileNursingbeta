package com.yuexunit.mobilenurse.module.Transportation.bean;

/**
 * Created by work-jx on 2016/12/2.
 */
public class OperNotice_Response {
    private String opslist;

    public String getOpslist() {
        return opslist;
    }

    public void setOpslist(String opslist) {
        this.opslist = opslist;
    }
}
