package com.yuexunit.mobilenurse.module.DocAdvice.presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by work-jx on 2016/2/3.
 */
public interface IInjectionPresenter {
    /**
     * 医嘱列表信息
     */
    void showInjectionList(String visitno);

    /**
     * 执行医嘱
     */
    void Execorder(boolean source,String visitno,String barcode,String nurseid);

    /**
     * 获得订阅者及取消订阅
     */
    CompositeSubscription getCompositeSubscription();
    void UnSubObservers();
}
