package com.yuexunit.mobilenurse.module.HealthArticles.bean;

/**
 * Created by hbprotoss on 12/1/15.
 */

//获取健康宣教内容

public class Article {
    private transient Integer articleId;

    private String title;

    private String content;

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
