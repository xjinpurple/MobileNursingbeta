package com.yuexunit.mobilenurse.module.HealthArticles.ui.view;

/**
 * Created by sslcjy on 16/1/26.
 */
public interface IArticleContentView {

    /**
     * 显示健康宣教文章内容
     */
    void showArticleContent(String Content);
}
