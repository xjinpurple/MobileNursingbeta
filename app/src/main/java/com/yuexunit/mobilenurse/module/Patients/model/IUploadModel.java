package com.yuexunit.mobilenurse.module.Patients.model;

import com.yuexunit.mobilenurse.base.bean.ActionBean;

import java.util.Map;

import rx.Observable;

/**
 * Created by work-jx on 2016/3/25.
 */
public interface IUploadModel {
    /**
     * 一键上传体征数据
     */
    Observable<ActionBean> uploadTypesData(Map<String, String> praise);


}
