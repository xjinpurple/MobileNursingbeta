package com.yuexunit.mobilenurse.module.DocAdvice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.module.DocAdvice.bean.DocAdvice_Injection;
import com.yuexunit.mobilenurse.module.DocAdvice.bean.DocAdvice_OrderExec;
import com.yuexunit.mobilenurse.module.DocAdvice.ui.fragment.Frag_Injection;

import java.util.ArrayList;

/**
 * Created by work-jx on 2015/11/24.
 */
public class InjectionListAdapter extends BaseAdapter
{
    private Context context;
    private Frag_Injection activity;
    private ArrayList<DocAdvice_Injection> mDatas;
    private ArrayList<DocAdvice_OrderExec> mDatas_Exec;

    public InjectionListAdapter(Frag_Injection activity,Context context,ArrayList<DocAdvice_Injection> mDatas){
        this.context = context;
        this.activity =activity;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return null == mDatas ? 0 : mDatas.size();
//        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null == mDatas ? null : mDatas.get(position);
//        return null;
    }

    @Override
    public long getItemId( int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.injectionlist_item, parent, false);
            holder = new ViewHolder();
            holder.injectionlist_list = (ListView)convertView.findViewById(R.id.injectionlist_list);
            holder.injectionlist_exec_list = (ListView)convertView.findViewById(R.id.injectionlist_exec_list);
            holder.injectionlist_statu_btn = (Button)convertView.findViewById(R.id.injectionlist_statu_btn);
            holder.injectionlist_plan_usage = (TextView)convertView.findViewById(R.id.injectionlist_plan_usage);
            holder.injectionlist_statu_tv = (TextView)convertView.findViewById(R.id.injectionlist_statu_tv);
            holder.injectionlist_count = (TextView)convertView.findViewById(R.id.injectionlist_count);
            holder.injectionlist_ll = (RelativeLayout)convertView.findViewById(R.id.injectionlist_ll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.injectionlist_plan_usage.setText(mDatas.get(position).getDocAdvice_orderlist().getExecfreq() + "  "+ mDatas.get(position).getDocAdvice_orderlist().getExecway());
        holder.injectionlist_statu_tv.setText(mDatas.get(position).getStatus());
        holder.injectionlist_count.setText(mDatas.get(position).getCount());
        if(mDatas.get(position).getStatus().equals("等待注射"))
        {
            holder.injectionlist_ll.setBackgroundResource(R.color.white);
            holder.injectionlist_statu_tv.setTextColor(context.getResources().getColor(R.color.doc_advice_01));
            holder.injectionlist_statu_btn.setVisibility(View.VISIBLE);
            holder.injectionlist_statu_btn.setEnabled(true);
        }
        else
        {
            holder.injectionlist_ll.setBackgroundResource(R.color.doc_advice_04);
            holder.injectionlist_statu_tv.setTextColor(context.getResources().getColor(R.color.doc_advice_03));
            holder.injectionlist_statu_btn.setVisibility(View.INVISIBLE);
            holder.injectionlist_statu_btn.setEnabled(false);
        }

        BaseAdapter adapter  = new InjectionListListAdapter(context,mDatas.get(position).getDespensing_detail());
        holder.injectionlist_list.setAdapter(adapter);

        mDatas_Exec = new ArrayList<DocAdvice_OrderExec>();
        for (int i = 0;i<mDatas.get(position).getDespensing_exec().size();i++)
        {
            if(mDatas.get(position).getDespensing_exec().get(i).getExectime() != null)
            {
                mDatas_Exec.add(mDatas.get(position).getDespensing_exec().get(i));
            }
        }

        BaseAdapter adapter_exec  = new InjectionListExecAdapter(holder.injectionlist_exec_list,mDatas_Exec,R.layout.injectionlist_item_exec);
        holder.injectionlist_exec_list.setAdapter(adapter_exec);
        holder.injectionlist_statu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String barcode ="-1";
                for(int i = 0;i<mDatas.get(position).getDespensing_exec().size();i++)
                {
                    if(((Double.valueOf(barcode) > Double.valueOf(mDatas.get(position).getDespensing_exec().get(i).getBarcode()))
                            || Double.valueOf(barcode)==-1)&& mDatas.get(position).getDespensing_exec().get(i).getExecempid() == null)
                    {
                        barcode = mDatas.get(position).getDespensing_exec().get(i).getBarcode();
                    }
                }
                activity.ExecuteOrder_inner(false,barcode,position);
            }
        });

        return convertView;
    }

    class ViewHolder{
        ListView injectionlist_list,injectionlist_exec_list;
        Button injectionlist_statu_btn;
        TextView injectionlist_statu_tv,injectionlist_plan_usage,injectionlist_count;
        RelativeLayout injectionlist_ll;
    }
}
