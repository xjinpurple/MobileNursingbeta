package com.yuexunit.mobilenurse.module.Satisfaction.ui;

import android.content.DialogInterface;
import android.view.View;
import android.widget.TextView;

import com.yuexunit.mobilenurse.R;
import com.yuexunit.mobilenurse.base.titlebar.TitleBar_DocAdvice;
import com.yuexunit.mobilenurse.config.AppConfig;
import com.yuexunit.mobilenurse.module.Satisfaction.adapter.SatisfactionAdapter;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBackBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionCommitBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionIsCommitBean;
import com.yuexunit.mobilenurse.module.Satisfaction.bean.SatisfactionLastBean;
import com.yuexunit.mobilenurse.module.Satisfaction.model.impl.SatisfactionModel;
import com.yuexunit.mobilenurse.module.Satisfaction.presenter.ISatisfactionPresenter;
import com.yuexunit.mobilenurse.module.Satisfaction.presenter.impl.SatisfactionPresenter;
import com.yuexunit.mobilenurse.module.Satisfaction.ui.view.ISatisfactionView;
import com.yuexunit.mobilenurse.widget.Dialog_Success;
import com.yuexunit.mobilenurse.widget.EmptyLayout;
import com.yuexunit.mobilenurse.widget.FixedSwipeListView;
import com.yuexunit.mobilenurse.widget.LoadingDialog;

import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by work-jx on 2016/5/27.
 */
public class Act_Satisfaction extends TitleBar_DocAdvice implements ISatisfactionView {

    @Bind(R.id.act_satisfaction_list)
    public FixedSwipeListView act_satisfaction_list;
    @Bind(R.id.empty_layout)
    public EmptyLayout mEmptyLayout;
    @Bind(R.id.titlebar_docadvice_tv_title)
    public TextView titlebar_docadvice_tv_title;
    @Bind(R.id.docactvice_name)
    public TextView docactvice_name;
    @Bind(R.id.docactvice_bedno)
    public TextView docactvice_bedno;
    @Bind(R.id.docactvice_visitno)
    public TextView docactvice_visitno;

    private SatisfactionAdapter adapter;
    private ArrayList<SatisfactionAdapter.CheckType> list;
    private ArrayList<SatisfactionCommitBean> satisfactionCommitBeans;

    ISatisfactionPresenter presenter;

    private ArrayList<SatisfactionLastBean> satisfactionLastBeans = new ArrayList<SatisfactionLastBean>();

    private int commit = 0;
    private LoadingDialog dialog;

    Dialog_Success.Builder builder_success;



    @Override
    public void setRootView() {
        setContentView(R.layout.act_satisfaction);
        ButterKnife.bind(this);
    }

    @Override
    public void initWidget() {
        super.initWidget();

    }

    @Override
    public void initData() {
        super.initData();
        dialog = new LoadingDialog(aty);
        presenter = new SatisfactionPresenter(this, new SatisfactionModel());
        mEmptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
            }
        });
        presenter.IscommitSatisfactionDate(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
    }

    @Override
    public void setTitle() {
        titlebar_docadvice_tv_title.setText("满意度调查");
        docactvice_name.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_NAME));
        docactvice_bedno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_BEDCODE));
        docactvice_visitno.setText(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));

    }

    @Override
    public void onBackClick() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showSatisfactionList(ArrayList<SatisfactionLastBean> list) {
        satisfactionLastBeans = list;
        adapter = new SatisfactionAdapter(act_satisfaction_list, satisfactionLastBeans, R.layout.item_satisfaction);
        act_satisfaction_list.setAdapter(adapter);
        mEmptyLayout.dismiss();
    }

    @Override
    public void commitSatisfactionDate(SatisfactionCommitBackBean satisfactionCommitBackBean) {
        if(commit != 18)
        {
            commit++;
        }
        else
        {
            ViewInject.toast("评价完成");
            loadingDialogStatus(AppConfig.DISMISS_DIALOG);
            finish();
        }
    }

    @Override
    public void iscommitSatisfaction(SatisfactionIsCommitBean satisfactionIsCommitBean) {
        if(Integer.valueOf(satisfactionIsCommitBean.getCOUNT()) > 0)
        {
            showDialog();
        }
        else {
            presenter.showSatisfactionList();
        }
    }

    public void showDialog()
    {
        builder_success = new Dialog_Success.Builder(this);
        builder_success.setMessage("是否进行重新评价？");
        builder_success.setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        presenter.showSatisfactionList();
                    }
                });

        builder_success.setNegativeButton("否",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        builder_success.create().show();
    }

    @OnClick(R.id.answer_commit)
    public void onClick() {
        loadingDialogStatus(AppConfig.SHOW_DIALOG);
        list = adapter.getArrayList();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss",java.util.Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);
        satisfactionCommitBeans = new ArrayList<SatisfactionCommitBean>();
        for(int i = 0;i<satisfactionLastBeans.size();i++)
        {
            SatisfactionCommitBean satisfactionCommitBean = new SatisfactionCommitBean();
            satisfactionCommitBean.setPJBBH(satisfactionLastBeans.get(i).getPJBBH());
            satisfactionCommitBean.setPJXBH(satisfactionLastBeans.get(i).getPJXBH());
            switch (list.get(i).type)
            {
                case SatisfactionAdapter.CheckType.TYPE_FIRST:
                    satisfactionCommitBean.setPFXBH(satisfactionLastBeans.get(i).getSatisfactionPFXBeans().get(0).getPFXBH());
                    break;
                case SatisfactionAdapter.CheckType.TYPE_SECOND:
                    satisfactionCommitBean.setPFXBH(satisfactionLastBeans.get(i).getSatisfactionPFXBeans().get(1).getPFXBH());
                    break;
                case SatisfactionAdapter.CheckType.TYPE_THIRD:
                    satisfactionCommitBean.setPFXBH(satisfactionLastBeans.get(i).getSatisfactionPFXBeans().get(2).getPFXBH());
                    break;
            }
            satisfactionCommitBean.setMZBRBH("");
            satisfactionCommitBean.setZYBRBH(PreferenceHelper.readString(aty, AppConfig.CURRENT_PATIENT_INFO, AppConfig.CURRENT_PATIENT_VISITNO));
            satisfactionCommitBean.setPFSJ(str);
            satisfactionCommitBeans.add(satisfactionCommitBean);
        }
        presenter.commitSatisfactionDate(satisfactionCommitBeans);
    }

    public void loadingDialogStatus(int status) {
        switch (status) {
            case AppConfig.SHOW_DIALOG:
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                break;
            case AppConfig.DISMISS_DIALOG:
                dialog.dismiss();
                break;

        }
    }


}
