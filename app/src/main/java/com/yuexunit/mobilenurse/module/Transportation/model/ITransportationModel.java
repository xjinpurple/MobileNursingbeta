package com.yuexunit.mobilenurse.module.Transportation.model;

import com.yuexunit.mobilenurse.module.Transportation.bean.RfidBean;

import rx.Observable;

/**
 * Created by work-jx on 2016/9/29.
 */
public interface ITransportationModel {
    /**
     * 手术转运
     */
    Observable<String> opsExec(String requestNo,String patState,String empId,String empName,String inputEmpId);

    Observable<RfidBean> getRfidInfo(String rfid);
}
