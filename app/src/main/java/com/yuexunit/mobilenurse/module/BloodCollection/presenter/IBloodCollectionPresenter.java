package com.yuexunit.mobilenurse.module.BloodCollection.presenter;

/**
 * Created by work-jx on 2017/5/5.
 */
public interface IBloodCollectionPresenter {
    void collect(String Visitno,String Code,String NurseId);
}
