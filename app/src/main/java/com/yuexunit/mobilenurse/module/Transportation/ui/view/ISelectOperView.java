package com.yuexunit.mobilenurse.module.Transportation.ui.view;

import com.yuexunit.mobilenurse.module.Transportation.bean.TransportationRecord_Opsinfo;

import java.util.ArrayList;

/**
 * Created by work-jx on 2016/9/27.
 */
public interface ISelectOperView {
    void showOpsList(ArrayList<TransportationRecord_Opsinfo> list);

}
