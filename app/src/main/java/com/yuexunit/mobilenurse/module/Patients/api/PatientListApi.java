package com.yuexunit.mobilenurse.module.Patients.api;

import com.yuexunit.mobilenurse.base.bean.ActionBean;
import com.yuexunit.mobilenurse.module.SignInput.bean.YXSignsInput;

import java.util.Map;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by sslcjy on 16/1/22.
 */
public interface PatientListApi {
    /**
     * 一键上传数据库中的体征信息
     */
    @FormUrlEncoded
    @POST("ydhl/api/sign/offical/add")
    Observable<ActionBean> oneKeyUpload(@FieldMap Map<String, String> praise);


    //获得基本体征项
    @GET("ydhl/api/sign/list")
    Observable<YXSignsInput> BaseTypes(@Query("areaId") String areaId);
    //获得全部体征项
    @GET("ydhl/api/sign/all")
    Observable<YXSignsInput> AllTypes();
}
