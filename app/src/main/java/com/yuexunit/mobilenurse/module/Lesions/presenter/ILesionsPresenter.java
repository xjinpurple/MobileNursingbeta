package com.yuexunit.mobilenurse.module.Lesions.presenter;

import com.yuexunit.mobilenurse.module.Login.bean.Login_Bq;

import java.util.ArrayList;

/**
 * Created by sslcjy on 16/1/21.
 */
public interface ILesionsPresenter {
    void showLesionList(ArrayList<Login_Bq> bqList);
}
