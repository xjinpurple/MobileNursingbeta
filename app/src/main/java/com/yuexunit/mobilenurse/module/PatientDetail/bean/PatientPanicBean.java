package com.yuexunit.mobilenurse.module.PatientDetail.bean;

/**
 * Created by work-jx on 2016/6/16.
 */
public class PatientPanicBean {
    //危急值名称
    private String name;
    //金唐地址
    private String web;

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
