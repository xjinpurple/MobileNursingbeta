package com.yuexunit.mobilenurse.module.Transportation.bean;

import com.yuexunit.mobilenurse.base.bean.Head;

/**
 * Created by work-jx on 2016/9/23.
 */
public class YXTransportationRecordBean {
    public YXTransportationRecordBean body;
    public Head head;
    public TransportationRecord_Response response;

    public YXTransportationRecordBean getBody() {
        return body;
    }

    public void setBody(YXTransportationRecordBean body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public TransportationRecord_Response getResponse() {
        return response;
    }

    public void setResponse(TransportationRecord_Response response) {
        this.response = response;
    }
}
