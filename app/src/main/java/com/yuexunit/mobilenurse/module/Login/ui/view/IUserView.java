package com.yuexunit.mobilenurse.module.Login.ui.view;

import com.yuexunit.mobilenurse.base.mvp.v.BasePromptView;
import com.yuexunit.mobilenurse.module.Login.bean.SystemConfig;
import com.yuexunit.mobilenurse.module.Login.bean.YXLoginData;

/**
 * Created by sslcjy on 16/1/20.
 */
public interface IUserView extends BasePromptView{

    void doInLogin(YXLoginData yxLoginData);

    void saveSystemListParams(SystemConfig systemConfig);

}
