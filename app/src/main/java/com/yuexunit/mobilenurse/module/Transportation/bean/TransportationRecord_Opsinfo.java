package com.yuexunit.mobilenurse.module.Transportation.bean;

import java.io.Serializable;

/**
 * Created by work-jx on 2016/9/27.
 */
public class TransportationRecord_Opsinfo implements Serializable{

    /**
     * requestno : 2016091800034
     * opsname : 腹腔镜探查+患侧卵巢肿瘤剥除术
     * opsgrand : 3
     * surgeon : 李梅
     * opstime : 2016/9/19 11:00:00
     * opsroom : 07
     * opspart : 腹部
     * resvtime : 2016/9/19 8:30:00
     * opsstate : 80
     * opspatstate : 98
     * outwardtime : 98
     * outwardemp : 98
     * OUTWARDTIME1 : 98
     * OUTWARDEMP1 : 98
     * OUTWARDTIME2 : 98
     * OUTWARDEMP2 : 98
     * inwardtime : 98
     * inwardemp : 98
     */

    private String requestno;
    private String opsname;
    private String opsgrand;
    private String surgeon;
    private String opstime;
    private String opsroom;
    private String opspart;
    private String resvtime;
    private String opsstate;
    private String opspatstate;

    private String outwardtime;
    private String outwardemp;
    private String outWardEmpId;
    private String outWardOper;
    private String outWardOperId;

    private String inOpsTime;
    private String inOpsEmp;
    private String inOpsEmpId;
    private String inOpsOper;
    private String inOpsOperId;

    private String outOpsTime;
    private String outOpsEmp;
    private String outOpsEmpId;
    private String outOpsOper;
    private String outOpsOperId;

    private String inwardtime;
    private String inwardemp;
    private String inWardEmpId;
    private String inWardOpsOper;
    private String inWardOperId;

    public String getRequestno() {
        return requestno;
    }

    public void setRequestno(String requestno) {
        this.requestno = requestno;
    }

    public String getOpsname() {
        return opsname;
    }

    public void setOpsname(String opsname) {
        this.opsname = opsname;
    }

    public String getOpsgrand() {
        return opsgrand;
    }

    public void setOpsgrand(String opsgrand) {
        this.opsgrand = opsgrand;
    }

    public String getSurgeon() {
        return surgeon;
    }

    public void setSurgeon(String surgeon) {
        this.surgeon = surgeon;
    }

    public String getOpstime() {
        return opstime;
    }

    public void setOpstime(String opstime) {
        this.opstime = opstime;
    }

    public String getOpsroom() {
        return opsroom;
    }

    public void setOpsroom(String opsroom) {
        this.opsroom = opsroom;
    }

    public String getOpspart() {
        return opspart;
    }

    public void setOpspart(String opspart) {
        this.opspart = opspart;
    }

    public String getResvtime() {
        return resvtime;
    }

    public void setResvtime(String resvtime) {
        this.resvtime = resvtime;
    }

    public String getOpsstate() {
        return opsstate;
    }

    public void setOpsstate(String opsstate) {
        this.opsstate = opsstate;
    }

    public String getOpspatstate() {
        return opspatstate;
    }

    public void setOpspatstate(String opspatstate) {
        this.opspatstate = opspatstate;
    }

    public String getOutwardtime() {
        return outwardtime;
    }

    public void setOutwardtime(String outwardtime) {
        this.outwardtime = outwardtime;
    }

    public String getOutwardemp() {
        return outwardemp;
    }

    public void setOutwardemp(String outwardemp) {
        this.outwardemp = outwardemp;
    }

    public String getOutWardEmpId() {
        return outWardEmpId;
    }

    public void setOutWardEmpId(String outWardEmpId) {
        this.outWardEmpId = outWardEmpId;
    }

    public String getInOpsTime() {
        return inOpsTime;
    }

    public void setInOpsTime(String inOpsTime) {
        this.inOpsTime = inOpsTime;
    }

    public String getInOpsEmp() {
        return inOpsEmp;
    }

    public void setInOpsEmp(String inOpsEmp) {
        this.inOpsEmp = inOpsEmp;
    }

    public String getInOpsEmpId() {
        return inOpsEmpId;
    }

    public void setInOpsEmpId(String inOpsEmpId) {
        this.inOpsEmpId = inOpsEmpId;
    }

    public String getOutOpsTime() {
        return outOpsTime;
    }

    public void setOutOpsTime(String outOpsTime) {
        this.outOpsTime = outOpsTime;
    }

    public String getOutOpsEmp() {
        return outOpsEmp;
    }

    public void setOutOpsEmp(String outOpsEmp) {
        this.outOpsEmp = outOpsEmp;
    }

    public String getOutOpsEmpId() {
        return outOpsEmpId;
    }

    public void setOutOpsEmpId(String outOpsEmpId) {
        this.outOpsEmpId = outOpsEmpId;
    }

    public String getInwardtime() {
        return inwardtime;
    }

    public void setInwardtime(String inwardtime) {
        this.inwardtime = inwardtime;
    }

    public String getInwardemp() {
        return inwardemp;
    }

    public void setInwardemp(String inwardemp) {
        this.inwardemp = inwardemp;
    }

    public String getInWardEmpId() {
        return inWardEmpId;
    }

    public void setInWardEmpId(String inWardEmpId) {
        this.inWardEmpId = inWardEmpId;
    }

    public String getOutWardOper() {
        return outWardOper;
    }

    public void setOutWardOper(String outWardOper) {
        this.outWardOper = outWardOper;
    }

    public String getOutWardOperId() {
        return outWardOperId;
    }

    public void setOutWardOperId(String outWardOperId) {
        this.outWardOperId = outWardOperId;
    }

    public String getInOpsOper() {
        return inOpsOper;
    }

    public void setInOpsOper(String inOpsOper) {
        this.inOpsOper = inOpsOper;
    }

    public String getInOpsOperId() {
        return inOpsOperId;
    }

    public void setInOpsOperId(String inOpsOperId) {
        this.inOpsOperId = inOpsOperId;
    }

    public String getOutOpsOper() {
        return outOpsOper;
    }

    public void setOutOpsOper(String outOpsOper) {
        this.outOpsOper = outOpsOper;
    }

    public String getOutOpsOperId() {
        return outOpsOperId;
    }

    public void setOutOpsOperId(String outOpsOperId) {
        this.outOpsOperId = outOpsOperId;
    }

    public String getInWardOpsOper() {
        return inWardOpsOper;
    }

    public void setInWardOpsOper(String inWardOpsOper) {
        this.inWardOpsOper = inWardOpsOper;
    }

    public String getInWardOperId() {
        return inWardOperId;
    }

    public void setInWardOperId(String inWardOperId) {
        this.inWardOperId = inWardOperId;
    }
}
