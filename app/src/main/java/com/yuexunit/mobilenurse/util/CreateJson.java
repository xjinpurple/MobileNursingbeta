package com.yuexunit.mobilenurse.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by work-jx on 2015/12/16.
 */
public class CreateJson {

    //登录
    public static String Login_Json(String uid,String password)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_login");

            JSONObject request = new JSONObject();
            request.put("logid", uid);
            request.put("logpass", password);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人列表
    public static String Patlist_Json(String wardno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_patlist");

            JSONObject request = new JSONObject();
            request.put("wardno", wardno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人列表-分类
    public static String PatlistClassify_Json(String wardId,String careLevel,String isKF,String isZS,String isLAB)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_pat_class");

            JSONObject request = new JSONObject();
            request.put("wardId", wardId);
            request.put("careLevel", careLevel);
            request.put("isKF", isKF);
            request.put("isZS", isZS);
            request.put("isLAB", isLAB);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //单个病人信息
    public static String Patinfo_Json(String visit_no)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_patinfo");
            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);
            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }


    //病人检验申请单列表
    public static String Aslist_Json(String visit_no)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_as_list");

            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人检验报告单
    public static String Asrpt_Json(String requestno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_as_rpt");

            JSONObject request = new JSONObject();
            request.put("requestno", requestno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人检查申请单列表
    public static String Exlist_Json(String visit_no)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_ex_list");

            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人检查报告单
    public static String Exrpt_Json(String requestno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_ex_rpt");

            JSONObject request = new JSONObject();
            request.put("requestno", requestno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人费用清单查询
    public static String Patfee_Json(String visit_no,String startdate,String endate)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_pat_fee");

            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);
            request.put("startdate", startdate);
            request.put("enddate", endate);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取病人化药信息
    public static String Chemical_Json(String barcode)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_infusion");

            JSONObject request = new JSONObject();
            request.put("HYTM", barcode);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //执行病人化药信息
    public static String Exec_Chemical_Json(String barcode,String nurseid)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_infu_exec");

            JSONObject request = new JSONObject();
            request.put("HYTM", barcode);
            request.put("HSGH", nurseid);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取病人皮试信息
    public static String SkinTest_Json(String visitno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_st_info");

            JSONObject request = new JSONObject();
            request.put("ZYH", visitno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取皮试批次
    public static String SkinTest_getcode_Json(String ypid)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_st_batch");

            JSONObject request = new JSONObject();
            request.put("YPID", ypid);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取皮试批次
    public static String SkinTest_Exec_Json(String sqdh, String czry, String kssj, String pssc, String pcnm, String psph)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_st_exec");

            JSONObject request = new JSONObject();
            request.put("SQDH", sqdh);
            request.put("CZRY", czry);
            request.put("KSSJ", kssj);
            request.put("PSSC", pssc);
            request.put("PCNM", pcnm);
            request.put("PSPH", psph);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }


    //病人医嘱执行
    public static String Orderexec_Json(String visit_no,String barcode,int execempid,String exectime,String exectype)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_order_exec");

            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);
            request.put("barcode", barcode);
            request.put("execempid", execempid);
            request.put("exectime", exectime);
            request.put("exectype", exectype);
            request.put("is_scan", "0");

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人检验标本采集
    public static String Ascollect_Json(String visitno,String barcode,int execempid,String exectime,String exectype)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_as_exec");

            JSONObject request = new JSONObject();
            request.put("visit_no", visitno);
            request.put("barcode", barcode);
            request.put("execempid", execempid);
            request.put("exectime", exectime);
            request.put("exectype", exectype);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //病人体征
    public static String Sign_Json(String inpNo,String timestamp,String recorder,String signData,String timepoint)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_enr_record");

            JSONObject request = new JSONObject();
            request.put("inpNo", inpNo);
            request.put("timestamp", timestamp);
            request.put("recorder", recorder);
            request.put("signData", signData);
            request.put("timepoint", timepoint);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取病人当日可执行医嘱
    public static String Orderlist_Json(String visit_no)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_order_list");

            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取体征 HIS5.0
    public static String Signlist_Json(String inpNo,String interval,String date)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_enr_list");

            JSONObject request = new JSONObject();
            request.put("inpNo", inpNo);
            request.put("interval",interval);
            request.put("date", date);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取病人所有医嘱
    public static String All_Orderlist_Json(String visit_no,String startdate,String endate)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
//            if(AppConfig.HISVerson == 5) {
                head.put("trans_no", "kmhs_order_all");
//            }
//            else
//            {
//                head.put("trans_no", "mhs_order_all");
//            }
            JSONObject request = new JSONObject();
            request.put("visit_no", visit_no);
            request.put("startdate",startdate);
            request.put("enddate", endate);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //手术转运状态
    public static String Ops_Json(String visit_no)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_ops_state");

            JSONObject request = new JSONObject();
            request.put("visitNo", visit_no);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //手术转运
    public static String OpsExec_Json(String requestNo,String patState,String empId,String empName,String inputEmpId)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_exec_ops");

            JSONObject request = new JSONObject();
            request.put("requestNo", requestNo);
            request.put("patState", patState);
            request.put("empId", empId);
            request.put("empName", empName);
            request.put("inputEmpId", inputEmpId);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //手术通知
    public static String OpsNotice_Json(String requestno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_ops_info");

            JSONObject request = new JSONObject();
            request.put("request_no", requestno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }


    //护理记录单
    public static String DocumentRecord_Json(String inpNo,String timestamp,String timepoint,String templateId,String signData,String recorder)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_enr_insert");

            JSONObject request = new JSONObject();
            request.put("inpNo", inpNo);
            request.put("timestamp", timestamp);
            request.put("timepoint", timepoint);
            request.put("templateId", templateId);
            request.put("signData", signData);
            request.put("recorder", recorder);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //获取护理记录单模板
    public static String getDocumentRecord_Json(String visitno)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_pat_tmpl");

            JSONObject request = new JSONObject();
            request.put("visitNo", visitno);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //当日采血记录
    public static String getBloodCollectionSearch_Json(String wardId)
    {
        JSONObject main = new JSONObject();
        try {
            JSONObject body = new JSONObject();
            JSONObject head = new JSONObject();
            head.put("userid", "mhs");
            head.put("password", "mhs");
            head.put("trans_no", "kmhs_as_spec");

            JSONObject request = new JSONObject();
            request.put("wardid", wardId);

            body.put("head",head);
            body.put("request",request);
            main.put("body",body);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

    //远程日志
    public static String LOG_JSON(String url,String type,String params,String result)
    {
        JSONObject main = new JSONObject();
        try {
            main.put("url", url);
            main.put("type", type);
            main.put("params", params);
            main.put("result", result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return main.toString();
    }

}
