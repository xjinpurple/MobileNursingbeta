package com.yuexunit.mobilenurse.util;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.honeywell.aidc.BarcodeReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 姚平 on 2015/12/1.
 */
public class ProUtil {


    //关闭及开启扫描功能
    private static final String scannerInputPlugin = "com.motorolasolutions.emdk.datawedge.api.ACTION_SCANNERINPUTPLUGIN";
    private static final String extraData = "com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER";

    public static void DisableDataWedgs(Context context) {
        Intent i = new Intent();
        i.setAction(scannerInputPlugin);
        i.putExtra(extraData, "DISABLE_PLUGIN");
        context.sendBroadcast(i);
    }

    public static void EnableDataWedgs(Context context) {
        Intent i = new Intent();
        i.setAction(scannerInputPlugin);
        i.putExtra(extraData, "ENABLE_PLUGIN");
        context.sendBroadcast(i);
    }


    //屏幕上按钮进行扫描功能
    private static final String ACTION_SOFTSCANTRIGGER = "com.motorolasolutions.emdk.datawedge.api.ACTION_SOFTSCANTRIGGER";
    private static final String EXTRA_PARAM = "com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER";
    private static final String DWAPI_START_SCANNING = "START_SCANNING";
    private static final String DWAPI_TOGGLE_SCANNING = "TOGGLE_SCANNING";

    public static void DataWedgsScan(Context context) {
        Intent i = new Intent();
        // set the intent action using soft scan trigger action string declared earlier
        i.setAction(ACTION_SOFTSCANTRIGGER);
        // add a string parameter to tell DW that we want to toggle the soft scan trigger
        i.putExtra(EXTRA_PARAM, DWAPI_TOGGLE_SCANNING);
        // now broadcast the intent
        context.sendBroadcast(i);
        // provide some feedback to the user that we did something
        Toast.makeText(context, "Soft scan trigger toggled.", Toast.LENGTH_SHORT).show();
    }

    //避免连续点击
    private static long lastClickTime;

    public synchronized static boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }


    //获取Logger对象
    public static Logger getLogger(Class logger) {
        return LoggerFactory.getLogger(logger);
    }

    public static Map<String, Object> honewellConfig() {
      Map<String, Object> properties = new HashMap<String, Object>();
    // Set Symbologies On/Off
      properties.put(BarcodeReader.PROPERTY_CODE_128_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_GS1_128_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_QR_CODE_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_CODE_39_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_DATAMATRIX_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_UPC_A_ENABLE,true);
      properties.put(BarcodeReader.PROPERTY_EAN_13_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_AZTEC_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_CODABAR_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_INTERLEAVED_25_ENABLED,true);
      properties.put(BarcodeReader.PROPERTY_PDF_417_ENABLED,true);
    // Set Max Code 39 barcode length
      properties.put(BarcodeReader.PROPERTY_CODE_39_MAXIMUM_LENGTH,10);
    // Turn on center decoding
//      properties.put(BarcodeReader.PROPERTY_CENTER_DECODE,true);
    // Enable bad read response
      properties.put(BarcodeReader.PROPERTY_NOTIFICATION_BAD_READ_ENABLED,true);
        return properties;
    };
}
