package com.yuexunit.mobilenurse.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yuexun on 2016/7/13.
 */
public class JtConstant {
    public static final String UserId="ydsy";
    public static final String Password="ydsy";
    public static String url = null;
    public static final String Charset = "utf-8";
    public static final String SUCCESSCODE = "0";
    private static String jtUrl = "http://218.0.5.114:8081//EsbBusService.asmx?op=ProcessJson";
    /**读取xml数据的文件夹路径**/
    private static final String DataAbsolutPath = "/resources/data/";
    public static final String DataOfYzyfList = DataAbsolutPath + "yzyf.xml";
    public static final String JtDateFormat = "yyyy/MM/dd";//海曙yyyy/MM/dd // 五乡yyyy-MM-dd
    public static final String JtDateTimeFormat = "yyyy/MM/dd HH:mm:ss";//海曙yyyy/MM/dd HH:mm:ss // 五乡yyyy-MM-dd HH:mm:ss
//    private static Map<String,JtYzyf> yzyfMap = null;
//
//    // CustomizedPropertyConfigurer.getContextProperty("jt.url");
//    public static String getUrl() {
//        if(url==null){
//            url = jtUrl;
//        }
//        return url;
//    }
//    public static Map<String,JtYzyf> attachYzyf(){
//        if(yzyfMap==null) {
//            List<JtYzyf> list = YzyfUtil.attachYzyfList();
//            if(list!=null) {
//                yzyfMap = new HashMap<>();
//                for (JtYzyf yzyf : list) {
//                    yzyfMap.put(yzyf.getDm(), yzyf);
//                }
//            }
//        }
//        return yzyfMap;
//    }

    public static String attachReqFront(){
//        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:Process><tem:XmlString>";
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "   <soap:Header/>\n" +
                "   <soap:Body>\n" +
                "      <tem:ProcessJson>\n" +
                "         <tem:XmlString>";
    }
    public static String attachReqEnd(){
//        return "</tem:XmlString></tem:Process></soapenv:Body></soapenv:Envelope>";
        return "</tem:XmlString>\n" +
                "      </tem:ProcessJson>\n" +
                "   </soap:Body>\n" +
                "</soap:Envelope>";
    }
}
