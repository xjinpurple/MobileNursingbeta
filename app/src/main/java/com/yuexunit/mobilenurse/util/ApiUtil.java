/**
 * <p>Project：venus</p>
 * <p>Package：	com.tp.venus.module.home.api</p>
 * <p>File：ApiUtil.java</p>
 * <p>Version： 4.0.0</p>
 * <p>Date： 2016/1/14/11:52.</p>
 * Copyright © 2016 www.qbt365.com Corporation Inc. All rights reserved.
 */
package com.yuexunit.mobilenurse.util;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.yuexunit.mobilenurse.base.rx.FastJsonConvertFactory;
import com.yuexunit.mobilenurse.config.AppConfig;

import java.util.List;

import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;


/**
 * <p>Class：com.tp.venus.util.ApiUtil</p>
 * <p>Description：</p>
 * <pre>
 *      API工具类
 * </pre>
 *
 * @version 1.0.0
 * @date 2016/1/14/11:52
 */

public class ApiUtil {

    private static ApiUtil instance = new ApiUtil();

    public ApiUtil() {
    }

    public static ApiUtil getInstance() {
        return instance;
    }

    public OkHttpClient InterceptClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient mOkHttpClient = new OkHttpClient();
        mOkHttpClient.networkInterceptors().add(new StethoInterceptor());
        List<Interceptor> interceptors = mOkHttpClient.interceptors();
        interceptors.add(logging);
        return mOkHttpClient;
    }
    public <T> T createRetrofitApi(Class<T> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.WEB_SYSTEM)
                .addConverterFactory(FastJsonConvertFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())//Retrofit访问服务器时候返回参数通过Gson解析
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(this.InterceptClient())
                .build();
        return retrofit.create(service);
    }

    public <T> T createRetrofitApi_new(Class<T> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.WEB_SYSTEM_SINGLE)
                .addConverterFactory(FastJsonConvertFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())//Retrofit访问服务器时候返回参数通过Gson解析
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(this.InterceptClient())
                .build();
        return retrofit.create(service);
    }

    public <T> T createRetrofitApi_Rfid(Class<T> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.RFID_WEB_SYSTEM)
                .addConverterFactory(FastJsonConvertFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())//Retrofit访问服务器时候返回参数通过Gson解析
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(this.InterceptClient())
                .build();
        return retrofit.create(service);
    }
}