package com.yuexunit.mobilenurse.config;


import android.support.annotation.IntDef;
import android.support.annotation.StringDef;


/**
 * Created by sslcjy on 16/2/15.
 */


public class AnnotationConfig {

    @StringDef({ToastContent.FAILED_CONTENT, ToastContent.SUCCESS_CONTENT})
    public @interface ToastContent{
        public static final String FAILED_CONTENT = "访问失败";
        public static final String SUCCESS_CONTENT = "访问成功";
    }

    @IntDef({DialogStatus.SHOW_DIALOG,DialogStatus.DISMISS_DIALOG})
    public @interface DialogStatus{
        public static final int SHOW_DIALOG = 1;
        public static final int DISMISS_DIALOG = -1;
    }

    @IntDef({ExecuteStatus.EXECUTE_STATUS,ExecuteStatus.CANCEL_STATUS})
    public @interface ExecuteStatus{
        public static final int EXECUTE_STATUS = 1;
        public static final int CANCEL_STATUS = 0;
    }

}