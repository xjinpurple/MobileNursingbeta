/*
 * Copyright (c) 2015, 张涛.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yuexunit.mobilenurse.config;

/**
 * 配置文件常量
 *
 * @author kymjs (https://www.kymjs.com)
 * @since 2015-3
 */
public class AppConfig {
    /**
     * 全局CrashHandler抓取报错信息到手机本地文件中
     */
    public static final String SAVEFOLDER = "QueryPatient";
    public static final String SAVEFILENAME = "QueryPatient.log";

    /**
     * 不同年龄分配不同头像的界限
     */

    public final static int AGE_LIMIT = 18;


    /**
     * 本地SharePreference 存储的相关XML文件名
     *
     */


    /**
     * OA查询功能，存储登录医生的账号和密码
     */
    //存储xml文件名
    public static final String DOCTOR_INFO = "nurse_info";
    //护士的id
    public static final String DOCTOR_ID = "doctorid";
    //护士的name
    public static final String DOCTOR_PASSWORD = "doctorpassword";


    /**
     * 登录人信息
     */
    //存储xml文件名
    public static final String NURSE_INFO = "nurse_info";
    //护士的id
    public static final String NURSE_LOGID = "logid";
    //护士的name
    public static final String NURSE_LOGNAME = "logname";
    //登陆人科室代码
    public static final String NURSE_DEPTNO = "deptno";
    //登陆人科室名称
    public static final String NURSE_DEPTNAME = "deptname";
    //护士进入的病区id
    public static final String NURSE_WARDNO_ID = "wardno";
    //护士进入的病区id
    public static final String NURSE_WARDNO_NAME = "wardno_name";

    //自动保存
    public static final String LOGIN_INFO = "login_info";
    public static final String LOGIN_NAME = "login_name";
    public static final String LOGIN_PASSWORD = "login_password";

    /**
     * 当前病人信息
     */
    //存储xml文件名
    public static final String CURRENT_PATIENT_INFO = "current_patient_info";
    //当前病人床号,姓名，预交总额，总费用,住院号,入院日期，性别，年龄
    public static final String CURRENT_PATIENT_BEDCODE = "bedcode";
    public static final String CURRENT_PATIENT_NAME = "patientname";
    public static final String CURRENT_PATIENT_PRE = "patientpre";
    public static final String CURRENT_PATIENT_TOTAL = "patienttotal";
    public static final String CURRENT_PATIENT_VISITNO = "visit_no";
    public static final String CURRENT_PATIENT_DATE = "date";
    public static final String CURRENT_PATIENT_SEX = "sex";
    public static final String CURRENT_PATIENT_AGE = "age";
    //
    public static final String CURRENT_PATIENT_BEDNO = "bed_no";
    //扫描执行医嘱时使用
    public static String VISITNO = "";
    public static String NURSEID = "";


    /**
     * 服务器上拉下来的系统配置信息
     */

    //存储xml文件名
    public static final String MOBILE_INFO = "mobile_info";
    public static final String COLLECTION_ISCHECK = "collecttion_ischeck";//false:批量；true:非批量

    //系统配置信息-jci
    public static final String JCI_INFO = "jci_info";
    //是否启用jci
    public static final String JCI_JCI = "jci";
    //是否启用包药机
    public static final String JCI_BYJ = "byj";
    //腕带规则
    public static final String JCI_WD_RULE = "wd_rule";
    //采血规则
    public static final String JCI_CX_RULE = "cx_rule";
    //是否必须扫瓶贴
    public static final String JCI_PTSM = "ptsm";
    //瓶贴规则
    public static final String JCI_PTSM_RULE = "ptsm_rule";
    //是否启用腕带
    public static final String JCI_WD = "wd";
    //是否启用
    public static final String JCI_ENABLE = "enabled";
    //包药机规则
    public static final String JCI_BYJ_RULE = "byj_rule";


    //系统配置地址
//    public static final String WEB_SYSTEM = "http://192.168.1.230/";//公司
    public static final String WEB_SYSTEM_SINGLE = "http://106.75.213.54/";
    public static final String WEB_SYSTEM = "http://106.75.213.54/";
    //获取系统配置参数
    public static final String GET_SYSTEM_PARAMS = WEB_SYSTEM + "ydhl/api/sysvar/list";
    //获取病区信息
    public static final String GET_HOSPITAL_AREA_PARAMS = WEB_SYSTEM + "ydhl/api/area";
    //获取体征配置信息
    public static final String GET_SIGN_PARAMS = WEB_SYSTEM + "ydhl/api/sign/list";
    //获取健康宣教
    public static final String GET_HEALTH_ARTICLE_LIST = WEB_SYSTEM + "ydhl/api/article/titles";
    //获取健康宣教内容
    public static final String GET_HEALTH_ARTICLE_CONTENT = WEB_SYSTEM + "ydhl/api/article";
    //获取应用最新版本
//    public static final String WEB_SYSTEM_DOWNLOAD = "http://158.18.1.196:8080/";//三院
    public static final String CHECK_APP_LASTVERSION = WEB_SYSTEM + "ydhl/api/version/last";
    //获取全部体征项
    public static final String GET_SIGN_PARAMS_ALL = WEB_SYSTEM + "ydhl/api/sign/all";
    //上传该病人的体征项内容到服务器
    public static final String UPLOAD_SIGN_PARAMS = WEB_SYSTEM + "ydhl/api/sign/offical/add";

    //远程日志的关键字搜索

    //RFID
    public static final String RFID_WEB_SYSTEM = "http://106.75.213.54/";
//    public static final String RFID_WEB_SYSTEM = "http://158.18.1.196/";

    //系统配置
    public static final String LOGGER_SYSTEM_PARAMS = "system_params";

    //金唐根WebService
//  public static final  String WEB_CONTENT = "http://www.kingtsoft.com:19090/EsbBusService.asmx";  //本地测试地址
//    public static final String WEB_CONTENT = "http://183.131.145.16:9090/EsbBusService.asmx"; //三院内网地址
//    public static final String WEB_CONTENT = "http://183.131.145.16:9090/EsbBusService.asmx?op=ProcessJson"; //三院内网地址
    public static final String WEB_CONTENT = "http://106.75.213.93:9090/EsbBusService.asmx"; //三院内网地址
    public static final String WEB_CONTENT_EXEC = "http://106.75.213.93:8089/EsbBusService.asmx"; //三院内网地址_执行
    //    public static final  String WEB_CONTENT = "http://218.0.5.114:9090/EsbBusService.asmx"; //三院外网地址
    public static final String WEB_NAME_SPACE = "http://tempuri.org/";
    public static final String WEB_NAME_METHOD = "ProcessJson";
    public static final String WEB_NAME_METHOD_TWO = "Process";

    //webservice timeout
    public static final int TIMEOUT = 10000;

    //加载界面提示框状态值
    public static final int SHOW_DIALOG = 1;
    public static final int DISMISS_DIALOG = -1;

    //执行标志
    public static final String EXECTYPE = "1";
    public static final String EXECTYPE_SEND = "2";

    //采血dialog显示
    public static final int BLOOD_DIALOG_REPEAT = 1;
    public static final int BLOOD_DIALOG_UNPASS = 2;
    public static final int BLOOD_DIALOG_PASS = 3;

    //oracle报错
    public static final String ORACLEERROR = "ORA-20014";

    //医嘱执行dialog显示
    public static final int EXEC_DIALOG_UNPASS_EXTERNAL = 1;
    public static final int EXEC_DIALOG_PASS_EXTERNAL = 2;
    public static final int EXEC_DIALOG_UNPASS_INNER = 3;
    public static final int EXEC_DIALOG_PASS_INNER = 4;

    public static final String UNSUCESS = "医嘱执行不成功!";
    public static final String MISMATCH = "药品信息，用法用量不匹配，\\n请核对信息!";
    public static final String NETERROR = "网络不通，请检查网络!";

    //医嘱是否是注射标志
    public static final String INJECTFLAG = "1";

    //进入医嘱的方式
    public static final String SEARCH = "search";//扫描进入
    public static final String CLICK = "click";//点击进入

    //扫描执行医嘱
    public static final String DISPENSING = "0";//发药
    public static final String INJECTION = "1";//注射

    public static final String TAG_DISPENSING = "2";//发药
    public static final String TAG_INJECTION = "1";//注射

    //判断病人列表是否加载完毕
    public static boolean ISOVER_PATIENT = false;


    public static String TEST_WEBSERVICE = "<?xml version=\"1.0\" encoding=\"utf-8\"?><body><head><userid>mhs</userid><password>mhs</password><trans_no>mhs_pjbxx</trans_no></head><resquest></resquest></body>";

    public static String SATISFACTION_COMMIT(String PJBBH, String PJXBH, String PFXBH, String MZBRBH, String ZYBRBH, String PFSJ) {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?><body><head><userid>mhs</userid><password>mhs</password><trans_no>mhs_pjbjg</trans_no></head><resquest><PJBBH>" + PJBBH + "</PJBBH><PJXBH>" + PJXBH + "</PJXBH><PFXBH>" + PFXBH + "</PFXBH><MZBRBH>" + MZBRBH + "</MZBRBH><ZYBRBH>" + ZYBRBH + "</ZYBRBH><PFSJ>" + PFSJ + "</PFSJ></resquest></body>";
    }

    public static String Is_Commit_Satisfaction(String BRBZ) {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?><body><head><userid>mhs</userid><password>mhs</password><trans_no>mhs_pjbjgcx</trans_no></head><resquest><mhs_pjbjgcx><BRBZ>" + BRBZ + "</BRBZ></mhs_pjbjgcx></resquest></body>";

    }

    //识凌
    public static final String BAR_READ_ACTION= "SYSTEM_BAR_READ";//条码广播
    public static final String RFID_READ_ACTION= "SYSTEM_RFID_READ";//RFID广播：

    //默认体征时间选择
    public static final String SIGN_INFO = "sign_info";
    public static final String SIGN_TIME = "sign_time";

}